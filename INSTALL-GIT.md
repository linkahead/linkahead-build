
# Installation using the git repository #

## Requirements ##
The following packages are needed by LinkAhead.
- acl (for `setfacl` executable)
- curl
- Docker >= 19.03
- docker-compose >= 1.29
- python >= 3.9
- git
Python3 packages:
- packaging
- yaml
- requests
- caosdb
- jsonschema

The following Python packages are strongly recommended:
- pytest
- pytest-shell

On a Debian system, you can install the requirements with:

```sh
sudo apt-get install curl docker.io docker-compose git python3-packaging python3-pip python3-pytest python3-requests python3-yaml
pip3 install --user pytest-shell
pip3 install --user caosdb
sudo usermod -aG docker $USER
```

## Start

If you cloned the git repository, you can simply run `./linkahead start`.
This will download a Docker image from the internet and start LinkAhead. If you
would like to build a new Docker image, add the build option:  
`./linkahead start --build`

This will build the docker image and start a respective container.

`./linkahead --help ` will inform you about further options.


## Using services ##
For automatic restart and certificate renewals it may be a good idea to use
services. You might want to look at systemd documentation if you are not familiar
with services.

You can find templates for services and timers in `utils/install`. Also see 
notes under "Using services" in `README_SETUP.md`.
