# Testing 

The following sections assume that any linkahead repositories needed 
(e.g. linkahead-deploy, linkahead-pylib or linkahead-pyinttest) reside
in a shared directory, and any commands are executed in their repositories
top-level directory. Should this not be the case, the example commands may
have to be adapted.

## Unit Tests

Under construction

## Integration Tests

In order to run the integration tests from 
[linkahead-pyinttest](https://gitlab.com/linkahead/linkahead-pyinttest) with
this version of linkahead, the following steps have to be taken:

1. There is a pre-configured profile to run the integration tests against
   included in the integration tests repository. Use this to start LinkAhead,
   e.g., with
   ```sh
   ./linkahead -p ../linkahead-pyinttest/test_profile/profile.yml start  # You may have to adapt the path
   ```
2. Export the certificates (see [README_SETUP](README_SETUP.html#certificates))
   by executing the `linkahead certs` command:
   ```sh
   ./linkahead -p ../linkahead-pyinttest/test_profile/profile.yml certs -d ../linkahead-pyinttest/test_profile/custom/other/cert/caosdb.cert.pem
   ```
3. Configure your Python client by copying the contents of
   `linkahead-pyinttest/pylinkahead.ini.template` to a new file
   `linkahead-pyinttest/pylinkahead.ini`. Should you have adapted the cert
   destination path in the commands above, you have to set the `cacert` 
   option in the new file to your custom path.
4. Now run `pytest` in the `linkahead-pyinttest` directory to execute the
   integration tests. 
