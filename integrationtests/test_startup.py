#!/usr/bin/env python3

# This file is a part of the CaosDB Project.
#
# Copyright (C) 2022 IndiScale GmbH <www.indiscale.com>
# Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Test if startup works under uncommon conditions.
"""


import os
import shutil
import subprocess

from pytest import (fixture, raises)


@fixture
def teardown():
    """Try to stop a running LinkAhead server again.
    """
    yield None
    call = ["./linkahead", "stop"]
    subprocess.run(call, check=False, capture_output=True)


def test_config_file_nonexistent(tmp_path, teardown):
    """What happens when the config file does not exist?"""
    call_help = ["./linkahead", "-h"]
    call_log = ["./linkahead", "log"]
    env = {"LINKAHEAD_CONF": os.path.join(tmp_path, "linkahead.conf")}

    # Step 1: Help should always be printed
    try:
        result = subprocess.run(call_help, env=env, timeout=5,
                                capture_output=True, check=False)
    except subprocess.TimeoutExpired:
        pass
    print(f"stderr: {result.stderr.decode()}")
    print(f"stdout: {result.stdout.decode()}")
    assert result.returncode == 0, "Unexpected return code, should have run without problems."

    print("-" * 20)
    # Step 2: Other commands should fail.
    try:
        result = subprocess.run(call_log, env=env, timeout=5,
                                capture_output=True, check=False)
    except subprocess.TimeoutExpired:
        pass
    print(f"stderr: {result.stderr.decode()}")
    print(f"stdout: {result.stdout.decode()}")
    assert "Config file in environment variable LINKAHEAD_CONF does not exist" in result.stderr.decode()
    assert result.returncode == 1, "Unexpected return code."


def test_config_file_unreadable(tmp_path, teardown):
    """What happens when the config file cannot be read?"""
    call_help = ["./linkahead", "-h"]
    call_log = ["./linkahead", "log"]
    linkahead_conf = os.path.join(tmp_path, "linkahead.conf")
    shutil.copyfile("./utils/install/linkahead.conf", linkahead_conf)
    env = {"LINKAHEAD_CONF": linkahead_conf}

    # Make file unreadable.
    os.chmod(linkahead_conf, 0)
    subprocess.run(["ls", "-l", linkahead_conf], check=False)

    # Step 1: Help should always be printed
    try:
        result = subprocess.run(call_help, env=env, timeout=5,
                                capture_output=True, check=False)
    except subprocess.TimeoutExpired:
        pass
    print(f"stderr: {result.stderr.decode()}")
    print(f"stdout: {result.stdout.decode()}")
    assert result.returncode == 0, "Unexpected return code, should have run without problems"

    print("-" * 20)
    # Step 2: Is the error message understandable if the config file cannot be read?
    try:
        result = subprocess.run(call_log, env=env, timeout=5,
                                capture_output=True, check=False)
    except subprocess.TimeoutExpired:
        pass
    print(f"stderr: {result.stderr.decode()}")
    print(f"stdout: {result.stdout.decode()}")
    assert result.returncode == 2, "Unexpected return code."
    assert f"Configuration: Permission problem with file: {linkahead_conf}" in result.stderr.decode(
    ).strip()
