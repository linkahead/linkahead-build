# Changelog #

## [0.16.0] - 2025-02-06 ##

### Added ###

* `debug_authtoken_dir` profile option (only relevant when running the
  Python integration tests) to specify a host directory which will be
  mounted into the LinkAhead docker container s.th. server-generated
  authentication tokens will be written to this directory.

### Removed ###

* Support for Python 3.8

### Fixed ###

* [linkahead-control#7](https://gitlab.com/linkahead/linkahead-control/-/issues/7)
  Missing permission when copying config files.

### Documentation ###

* Improved documentation on how to run the Python integration tests
  against LinkAhead, and moved test documentation to its own README.

## [0.15.1] - 2024-11-18 ##

### Fixed ###

* [linkahead-control#19](https://gitlab.com/linkahead/linkahead-control/-/issues/19)
  Missing privileges for config files created by
  `docker/transfer/build_docker/setup.sh` during image build.
* [linkahead-control#17](https://gitlab.com/linkahead/linkahead-control/-/issues/17)
  Missing git dependency in image build

## [0.15.0] - 2024-11-14 ##

### Added ###

* Support for SSSD authentication; controlled by the new `sssd` and
  `sssd_config_path` profile variables.
* New variables in profile.yml that allow setting proxy-options:
  `http_proxy`, `https_proxy`, `ftp_proxy`, `no_proxy`
* Official support for Python 3.13

### Changed ###

* Proxy environment variables `HTTP_PROXY`, `HTTPS_PROXY`,
  `FTP_PROXY`, and `NO_PROXY` are now automatically transferred from
  the host to the docker container. They can be overwritten by
  setting the corresponding profile variables (see above) to empty strings.
* Updated MariaDB 10.11, and for this, to LinkAhead MariaDB Backend
  8.0.0 and LinkAhead server 0.13.0. Note that the upgrade to MariaDB
  10.11 renders old SQL dumps incompatible which have to be updated
  manually. The MariaDB Backend repo provides an [update
  script](https://gitlab.com/linkahead/linkahead-mariadbbackend/-/tree/main/dump_updates?ref_type=heads).

### Fixed ###

* [linkahead-control#10](https://gitlab.com/linkahead/linkahead-control/-/issues/10)
  Use correct docker-compose dependency
* [caosdb-deploy#345](https://gitlab.indiscale.com/caosdb/src/caosdb-deploy/-/issues/345)
  Use docker-compose v2 by default
* [#13](https://gitlab.com/linkahead/linkahead-control/-/issues/13)
  LinkAhead hangs at start when no docker credentials are given
* Base image for Docker is Debian 12 now.

### Documentation ###

* Added a tutorial for the asynchronous execution of server-side
  scripts.

## [0.14.0] - 2024-07-31 ##

### Changed ###

* `linkahead start` does no longer build a new Docker image. Use `--build` to
  build a new image.

### Deprecated ###

* The command line option `--no-build` is deprecated. Please use `--build` to
  control the behavior.

### Removed ###

* Removed the deprecated `--no-health` command line option

### Fixed ###

* Force ipv4 in docker network caosnet as a workaround for
  [#6](https://gitlab.com/linkahead/linkahead-control/-/issues/6).

### Documentation ###

* Added remark on getting user info in server-side scripts.

## [0.13.0] - 2024-04-23

### Added ###

* Support for Python 3.12 and experimental support for 3.13

### Changed ###

* Custom scripting packages with ``mode: "copy"`` can now be specified either by
  the absolute path on the host machine or a path relative to the profile
  directory.

### Removed ###

* Support for Python 3.7

## [0.12.1] - 2023-12-15

### Fixed ###

* [linkahead-webui#242](https://gitlab.com/linkahead/linkahead-webui/-/issues/242):
  Missing image in footer

### Security ###

* [linkahead-server#242](https://gitlab.com/linkahead/linkahead-server/-/issues/242)
  and
  [linkahead-server#244](https://gitlab.com/linkahead/linkahead-server/-/issues/244)
  insufficient permission checks in SELECT queries.

## [0.12.0] - 2023-11-01
(Daniel Hornung)

### Fixed ###

- Fixed registry image default.

## [0.11.1] - 2023-09-25
(Daniel Hornung)

### Changed ###

* Update envoy proxy from 1.21 to 1.27. This might break your custom
  envoy.yaml. In particular the configurations of `http_filters` need
  additional configuration like this:
  ```
			 http_filters:
			 - name: envoy.filters.http.grpc_web
  +            typed_config:
  +              "@type": type.googleapis.com/envoy.extensions.filters.http.grpc_web.v3.GrpcWeb
			 - name: envoy.filters.http.cors
  +            typed_config:
  +              "@type": type.googleapis.com/envoy.extensions.filters.http.cors.v3.Cors
			 - name: envoy.filters.http.router
  +            typed_config:
  +              "@type": type.googleapis.com/envoy.extensions.filters.http.router.v3.Router
  ```

### Fixed ###

* [#325](https://gitlab.indiscale.com/caosdb/src/caosdb-deploy/-/issues/325) The Debian installer now asks for subscription.  If no subscription is given, the
  installer chooses sensible defaults.

## [0.11.0] - 2023-07-10
(Timm Fitschen)

### Removed ###

* Config option `anonymous_admin`. This feature was intended for usage with the
  trial profile only. If your linkahead instance has an anonymous role with
  admin privileges you will be prompted to confirm that the admin privileges
  will be taken away from the anonymous role. There is no option to prevent
  this.

  If the anonymous user had admin-like permissions before, you may want to
  revoke these permissions.  This can be done with the following line which
  calls a script inside the Docker container (replace `CONTAINER_NAME`, probably
  it is `linkahead`):

  ```sh
  docker exec -ti -u 0:0 -w /opt/caosdb/run_docker/ CONTAINER_NAME /opt/caosdb/run_docker/bin/handle_anonymous_admin
  ```

  Alternatively, you can use this Python library utility:
  `caosdb_admin.py retrieve_role_permissions anonymous`

## [0.10.0] - 2023-06-16
(Timm Fitschen)

### Added ###

* Caching proxy in Docker for apt-get commands.

### Changed ###

* Health checks are no longer executed by default. If you want to execute the
  remainders of the health checks, you have to run the tests in `utils/health`
  manually until a future `--diag` option has been implemented (see
  https://gitlab.indiscale.com/caosdb/src/caosdb-deploy/-/issues/314).

### Deprecated ###

* `--no-health` option; the behavior is now the default.

### Removed ###

* The no longer supported health checks `test_10_python.py` and `test_30_scripting.py`

### Fixed ###

* Minimum docker-compose version is 1.27

## [0.9.2] - 2023-02-28

### Added ###

* `envoy_connect_timeout` and `envoy_route_timeout` profile option to configure
  envoy proxy timeouts
* Re-introduced support for Python 3.7
* The ACM module for the LinkAhead WebUI is now directly shipped with LinkAhead.
  Enable the module in the buildvars of the webui and enable Envoy to use it.

### Changed ###

* Better output when config file is not readable. [linkahead#2](https://gitlab.com/indiscale/linkahead/issues/-/issues/2)
* Tour updated for default roles in queries.
* Now depends on Docker >= 19.3.
* Removed login shell wrapper when starting LinkAhead via systemd service. [linkahead#7](https://gitlab.com/indiscale/linkahead/issues/-/issues/7)
  If your configuration depended on a login shell (so that ~/.profile was read first), you can
  restore the old behaviour with override service files.  Read the [systemd documentation](https://www.freedesktop.org/software/systemd/man/systemd.unit.html) for
  details (Ctrl-F override).

### Deprecated ###

### Removed ###

### Fixed ###

* [linkahead#5](https://gitlab.com/indiscale/linkahead/issues/-/issues/5)
  Timeouts when using the Envoy proxy: Timeouts can now be configured via the
  profile.yml. Also, more forgiving defaults have been chosen.
* Better output when config file is not
  readable. [linkahead#2](https://gitlab.com/indiscale/linkahead/issues/-/issues/2)

### Security ###

## [0.9.1] - 2022-11-02
(Florian Spreckelsen)

### Fixed

* Timezone is not set correctly
  [#121](https://gitlab.indiscale.com/caosdb/src/caosdb-deploy/-/issues/121)

## [0.9.0] - 2022-07-18 ##

### Fixed ###

* [#262](https://gitlab.indiscale.com/caosdb/src/caosdb-deploy/-/issues/262)
  Formerly missing envoy files are now copied correctly during building of the
  debian package

## [0.8.0] - 2022-06-28 ##

### Added ###

* An envoy default configuration has been added together with the corresponding
  options in LinkAhead profile. It allows serving GRPC and XML API under the
  same port. See `docs/deploy_concept.md`
* Templates for certificate renewal servies are now available in the Debian package.  They are
  located in `/etc/systemd/system/linkahead_renewcert_default.*`, documentation is in
  `README_SETUP.md`.

### Fixed ###

* [#257](https://gitlab.indiscale.com/caosdb/src/caosdb-deploy/-/issues/257) The
  profile schema now also validates an empty `scripting` field
* Correct default docker image name (i.e. `0.X.Y` instead of `v0.X.Y`)

## [0.7.1] - 2022-05-31 ##

### Added ###

* `additional_groups` setting in `profile.yml` to add groups to the user inside
  docker
* Config option for using LDAP over PAM for the authentication and
  group-assignment. This is documented in detail in
  `docs/administration/user_authentication_with_ldap.md`.

## [0.7.0] - 2022-05-04 ##

(Daniel Hornung)

### Security ###

- Unsafe default configuration: Removed `testuser` with admin rights from default configuration.

### Changed ###

- Default server version now uses different LDAP authentication configuration.  Check the server
  documentation for details.

## [0.6.0] - 2022-04-11 ##

### Security ###

- The `ypsetup` executable now checks its number of arguments.

### Changed ###

- Updated versions of underlying packages:
  ```
	SERVER:            v0.7.2
	PYLIB:             v0.7.2
	MYSQLBACKEND:      v5.0.0
	WEBUI:             v0.5.0
	ADVANCEDUSERTOOLS: v0.4.0
  ```

### Fixed ###

- #244: could not connect to running server

## [0.5.0] - 2022-01-25 ##

### Added ###

- Documentation.
- More automated building of Debian packages.
- Cleaner Debian package building.

### Deprecated ###

- `backup_dir` in the profile is deprecated, use the `-d` parameter in `linkahead backup`.

## [0.4.1] - 2021-12-13 ##

### Security ###

- Updated caosdb-server to 0.7.1 to incorporate Log4J ([CVE-2021-44228](https://nvd.nist.gov/vuln/detail/CVE-2021-44228)) fix.

### Added ###

- Documentation.
- Better logging output for failing tests.

## [0.4] - 2021-12-06 ##

### Added ###

- Allow to build different Debian packages by supplying the PKG_KIND env var.
- Added support for GRPC API

### Changed ###

- extroot folders now must be provided as key: value pairs
- updated underlying image to debian:bullseye

### Fixed ###

- #177: Added missing files to debian-installation routine.
- #193: Removed documentation link to empty search page
- #199: Fixed incompatibility with docker-compose v2
- #203: Added missing key-value option for extroot in json schema
- #213: Allow custom/other directory to be missing
- #215: Allow extroot directory to be missing

## [0.3] - 2021-11-05 ##

## [0.3-rc.3] - 2021-06-29 ##

### Fixed ###

- #177: Added missing files to debian-installation routine.


## [0.3-rc.2] - 2021-06-25 ##

### Added ###

- New target for make: `make pycaosdb.ini` provides a ready to use pycaosdb.ini
  which can be linked or copied to the home directory or working directory of
  a caosdb python client which is to be executed on same host where linkahead
  has been started.
- Configuration files for the mysql backend can be added.
- New `--debug-mode` option for the linkahead executable which starts linkahead in
  debug mode and overrides `debug: false` in the profile.yml.
- New `build` action to only build the Docker image, without starting a
  container.
- New `conf:sendmail` option in the profile which is a path to a sendmail client
  executable inside the linkahead docker container. The option defaults to
  `/usr/sbin/sendmail`.
  For testing purposes, a dummy sendmail executable which dumps all mails into
  the `/tmp/mail/` directory inside the linkahead container. To use the dummy
  configure `conf:sendmail: /usr/local/bin/sendmail_to_file` in your
  `profile.yml`. Resolves: #568
- Added caosmodels and caosadvancedtools to the image.
- #97 Log into Docker registry for pulling an image.
- #103 Added Debian build script, with testing.
- Added & improved introductory tour from demo.indiscale.com to default profile.
- Allowing relative paths in `profile.yml`.
- #118 Mounting of `dropoffbox` and `extroot` is optional now, the former is disabled by default.
- #113 Local users can now be used for authentication.
- #128 Anonymous user can be made admin via profile option now.
- #107, 157 A json schema for validating the profile.yml and finding
  missing keys has been added.
- Unit tests, which currently only test the profile validation.
- #135 Documentation on how to migrate from LinkAhead to a plain
  CaosDB installation
- Checking whether the Linkahead docker image exits before calling
  docker-compose. This leads to a clearer error message if no image is
  accessible.

### Changed ###

- Updated documentation.
- `linkahead start` works now when restarting a server and restoring the
  content.
- "rX" ACL permissions are given to docker user id for the temporary
  directories created by linkahead.  This requires the `setfacl` binary.
- The default profile is now the same as on demo.indiscale.com.
- #131: New location for Debian files.
- Use the primary repositories at gitlab.indiscale.com now.
- #183: The caosdb-advanced-user-tools commit can now be specified as well in
  the `profile.yml`.

### Deprecated ###

- Autmatic mounting of `extroot`.  It shall be stated explicit in the future.

### Removed ###

- `install` script, was deprecated by `make install`.
- Old default profile

### Fixed ###

- #55: Fixed ineffective timeout, linkahead used to wait forever for the server.
- #93: Better error message if trying to build without build environment.
- #66: Health checks: Creating certificate from key, if it does not exist yet.
- #115: Hard dependency on Docker service.
- #117: Better `make install` for local installation.  Still needs manual editing of config files.
- #162: Automatic renewal of Let's Encrypt certificates now doesn't require apache anymore


## [0.2-rc.0] - 2020-03-19 ##

### Changed ###
The various startup scripts were merged into one `linkahead` script that can be
used to start and stop the server. This implies that the `linkahead` script now
has different command line arguments.

There are no longer multiple docker-compose files that are merged, but only one
with defaults. It is loaded, extended and then written to a top level
`docker-compose.yml`.

In order to consolidate default values for variables in `profile.yml`, a
default file was created `.defaults.yml`. The variable `project` is no longer
used.

The recommended way to deal with this change is to:
- change the top level variable name in `profile.yml` to `caosdb-deploy`
- remove the variable `project` in `profile.yml`. The value of this variable
should be the top level key (`caosdb-deploy` by default).

## [0.1] - 2020-03-05 ##

### Added ###

- The `custom` profile setting should now contain a list of folders which are
  all copy-merged into a temporary folder before starting the Docker
  environment.
- LinkAhead can be installed system-wide and started as a systemd service.

### Changed ###

- `custom` folder: This folder was split up into `custom` and `paths`, where
  `custom` contains only (read-only) configuration that should not be changed by
  the Docker container, and `paths` contains folders which may be changed inside
  Docker.  Please have a look at `profiles/default` for an example of the new
  layout.  If you need to migrate from an earlier version, simply copy your
  `sql_data` folder from `custom` to `paths`.


### Deprecated ###

- The `custom` profile setting containing a single directory is now deprecated.

# Meta #

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
