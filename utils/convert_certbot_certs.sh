#!/bin/bash

# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (c) 2021 IndiScale GmbH <info@indiscale.com>
# Copyright (c) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

######### Documentation ########################################################
# This script: converts the output certificates generated by certbot
# to pkcs12 and jks keystores that are used by the CaosDB server.
#
# Requirements:
# - This script must be run as root, because certbot requires root
#   privileges.
# - In `/var/local/letsencrypt/` or wherever the letsencrypt
#   configuration directory is located on your system, there must be
#   the typical Let's-Encrypt folders `conf`, `work`, `logs`.
#
# Environment variables:
# - LETSENCRYPT_BASEDIR: The directory in which the let's encrypt
#   configuration is stored. The default is `/var/local/letsencrypt`.
# - KEYPW: Password for the SSL secret key and the keystore (same
#   because of Java limitations). Default is `CaosDBSecret`.
# - PROFILE_DIR: The `profile` directory, typically contained in a folder like
#   `server-profile`. Default is `$PWD/../../profile`, i.e., the parent 
#   directory of caosdb-deploy.
# - DOCKER_NAME: Name of user executing linkahead. Default is
#   `caosdb_docker`.
# - DOCKER_GROUP: Name of docker group. Default is `docker`.
#
# Arguments:
# - domain name for which the certs shall be converted.

set -e

profile_dir="${PROFILE_DIR:-$PWD/../../profile}"
letsencrypt_basedir="${LETSENCRYPT_BASEDIR:-/var/local/letsencrypt}"
KEYPW="${KEYPW:-CaosDBSecret}"
KEYSTOREPW="${KEYPW:-}"
docker_name="${DOCKER_NAME:-caosdb_docker}"
docker_group="${DOCKER_GROUP:-docker}"

DOMAIN="$1"

cd ${LETSENCRYPT_BASEDIR}/conf/live/${DOMAIN}/

# Prevent questions about overwriting:
[ -e all-certs.pkcs12 ] && rm all-certs.pkcs12
# combine private key and full chain to pkcs12 that can bey used by
# keytool
KEYPW="${KEYPW}" openssl pkcs12 -export \
     -inkey privkey.pem -in fullchain.pem -out all-certs.pkcs12 \
     -passin env:KEYPW -passout env:KEYPW

# Create jks that is used by CaosDB
[ -e caosdb.jks ] && rm caosdb.jks

keytool -importkeystore -srckeystore all-certs.pkcs12 -srcstoretype PKCS12 \
        -deststoretype pkcs12 -destkeystore caosdb.jks \
        <<< "${KEYPW}"$'\n'"${KEYPW}"$'\n'"$KEYSTOREPW"

# Copy the resulting jks to the CaosDB cert directory
target_jks="${profile_dir}/custom/other/cert/caosdb.jks"
mkdir -p $(dirname ${target_jks})
cp caosdb.jks "${target_jks}"
chown ${docker_name}:${docker_group} "${target_jks}"
chmod 644 "${target_jks}"
