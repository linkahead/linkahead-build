#!/usr/bin/env python3

import os
import subprocess

import yaml
from pytest import mark


def get_config():
    config = os.environ.get("CONFIG_FILE")
    with open(config) as yaml_f:
        config = yaml.safe_load(yaml_f)

    return config


def get_container_name(config):

    return os.environ.get("CONTAINER_NAME", config["conf"]["container_name"])


@mark.xfail(condition=get_config()["conf"]["mail"] is False,
            reason="Mail is disabled")
def test_sending_mail(bash):
    with bash() as s:
        config = get_config()
        args = ['exec']

        if config["conf"]["mail"]:
            args.extend(
                ["-e", "SENDMAIL={}".format(get_config()["conf"]["sendmail"])])
        else:
            args.extend(["-e", "SENDMAIL=/do/not/send/mail"])
        containername = get_container_name(config)
        args.extend([containername, 'check_mail'])
        res = s.run_script('docker', args)

        assert "postdrop" not in res, "sendmail did not work properly"


def test_mounts_are_readable(bash):
    containername = get_container_name(get_config())
    with bash() as s:
        res = s.run_script(
            'docker',
            ['exec', containername, 'bash', '-c', "ls /opt/caosdb/mnt/*"])


def test_anonymous(bash):
    config = get_config()
    try:
        anonymous = config["conf"]["server"]["conf"]["auth_optional"]

        port = config["conf"]["network"]["port_ssl"]
        context_root = ""
        context_root = config["conf"]["server"]["conf"]["context_root"]
    except (KeyError, TypeError):
        pass

    cmd = ["curl", "-k",
           f"https://localhost:{port}{context_root}/Entity?query=count%20record"]
    process = subprocess.run(cmd, stdout=subprocess.PIPE)
    assert ('Error code="401"' in process.stdout.decode()) is not anonymous
