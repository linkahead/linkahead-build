#!/usr/bin/env python3

MAX_DISK_USAGE = 90


def test_internet_connection(bash):
    with bash() as s:
        s.run_script('ping', ['-c', '1', "8.8.8.8"])


def test_indiscale_gitlab(bash):
    with bash() as s:
        s.run_script('ping', ['-c', '1', "gitlab.indiscale.com"])


def test_disk_space(bash):
    el = bash.run_script_inline(
        ['df --output=pcent . | tail -n 1 | sed s/%//'])
    assert float(el.strip()) < MAX_DISK_USAGE
