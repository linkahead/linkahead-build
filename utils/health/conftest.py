
def pytest_itemcollected(item):
    if item._obj is not None and item._obj.__doc__ is not None:
        item._nodeid += "\n    {}".format(item._obj.__doc__)
