#!/bin/bash
# This is a specialised command to be used in the gitlab CI.
# It replaces the tag in the profile with the given argument
# Needs to be executed in ./debian

sed -i 's@.*tag:."latest"@    tag: "'"$1"'"@' debian_files/tests/test_profile.yml
