Build the docker image using the docker file `docker build -t debian_build .`.
Start a container in the top level directory with `docker run -ti -v "$PWD:/deploy" debian_build bash`
