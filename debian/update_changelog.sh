#!/bin/bash
# Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2022 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This script adds a default entry to the Debian changelog.
#
# This script can be configured only via environment variables and the setup.sh script at the
# moment:
#
# Environment variables
# ---------------------
#
# DEBEMAIL : str
#   The email address of the maintainer. See `man 1 debchange` for details.
#
# DEBFULLNAME : str
#   The full name of the maintainer. See `man 1 debchange` for details.

set -e

. setup.sh

export DEBEMAIL
export DEBFULLNAME

# Guarantee that the previous release is marked as released, without opening an editor.
# VISUAL=/bin/true dch -c debian_files/changelog -r --nomainttrailer --nomultimaint \
# --no-force-save-on-release
sed -i 's/) UNRELEASED;/) unstable;/' debian_files/changelog

# Add new entry with default text.
TEXT="Released upstream version ${VERSION}; see full changelog for details."
dch -c debian_files/changelog -v "${VERSION_DEB}" \
    --allow-lower-version "[0-9]+\.[0-9]+\.[0-9]+" -u low "${TEXT}"

echo "You may now review and edit the changelog."
dch -c debian_files/changelog --edit

