#!/bin/bash
# Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2022 Daniel Hornung <d.hornung@indiscale.com>
# Copyright (C) 2021 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This script prepares the directory for the Debian package to be built.
#
# This script can be configured only via environment variables and the setup.sh script at the
# moment:
#
# Environment variables
# ---------------------
#
# PKG_KIND: str
#
#   Must either be unset, empty, or "demo".  If set, specific patches may be
#   applied to this directory's content, especially the content of ``debian_files``.
#
# Configuration
# -------------
#
# setup.sh
#
#   Set the program version here, and also a few other variables like contact person and program
#   name.

set -e

. setup.sh

# Apply patches for different kinds of the Debian package.
if [[ $PKG_KIND = "demo" ]] ; then
    PKGDIFF=demo.patch

# -- cut here --- 8< --- cut here --- 8< ---
#
# Add a new "elif" section here if you want to use your own PKG_KIND.
#
# -- cut here --- 8< --- cut here --- 8< ---

fi

DEBIAN_FILES="debian_files"
BASE_LA="linkahead"
if [[ $PKGDIFF ]] ; then
    DEBIAN_FILES="${DEBIAN_FILES}_${PKG_KIND}"
    BASE_LA="${BASE_LA}_${PKG_KIND}"
    [[ -d "${DEBIAN_FILES}" ]] && rm -r "${DEBIAN_FILES}"  # Clean up first
    cp -r debian_files "${DEBIAN_FILES}"
    echo "executing: patch -t -p0 -r ${DEBIAN_FILES} < $PKGDIFF"
    patch -t -p1 -d "${DEBIAN_FILES}" < "$PKGDIFF"
fi

# Create archive with files ###################################################

# E.g. `linkahead_demo/linkahead-0.4.2`
PVDIR="${BASE_LA}/$PV"
[[ -d "$PVDIR" ]] && rm -rf "$PVDIR"  # Clean up first
mkdir -p "$PVDIR"

# Hard link debian files: Changes made to the linked files will be seen on the originals.
mkdir -p "$PVDIR/debian/"
echo "cp -rlP $DEBIAN_FILES/* $PVDIR/debian/"
cp -rlP "$DEBIAN_FILES/"* "$PVDIR/debian/"

# TODO Instead of copying single files, clone whole directory and apply patches
# via quilt/dquilt?
# https://www.debian.org/doc/manuals/debmake-doc/ch03.en.html#quilt-setup
files="
    linkahead
    Makefile
    docs/linkahead.1
    .docker/wait-for-it.sh
    utils/renewcert.service
    utils/renewcert.timer
    utils/convert_certbot_certs.sh
    utils/install
    utils/health
    defaults.yml
    schema-profile.yml
    profiles/default
    profiles/demo
    profiles/debug
    compose/docker-compose-default.yml
    compose/envoy-compose-additions.yml
    compose/envoy.yaml
"

# Test if LinkAhead theme was checked out and is clean, else fail.
if [[ $(git submodule status ../profiles/demo/theme-linkahead/ | cut -b 1 ) != " " ]] ; then
    echo "LinkAhead theme does not seem to be at the specified commit state.  Exiting now."
    exit 1
fi
pushd ../profiles/demo/theme-linkahead/
if [[ $(git status -s -u | wc -l ) -gt 0 ]] ; then
    echo "There are uncommitted changes in the LinkAhead theme. Exiting now."
    exit 1
fi
popd
# LinkAhead theme seems to be installed OK.

pushd "$PVDIR"
for file in $files; do
    dir="."
    if [[ $file = */* ]] ; then
        dir="${file%/*}/"
        mkdir -p "$dir"
    fi
    cp -a ../../../"$file" "$dir"
done

if [[ ! -e debian/tests/pyinttests ]] ; then
    git clone --branch main --depth 1 https://gitlab.indiscale.com/caosdb/src/caosdb-pyinttest.git debian/tests/pyinttests
else
    pushd debian/tests/pyinttests
    git pull
    popd
fi

pushd debian/tests/pyinttests
git rev-parse HEAD
popd

pushd ..
[[ $VERBOSE ]] && ls -l
tar -czf "$PV.tar.gz" "$PV"
[[ $VERBOSE ]] && ls -l
popd

# Sometimes, it is necessary to make quilt forget previous patch applications?
# dquilt pop -a -f

# stage 1: create template `debian` folder ################################
# dh_make creates a `debian` directory with template files
# Only necessary once.  And outdated, use debmake instead.
#dh_make --packageclass=i --createorig --yes

# debmake only creates new files, does not override old files.  So it should be
# safe to be called more than once.
debmake -x4 -b ':py'

# Go only until stage 1
if [[ $1 = "-1" ]] ; then
    exit
fi

popd
