#!/bin/bash

systemctl status -n 100 linkahead

pass init nobody@pass.example

docker login -u gitlab-ci-token -p $INDISCALE_REGISTRY_TOKEN $INDISCALE_REGISTRY

# change user to root scince ladocker does not exist
sed -i 's/ladocker/root/g' /lib/systemd/system/linkahead.service
# enable debug in the server since it is needed for pyinttest
mkdir -p /usr/share/linkahead/profiles/default/custom/other/restore/
cp /usr/share/linkahead/profiles/debug/custom/other/restore/* /usr/share/linkahead/profiles/default/custom/other/restore/
cp debian/tests/test_profile.yml /usr/share/linkahead/profiles/default/profile.yml

mkdir -p /usr/share/linkahead/profiles/default/custom/caosdb-server/scripting/home
cp debian/tests/serverside_pycaosdb.ini /usr/share/linkahead/profiles/default/custom/caosdb-server/scripting/home/.pycaosdb.ini

/usr/bin/systemctl daemon-reload
/usr/bin/systemctl restart linkahead

cp debian/tests/pycaosdb.ini debian/tests/pyinttests/pycaosdb.ini

debian/tests/wait-for-it.sh localhost:10443 -t 1800 -- \
    sh -c "sleep 340 "

curl -k https://localhost:10443 > /dev/null
if [ 0 -ne $? ]
then
    echo "Could not connect to server with curl:"
    echo "curl -k https://localhost:10443"
    curl -k https://localhost:10443
    echo "curl return value: $?"
    echo -e "\n\njournal\n"
    /usr/bin/journalctl -b -u linkahead
    echo -e "\n\nstatus\n"
    /usr/bin/systemctl status linkahead
    echo -e "\n\nimages\n"
    docker image ls
    exit 1
fi


pip3 install --break-system-packages linkahead
pip3 install --break-system-packages pytest-cov python-dateutil

cd debian/tests/pyinttests
echo "pyinttest git hash:"
git rev-parse HEAD

echo "mysql git hash:"
docker cp linkahead:/opt/caosdb/git/caosdb_mysqlbackend_commit . && cat caosdb_mysqlbackend_commit

echo "webui git hash:"
docker cp linkahead:/opt/caosdb/git/caosdb_webui_commit . && cat caosdb_webui_commit

echo "server git hash:"
docker cp linkahead:/opt/caosdb/git/caosdb_server_commit . && cat caosdb_server_commit

# get certs from container and set the path in pycaosdb.ini
linkahead certs
sed -i 's@CACERT@'"$PWD"'/certs/caosdb.cert.pem@' pycaosdb.ini

# Print pycaosdb version
echo "caosdb-pylib version:"
python3 -c "import linkahead; print(linkahead.version.version)"

# Run integration tests
python3 -m pytest -m "not local_server" --cov=linkahead -vv
