#!/bin/bash
# Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2022 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This script actually builds the Debian package.
#
# This script can be configured only via environment variables and the setup.sh script at the
# moment:
#
# Environment variables
# ---------------------
#
# PKG_KIND: str
#
#   Must either be unset, empty, or "demo".  If set, use the prepared files in
#   the respective linkahead directory.
#
# Configuration
# -------------
#
# setup.sh
#
#   Set the program version here, and also a few other variables like contact person and program
#   name.


set -e

. setup.sh

# Stage 1: Set directory ######################################################

# Set linkahead base directory
BASE_LA="linkahead"
if [[ -v PKG_KIND ]] && [[ $PKG_KIND != "" ]] ; then
    BASE_LA="${BASE_LA}_${PKG_KIND}"
fi

# Stage 2: build deb package from configuration ###############################
# Notes:
# - `utils/install/linkahead.service` is copied to `debian/` via the rules file

pushd "${BASE_LA}/$PV"
# TODO No package signing in the cluster pipeline yet.
debuild --no-sign
popd
