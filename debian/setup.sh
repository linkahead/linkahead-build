#!/bin/bash
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2022 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

export DEBEMAIL="d.hornung@indiscale.com"
export DEBFULLNAME="Daniel Hornung"
VERSION="${VERSION:-0.16.0}"
VERSION_DEB="${VERSION}-1"  # Account for package revision here.
PROGRAM="linkahead"
PV="$PROGRAM-$VERSION"
PV_DSC="${PROGRAM}_${VERSION_DEB}.dsc"  # Required for CI pipeline.

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

