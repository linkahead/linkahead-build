# How to build a Debian package

There are currently three types of Debian images which can be built, the plain image and two
specialized ones.  For the specialized images, the environment variable `PKG_KIND` must be set:

- **Plain image** This is the default.  Nothing special needs to be done for this.
- **Demo** This is for the demo server package.  Set `PKG_KIND=demo` to build this package.

With the environment variable either set or not set, run the following commands in the `debian`
directory (the environment variable must be equal for all commands):

```console
$ ./prepare_deb.sh
$ ./build_deb
```

# Create new package kinds #

For new package kinds, similar to the *demo* packages:

1. Add a new `elif` section to `prepare_deb.sh`.  This may, but needs not, involve:
   1. Create patches which are applied to your `debian_files_<new>`.
   2. Special handling in the scripts.
