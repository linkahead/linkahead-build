#!/usr/bin/env python3

# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021,2022 IndiScale GmbH <www.indiscale.com>
# Copyright (C) 2021 Alexander Schlemmer <a.schlemmer@indiscale.com>
# Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Test the validation of the schema file.
"""


import os
from glob import glob

import yaml
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from pytest import raises


def validate_yaml_file(valfn):
    """Worker function: Validate the file.

A ValidationError will be raised if the validation fails.

Parameters
----------
valfn : String
  The file name to be validated.
"""
    with open(valfn) as filename:
        valobj = yaml.load(filename, Loader=yaml.SafeLoader)
    with open(os.path.join(os.path.dirname(__file__), "../schema-profile.yml")) as filename:
        schema = yaml.load(filename, Loader=yaml.SafeLoader)
    # assume that the profile definition is always the first element in the profile yml
    profile_key = list(valobj.keys())[0]
    validate(instance=valobj[profile_key], schema=schema["caosdb-deploy"])


def test_profiles():
    for filename in glob(os.path.join(os.path.dirname(__file__), "test_profiles", "*.yml")):
        validate_yaml_file(filename)


def test_broken_profiles():
    for filename in glob(os.path.join(os.path.dirname(__file__), "broken_profiles", "*.yml")):
        with raises(ValidationError):
            validate_yaml_file(filename)
