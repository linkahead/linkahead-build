# Basic Setup

Change the desired profile:

```yaml
any-profile:
  refs:
    # or any other f-grpc-* branch
    SERVER: f-grpc-dev
  conf:
    server:conf:
      grpc_server_port_https: 8443
      grpc_server_port_http: 8080
```

# Setup for caosdb-cppinttest

The tests currently expect "auth_optional" to be false and to run against the
**default** profile (for some test entities).

Change the default profile:

```yaml
default:
  conf:
    local_users: false
    server:conf:
      auth_optional: false
      grpc_server_port_https: 8443
      grpc_server_port_http: 8080
```


# Ports

Standard ports of the GRPC end-point are currently

* 8443 for https over tls
* 8080 for plain http

Change this via:

```yml
any-profile:
  conf:
    network:
      port_grpc_ssl: 8443
      port_grpc_plain: 8080
```
