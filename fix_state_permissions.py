#!/usr/bin/env python3

import caosdb as db


def remove_root_permissions_from_anonymous():
    admin = db.administration
    perms = admin._get_permissions("anonymous")

    g = admin.PermissionRule(action="Grant", permission="*", priority=True)
    if g in perms:
        perms.remove(g)

    admin._set_permissions(role="anonymous", permission_rules=perms)

def update_state_model():
    acl_not_published = db.ACL()

    # remove all permissions of others from the entity.
    acl_not_published.deny(role="?OTHER?", permission="*", priority=True)

    for state_name in ["Under Review", "Unpublished"]:
        state = db.execute_query(
            "FIND RECORD State WITH name = '{}'".format(state_name),
            unique=True)

        # this is the magic...
        state.acl = db.State.create_state_acl(acl_not_published)
        state.update_acl()

def update_under_review():
    under_review = db.execute_query("FIND RECORD WITH State = 'Under Review'")
    for rec in under_review:
        rec.state = db.State("Publish Life-cycle", "Unpublished")
        rec.update()
        rec.state = db.State("Publish Life-cycle", "Under Review")
        rec.update()

def update_unpublished():
    unpublished = db.execute_query("FIND RECORD WITH State = Unpublished")
    for rec in unpublished:
        rec.update()

remove_root_permissions_from_anonymous()
update_state_model()
update_unpublished()
update_under_review()
