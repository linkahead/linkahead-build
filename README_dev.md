# Branches of this repository

Branches in this repository are a bit special, because some of them have
hard-coded git references.  This is the used nomenclature:

- `main` :: The `main` branch should be the current stable branch.
- `dev` :: The `dev` branch is where general development of the deploy
  repository takes place.
- `f-*` :: These are feature branches for development.
- Branches named after caosdb's branches :: If a branch is named like a branch
  of other caosdb repositories (except `dev`, which refers to development in
  this repository), it should use references that are known to work.
