
# Automatic Triggering of Pipelines

This repository's pipeline is triggered by other repositories (most prominently
[caosdb-server], [caosdb-mysqlbackend], [caosdb-pylib], and [caosdb-webui]) and
triggers the [caosdb-pyinttest] pipeline in return.

The branches which are used during the build phase of this repository's
pipeline depend on the pipeline variables. Per default, all repositories work
on the `dev` branch. However, when a triggering repository submits the variable
`F_BRANCH`, and the variable begins with `f-`, the build stage builds all
repositories in that branch (if existent, otherwise it falls back to `dev`) and
also triggers the `f-` branch of the [caosdb-pyinttest] repository (if
existent, otherwise `dev`).

This enables the developers to have automatic pipelines running where all
branches fit together as long as they keep their names consistent at comply
with the `f-` naming style.

# Manual Triggerin of Pipelines

If developers want to trigger a pipeline with special branches (or even
commits) checked out during the build phase and a special branch for the
[caosdb-pyinttest], they can use the `Run Pipeline` feature of gitlab.

Just go to the Pipelines view and click the `Run Pipeline` button. Then you may
set the `F_BRANCH` variable by hand which results in the same behavior as
described for the automatic pipelines.

You can even checkout special branches or commits by setting the variables
`SERVER`, `WEBUI`, `PYLIB`, `MYSQLBACKEND`, and `PYINT` which set the commits
for each branch explicitely.

# Variables Overview

+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| Variable          | Description                                                                                                                                           |
+===================+=======================================================================================================================================================+
| F_BRANCH          | Triggers the build of all repositories in the same branch. Needs to comply with the "f-branch naming convention".                                     |
+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| TRIGGERED_BY_REPO | For debugging purposes. Indicates the triggering repository.                                                                                          |
+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| TRIGGERED_BY_REF  | For debugging purposes. Indicates the triggering branch.                                                                                              |
+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| TRIGGERED_BY_HASH | For debugging purposes. Indicates the triggering commit hash.                                                                                         |
+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| SERVER            | Set the server's branch or commit. (build stage)                                                                                                      |
+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| WEBUI             | Set the webui's branch or commit. (build stage)                                                                                                       |
+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| PYLIB             | Set the pylib's branch or commit. (build stage)                                                                                                       |
+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| MYSQLBACKEND      | Set mysql-backend's branch or commit (build stage)                                                                                                    |
+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| PYINT             | Set the caosdb-pyinttest's branch or commit when triggering the remote repository.                                                                    |
+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| CPPINT            | Set the caosdb-cppinttest's branch or commit when triggering the remote repository.                                                                   |
+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| CPPLIB            | Set the caosdb-cpplib's branch or commit when triggering the remote repository.                                                                       |
+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| CAOSDB_TAG        | Generated by caosdb-deploy. Uniquely identifies the build which is to be used to test the integration test suites against.                            |
+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| REFTAG            | Generated by caosdb-deploy. Hint for the integration tests. They may load the image at /image-cache/caosdb-${REFTAG}.tar before starting a container. |
+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
