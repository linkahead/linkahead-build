# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2020 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

prefix = /usr/local
bindir = $(DESTDIR)$(prefix)/bin
etc = $(DESTDIR)/etc
pwd = $(shell pwd)
servicedir = $(etc)/systemd/system
initdir = $(etc)/init.d
confdir = $(etc)/linkahead
mandir = $(DESTDIR)$(prefix)/share/man
sharedir = $(DESTDIR)$(prefix)/share/linkahead

INSTALL=install
PROFILENAME=default

.PHONY: install default pycaosdb.ini

# No default target
default:
	@echo "There is no default make target, try 'make install' for example."
	@echo "Installation requirements:"
	@echo "- There must be a user named 'ladocker', it must be in the 'docker' group."
	@echo "  Or, alternatively, the service script must by adaptes accordingly."
	@echo "\nRuntime requirements are listed in the general documentation."
# @exit 1

install:
	$(INSTALL) -D -t $(bindir) linkahead
	$(INSTALL) -D -t $(mandir)/man1 docs/linkahead.1
	$(INSTALL) -D -t $(confdir) utils/install/linkahead.conf
	$(INSTALL) -D -t $(sharedir) defaults.yml
	$(INSTALL) -D -t $(sharedir) utils/renewcert.service
	$(INSTALL) -D -t $(sharedir) utils/renewcert.timer
	$(INSTALL) -D -t $(bindir) utils/convert_certbot_certs.sh
	$(INSTALL) -D -t $(sharedir) schema-profile.yml
	$(INSTALL) -D -t $(sharedir)/compose compose/docker-compose-default.yml
	$(INSTALL) -D -t $(sharedir)/compose compose/envoy-compose-additions.yml
	$(INSTALL) -D -t $(sharedir)/compose compose/envoy.yaml
# no recursive $(INSTALL)?
# find profiles -type f -exec $(INSTALL) -D -t $(sharedir) \{\} \;
	cp -rP --preserve=mode,links -v profiles $(sharedir)
	@echo "\nPlease configure /etc/linkahead/linkahead.conf according to your needs."
	@echo "Especially, there should be a Docker image in the default dir or a 'docker/'"
	@echo "directory to build your own image."
	@echo "Please do not call this manually! This is intended for use in the debian package"

uninstall:
	rm -f $(bindir)/linkahead
	rm -f $(servicedir)/linkahead.service
	rm -f $(initdir)/linkahead.service
	rm -f $(mandir)/man1/linkahead.1

pycaosdb.ini:
	@export CAOSRUN=$$(./linkahead print_caosrun) ; \
		rm -rf "$${CAOSRUN}/.certs" ; \
		./linkahead certs -d "$${CAOSRUN}/.certs" ; \
		export CERTFILE="$${CAOSRUN}/.certs/caosdb.cert.pem"; \
		envsubst '$$CERTFILE' < $${CAOSRUN}/.pycaosdb.ini.template > $${CAOSRUN}/pycaosdb.ini ; \
		echo "export PYCAOSDBINI=$${CAOSRUN}/pycaosdb.ini"

doc:
	$(MAKE) -C docs html

unittest:
	python3 -m pytest unittests
.PHONY: unittest

inttest:
	python3 -m pytest integrationtests
.PHONY: inttest
