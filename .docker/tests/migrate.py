#!/usr/bin/env python3

# This file is a part of the CaosDB Project.
#
# Copyright (c) 2020 IndiScale GmbH
# Copyright (c) 2020 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

"""Allows to create a tar from caosroot of an older LinkAhead version """

import argparse
import os

from linkahead import get_volumes_backup_base

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--container-name", default="linkahead")
    parser.add_argument("-d", "--target-dir", default="backup")
    args = parser.parse_args()
    get_volumes_backup_base(args.container_name,
                            os.path.abspath(args.target_dir))
