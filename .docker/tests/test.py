#!/usr/bin/env python
# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2020 Henrik tom Wörden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
import os
import unittest

from linkahead import recursive_update


class RecursiveUpdate(unittest.TestCase):
    def test(self):
        self.assertEqual(recursive_update({"a": 2}, {"b": 5}),
                         {"a": 2, "b": 5})
        self.assertEqual(recursive_update({"a": 2}, {"a": 5}),
                         {"a": 5})
        self.assertEqual(recursive_update({"a": {"c": 7}}, {"a": 5}),
                         {"a": 5})
        self.assertEqual(recursive_update({"a": {"c": 7}}, {"a": {"d": 9}}),
                         {"a": {"c": 7, "d": 9}})
        self.assertEqual(recursive_update({"a": {"c": 7, "e": 1}},
                                          {"a": {"c": 9}}),
                         {"a": {"c": 9, "e": 1}})
