#!/usr/bin/env python3

# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header

from datetime import datetime, timedelta

import caosdb as db

__version_parts = db.version.short_version.split(".")
if (int(__version_parts[0]) <= 0 and int(__version_parts[1]) <= 5 and int(__version_parts[2]) <= 2
        and "dev" not in db.version.full_version):
    raise ImportError("PyCaosDB is too old, need state machine handling.  Try PyCaosDB > 0.5.2")

GENERATED_PROP_NAMES = ["generated", "new_insertion"]
RESET_TIME_MINUTES = 10
DATE_FMT = "%Y-%m-%dT%H:%M:%S.%f%z"
# ID and version string of the analysis that has to be restored. Note
# that these may have to be adapted.
ANALYSIS_ID = 124
ANALYSIS_VERSION = "154a1168a2ec0df5d8bc0711b67d7ce53fccc7b7"
PUBLISHED_NAME = "Published"
UNPUBLISHED_NAME = "Unpublished"
UNDER_REVIEW_NAME = "Under Review"

# ID and versions of the recrod types from the other use cases that
# have to be restored.
OTHER_DATA_VERSIONS = {
    "bike": {
        "id": 253,
        "version": "bc2f14ee33f147daec27e50d375d94b6d71a22bf"
    },
    "qc_result": {
        "id": 248,
        "version": "9e575e03ecf97d1228e19778476ab90c297bd529"
    },
    "tensile_analysis": {
        "id": 286,
        "version": "8d27befde5090e46b8a08f705456a61154315b32"
    },
}


def _older_than_cutoff(entity):
    """Check whether the latest version of an entity is older than the
    global cutoff.

    """

    cutoff_timedelta = timedelta(seconds=60*RESET_TIME_MINUTES)

    version_time = datetime.strptime(entity.version.date, DATE_FMT)
    version_delta = datetime.now(tz=version_time.tzinfo) - version_time

    return version_delta > cutoff_timedelta


def _delete_old_generated():
    """Find all entities that were generated during the execution of the
    tour, i.e., which have a GENERATED_PROP_NAME=True and which are
    older than RESET_TIME_MINUTES minutes.

    """

    # use history flag for full version information
    candidates = db.Container()
    for name in GENERATED_PROP_NAMES:
        tmp = db.execute_query("FIND ENTITY WITH {}=TRUE".format(
            name), flags={'H': None})
        candidates.extend(tmp)

    if not candidates:
        # Don't continue if there aren't any generated entities
        return

    to_be_deleted = db.Container()
    for ent in candidates:

        if _older_than_cutoff(ent):
            to_be_deleted.append(ent)

    if to_be_deleted:
        to_be_deleted.delete()


def _restore_analysis_state():
    """Restore the analysis that was changed from "Under review" to
    "Published" during the tour to its original state.

    """

    # Get old analysis in its specific version
    old_analysis = db.Container().retrieve(
        query="{}@{}".format(ANALYSIS_ID, ANALYSIS_VERSION),
        sync=False)[0]

    # retrieve with full history. Will raise an error if the record
    # has been deleted but this is intendend.
    analysis = db.Record(id=ANALYSIS_ID).retrieve(flags={'H': None})

    if analysis.version.id == old_analysis.version.id:
        # no changes, so don't do anything
        return

    if _older_than_cutoff(analysis) and not analysis.state.name == UNDER_REVIEW_NAME:
        # set state to "Unpublished" to allow transaction to "Under
        # Review"
        if analysis.state.name == PUBLISHED_NAME:
            analysis.state = db.State(model=analysis.state.model,
                                      name=UNPUBLISHED_NAME)
            analysis.update()
        # replace by old version
        old_analysis.version = None
        old_analysis.update()


def _restore_other_use_cases():
    """Restore all the RecordTypes changed during the presentation of the
    other use cases to their original states.

    """

    for rec_info in OTHER_DATA_VERSIONS.values():

        new_rec = db.Record(id=rec_info["id"]).retrieve(flags={'H': None})

        if _older_than_cutoff(new_rec) and not new_rec.version.id == rec_info["version"]:

            old_rec = db.Container().retrieve(
                query="{}@{}".format(rec_info["id"], rec_info["version"]), sync=False)[0]
            old_rec.version = None
            old_rec.update()


def main():

    _delete_old_generated()
    _restore_analysis_state()
    _restore_other_use_cases()


if __name__ == "__main__":

    main()
