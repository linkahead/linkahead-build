#!/usr/bin/env python3

# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019, 2020 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2019 Daniel Hornung <d.hornung@indiscale.com>
# Copyright (C) 2020 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

"""Initializes the database for the demo server.

This script expects an "empty" database and creates the content required for the
demo server tour.

"""

import datetime

import caosdb as db


def create_data_model():
    """Create and return a Container with data model."""
    print("Creating data model.")
    properties = {
        # "color": db.Property(name="color", datatype=db.TEXT),
        "price": db.Property(name="price", datatype=db.DOUBLE, unit="€"),
        "quality": db.Property(name="quality_factor", datatype=db.DOUBLE),
        "date": db.Property(name="date", datatype=db.DATETIME),
        "visitors": db.Property(name="visitors", datatype=db.INTEGER),
        "tickets": db.Property(name="ticket_sales", datatype=db.DOUBLE),
        "serialNumber": db.Property(name="serialNumber", datatype=db.TEXT),
        "report": db.Property(name="report", datatype=db.REFERENCE),
        "electric": db.Property(name="electric", datatype=db.BOOLEAN),
    }
    Manufacturer = db.RecordType(name="Manufacturer")
    MusicalInstrument = db.RecordType(name="MusicalInstrument")
    MusicalInstrument.add_property(property=properties["price"])
    MusicalInstrument.add_property(property=Manufacturer)
    Violin = db.RecordType(name="Violin")
    Violin.add_parent(MusicalInstrument)
    Guitar = db.RecordType(name="Guitar")
    Guitar.add_parent(MusicalInstrument)
    Guitar.add_property(property=properties["electric"])
    SoundQualityAnalyzer = db.RecordType(name="SoundQualityAnalyzer")
    SoundQualityAnalyzer.add_property("serialNumber")
    Analysis = db.RecordType(name="MusicalAnalysis")
    Analysis.add_property(property=properties["quality"])
    Analysis.add_property(property=properties["date"])
    Analysis.add_property(property=properties["report"])
    Analysis.add_property(property=SoundQualityAnalyzer)
    Analysis.add_property(property=MusicalInstrument)
    # Added manually later by the user
    # Concert = db.RecordType(name="Concert")
    # Concert.add_property(property=properties["date"])
    # Concert.add_property(property=properties["visitors"])
    # Concert.add_property(property=properties["ticket_sales"])

    container = db.Container()
    container.extend(list(properties.values()))
    container.extend([Manufacturer, MusicalInstrument, Violin, Guitar,
                      SoundQualityAnalyzer, Analysis])
    return container


def create_data(container):
    print("Creating Records.")
    MusicalInstrument = container.get_entity_by_name("MusicalInstrument")
    Manufacturer = container.get_entity_by_name("Manufacturer")
    Guitar = container.get_entity_by_name("Guitar")
    price = container.get_entity_by_name("price")
    electric = container.get_entity_by_name("electric")

    g1 = db.Record(name="My first guitar")
    g1.add_parent(Guitar)
    g1.add_property(property=price, value=48.0)
    g1.add_property(electric, False)

    man1 = db.Record(name="Gibson")
    man1.add_parent(Manufacturer)

    g2 = db.Record(name="Nice guitar")
    g2.add_parent(Guitar)
    g2.add_property(property=price, value=2474.0)
    g2.add_property(electric, True)
    g2.add_property(Manufacturer, man1)

    g3 = db.Record(name="John Lennon's guitar",
                   description="The greatest guitar that was ever made.")
    g3.add_parent(Guitar)
    g3.add_property(property=price, value=2.12e6)
    g3.add_property(electric, True)
    g3.add_property(Manufacturer, man1)

    Violin = container.get_entity_by_name("Violin")

    man2 = db.Record(name="Antonio Stradivari")
    man2.add_parent(Manufacturer)

    v1 = db.Record(name="Sherlock Holmes' violin")
    v1.add_parent(Violin)
    v1.add_property(property=price, value=814873)
    v1.add_property(Manufacturer, man2)

    SoundQualityAnalyzer = container.get_entity_by_name("SoundQualityAnalyzer")

    sqa1 = db.Record(name="Harmonics Scanner HS 142")
    sqa1.add_parent(SoundQualityAnalyzer)
    sqa1.add_property("serialNumber", "KN-150.8888")

    sqa2 = db.Record(name="Riff-o-Tron 4")
    sqa2.add_parent(SoundQualityAnalyzer)
    sqa2.add_property("serialNumber", "KN-140.5555")

    Analysis = container.get_entity_by_name("Analysis")
    quality_factor = container.get_entity_by_name("quality_factor")
    date = container.get_entity_by_name("date")

    an1 = db.Record(name="G / 2019-023")
    an1.add_parent(Analysis)
    an1.add_property(MusicalInstrument, g1)
    an1.add_property(SoundQualityAnalyzer, sqa1)
    an1.add_property(quality_factor, "0.08")
    an1.add_property(date, datetime.date(2019, 5, 16))

    an2 = db.Record(name="G / 2019-024")
    an2.add_parent(Analysis)
    an2.add_property(MusicalInstrument, g2)
    an2.add_property(SoundQualityAnalyzer, sqa2)
    an2.add_property(quality_factor, "0.97")
    an2.add_property(date, datetime.date(2019, 7, 24))

    an3 = db.Record(name="V / 2019-025")
    an3.add_parent(Analysis)
    an3.add_property(MusicalInstrument, v1)
    an3.add_property(SoundQualityAnalyzer, sqa1)
    an3.add_property(quality_factor, "0.84")
    an3.add_property(date, datetime.date(2019, 9, 9))

    records = db.Container()
    records.extend([g1, man1, g2, g3, man2, v1, sqa1, sqa2, an1, an2, an3])
    return records


def create_files(container):
    print("Creating files.")
    name1 = "G / 2019-023"
    name2 = "G / 2019-024"
    name3 = "V / 2019-025"

    pdf1 = db.File(name="Report " + name1, path='/reports/report_g_2019-023.pdf',
                   file="./files/report_1.pdf", thumbnail="./files/report_1.png")
    pdf2 = db.File(name="Report " + name2, path='/reports/report_g_2019-024.pdf',
                   file="./files/report_2.pdf", thumbnail="./files/report_2.png")
    pdf3 = db.File(name="Report " + name3, path='/reports/report_v_2019-025.pdf',
                   file="./files/report_3.pdf", thumbnail="./files/report_3.png")

    new_files = db.Container()
    new_files.extend([pdf1, pdf2, pdf3])
    new_files.insert()
    print("Files inserted.")

    container.extend(new_files)

    p_report = container.get_entity_by_name("report")
    an1 = container.get_entity_by_name(name1)
    an2 = container.get_entity_by_name(name2)
    an3 = container.get_entity_by_name(name3)
    an1.add_property(p_report, pdf1)
    an2.add_property(p_report, pdf2)
    an3.add_property(p_report, pdf3)

    container.update()

    print("Records updated with file properties.")

    return container


def remove_update_and_delete_permissions(container):
    for entity in container:
        # TODO: Remove when fixed in server
        if isinstance(entity, db.File):
            continue
        entity.retrieve_acl()
        entity.deny(realm="PAM", username="admin", priority=True,
                    permission="UPDATE:*")
        entity.deny(realm="PAM", username="admin", priority=True,
                    permission="DELETE")
        entity.update_acl()


def main():
    conn = db.connection.connection.configure_connection(
        url="https://localhost:10443/", ssl_insecure=True,
        username="admin", password_method="plain", password="caosdb")

    db.execute_query("FIND Record Experiment")
    container = create_data_model()
    # manual _login() needed because of improper anon user handling by pylib
    # not necessary anymore?
    # conn._login()
    try:
        print("Inserting data model.")
        container.insert()
    except Exception as e:
        print(e)
        container.retrieve()
    print("Data model inserted.")

    data = create_data(container)
    try:
        print("Inserting data.")
        data.insert()
    except Exception as e:
        print(e)
        data.retrieve()
    print("Data inserted.")

    container.extend(data)

    container = create_files(container)
    remove_update_and_delete_permissions(container)


if __name__ == '__main__':
    main()
