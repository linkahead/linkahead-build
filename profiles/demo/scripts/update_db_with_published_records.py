#!/usr/bin/env python3

# This file and the two data folders can be removed after the changes have been inserted.

import os

import caosdb as db

MA = db.RecordType(name="MusicalAnalysis").retrieve()
ST = db.RecordType(name="SettingsTable").retrieve()
RR = db.RecordType(name="RiffRecording").retrieve()

entities = db.Container()

reports = [
    {
        "dir":  "2019-05-20_021",
        "filename": "report_g_2019-021.pdf",
        "date": "2019-05-20",
        "Guitar": 146,
        "Analyzer": 122,
        "quality": 0.3,
    },
    {
        "dir":  "2019-07-24_022",
        "filename": "report_g_2019-022.pdf",
        "date": "2019-07-24",
        "Guitar": 117,
        "Analyzer": 121,
        "quality": 0.3,
    },
]

analyses = []

for rep in reports:
    f_info = db.File(path="/Analyses/{}/info.yaml".format(rep["dir"]),
                     file=os.path.join(rep["dir"], "info.yaml"))
    f_mode = db.File(path="/Analyses/{}/mode.npy".format(rep["dir"]),
                     file=os.path.join(rep["dir"], "mode.npy"))
    f_set = db.File(path="/Analyses/{}/settings.csv".format(rep["dir"]),
                    file=os.path.join(rep["dir"], "settings.csv"))
    f_rep = db.File(path="/Analyses/{}/{}".format(rep["dir"], rep["filename"]),
                    file=os.path.join(rep["dir"], rep["filename"]))

    f_mode.add_parent(RR)
    f_set.add_parent(ST)

    ana = db.Record().add_parent(MA)
    ana.add_property(name="MusicalInstrument", value=rep["Guitar"])
    ana.add_property(name="quality_factor", value=rep["quality"])
    ana.add_property(name="date", value=rep["date"])
    ana.add_property(name="report", value=f_rep)
    ana.add_property(name="SettingsTable", value=f_set)
    ana.add_property(name="RiffRecording", value=f_mode)

    analyses.append(ana)
    entities.extend([f_info, f_mode, f_set, f_rep, ana])

entities.insert()

review_state = db.State(name="Under Review", model="Publish Life-cycle")
published_state = db.State(name="Published", model="Publish Life-cycle")
for ana in analyses:
    ana.state = review_state
    ana.update()
    ana.state = published_state
    ana.update()
