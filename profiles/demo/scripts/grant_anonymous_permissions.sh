#!/bin/bash

# This script will grant the anonymous user permissions to execute the
# server-side scripts for table and graph previews (and nothing
# else). For it to work, you have to create a symlink to
# caosdb/utils/caosdb_admin.py in this directory.
python3 caosdb_admin.py grant_role_permissions anonymous "SCRIPTING:EXECUTE:showcase.py"
python3 caosdb_admin.py grant_role_permissions anonymous "SCRIPTING:EXECUTE:ext_table_preview:pandas_table_preview.py"
