#!/bin/bash

# Insert model and fill in example data sets
python3 insert_datamodel.py
python3 initialize_bikes.py
python3 initialize_lab.py
