# -*- coding: utf-8 -*-
#
# ** header v3.0
#
# Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2021 Florian Spreckelsen
# <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#

from dataclasses import dataclass

import caosdb as db


@dataclass
class ResearchLabModel:
    """List names of RecordTypes and properties necessary for the research
    lab example.

    """

    person: str = "Person"
    email: str = "email_address"
    manufacturer: str = "Manufacturer"
    address: str = "address"
    contact: str = "contact"
    latitude: str = "latitude"
    longitude: str = "longitude"
    prod_date: str = "production_date"
    prod_machine: str = "ProductionMachine"
    tensile_device: str = "TensileAnalysisDevice"
    author: str = "author"
    procedure: str = "procedure"
    prep_temp: str = "preparation_temperature"
    ana_freq: str = "analysis_frequency"
    sop: str = "SOP"
    simple: str = "SimpleProcedure"
    source: str = "SourceMaterial"
    serial: str = "serial_no"
    preparation: str = "Preparation"
    temp_set: str = "temperature_set"
    temp_meas: str = "temperature_measured"
    supervisor: str = "supervisor"
    analysis: str = "TensileAnalysis"
    freq_set: str = "frequency_set"
    freq_meas: str = "frequency_measured"
    color: str = "color"
    yield_stress: str = "tensile_strength"
    raw_data: str = "RawTensileAnalysisData"
    sample: str = "Sample"
    study: str = "Study"
    responsible: str = "responsible"
    start: str = "start_date"
    end: str = "end_date"


DATAMODEL = ResearchLabModel()


def insert_persons():
    """Insert persons that may be scientists, lab assistents, contact
    persons with manufacturers...

    """
    # name, email
    persons = [
        ("Simone Scientist", "s.scientist@lab.edu"),
        ("Sebastian Sample", "sebastian@sample.com"),
        ("Lucas Labassistant", "l.labassistant@lab.edu"),
        ("Dorothee Device", "dorothee@device.org")
    ]

    person_rt = db.RecordType(name=DATAMODEL.person).retrieve()
    cont = db.Container()
    for name, email in persons:
        person = db.Record(name=name).add_parent(person_rt)
        person.add_property(name=DATAMODEL.email, value=email)
        cont.append(person)

    cont.insert()
    return cont


def insert_manufacturers(persons):
    """Insert manufacturers of materials and devices."""
    # name, adress, contact, lat, long
    manufacturers = [
        ("Prima Proben AG", "Probenstraße 1, 98765 Probstadt",
         persons[1].id, 47.4, 7.7),
        ("Scientific Devices Ltd", "24 Science Street, 681012 Labbington",
         persons[3].id, 19.6, -98.5)
    ]

    manu_rt = db.RecordType(name=DATAMODEL.manufacturer).retrieve()
    cont = db.Container()
    for name, addr, contact, latitude, longitude in manufacturers:
        manu = db.Record(name=name).add_parent(manu_rt)
        manu.add_property(name=DATAMODEL.address, value=addr)
        manu.add_property(name=DATAMODEL.contact, value=contact)
        manu.add_property(name=DATAMODEL.latitude, value=latitude)
        manu.add_property(name=DATAMODEL.longitude, value=longitude)
        cont.append(manu)

    cont.insert()
    return cont


def insert_devices(manufacturers):
    """Insert production macines and analysis devices"""
    # name, manufacturer, production date
    prod_machines = [
        ("Sampl-o-Tron 5000", manufacturers[1].id, "2020-12-02"),
        ("TT Shak-o-matic", manufacturers[1].id, "2017-08-09")
    ]
    tensile_devices = [
        ("RheoTens 500", manufacturers[1].id, "2011-10-13")
    ]

    prod_rt = db.RecordType(name=DATAMODEL.prod_machine).retrieve()
    tens_rt = db.RecordType(name=DATAMODEL.tensile_device).retrieve()
    cont = db.Container()
    for machine_list, parent in [(prod_machines, prod_rt), (tensile_devices, tens_rt)]:
        for name, manu, date in machine_list:
            machine = db.Record(name=name).add_parent(parent)
            machine.add_property(name=DATAMODEL.manufacturer, value=manu)
            machine.add_property(name=DATAMODEL.prod_date, value=date)
            cont.append(machine)

    cont.insert()
    return cont


def insert_files():
    """Insert the files containing the raw analysis data."""

    soft_file = db.File(name="soft_sample.npy", path="Tensile Stress Analyses/soft_sample.npy",
                        file="../paths/extroot/tensile_samples/soft_sample.npy")
    hard_file = db.File(name="hard_sample.npy", path="Tensile Stress Analyses/hard_sample.npy",
                        file="../paths/extroot/tensile_samples/hard_sample.npy")

    cont = db.Container().extend([soft_file, hard_file])
    raw_rt = db.RecordType(name=DATAMODEL.raw_data).retrieve()
    for f in cont:
        f.add_parent(raw_rt)
    cont.insert()

    return cont


def insert_procedures(persons):
    """Insert procedures for sample preparation, analysis, and studies."""

    sop_rt = db.RecordType(name=DATAMODEL.sop).retrieve()
    simple_rt = db.RecordType(name=DATAMODEL.simple).retrieve()

    cont = db.Container()
    biop_procedure = db.Record(name="Biopolymer preparation procedure")
    biop_procedure.description = "Create a biopolymer sample from source materials for later analysis."
    biop_procedure.add_parent(simple_rt)
    biop_procedure.add_property(name=DATAMODEL.author, value=persons[2].id)
    biop_procedure.add_property(
        name=DATAMODEL.procedure, value="Mix the source materials, then let the gel solidify for 5 hours.")
    biop_procedure.add_property(
        name=DATAMODEL.prep_temp, value=320)
    cont.append(biop_procedure)

    osc_analysis = db.Record(name="Oscillatory tensile stress test")
    osc_analysis.add_parent(simple_rt)
    osc_analysis.description = "Measure the tensile stress response of a sample to an oscillatory strain."
    osc_analysis.add_property(name=DATAMODEL.author, value=persons[2].id)
    osc_analysis.add_property(
        name=DATAMODEL.procedure, value="Apply oscillatory strain at a fixed frequency and measure the tensile stress response with increasing amplitude.")
    osc_analysis.add_property(name=DATAMODEL.ana_freq, value=1)
    cont.append(osc_analysis)

    tensile_study = db.Record(name="Tensile stress response of biopolymers")
    tensile_study.add_parent(sop_rt)
    tensile_study.description = "Compare the tensile stress response and the yield stress of different biopolymer samples."
    tensile_study.add_property(
        name=DATAMODEL.procedure, value="After sample preparation, apply oscillatory tensile strain with increasing amplitudes and record the stress response. Compare tensile stress and yield stress of different samples.")
    tensile_study.add_property(name=DATAMODEL.author, value=persons[0].id)
    cont.append(tensile_study)

    cont.insert()
    return cont


def insert_materials(manufacturers):
    """Insert source materials for samples."""
    # name, manufacturer, serial_no
    materials = [
        ("PBS", manufacturers[0].id, "2020ABC"),
        ("NaOH", manufacturers[0].id, "naoh_123"),
        ("Bovine Collagen I", manufacturers[0].id, "21BC456"),
        ("P-Fibrin", manufacturers[0].id, "21PF789")
    ]

    source_rt = db.RecordType(name=DATAMODEL.source).retrieve()
    cont = db.Container()
    for name, manu, serial in materials:
        material = db.Record(name=name).add_parent(source_rt)
        material.add_property(name=DATAMODEL.manufacturer, value=manu)
        material.add_property(name=DATAMODEL.serial, value=serial)
        cont.append(material)

    cont.insert()
    return cont


def insert_preparations(materials, procedures, machines, persons):
    """Insert preparation of samples from source materials."""
    # name, list of sources, procedure, production machine, temp_set, temp_meas, supervisor
    preparations = [
        ("SoftSamplePreparation", [materials[0].id, materials[1].id, materials[2].id],
         procedures[0].id, machines[1].id, 320, 321.3, persons[2].id),
        ("HardSamplePreparation", [materials[0].id, materials[1].id, materials[3].id],
         procedures[0].id, machines[0].id, 320, 319.9, persons[2].id),
    ]

    prep_rt = db.RecordType(name=DATAMODEL.preparation).retrieve()
    cont = db.Container()

    for name, sources, sop, machine, temp_set, temp_meas, supervisor in preparations:

        prep = db.Record(name=name).add_parent(prep_rt)
        prep.add_property(name=DATAMODEL.source, datatype=db.LIST(
            DATAMODEL.source), value=sources)
        prep.add_property(name=DATAMODEL.sop, value=sop)
        prep.add_property(name=DATAMODEL.prod_machine, value=machine)
        prep.add_property(name=DATAMODEL.temp_set, value=temp_set)
        prep.add_property(name=DATAMODEL.temp_meas, value=temp_meas)
        prep.add_property(name=DATAMODEL.supervisor, value=supervisor)
        cont.append(prep)

    cont.insert()
    return cont


def insert_analyses(procedures, devices, persons, files):
    """Insert analysis records for the prepared samples."""
    # name, procedure, device, supervisor, freq_set, freq_meas, color,
    # yield stress, raw data
    analyses = [
        ("SoftSampleAnalysis", procedures[1].id, devices[2].id,
         persons[0].id, 1, 1.007, "white", 146.3, files[0].id),
        ("HardSampleAnalysis", procedures[1].id, devices[2].id,
         persons[0].id, 1, 0.999, "grey", 1.463E7, files[1].id),
    ]

    analysis_rt = db.RecordType(name=DATAMODEL.analysis).retrieve()
    cont = db.Container()
    for name, sop, machine, supervisor, freq_set, freq_meas, color, yield_stress, raw_data in analyses:

        analysis = db.Record(name=name).add_parent(analysis_rt)
        analysis.add_property(name=DATAMODEL.sop, value=sop)
        analysis.add_property(name=DATAMODEL.tensile_device, value=machine)
        analysis.add_property(name=DATAMODEL.supervisor, value=supervisor)
        analysis.add_property(name=DATAMODEL.freq_set, value=freq_set)
        analysis.add_property(name=DATAMODEL.freq_meas, value=freq_meas)
        analysis.add_property(name=DATAMODEL.color, value=color)
        analysis.add_property(name=DATAMODEL.yield_stress, value=yield_stress)
        analysis.add_property(name=DATAMODEL.raw_data, value=raw_data)
        cont.append(analysis)

    cont.insert()
    return cont


def insert_samples_and_studies(preparations, analyses, procedures, persons):
    """Insert the samples wih preparation and analysis, and the study
    comparing the samples.

    """
    # name, prep, analysis
    samples = [
        ("SoftSample", preparations[0].id, analyses[0].id),
        ("HardSample", preparations[1].id, analyses[1].id),
    ]

    sample_rt = db.RecordType(name=DATAMODEL.sample).retrieve()
    sample_cont = db.Container()

    for name, prep, analysis in samples:

        sample = db.Record(name=name).add_parent(sample_rt)
        sample.add_property(name=DATAMODEL.preparation, value=prep)
        sample.add_property(name=DATAMODEL.analysis, value=analysis)
        sample_cont.append(sample)

    sample_cont.insert()

    # name, description, sop, responsible, sample list, start date, end date
    studies = [
        ("Yield-stress study", "A comparison of the tensile stress response and the yield stress of different biopolymers.",
         procedures[2].id, persons[0].id, [sample.id for sample in sample_cont], "2021-03-01", "2021-04-30")
    ]

    study_rt = db.RecordType(name=DATAMODEL.study).retrieve()
    cont = db.Container()
    for name, desc, sop, resp, samples, start, end in studies:

        study = db.Record(name=name).add_parent(study_rt)
        study.description = desc
        study.add_property(name=DATAMODEL.sop, value=sop)
        study.add_property(name=DATAMODEL.responsible, value=resp)
        study.add_property(name=DATAMODEL.sample, datatype=db.LIST(
            DATAMODEL.sample), value=samples)
        study.add_property(name=DATAMODEL.start, value=start)
        study.add_property(name=DATAMODEL.end, value=end)
        cont.append(study)

    cont.insert()


def main():

    persons = insert_persons()
    manufacturers = insert_manufacturers(persons)
    devices = insert_devices(manufacturers)
    files = insert_files()
    procedures = insert_procedures(persons)
    materials = insert_materials(manufacturers)
    preparations = insert_preparations(
        materials, procedures, devices, persons)
    analyses = insert_analyses(procedures, devices, persons, files)
    insert_samples_and_studies(preparations, analyses, procedures, persons)


if __name__ == "__main__":

    main()
