# -*- coding: utf-8 -*-
#
# ** header v3.0
#
# Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2021 Florian Spreckelsen
# <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#

from dataclasses import dataclass

import caosdb as db


@dataclass
class BicycleDataModel:
    """list names of RecordTypes and Properties of the bicycle data
    model.

    """
    person: str = "Person"
    email: str = "email_address"
    manufacturer: str = "Manufacturer"
    address: str = "address"
    contact: str = "Contact"
    latitude: str = "latitude"
    longitude: str = "longitude"
    sprockets: str = "sprockets"
    front_gear: str = "FrontGear"
    back_gear: str = "BackGear"
    diameter: str = "diameter"
    frame: str = "Frame"
    weight: str = "weight"
    qc_result: str = "QCResult"
    bike: str = "Bicycle"
    serial: str = "serial_no"
    production_date: str = "production_date"
    bike_assembly_machine: str = "BikeAssemblyMachine"
    qc_device: str = "QCDevice"
    assembly_date: str = "assembly_date"
    operator: str = "operator"
    assembly: str = "BikeAssembly"
    assembly_speed: str = "assembly_speed"
    firmware: str = "firmware_version"
    temperature: str = "temperature"
    qc: str = "QualityControl"
    passed: str = "passed"
    front_ok: str = "front_gear_ok"
    back_ok: str = "back_gear_ok"
    qc_report: str = "qc_report"


DATAMODEL = BicycleDataModel()


def insert_persons():
    """Insert some default persons with names and emails."""
    persons = [
        ("Frauke Fahrrad", "frauke@fahrad.de"),
        ("Manfred Manufacturer", "manfred@manufacturer.com"),
        ("Valentin Vélo", "valentin@velo.fr")
    ]

    person_rt = db.RecordType(name=DATAMODEL.person).retrieve()
    cont = db.Container()
    for name, email in persons:
        person = db.Record(name=name).add_parent(person_rt)
        person.add_property(name=DATAMODEL.email, value=email)
        cont.append(person)

    cont.insert()

    return cont


def insert_manufacturers(persons):
    """Insert manufacturers of bike parts and of machines."""
    manufacturers = [
        {
            "name": "Budget Bikes",
            "address": "123 Bike Street, 45678 Bicycletown",
            "contact": persons[1].id,
            "latitude": -25,
            "longitude": 126.4
        },
        {
            "name": "Top Mountainbikes Ltd.",
            "address": "Rue de Cycle, 1234 Véloville",
            "contact": persons[2].id,
            "latitude": 49.2,
            "longitude": 4
        },
        {
            "name": "Easy Assemblies",
            "address": "987 Forest Drive, 6543 Fairmountains",
            "contact": persons[1].id,
            "latitude": 21.5,
            "longitude": 25.4
        },
        {
            "name": "Bike QC AG",
            "address": "Fahrradweg 1, 23456 Raddorf",
            "contact": persons[0].id,
            "latitude": 51.4,
            "longitude": 13
        }
    ]

    manufacturer_rt = db.RecordType(name=DATAMODEL.manufacturer).retrieve()
    cont = db.Container()
    for manufacturer in manufacturers:
        rec = db.Record(name=manufacturer["name"]).add_parent(manufacturer_rt)
        rec.add_property(name=DATAMODEL.address, value=manufacturer["address"])
        rec.add_property(name=DATAMODEL.contact, value=manufacturer["contact"])
        rec.add_property(name=DATAMODEL.longitude,
                         value=manufacturer["longitude"])
        rec.add_property(name=DATAMODEL.latitude,
                         value=manufacturer["latitude"])
        cont.append(rec)

    cont.insert()

    return cont


def insert_bikes_and_parts(manufacturers):
    """Create and insert gears, frames, and bikes."""
    # name, manufacturer, number of sprockets, diameter
    backs = [
        ("Simple back", manufacturers[0].id, 7, 11),
        ("Trekking back", manufacturers[0].id, 10, 11),
        ("MTB 12 speed", manufacturers[1].id, 12, 16),
    ]
    # no diameter here
    fronts = [
        ("simple front", manufacturers[0].id, 3),
        ("MTB front", manufacturers[1].id, 1)
    ]
    frames = [
        ("Small frame", manufacturers[0].id, 5),
        ("Trekking frame", manufacturers[0].id, 10),
        ("MTB frame", manufacturers[1].id, 7.5)
    ]
    back_rt = db.RecordType(name=DATAMODEL.back_gear).retrieve()
    front_rt = db.RecordType(name=DATAMODEL.front_gear).retrieve()
    frame_rt = db.RecordType(name=DATAMODEL.frame).retrieve()
    parts = db.Container()
    for name, man, sprockets, diam in backs:
        rec = db.Record(name=name).add_parent(back_rt)
        rec.add_property(name=DATAMODEL.manufacturer, value=man)
        rec.add_property(name=DATAMODEL.sprockets, value=sprockets)
        rec.add_property(name=DATAMODEL.diameter, value=diam)
        parts.append(rec)

    for name, man, sprockets in fronts:
        rec = db.Record(name=name).add_parent(front_rt)
        rec.add_property(name=DATAMODEL.manufacturer, value=man)
        rec.add_property(name=DATAMODEL.sprockets, value=sprockets)
        parts.append(rec)

    for name, man, weight in frames:
        rec = db.Record(name=name).add_parent(frame_rt)
        rec.add_property(name=DATAMODEL.manufacturer, value=man)
        rec.add_property(name=DATAMODEL.weight, value=weight)
        parts.append(rec)

    parts.insert()

    # name, serial no, frame, front gear, back gear
    bikes = [
        ("Children's bike", "A3", parts[5].id, parts[3].id, parts[0].id),
        ("Nice trekking bike", "B1", parts[6].id, parts[3].id, parts[1].id),
        ("Lightweight MTB", "C4", parts[-1].id, parts[4].id, parts[2].id)
    ]

    bike_cont = db.Container()
    bike_rt = db.RecordType(name=DATAMODEL.bike).retrieve()
    for name, serial, frame, front, back in bikes:
        bike = db.Record(name=name).add_parent(bike_rt)
        bike.add_property(name=DATAMODEL.serial, value=serial)
        bike.add_property(name=DATAMODEL.frame, value=frame)
        bike.add_property(name=DATAMODEL.front_gear, value=front)
        bike.add_property(name=DATAMODEL.back_gear, value=back)
        bike_cont.append(bike)

    bike_cont.insert()
    return bike_cont


def insert_machines(manufacturers):
    """Bike assembly machines and QC devices"""
    bikes = [
        ("Easy Assemblies Assemblebot 9000", manufacturers[2].id, "2010-01-02")
    ]
    qcs = [
        ("BQC S123", manufacturers[3].id, "2011-02-01")
    ]
    machines = db.Container()
    bike_rt = db.RecordType(name=DATAMODEL.bike_assembly_machine).retrieve()
    qc_rt = db.RecordType(name=DATAMODEL.qc_device).retrieve()
    for machine_list, parent in [(bikes, bike_rt), (qcs, qc_rt)]:
        for name, man, prod_date in machine_list:
            machine = db.Record(name=name).add_parent(parent)
            machine.add_property(name=DATAMODEL.manufacturer, value=man)
            machine.add_property(
                name=DATAMODEL.production_date, value=prod_date)
            machines.append(machine)

    machines.insert()
    return machines


def insert_assemblies(persons, bikes, machines):
    """Insert bike assembly data with operators, assembled bikes, and
    assembly machines.

    """
    # date, operator, bike, machine, speed, firmware, temperature
    # (frame and gears are taken from bike)
    assemblies = [
        ("2021-04-14", persons[0].id, bikes[0], machines[0].id, 0.5, 3.14, 23),
        ("2021-04-15", persons[0].id, bikes[1], machines[0].id, 0.7, 3.15, 27),
        ("2021-04-16", persons[0].id, bikes[2], machines[0].id, 0.5, 3.14, 23),
    ]

    assembly_cont = db.Container()
    assembly_rt = db.RecordType(name=DATAMODEL.assembly).retrieve()
    for date, op, bike, mach, speed, firm, temp in assemblies:
        assembly = db.Record().add_parent(assembly_rt)
        assembly.add_property(name=DATAMODEL.assembly_date, value=date)
        assembly.add_property(name=DATAMODEL.operator, value=op)
        assembly.add_property(name=DATAMODEL.bike, value=bike.id)
        assembly.add_property(name=DATAMODEL.bike_assembly_machine, value=mach)
        assembly.add_property(name=DATAMODEL.assembly_speed, value=speed)
        assembly.add_property(name=DATAMODEL.firmware, value=firm)
        assembly.add_property(name=DATAMODEL.temperature, value=temp)
        frame = bike.get_property(DATAMODEL.frame).value
        front = bike.get_property(DATAMODEL.front_gear).value
        back = bike.get_property(DATAMODEL.back_gear).value
        assembly.add_property(name=DATAMODEL.frame, value=frame)
        assembly.add_property(name=DATAMODEL.front_gear, value=front)
        assembly.add_property(name=DATAMODEL.back_gear, value=back)

        assembly_cont.append(assembly)

    assembly_cont.insert()


def insert_qc_files():
    """Create file records from the csv files representing the qc
    reports.

    """
    # filename, path in CaosDB, local path
    qc_files = [
        ("Report: children's bike", "Bikes/2021-04-21_qc_report.csv",
         "../paths/extroot/bikes/2021-04-21_qc_report.csv"),
        ("Report: nice trekking bike", "Bikes/2021-04-22_qc_report.csv",
         "../paths/extroot/bikes/2021-04-22_qc_report.csv"),
        ("Report: lightweight MTB", "Bikes/2021-04-23_qc_report.csv",
         "../paths/extroot/bikes/2021-04-23_qc_report.csv"),
    ]

    files = db.Container()
    for name, cpath, lpath in qc_files:
        qc_file = db.File(name=name, path=cpath, file=lpath)
        files.append(qc_file)

    files.insert()
    return files


def insert_qc_results(machines, persons, bikes, files):
    """Insert the quality control events and the result records
    thereof.

    """
    # passed, front_ok, back_ok
    qcrs = [
        (True, True, True),
        (False, True, False),
        (True, True, True)
    ]

    qcr_rt = db.RecordType(name=DATAMODEL.qc_result).retrieve()
    qcr_cont = db.Container()
    for (passed, front_ok, back_ok), report in zip(qcrs, files):
        qcr = db.Record().add_parent(qcr_rt)
        qcr.add_property(name=DATAMODEL.passed, value=passed)
        qcr.add_property(name=DATAMODEL.front_ok, value=front_ok)
        qcr.add_property(name=DATAMODEL.back_ok, value=back_ok)
        qcr.add_property(name=DATAMODEL.qc_report, value=report.id)
        qcr_cont.append(qcr)

    qcr_cont.insert()

    for bike, qcr in zip(bikes, qcr_cont):

        bike.add_property(id=qcr_rt.id, value=qcr.id)
        bike.update()

    # device, firmware, op, bike
    qcs = [
        (machines[1].id, "1.1", persons[0].id, bikes[0].id),
        (machines[1].id, "1.1", persons[0].id, bikes[1].id),
        (machines[1].id, "1.1a", persons[0].id, bikes[2].id),
    ]

    qc_rt = db.RecordType(name=DATAMODEL.qc).retrieve()
    qc_cont = db.Container()
    for (mach, firm, op, bike), qcr in zip(qcs, qcr_cont):
        qc = db.Record().add_parent(qc_rt)
        qc.add_property(name=DATAMODEL.qc_device, value=mach)
        qc.add_property(name=DATAMODEL.firmware, value=firm)
        qc.add_property(name=DATAMODEL.operator, value=op)
        qc.add_property(name=DATAMODEL.bike, value=bike)
        qc.add_property(name=DATAMODEL.qc_result, value=qcr.id)
        qc_cont.append(qc)

    qc_cont.insert()

    


def main():

    persons = insert_persons()
    manufacturers = insert_manufacturers(persons)
    bikes = insert_bikes_and_parts(manufacturers)
    machines = insert_machines(manufacturers)
    insert_assemblies(persons, bikes, machines)
    files = insert_qc_files()
    insert_qc_results(machines, persons, bikes, files)


if __name__ == "__main__":

    main()
