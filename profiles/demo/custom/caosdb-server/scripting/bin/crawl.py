#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2020 Henrik tom Wörden <h.tomwoerden@indiscale.com>
# Copyright (C) 2018 Research Group Biomedical Physics
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

import logging
import os
import sys
from datetime import datetime

import caosdb as db
from caosadvancedtools.cfood import fileguide
from caosadvancedtools.crawler import FileCrawler
from caosadvancedtools.guard import INSERT, RETRIEVE, UPDATE
from caosadvancedtools.loadFiles import loadpath
from caosadvancedtools.serverside.helper import get_argument_parser
from caosadvancedtools.serverside.logging import configure_server_side_logging
from caosadvancedtools.webui_formatter import WebUI_Formatter

from analysis_cfood import AnalysisCFood

#from caosadvancedtools.verbosity import DEBUG, INFO, QUIET, VERBOSE



def get_parser():
    # TODO allow **kwargs or at least more options in the default parser?
    # E.g.:description=__doc__, formatter_class=RawTextHelpFormatter
    # with SSS this default parser will have SSS apropriate arguments.

    parser = get_argument_parser()
    parser.add_argument("path")
    parser.add_argument("-a", "--authorize-run", action='append',
                        help="supply the id of the run that you want to"
                        " authorize")
    parser.add_argument("-s", "--suppress",
                        help="suppress previously shown messages")

    return parser


def local_access(path):
    return "../extroot" + path


def sss_access(path):
    return "/opt/caosdb/mnt/caosroot" + path


if __name__ == "__main__":
    conlogger = logging.getLogger("connection")
    conlogger.setLevel(level=logging.ERROR)

    parser = get_parser()
    args = parser.parse_args()

    if args.suppress is not None:
        args.suppress = True
    else:
        args.suppress = False

    # assuming SSS

    if hasattr(args, "auth_token") and args.auth_token:
        db.configure_connection(auth_token=args.auth_token)
        fileguide.access = sss_access
        debug_file = configure_server_side_logging()
        logger = logging.getLogger("caosadvancedtools")
    else:
        fileguide.access = local_access
        # logging config for local execution
        logger = logging.getLogger("caosadvancedtools")
        logger.addHandler(logging.StreamHandler(sys.stdout))
        logger.setLevel(logging.DEBUG)
        debug_file = None

    if args.authorize_run:
        for run_id in args.authorize_run:
            FileCrawler.update_authorized_changes(run_id)
    else:
        files = FileCrawler.query_files(args.path)
        c = FileCrawler(files=files, use_cache=False,
                        abort_on_exception=False,
                        interactive=False,
                        hideKnown=args.suppress,
                        cfood_types=[
                            AnalysisCFood,
                        ],
                        debug_file=debug_file,
                        cache_file="/tmp/crawler_cache.db"
                        )
        c.crawl(security_level=INSERT, path=args.path)
