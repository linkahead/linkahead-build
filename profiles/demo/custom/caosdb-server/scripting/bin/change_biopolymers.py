#!/usr/bin/env python3
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Florian Sprecklesen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header

import logging
import os
import sys
from dataclasses import dataclass

import numpy as np

import caosdb as db
from caosadvancedtools.serverside import helper
from caosadvancedtools.serverside.logging import configure_server_side_logging


@dataclass
class BiopolymerDataModel:
    """list names of RecordTypes and Properties of the datamodel changed
    for ebikes.

    """

    inserted: str = "new_insertion"
    manufacturer: str = "Manufacturer"
    contact: str = "Contact"
    firmware: str = "firmware_version"
    operator: str = "operator"
    tensile_analysis: str = "TensileAnalysis"
    tensile_device: str = "TensileAnalysisDevice"
    freq_set: str = "frequency_set"
    freq_meas: str = "frequency_measured"
    tensile_strength: str = "tensile_strength"
    orient_set: str = "orientation_set"
    orient_meas: str = "orientation_meas"
    axial_strength: str = "tensile_strength_axial"
    radial_strength: str = "tensile_strength_radial"
    improved_device: str = "ImprovedTensileAnalysisDevice"
    prod_date: str = "production_date"
    raw_data: str = "RawTensileAnalysisData"
    improved_procedure: str = "ImprovedProcedure"
    ana_orient: str = "analysis_orientation"
    procedure: str = "procedure"
    author: str = "author"
    sop: str = "SOP"
    supervisor: str = "supervisor"
    color: str = "color"
    sample: str = "Sample"
    preparation: str = "Preparation"
    study: str = "Study"
    responsible: str = "responsible"
    start: str = "start_date"
    end: str = "end_date"


DATAMODEL = BiopolymerDataModel()


def _copy_record(old_rec, new_name):
    """Return a new record with the same parents and properties as a given
    one, but with a new name.

    """
    new_rec = db.Record(name=new_name)
    new_rec.properties = old_rec.properties
    new_rec.parents = old_rec.parents

    return new_rec


def retrieve_or_insert_prop(name, datatype, unit=None):
    """Find or create a property for newly inserted records."""

    prop = db.Property(name=name, datatype=datatype, unit=unit)
    try:
        prop.retrieve()
    except:
        prop.insert()

    return prop


def update_recordtypes():
    """Replace analysis device by improved device and change the other
    properties accordingly.

    """

    # only change analysis if it hasn't been done manually already
    update = False
    ana_rt = db.RecordType(name=DATAMODEL.tensile_analysis).retrieve()
    for prop in [DATAMODEL.tensile_device, DATAMODEL.freq_set,
                 DATAMODEL.freq_meas, DATAMODEL.tensile_strength]:
        if ana_rt.get_property(prop):
            ana_rt.remove_property(prop)
            update = True

    improved_device_rt = db.RecordType(
        name=DATAMODEL.improved_device).retrieve()
    orient_set_prop = retrieve_or_insert_prop(DATAMODEL.orient_set, db.DOUBLE)
    orient_meas_prop = retrieve_or_insert_prop(
        DATAMODEL.orient_meas, db.DOUBLE)
    axial_strength_prop = retrieve_or_insert_prop(
        DATAMODEL.axial_strength, db.DOUBLE, "Pa")
    radial_strength_prop = retrieve_or_insert_prop(
        DATAMODEL.radial_strength, db.DOUBLE, "Pa")
    for prop in [improved_device_rt, orient_set_prop,
                 orient_meas_prop, axial_strength_prop,
                 radial_strength_prop]:
        if not ana_rt.get_property(prop.name):
            ana_rt.add_property(prop.id)
            update = True

    if update:
        ana_rt.update()

    return ana_rt, improved_device_rt


def insert_records(inserted_prop, ana_rt, improved_device_rt):
    """Insert two new samples with analyses and an improved study."""

    # use existing manufacturer for improved device
    manu_device = db.Record(name="Scientific Devices Ltd").retrieve()
    # insert new analysis device
    improved_device = db.Record(
        name="RheoTens 701").add_parent(improved_device_rt)
    improved_device.description = "A tensile rheometer that can measure the axial and the radial stress and strain components separately."
    improved_device.add_property(name=inserted_prop.name, value=True)
    improved_device.add_property(
        name=DATAMODEL.manufacturer, value=manu_device.id)
    improved_device.add_property(name=DATAMODEL.prod_date, value="2020-11-22")
    improved_device.insert()

    # insert dummy analysis files
    raw_rt = db.RecordType(name=DATAMODEL.raw_data).retrieve()
    improved_soft_file = db.File(name="improved_soft_sample.npy", path="Tensile Stress Analyses/improved_soft_sample.npy",
                                 file="/opt/caosdb/mnt/extroot/tensile_samples/improved_soft_sample.npy")
    improved_hard_file = db.File(name="improved_hard_sample.npy", path="Tensile Stress Analyses/improved_hard_sample.npy",
                                 file="/opt/caosdb/mnt/extroot/tensile_samples/improved_hard_sample.npy")
    for f in [improved_soft_file, improved_hard_file]:
        f.add_parent(raw_rt)
        # retrieve if existing, otherwise insert
        try:
            f.retrieve()
        except:
            f.insert()

    # existing person for procedures
    lucas_labassistant = db.Record(name="Lucas Labassistant").retrieve()
    # insert improved analysis procedure
    improved_procedure_rt = db.RecordType(
        name=DATAMODEL.improved_procedure).retrieve()
    improved_procedure = db.Record(name="Axial and radial tensile stress test")
    improved_procedure.add_parent(improved_procedure_rt)
    improved_procedure.add_property(name=inserted_prop.name, value=True)
    improved_procedure.description = "Measure axial and radial tensile stress response of a sample to a constant strain."
    improved_procedure.add_property(
        name=DATAMODEL.author, value=lucas_labassistant.id)
    improved_procedure.add_property(
        name=DATAMODEL.procedure, value="Apply a constant strain and measure the axial and the radial component of the stress response.")
    improved_procedure.add_property(name=DATAMODEL.ana_orient, value=np.pi/4)
    improved_procedure.insert()

    # copy and edit old preparations
    old_soft_prep = db.Record(name="SoftSamplePreparation").retrieve()
    improved_soft_prep = _copy_record(
        old_soft_prep, "Improved" + old_soft_prep.name)
    old_hard_prep = db.Record(name="HardSamplePreparation").retrieve()
    improved_hard_prep = _copy_record(
        old_hard_prep, "Improved" + old_hard_prep.name)
    for prep in [improved_soft_prep, improved_hard_prep]:
        prep.add_property(name=inserted_prop.name, value=True)
        prep.insert()

    # existing person for analyses
    simone_scientist = db.Record(name="Simone Scientist").retrieve()
    # insert improved analyses
    improved_soft_analysis = db.Record(
        name="ImprovedSoftSampleAnalysis").add_parent(ana_rt)
    improved_soft_analysis.add_property(name=inserted_prop.name, value=True)
    improved_soft_analysis.add_property(
        name=DATAMODEL.sop, value=improved_procedure.id)
    improved_soft_analysis.add_property(
        name=DATAMODEL.improved_device, value=improved_device.id)
    improved_soft_analysis.add_property(
        name=DATAMODEL.supervisor, value=simone_scientist.id)
    improved_soft_analysis.add_property(
        name=DATAMODEL.orient_set, value=np.pi/4)
    improved_soft_analysis.add_property(
        name=DATAMODEL.orient_meas, value=1.01*np.pi/4)
    improved_soft_analysis.add_property(name=DATAMODEL.color, value="white")
    improved_soft_analysis.add_property(
        name=DATAMODEL.axial_strength, value=3/2*146.3)
    improved_soft_analysis.add_property(
        name=DATAMODEL.radial_strength, value=2/3*146.3)
    improved_soft_analysis.add_property(
        name=DATAMODEL.raw_data, value=improved_soft_file.id)

    improved_hard_analysis = db.Record(
        name="ImprovedHardSampleAnalysis").add_parent(ana_rt)
    improved_hard_analysis.add_property(name=inserted_prop.name, value=True)
    improved_hard_analysis.add_property(
        name=DATAMODEL.sop, value=improved_procedure.id)
    improved_hard_analysis.add_property(
        name=DATAMODEL.improved_device, value=improved_device.id)
    improved_hard_analysis.add_property(
        name=DATAMODEL.supervisor, value=simone_scientist.id)
    improved_hard_analysis.add_property(
        name=DATAMODEL.orient_set, value=np.pi/4)
    improved_hard_analysis.add_property(
        name=DATAMODEL.orient_meas, value=0.97*np.pi/4)
    improved_hard_analysis.add_property(name=DATAMODEL.color, value="grey")
    improved_hard_analysis.add_property(
        name=DATAMODEL.axial_strength, value=3/2*146.3E7)
    improved_hard_analysis.add_property(
        name=DATAMODEL.radial_strength, value=2/3*146.3E7)
    improved_hard_analysis.add_property(
        name=DATAMODEL.raw_data, value=improved_hard_file.id)

    db.Container().extend(
        [improved_soft_analysis, improved_hard_analysis]).insert()

    # insert new samples
    sample_rt = db.RecordType(name=DATAMODEL.sample).retrieve()
    improved_soft_sample = db.Record(
        name="ImprovedSoftSample").add_parent(sample_rt)
    improved_soft_sample.add_property(name=inserted_prop.name, value=True)
    improved_soft_sample.add_property(
        name=DATAMODEL.preparation, value=improved_soft_prep.id)
    improved_soft_sample.add_property(
        name=DATAMODEL.tensile_analysis, value=improved_soft_analysis.id)

    improved_hard_sample = db.Record(
        name="ImprovedHardSample").add_parent(sample_rt)
    improved_hard_sample.add_property(name=inserted_prop.name, value=True)
    improved_hard_sample.add_property(
        name=DATAMODEL.preparation, value=improved_hard_prep.id)
    improved_hard_sample.add_property(
        name=DATAMODEL.tensile_analysis, value=improved_hard_analysis.id)

    db.Container().extend(
        [improved_soft_sample, improved_hard_sample]).insert()

    # insert an improved study procedure
    sop_rt = db.RecordType(name=DATAMODEL.sop).retrieve()
    improved_study_procedure = db.Record(
        name="Axial and radial yield stress study").add_parent(sop_rt)
    improved_study_procedure.description = "Compare the axial and radial components of the yield stress of different biopolymer samples."
    improved_study_procedure.add_property(name=inserted_prop.name, value=True)
    improved_study_procedure.add_property(
        name=DATAMODEL.procedure, value="After sample preparation, apply axial and radial strain and measure the axial and radial stress responses of different samples.  Increase strain amplitude until yield stress.")
    improved_study_procedure.add_property(
        name=DATAMODEL.author, value=simone_scientist.id)
    improved_study_procedure.insert()

    # insert improved study
    study_rt = db.RecordType(name=DATAMODEL.study).retrieve()
    improved_study = db.Record(
        name="Two-component yield-stress study").add_parent(study_rt)
    improved_study.description = "A comparison of axial and radial components of the yield stress of different biopolymers."
    improved_study.add_property(name=inserted_prop.name, value=True)
    improved_study.add_property(
        name=DATAMODEL.sop, value=improved_study_procedure.id)
    improved_study.add_property(
        name=DATAMODEL.responsible, value=simone_scientist.id)
    improved_study.add_property(name=DATAMODEL.sample, value=[
                                improved_soft_sample.id, improved_hard_sample.id], datatype=db.LIST(DATAMODEL.sample))
    improved_study.add_property(name=DATAMODEL.start, value="2021-04-01")
    improved_study.add_property(name=DATAMODEL.start, value="2021-05-31")
    improved_study.insert()


def main():
    """Update the datamodel."""

    parser = helper.get_argument_parser()
    args = parser.parse_args()

    if hasattr(args, "auth_token") and args.auth_token:
        # when run with an auth_toke argument, assume it's server-side
        db.configure_connection(auth_token=args.auth_token)
        debug_file = configure_server_side_logging()
        logger = logging.getLogger("caosadvancedtools")

    else:
        # otherwise configure for local execution
        logger = logging.getLogger("caosadvancedtools")
        logger.addHandler(logging.StreamHandler(sys.stdout))
        logger.setLevel(level=logging.DEBUG)

    inserted_prop = retrieve_or_insert_prop(DATAMODEL.inserted, db.BOOLEAN)
    ana_rt, improved_device_rt = update_recordtypes()
    insert_records(inserted_prop, ana_rt, improved_device_rt)

    logger.info("""The biopolymer datamodel was updated successfully. Visit the new
Records <a href="/Entity/?query=FIND RECORD WITH new_insertion=TRUE">here</a>.""")


if __name__ == "__main__":

    main()
