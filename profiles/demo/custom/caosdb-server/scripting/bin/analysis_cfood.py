#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (C) 2019 Henrik tom Wörden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging
import os
from dataclasses import dataclass
from itertools import chain

import caosdb as db
import yaml
from caosadvancedtools.cfood import (AbstractFileCFood, assure_has_description,
                                     assure_has_parent, assure_has_property,
                                     assure_object_is_in_list, fileguide,
                                     get_entity)
from caosadvancedtools.read_md_header import get_header
from caosadvancedtools.scifolder.generic_pattern import (date_pattern,
                                                         date_suffix_pattern)


@dataclass
class DataModel(object):
    report: str = "report"
    generated: str = "generated"
    date: str = "date"
    Analysis: str = "MusicalAnalysis"
    MusicalInstrument: str = "MusicalInstrument"
    quality_factor: str = "quality_factor"
    description: str = "description"
    SoundQualityAnalyzer: str = "SoundQualityAnalyzer"
    RiffRecording: str = "RiffRecording"
    SettingsTable: str = "SettingsTable"


DATAMODEL = DataModel()
dm = DATAMODEL


class AnalysisCFood(AbstractFileCFood):
    _prefix = ".*/Analyses/"

    def __init__(self,  *args, **kwargs):
        super().__init__(*args, **kwargs)

    @staticmethod
    def get_re():
        return (AnalysisCFood._prefix + date_pattern + date_suffix_pattern
                + "info.yaml")

    def looking_for(self, crawled_file):
        if os.path.dirname(crawled_file) == os.path.dirname(self.crawled_path):
            return True

        return False

    def create_identifiables(self):
        # create the Analysis identifiable
        self.analysis = db.Record()
        self.analysis.add_parent(name=dm.Analysis)
        self.analysis.add_property(
            name=dm.date, value=self.match.group("date"))

        with open(fileguide.access(self.crawled_path)) as fi:
            data = yaml.safe_load(fi)

        self.analysis.add_property(name="SoundQualityAnalyzer", value=db.Record(
            name=data["SoundQualityAnalyzer"]).retrieve()
        )
        self.analysis.add_property(name="MusicalInstrument", value=db.Record(
            name=data["MusicalInstrument"]).retrieve()
        )

        self.identifiables.append(self.analysis)

    def update_identifiables(self):
        for fi in self.attached_items:
            if fi.endswith(".pdf"):
                fiobj = db.File(path=fi).retrieve()
                assure_object_is_in_list(obj=fiobj,
                                         containing_object=self.analysis,
                                         property_name=dm.report,
                                         to_be_updated=self.to_be_updated,
                                         datatype=db.LIST(db.REFERENCE)
                                         )

            if fi.endswith(".npy"):
                fiobj = db.File(path=fi).retrieve()
                assure_has_parent(fiobj, dm.RiffRecording, force=True,
                                  unique=False)
                assure_object_is_in_list(obj=fiobj,
                                         containing_object=self.analysis,
                                         property_name=dm.RiffRecording,
                                         to_be_updated=self.to_be_updated,
                                         )

            if (fi.endswith(".xlsx") or fi.endswith(".xls")
                    or fi.endswith(".csv")):
                fiobj = db.File(path=fi).retrieve()
                assure_has_parent(fiobj, dm.SettingsTable, force=True,
                                  unique=False)
                assure_object_is_in_list(obj=fiobj,
                                         containing_object=self.analysis,
                                         property_name=dm.SettingsTable,
                                         to_be_updated=self.to_be_updated,
                                         )

        with open(fileguide.access(self.crawled_path)) as fi:
            data = yaml.safe_load(fi)
        self.analysis.add_property(
            name="quality_factor", value=data["quality_factor"])
        # only generated for new ones

        if self.analysis.id not in [123, 124]:
            self.analysis.add_property(name="generated", value=True)
