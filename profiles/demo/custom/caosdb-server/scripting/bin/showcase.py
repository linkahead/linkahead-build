#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 IndiScale GmbH
# Copyright (C) 2019 Henrik tom Wörden
# Copyright (C) 2021 Alexander Schlemmer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import argparse
import datetime
import io
import json
import os
import sys
from tempfile import NamedTemporaryFile

import caosdb as db
import numpy as np


def _parse_arguments():
    """Parses the command line arguments.

    Takes into account defaults from the environment (where known).
    """
    parser = argparse.ArgumentParser(description='__doc__')
    tempdir = os.environ["SHARED_DIR"]
    parser.add_argument('-t', '--tempdir', required=False, default=tempdir,
                        help="Temporary dir for saving the result.")
    parser.add_argument(
        '-a', '--auth-token', required=False,
        help="An authentication token (not needed, only for compatibility).")
    parser.add_argument('numpy_file_id', help="The numpy file.")

    return parser.parse_args()


# def array_to_string(data):
#     return '","'.join(map(str, data))


def main():
    args = _parse_arguments()
    db.configure_connection(auth_token=args.auth_token,
                            url="https://127.0.0.1:10443",
                            ssl_insecure=True)
    # This is hopefully a pulsar event
    npfile = db.File(id=args.numpy_file_id)
    npfile.retrieve()

    if ((npfile.get_parent("RiffRecording") is not None) or
            (npfile.get_parent("RawTensileAnalysisData") is not None)):
        pass
    else:
        print("The provided numpy file should have PulsarEvent as parent.",
              file=sys.stderr)
        sys.exit(1)

    npfile_dl = npfile.download()
    data = np.load(npfile_dl)
    datalist = []
    updatemenulist = []
    names = ["spectrum", "mode"]
    falselist = [False] * len(names)

    for ii, name in enumerate(names):
        dat = data[ii]
        times = np.linspace(0, 100, dat.size)
        datalist.append({"x": list(times), "y": list(dat), "visible": 0 == ii})
        tmp = list(falselist)
        tmp[ii] = True
        updatemenulist.append({
            "method": "restyle",
            "label": name,
            "args": ["visible", tmp]})

    data_and_layout = {
        "data": datalist,
        "layout": {
            "margin": {"t": 0},
            "xaxis": {"title": "time [s]"},
            "yaxis": {"title": "signal [a.u.]"},
            "updatemenus": [{
                "y": 1,
                "yanchor": "top",
                "buttons": updatemenulist}],
        }
    }

    # See: https://docs.python.org/3/library/json.html
    data_and_layout_string = json.dumps(data_and_layout)

    with NamedTemporaryFile(delete=False, mode="w", dir=args.tempdir) as fi:
        fi.write(data_and_layout_string)

    randname = os.path.basename(os.path.abspath(args.tempdir))
    filename = os.path.join(randname, os.path.basename(fi.name))
    print(filename)


if __name__ == "__main__":
    main()
