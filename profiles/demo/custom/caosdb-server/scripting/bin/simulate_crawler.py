#!/usr/bin/env python3
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2019 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

"""Simulates a data crawler.
"""

import argparse
import datetime
import os
import random
import uuid

import numpy as np
from matplotlib import pyplot as plt

import caosdb as db
import cleanup_simulated_crawler


# How many (random) Entities to create.
MIN = 3
MAX = 5


def _simulate_crawler(token):
    """Simulates a data crawler.

The file is named `seleceted_data.{datetime}.xlsx`, where `{datetime}` is an
ISO8601 date and time string, formatted like "%Y-%m-%dT%H_%M_%S". The file name
does not have any magic functionality and the date and time is only there for
the user's convenience.

Parameters
----------
dataframe : pd.DataFrame
  The data frame to be written.
directory : str
  The string representation of the directory where the file shall be written.

Returns
-------
out : str
  The filename (last component of directory and basename).
    """

    # Removing previously generated entities first
    cleanup_simulated_crawler._cleanup(token=token)

    conn = db.configure_connection(auth_token=token)
    # conn._login()  # pylint: disable=protected-access
    generated = db.Property(name="generated", datatype=db.BOOLEAN)
    try:
        generated.retrieve()
    except db.TransactionError as err:
        print(err)
        generated.insert()

    instruments = _create_instruments(conn)
    devices = _create_devices(conn)
    # conn._login()  # pylint: disable=protected-access
    instruments.insert()
    devices.insert()
    analyses = _create_analyses(conn)
    analyses.insert()


def _create_instruments(conn):
    "Create a random number of instruments."

    names = ["James", "John", "Robert", "Michael", "William", "David",
             "Richard", "Charles", "Joseph", "Thomas", "Mary", "Patricia", "Linda",
             "Barbara", "Elizabeth", "Jennifer", "Maria", "Susan", "Margaret", "Dorothy"]

    container = db.Container()
    # conn._login()  # pylint: disable=protected-access
    generated = db.Property(name="generated").retrieve()
    Violin = db.RecordType(name="Violin").retrieve()
    Guitar = db.RecordType(name="Guitar").retrieve()
    price = db.Property(name="price").retrieve()
    electric = db.Property(name="electric").retrieve()
    price_scale_violin = 7000
    price_scale_guitar = 4000

    assert len(names) >= 2 * MAX
    for _ in range(random.randrange(MIN, MAX+1)):
        name = random.choice(names)
        names.remove(name)
        violin = db.Record(name=name+"'s Violin")
        violin.add_property(generated, True)
        violin.add_parent(Violin)
        violin.add_property(price,
                            round(random.weibullvariate(price_scale_violin, 2), 2))
        container.append(violin)
    for _ in range(random.randrange(MIN, MAX+1)):
        name = random.choice(names)
        names.remove(name)
        guitar = db.Record(name=name+"'s Guitar")
        guitar.add_property(generated, True)
        guitar.add_parent(Guitar)
        guitar.add_property(price,
                            round(random.weibullvariate(price_scale_guitar, 2)))
        guitar.add_property(electric, random.choice([True, False]))
        container.append(guitar)

    return container


def _create_devices(conn):
    "Create a random number of devices."

    adjectives = ["Geometric", "Holographic", "Hyper-realistic", "Fundamental",
                  "Inexplicable", "Mysterious", "Non-destructive", "Nuclear",
                  "Precise", "Predictive", "Relativistic", "Speculative"]
    device_bases = ["Bluenotifier", "Harmonizer", "Stringalyzer", "MultiScope",
                    "Musicometer", "MusiCorp", "Soundlink", "SymphiTec"]
    numbers = list(range(1, 6)) \
        + random.sample(range(100, 5001, 100), 10) \
        + ["alpha", "beta", "epsilon", "omega"]

    container = db.Container()
    # conn._login()  # pylint: disable=protected-access
    generated = db.Property(name="generated").retrieve()
    SQA = db.RecordType(name="SoundQualityAnalyzer").retrieve()
    names = set()
    for _ in range(random.randrange(MIN, MAX+1)):
        while True:
            name = " ".join([random.choice(adjectives),
                             random.choice(device_bases),
                             str(random.choice(numbers))])
            if name not in names:
                break
        names.add(name)
        analyzer = db.Record(name=name)
        analyzer.add_property(generated, True)
        analyzer.add_parent(SQA)
        container.append(analyzer)
    return container


def _create_analyses(conn):
    "Create a random number of analyses, and their files."
    # conn._login()  # pylint: disable=protected-access
    generated = db.Property(name="generated").retrieve()
    instruments = db.execute_query("FIND Record MusicalInstrument")
    devices = db.execute_query("FIND Record SoundQualityAnalyzer")
    Analysis = db.RecordType(name="Analysis").retrieve()
    SQA = db.RecordType(name="SoundQualityAnalyzer").retrieve()
    MusicalInstrument = db.RecordType(name="MusicalInstrument").retrieve()
    quality_factor = db.Property(name="quality_factor").retrieve()
    date_p = db.Property(name="date").retrieve()
    report = db.Property(name="report").retrieve()

    container = db.Container()
    i = 1
    for _ in range(random.randrange(2*MIN, 2*MAX+1)):
        instrument = random.choice(instruments)
        device = random.choice(devices)
        quality = round(random.uniform(0, 1), 2)
        days_ago = random.randint(0, 2*MAX)
        date = datetime.date.today() - datetime.timedelta(days=days_ago)
        filename = _create_report_pdf(instrument.name, device.name, quality,
                                      date)
        file_record = db.File(name=filename, path="/generated/"+filename, file=filename)
        file_record.add_property(generated, True)
        file_record.insert()

        analysis = db.Record(name="Auto-generated report {:d}".format(i))
        i += 1

        analysis.add_parent(Analysis)
        analysis.add_property(generated, True)
        analysis.add_property(date_p, date.isoformat())
        analysis.add_property(quality_factor, quality)
        analysis.add_property(SQA, device)
        analysis.add_property(MusicalInstrument, instrument)
        analysis.add_property(report, file_record)

        container.append(analysis)

    return container


def _create_report_pdf(instrument, device, quality, date):
    "Creates a random report pdf file and returns the file name."
    x = np.arange(0, 200, 0.1)
    size = 7
    widths = np.random.rayleigh(scale=20, size=size)
    offsets = np.random.uniform(low=20, high=150, size=size)
    heights = np.random.rayleigh(scale=5000, size=size)
    y = np.zeros(x.size)
    for width, offset, height in zip(widths, offsets, heights):
        y += height * np.exp(-(x - offset)**2 / width)

    content = ("Instrument: {}\nDevice: {}\n\n"
               "Quality: {:.2f}\nDate: {}".format(
                   instrument, device, quality, date
               ))
    fig = plt.figure(figsize=(8.27, 11.69))
    fig.text(0.5, 0.93, "Sound Quality report", ha='center', va='center',
             size=28)
    fig.text(0.07, 0.8, content, ha='left', va='top', size=20)
    plt.subplot(212)
    plt.plot(y)
    # plt.show()

    filename = "generated_report_file_{}.pdf".format(uuid.uuid4())
    plt.savefig(filename)

    return filename


def _parse_arguments():
    """Parses the command line arguments.

    Takes into account defaults from the environment (where known).
    """
    parser = argparse.ArgumentParser(description='__doc__')
    tempdir = os.environ["SHARED_DIR"]
    parser.add_argument('-t', '--tempdir', required=False, default=tempdir,
                        help="Temporary dir for saving the result.")
    parser.add_argument('-a', '--auth-token', required=False,
                        help="An authentication token (not needed, only for compatibility).")
    return parser.parse_args()


def main():
    "The main function."
    args = _parse_arguments()
    _simulate_crawler(token=args.auth_token)
    print("{success: true}")


if __name__ == "__main__":
    main()
