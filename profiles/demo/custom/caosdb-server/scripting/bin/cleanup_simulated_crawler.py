#!/usr/bin/env python3
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2019 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

"""Cleans up the Entities which were created by the simulate_crawler script.
"""

import argparse
import sys

import caosdb as db


def _cleanup(token):
    "Cleans up the Entities which were created by the simulate_crawler script."
    conn = db.configure_connection(auth_token=token)
    # conn._login()  # pylint: disable=protected-access

    files = db.execute_query("FIND File WHICH IS STORED AT /generated/*")
    records = db.execute_query("FIND Record WITH generated=TRUE")

    deletables = db.Container()
    deletables.extend(files)
    deletables.extend(records)

    if deletables:
        deletables.delete()

    return "{success: true}"


def _parse_arguments():
    """Parses the command line arguments.

    Takes into account defaults from the environment (where known).
    """
    parser = argparse.ArgumentParser(description='__doc__')
    parser.add_argument('-a', '--auth-token', required=False,
                        help="An authentication token (not needed, only for compatibility).")
    return parser.parse_args()


def main():
    "The main function."
    args = _parse_arguments()
    result = _cleanup(token=args.auth_token)
    print(result)


if __name__ == "__main__":
    main()
