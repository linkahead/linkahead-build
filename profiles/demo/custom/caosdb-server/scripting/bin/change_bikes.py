#!/usr/bin/env python3
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Florian Sprecklesen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header

import logging
import os
import sys
from dataclasses import dataclass

import caosdb as db
from caosadvancedtools.serverside import helper
from caosadvancedtools.serverside.logging import configure_server_side_logging


@dataclass
class EBikeDataModel:
    """list names of RecordTypes and Properties of the datamodel changed
    for ebikes.

    """

    inserted: str = "new_insertion"
    motor: str = "Motor"
    manufacturer: str = "Manufacturer"
    weight: str = "weight"
    power: str = "power"
    bike: str = "Bicycle"
    front_gear: str = "FrontGear"
    qc_result: str = "QCResult"
    front_gear_ok: str = "front_gear_ok"
    motor_ok: str = "motor_ok"
    front_gear: str = "FrontGear"
    back_gear: str = "BackGear"
    frame: str = "Frame"
    person: str = "Person"
    email: str = "email_address"
    address: str = "address"
    contact: str = "Contact"
    latitude: str = "latitude"
    longitude: str = "longitude"
    assembly_machine: str = "EBikeAssemblyMachine"
    assembly: str = "EBikeAssembly"
    assembly_speed: str = "assembly_speed"
    firmware: str = "firmware_version"
    temperature: str = "temperature"
    motor_firmware: str = "motor_firmware_version"
    production_date: str = "production_date"
    serial: str = "serial_no"
    assembly_date: str = "assembly_date"
    operator: str = "operator"
    back_ok: str = "back_gear_ok"
    passed: str = "passed"
    qc_report: str = "qc_report"
    qc_device: str = "QCDevice"


DATAMODEL = EBikeDataModel()


def retrieve_or_insert_prop(name, datatype):
    """Find or create a property for newly inserted records."""

    prop = db.Property(name=name, datatype=datatype)
    try:
        prop.retrieve()
    except:
        prop.insert()

    return prop


def update_recordtypes():
    """Replace front gear by motor for e-bikes."""

    # only change bikes if it hasn't been done manually already
    bike_rt = db.RecordType(name=DATAMODEL.bike).retrieve()
    motor_rt = db.RecordType(name=DATAMODEL.motor).retrieve()
    update = False
    if bike_rt.get_property(DATAMODEL.front_gear):
        bike_rt.remove_property(DATAMODEL.front_gear)
        update = True
    if not bike_rt.get_property(DATAMODEL.motor):
        bike_rt.add_property(id=motor_rt.id)
        update = True
    if update:
        bike_rt.update()

    motor_ok_prop = retrieve_or_insert_prop(DATAMODEL.motor_ok, db.BOOLEAN)
    qc_rt = db.RecordType(name=DATAMODEL.qc_result).retrieve()
    qc_rt.remove_property(DATAMODEL.front_gear_ok)
    qc_rt.add_property(id=motor_ok_prop.id)
    qc_rt.update()

    return bike_rt, motor_rt, qc_rt


def insert_records(inserted_prop, bike_rt, motor_rt, qc_rt):
    """Insert two new e-bikes with parts and corresponding qc results."""

    # Use existing frames and gears
    frame_rt = db.RecordType(name=DATAMODEL.frame).retrieve()
    trekking_frame = db.Record(name="Trekking frame").retrieve()
    mtb_frame = db.Record(name="MTB frame").retrieve()
    trekking_back = db.Record(name="Trekking back").retrieve()
    mtb_back = db.Record(name="MTB 12 speed").retrieve()

    # contact person for manufacturer
    person_rt = db.RecordType(name=DATAMODEL.person).retrieve()
    erik_ebike = db.Record(name="Erik Ebike").add_parent(person_rt)
    erik_ebike.add_property(name=inserted_prop.name, value=True)
    erik_ebike.add_property(name=DATAMODEL.email, value="erik@e-bike.com")
    erik_ebike.insert()

    # manufacturer for e-bike parts
    manu_rt = db.RecordType(name=DATAMODEL.manufacturer).retrieve()
    manu = db.Record(name="Erik's E-bikes, Inc.").add_parent(manu_rt)
    manu.add_property(name=inserted_prop.name, value=True)
    manu.add_property(name=DATAMODEL.contact, value=erik_ebike.id)
    manu.add_property(name=DATAMODEL.address,
                      value="111 E-bike Lane, 2222 Etown")
    manu.add_property(name=DATAMODEL.latitude, value=-22)
    manu.add_property(name=DATAMODEL.longitude, value=-44)
    manu.insert()

    # motors
    light_motor = db.Record(name="Light Motor").add_parent(motor_rt)
    light_motor.add_property(name=inserted_prop.name, value=True)
    light_motor.add_property(name=DATAMODEL.manufacturer, value=manu.id)
    light_motor.add_property(name=DATAMODEL.power, value=60)
    light_motor.add_property(name=DATAMODEL.weight, value=2)

    power_motor = db.Record(name="Power Motor").add_parent(motor_rt)
    power_motor.add_property(name=inserted_prop.name, value=True)
    power_motor.add_property(name=DATAMODEL.manufacturer, value=manu.id)
    power_motor.add_property(name=DATAMODEL.power, value=90)
    power_motor.add_property(name=DATAMODEL.weight, value=3)

    db.Container().extend([light_motor, power_motor]).insert()

    # use existing manufacturer
    machine_manu = db.Record(name="Easy Assemblies").retrieve()
    # insert assembly machine
    assembly_machine_rt = db.RecordType(
        name=DATAMODEL.assembly_machine).retrieve()
    assembly_machine = db.Record(
        name="Easy Assemblies Assemblebot 9001e").add_parent(assembly_machine_rt)
    assembly_machine.add_property(name=inserted_prop.name, value=True)
    assembly_machine.add_property(
        name=DATAMODEL.manufacturer, value=machine_manu.id)
    assembly_machine.add_property(
        name=DATAMODEL.production_date, value="2017-06-05")
    assembly_machine.insert()

    # Bikes
    trekking = db.Record(name="Electric Trekking Bike").add_parent(bike_rt)
    trekking.add_property(name=inserted_prop.name, value=True)
    trekking.add_property(name=DATAMODEL.serial, value="E1")
    trekking.add_property(name=DATAMODEL.frame, value=trekking_frame.id)
    trekking.add_property(name=DATAMODEL.motor, value=light_motor.id)
    trekking.add_property(name=DATAMODEL.back_gear, value=trekking_back.id)

    mtb = db.Record(name="Electric MTB").add_parent(bike_rt)
    mtb.add_property(name=inserted_prop.name, value=True)
    mtb.add_property(name=DATAMODEL.serial, value="E2")
    mtb.add_property(name=DATAMODEL.frame, value=mtb_frame.id)
    mtb.add_property(name=DATAMODEL.motor, value=light_motor.id)
    mtb.add_property(name=DATAMODEL.back_gear, value=mtb_back.id)

    db.Container().extend([trekking, mtb]).insert()

    # Assemblies
    assembly_rt = db.RecordType(name=DATAMODEL.assembly).retrieve()
    # use existing operator
    frauke_fahrrad = db.Record(name="Frauke Fahrrad").retrieve()
    trekking_assembly = db.Record().add_parent(assembly_rt)
    trekking_assembly.add_property(name=inserted_prop.name, value=True)
    trekking_assembly.add_property(name=DATAMODEL.bike, value=trekking.id)
    trekking_assembly.add_property(
        name=DATAMODEL.assembly_date, value="2021-05-03")
    trekking_assembly.add_property(
        name=DATAMODEL.operator, value=frauke_fahrrad.id)
    trekking_assembly.add_property(
        name=DATAMODEL.assembly_machine, value=assembly_machine.id)
    trekking_assembly.add_property(name=DATAMODEL.assembly_speed, value=0.3)
    trekking_assembly.add_property(name=DATAMODEL.firmware, value=4.25)
    trekking_assembly.add_property(name=DATAMODEL.temperature, value=23)
    trekking_assembly.add_property(
        name=DATAMODEL.frame, value=trekking_frame.id)
    trekking_assembly.add_property(name=DATAMODEL.motor, value=light_motor.id)
    trekking_assembly.add_property(name=DATAMODEL.motor_firmware, value=1.23)
    trekking_assembly.add_property(
        name=DATAMODEL.back_gear, value=trekking_back.id)

    mtb_assembly = db.Record().add_parent(assembly_rt)
    mtb_assembly.add_property(name=inserted_prop.name, value=True)
    mtb_assembly.add_property(name=DATAMODEL.bike, value=mtb.id)
    mtb_assembly.add_property(name=DATAMODEL.assembly_date, value="2021-05-04")
    mtb_assembly.add_property(name=DATAMODEL.operator, value=frauke_fahrrad.id)
    mtb_assembly.add_property(
        name=DATAMODEL.assembly_machine, value=assembly_machine.id)
    mtb_assembly.add_property(name=DATAMODEL.assembly_speed, value=0.3)
    mtb_assembly.add_property(name=DATAMODEL.firmware, value=4.25)
    mtb_assembly.add_property(name=DATAMODEL.temperature, value=23)
    mtb_assembly.add_property(name=DATAMODEL.frame, value=mtb_frame.id)
    mtb_assembly.add_property(name=DATAMODEL.motor, value=power_motor.id)
    mtb_assembly.add_property(name=DATAMODEL.motor_firmware, value=1.01)
    mtb_assembly.add_property(name=DATAMODEL.back_gear, value=mtb_back.id)

    db.Container().extend([trekking_assembly, mtb_assembly]).insert()

    # QC Report files
    trekking_file = db.File(
        name="Report: Electric trekking bike",
        path="Bikes/2021-05-06_qc_report.csv",
        file="/opt/caosdb/mnt/extroot/bikes/2021-05-06_qc_report.csv"
    )
    mtb_file = db.File(
        name="Report: Electric MTB",
        path="Bikes/2021-05-07_qc_report.csv",
        file="/opt/caosdb/mnt/extroot/bikes/2021-05-07_qc_report.csv"
    )

    # insert if not existing already
    for ff in [trekking_file, mtb_file]:
        try:
            ff.retrieve()
        except:
            ff.insert()

    # use existing qc device
    qc_machine = db.Record(name="BQC S123").retrieve()
    # insert qc result records and append them to bikes
    trekking_result = db.Record().add_parent(qc_rt)
    mtb_result = db.Record().add_parent(qc_rt)
    for result, ff, bike in zip([trekking_result, mtb_result], [trekking_file, mtb_file], [trekking, mtb]):
        result.add_property(name=DATAMODEL.operator, value=frauke_fahrrad.id)
        result.add_property(name=DATAMODEL.qc_device, value=qc_machine.id)
        result.add_property(name=DATAMODEL.firmware, value=1.2)
        result.add_property(name=DATAMODEL.bike, value=bike)
        result.add_property(name=inserted_prop.name, value=True)
        result.add_property(name=DATAMODEL.passed, value=True)
        result.add_property(name=DATAMODEL.back_ok, value=True)
        result.add_property(name=DATAMODEL.motor_ok, value=True)
        result.add_property(name=DATAMODEL.qc_report, value=ff.id)

    db.Container().extend([trekking_result, mtb_result]).insert()

    for bike, result in zip([trekking, mtb], [trekking_result, mtb_result]):
        bike.add_property(id=qc_rt.id, value=result.id)
        bike.update()


def main():
    """Update the datamodel."""

    parser = helper.get_argument_parser()
    args = parser.parse_args()

    if hasattr(args, "auth_token") and args.auth_token:
        # when run with an auth_toke argument, assume it's server-side
        db.configure_connection(auth_token=args.auth_token)
        debug_file = configure_server_side_logging()
        logger = logging.getLogger("caosadvancedtools")

    else:
        # otherwise configure for local execution
        logger = logging.getLogger("caosadvancedtools")
        logger.addHandler(logging.StreamHandler(sys.stdout))
        logger.setLevel(level=logging.DEBUG)

    inserted_prop = retrieve_or_insert_prop(DATAMODEL.inserted, db.BOOLEAN)
    bike_rt, motor_rt, qc_rt = update_recordtypes()
    insert_records(inserted_prop, bike_rt, motor_rt, qc_rt)

    logger.info("""The bicycle datamodel was updated successfully. Visit the new
Records <a href="/Entity/?query=FIND RECORD WITH new_insertion=TRUE">here</a>.""")


if __name__ == "__main__":

    main()
