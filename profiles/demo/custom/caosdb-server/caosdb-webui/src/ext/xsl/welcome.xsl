<!--
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 * Copyright (C) 2019 Daniel Hornung (d.hornung@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:template name="welcome">
    <div class="caosdb-v-welcome-panel mt-auto">
      <div class="container text-center my-5 caosdb-v-welcome-header">
        <h1>Welcome to LinkAhead</h1>
      </div>
      <div class="bg-light container pb-5 p-3 caosdb-f-welcome-panel" style="border-radius:5px;">
        <div class="row justify-content-around">
          <div class="col-xs-6 col-lg-5 order-last align-self-center d-flex justify-content-center">
            <img class="w-75" src="/webinterface/${BUILD_NUMBER}/pics/5_circles.png"
                 alt="LinkAhead - Semantic data management."/>
          </div>
          <div class="col-xs-6 col-lg-7 px-5">
            <h1 class="caosdb-v-welcome-caption">Thinking Data Management Ahead</h1>
            <p class="fs-4 fs-lg-3">With LinkAhead, you can manage your data in a more flexible way than ever. Experience what it's like to think data management ahead!</p>
            <p class="fs-4 fs-lg-3">
              You can explore LinkAhead yourself or take the interactive tour.
              Login as <code>admin</code> with password <code>caosdb</code>.
            </p>
            <p>
              <button id="caosdb-tour-welcome-start-btn" class="btn btn-lg" style="background:#ff593c;">Start a Tour</button>
              <a class="ms-4 btn btn-lg btn-primary" href="mailto:info@indiscale.com">Contact</a>
            </p>
            <p class="fs-5 fs-lg-3">
              Note that any data you enter on this demo server is public and that changes are reset
              daily.
            </p>
          </div>
        </div>
      </div>
      <div class="my-5"></div>
    </div>
  </xsl:template>
</xsl:stylesheet>
