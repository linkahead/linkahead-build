var demo_markdown = function() {

    var init = function() {
        const stdout_container = $("#caosdb-stdout");
        if (stdout_container.length == 0) {
            return;
        }

        const text = stdout_container[0].textContent;
        const html = markdown.textToHtml(text);

        stdout_container.css({padding: 10});
        stdout_container.empty();
        stdout_container.append(html);

		var errortext = $("#caosdb-stderr").text();
		if (errortext.length == 0) {
			$("#caosdb-container-stderr").hide();
		}
    }

    return {
        init: init
    };

}();


$(document).ready(function() {
    demo_markdown.init();
});
