/*  
 * ** header v3.0  
 * This file is a part of the CaosDB Project.  
 * 
 * Copyright (C) 2018 Max Planck Institut for Dynamics and
 * Self-organization
 * Copyright (C) 2018 Alexander Schlemmer
 * Copyright (C) 2018-2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Henrik tom Wörden <h.tomwoerden@indiscale.com>
 * Copyright (C) 2021 Florian Spreckelsen  
 * <f.spreckelsen@indiscale.com>  
 *  
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public
 * License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *  
 * ** end header  
 */


var demo = new function () {
    /**
     * Find all entities
     */
    this.init = function () {
        if (isAuthenticated()) {
            demo.toolbox();
            demo.other_models();
        }
        $("#caosdb-tour-welcome-start-btn").on("click", () => {
            $(".caosdb-f-start-tour-btn").click();
        });
        // add username and password to input form
        $("#caosdb-f-login-form").find("#username").val("admin");
        $("#caosdb-f-login-form").find("#password").val("caosdb");

    }

    this.toolbox = function () {

        var init = function () {

            const script = "crawl.py"
            const button_name = "Trigger Crawler";
            const title = "The LinkAhead-Crawler will run over the filesystem and make necessary updates.";

            const crawler_form = make_scripting_caller_form(
                script, button_name);
            const modal = make_form_modal(crawler_form);

            navbar.add_tool(button_name, "Curator Tools", {
                title: title,
                callback: () => {
                    $(modal).modal("toggle");
                }
            });

            // Add the button to the user administration
            navbar.add_button($('<a class="nav-link" href="/webinterface/acm/">User Administration</a>')[0], {
                title: "Go to user administration"
            });
        }

        var make_form_modal = function (form) {
            const title = "Trigger the Crawler";
            const modal = $(`
              <div class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">${title}</h4>
                      <button type="button"
                        class="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close">
                      </button>
                    </div>
                    <div class="modal-body">
                    </div>
                  </div>
                </div>`);
            modal.find(".modal-body").append(form);
            return modal[0];
        }

        var make_scripting_caller_form = function (script, button_name) {
            const path_field = form_elements.make_text_input({
                name: "-p1",
                value: "/Analyses",
                label: "Path"
            });
            const warning_checkbox = form_elements.make_checkbox_input({
                name: "-Osuppress",
                label: "Suppress Warnings"
            });
            $(warning_checkbox).find("input").attr("value", "TRUE");

            const scripting_caller = $(`
            <form method="POST" action="/scripting">
              <input type="hidden" name="call" value="${script}"/>
              <input type="hidden" name="-p0" value=""/>
              <div class="form-group">
                <input type="submit"
                  class="form-control btn btn-primary" value="${button_name}"/>
              </div>
            </form>`);

            scripting_caller.prepend(warning_checkbox).prepend(path_field);

            return scripting_caller[0];
        }

        init()

    }

    this.other_models = function () {

        var init = function () {

            // bicycle data model
            insert_script_button(
                "change_bikes.py",
                "Update bicycle data",
                "Change the datamodel to include e-bikes and insert several e-bike records.",
                "Update bicycle data",
                "Note that this will temporarily change the bicycle data model. The changes will be reverted automatically after 10 minutes."
            );

            // biopolymer data model
            insert_script_button(
                "change_biopolymers.py",
                "Update biopolymer measurements",
                "Reflect an improvement to the measurement setup and change the data accordingly.",
                "Insert improved tensile analyses",
                "Note that this will temporarily change the biopolymer data model. The changes will be reverted automatically after 10 minutes."
            );
        }

        var insert_script_button = function (script, button_name, hover_title, form_title, form_text) {

            const scripting_form = make_scripting_caller_form(script, button_name);

            const form_modal = make_form_modal(form_title, form_text, scripting_form);

            navbar.add_tool(
                button_name, "Other examples", {
                    title: hover_title,
                    callback: () => {
                        $(form_modal).modal("toggle");
                    }
                }
            );

        }

        var make_form_modal = function (form_title, form_text, scripting_form) {

            const modal = $(`<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">${form_title}</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <p>${form_text}</p>
            </div>
        </div>
    </div>
</div>`);

            modal.find(".modal-body").append(scripting_form);

            return modal[0];

        }

        var make_scripting_caller_form = function (script, button_name) {

            const scripting_caller = $(`<form method=POST action="/scripting">
    <input type="hidden" name="call" value="${script}"/>
    <input type="hidden" name="-p0" value=""/>
    <div class="form-group">
        <input type="submit" class="form-control btn btn-primary" value="${button_name}"/>
    </div>
</form>`);

            return scripting_caller[0];

        }

        init();

    }
}

/**
 * Add the extensions to the webui.
 */
$(document).ready(function () {
    demo.init();
});
