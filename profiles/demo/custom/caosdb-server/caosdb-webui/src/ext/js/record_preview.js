/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Florian Spreckelsen
 * <f.spreckelsen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

/**
 * Create preview plot if entity is IceCore or SnowCore.
 */
var record_preview = function($, logger, plotly_preview, connection, getEntityID, getEntityRole) {

    var preview_plot = async function(entity) {

	const script = "showcase.py";
	const eid = getEntityID(entity)

	// empty first argument because of caosadvancedtool parser
	const script_result =
	      await connection.runScript(script, {"-p0": eid});

	const code = script_result.getElementsByTagName("script")[0].getAttribute("code");
	if (parseInt(code) > 0) {

	    throw ("An error occured during creation of the preview plot:\n" +
		   script_result.getElementsByTagName("stderr")[0]);

	} else {

	    const filename = script_result.getElementsByTagName("stdout")[0].textContent;

	    if (filename.length == 0) {

		throw ("The preview script did not return a valid filename:\n" +
		       script_result.getElementsByTagName("script")[0].outerHTML);

	    } else {

		// do the actual plotting
		const uri = connection.getBasePath() + "Shared/" + filename;
		const plot_info = await $.ajax({

		    url: uri,
		    dataType: "json",

		});

		return plotly_preview.create_plot(plot_info["data"], plot_info["layout"], plot_info["settings"]);

	    }

	}
	
    };
    const init = function() {
    };

    return {
	
	init: init,
	preview_plot: preview_plot,
	
    };

}($, log.getLogger("record_preview"), plotly_preview, connection, getEntityID, getEntityRole);

$(document).ready(function(){
    caosdb_modules.register(record_preview)
});
