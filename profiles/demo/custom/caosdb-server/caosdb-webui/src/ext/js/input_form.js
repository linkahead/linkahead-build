/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Florian Spreckelsen
 * <f.spreckelsen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

"use strict";

/*
 * Define the parent object that will contain the input form and the
 * button to trigger it that will be appended onto the CaosDB main
 * panel in the webinterface
 */
var instrument_input_form = new function () {

    this.init = function () {

        // only show the button and the form to logged in users
        if (isAuthenticated()) {

            instrument_input_form.create_form_button();

        }

    }

    this.create_form_button = function () {

        var init = function () {

            const scriptname = "insert_instrument.py";
            const button_name = "Insert new instrument";
            const hover = "Open a form to insert a new instrument into the database.";


            const insertion_form = make_insertion_form(scriptname);

            const modal = make_insertion_modal(insertion_form);

            const button_element = $('<button class="btn-manual-insert">' + button_name + '</button>');
            navbar.add_tool(
                button_element[0],
                "Curator Tools", {
                    title: hover,
                    callback: () => {
                        $(modal).modal("toggle");
                    }
                }
            );
        }

        var make_insertion_modal = function (form) {

            const title = "Insert an instrument";
            const modal = $(`<div class="modal fade" tabindex="-1" role="dialog" id="insertion-form-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">${title}</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <p>Choose the type and enter the name of the instrument you want to insert. Enter the price and either choose from the existing manufacturers or enter a new one.</p>
            </div>
        </div>
    </div>
</div>`);

            modal.find(".modal-body").append(form);

            // don't show the cancel button of the form in this example
            modal.find(".caosdb-f-form-elements-cancel-button").hide();

            // Rename the submit button
            modal.find(".caosdb-f-form-elements-submit-button").text("Insert instrument");

            return modal[0];
        }

        var make_insertion_form = function (scriptname) {

            const form_config = {
                script: scriptname,
                fields: [{
                    name: "instrument_type",
                    label: "Instrument type",
                    type: "reference_drop_down",
                    required: true,
                    multiple: false,
                    // exclude the abstract musical instrument type
                    query: "FIND RECORDTYPE MusicalInstrument WHICH HAS NOT name=MusicalInstrument",
                    make_desc: getEntityName
                }, {
                    name: "name",
                    label: "Name of the instrument",
                    type: "text",
                    required: true,
                }, {
                    name: "price",
                    label: "Price",
                    type: "double",
                    required: false
                }, {
                    name: "manufacturer_id",
                    label: "Choose a manufacturer...",
                    type: "reference_drop_down",
                    required: false,
                    multiple: false,
                    query: "FIND RECORD Manufacturer",
                    make_desc: getEntityName
                }, {
                    name: "new_manufacturer",
                    label: "...or enter a new one.",
                    type: "text",
                    required: false
                }],
                proceed_text: "Insert instrument"
            };

            const form = form_elements.make_form(form_config);

            return form;
        }

        init();
    }
}

/*
 * Finally, add the from button to the main panel when the page is shown
 */
$(document).ready(function () {
    instrument_input_form.init();
});