#!/usr/bin/env python3
"""Initialize the demo server with content.
"""

import caosdb

# TODO is this the correct file?
CERT_PATH = "/opt/caosdb/cert/caosdb.cert.pem"


def main():
    """ Main method """
    print("Initializing demo server")


if __name__ == '__main__':
    main()
