#!/bin/bash

# Start this script with:
# docker exec -ti -w /opt/caosdb/git/caosdb-server/startup \
#   compose_caosdb-server_1 ./init_demo.sh

# Install pylib
git clone https://gitlab.gwdg.de/bmp-caosdb/caosdb-pylib.git
pushd caosdb-pylib
pip3 install --user .
popd

echo -e "\n"

# Start setup script
python3 ./init_demo.py

