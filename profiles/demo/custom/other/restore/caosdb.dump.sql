-- MariaDB dump 10.19  Distrib 10.5.12-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: sqldb    Database: caosdb
-- ------------------------------------------------------
-- Server version	10.5.10-MariaDB-1:10.5.10+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `archive_collection_type`
--

DROP TABLE IF EXISTS `archive_collection_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_collection_type` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `collection` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  UNIQUE KEY `archive_collection_type-d-e-p-v` (`domain_id`,`entity_id`,`property_id`,`_iversion`),
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_collection_type_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_collection_type_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_collection_type_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_collection_type`
--

LOCK TABLES `archive_collection_type` WRITE;
/*!40000 ALTER TABLE `archive_collection_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_collection_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_data_type`
--

DROP TABLE IF EXISTS `archive_data_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_data_type` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `datatype` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  UNIQUE KEY `archive_data_type-d-e-p-v` (`domain_id`,`entity_id`,`property_id`,`_iversion`),
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  KEY `datatype` (`datatype`),
  CONSTRAINT `archive_data_type_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_data_type_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_data_type_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_data_type_ibfk_4` FOREIGN KEY (`datatype`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_data_type`
--

LOCK TABLES `archive_data_type` WRITE;
/*!40000 ALTER TABLE `archive_data_type` DISABLE KEYS */;
INSERT INTO `archive_data_type` VALUES (0,0,129,13,1),(0,0,130,13,1),(0,120,113,107,1),(0,146,113,107,1),(0,112,114,108,1),(0,112,114,108,2),(0,112,114,108,3),(0,112,114,108,4),(0,123,113,108,1),(0,123,113,108,2),(0,123,113,108,3),(0,123,113,108,4),(0,123,113,108,5),(0,123,113,108,6),(0,123,113,108,7),(0,123,113,108,8),(0,124,113,108,1),(0,124,113,108,2),(0,124,113,108,3),(0,124,113,108,4),(0,124,113,108,5),(0,124,113,108,6),(0,124,113,108,7),(0,124,113,108,8),(0,124,113,108,9),(0,124,113,108,10),(0,125,113,108,1),(0,125,113,108,2),(0,125,113,108,3),(0,125,113,108,4),(0,125,113,108,5),(0,125,113,108,6),(0,125,113,108,7),(0,125,113,108,8),(0,359,113,108,2),(0,364,113,108,2),(0,112,113,111,1),(0,112,113,111,2),(0,112,113,111,3),(0,112,113,111,4),(0,123,114,111,1),(0,123,114,111,2),(0,123,114,111,3),(0,123,114,111,4),(0,123,114,111,5),(0,123,114,111,6),(0,123,114,111,7),(0,123,114,111,8),(0,124,114,111,1),(0,124,114,111,2),(0,124,114,111,3),(0,124,114,111,4),(0,124,114,111,5),(0,124,114,111,6),(0,124,114,111,7),(0,124,114,111,8),(0,124,114,111,9),(0,124,114,111,10),(0,125,114,111,1),(0,125,114,111,2),(0,125,114,111,3),(0,125,114,111,4),(0,125,114,111,5),(0,125,114,111,6),(0,125,114,111,7),(0,125,114,111,8),(0,115,113,131,2),(0,115,113,131,3),(0,115,114,131,3),(0,146,114,131,1),(0,123,138,136,2),(0,123,138,136,3),(0,123,138,136,4),(0,123,138,136,5),(0,123,138,136,6),(0,123,138,136,7),(0,123,138,136,8),(0,124,138,136,2),(0,124,138,136,3),(0,124,138,136,4),(0,124,138,136,5),(0,124,138,136,6),(0,124,138,136,7),(0,124,138,136,8),(0,124,138,136,9),(0,124,138,136,10),(0,125,138,136,2),(0,125,138,136,3),(0,125,138,136,4),(0,125,138,136,5),(0,125,138,136,6),(0,125,138,136,7),(0,125,138,136,8),(0,359,114,136,2),(0,364,114,136,2),(0,123,143,141,3),(0,123,143,141,4),(0,123,143,141,5),(0,123,143,141,6),(0,123,143,141,7),(0,123,143,141,8),(0,124,143,141,3),(0,124,143,141,4),(0,124,143,141,5),(0,124,143,141,6),(0,124,143,141,7),(0,124,143,141,8),(0,124,143,141,9),(0,124,143,141,10),(0,125,143,141,3),(0,125,143,141,4),(0,125,143,141,5),(0,125,143,141,6),(0,125,143,141,7),(0,125,143,141,8),(0,359,138,141,2),(0,364,138,141,2);
/*!40000 ALTER TABLE `archive_data_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_date_data`
--

DROP TABLE IF EXISTS `archive_date_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_date_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` int(11) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_date_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_date_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_date_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_date_data`
--

LOCK TABLES `archive_date_data` WRITE;
/*!40000 ALTER TABLE `archive_date_data` DISABLE KEYS */;
INSERT INTO `archive_date_data` VALUES (0,123,102,20190516,'FIX',3,1),(0,124,102,20190724,'FIX',3,1),(0,125,102,20190909,'FIX',3,1),(0,125,102,20190909,'FIX',3,2),(0,123,102,20190516,'FIX',3,2),(0,124,102,20190724,'FIX',3,2),(0,123,102,20190516,'FIX',3,3),(0,124,102,20190724,'FIX',3,3),(0,125,102,20190909,'FIX',3,3),(0,123,102,20190516,'FIX',3,4),(0,124,102,20190724,'FIX',3,4),(0,125,102,20190909,'FIX',3,4),(0,123,102,20190516,'FIX',3,5),(0,124,102,20190724,'FIX',3,5),(0,124,102,20190724,'FIX',3,6),(0,125,102,20190909,'FIX',3,5),(0,123,102,20190516,'FIX',3,6),(0,123,102,20190516,'FIX',3,7),(0,124,102,20190724,'FIX',3,7),(0,125,102,20190909,'FIX',3,6),(0,125,102,20190909,'FIX',3,7),(0,125,102,20190909,'FIX',3,8),(0,124,102,20190724,'FIX',3,8),(0,123,102,20190516,'FIX',3,8),(0,124,102,20190724,'FIX',3,9),(0,124,102,20190724,'FIX',3,10),(0,359,102,20190520,'FIX',2,1),(0,359,102,20190520,'FIX',2,2),(0,364,102,20190724,'FIX',2,1),(0,364,102,20190724,'FIX',2,2);
/*!40000 ALTER TABLE `archive_date_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_datetime_data`
--

DROP TABLE IF EXISTS `archive_datetime_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_datetime_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` bigint(20) NOT NULL,
  `value_ns` int(10) unsigned DEFAULT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_datetime_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_datetime_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_datetime_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_datetime_data`
--

LOCK TABLES `archive_datetime_data` WRITE;
/*!40000 ALTER TABLE `archive_datetime_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_datetime_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_desc_overrides`
--

DROP TABLE IF EXISTS `archive_desc_overrides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_desc_overrides` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  UNIQUE KEY `archive_desc_overrides-d-e-p-v` (`domain_id`,`entity_id`,`property_id`,`_iversion`),
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_desc_overrides_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_desc_overrides_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_desc_overrides_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_desc_overrides`
--

LOCK TABLES `archive_desc_overrides` WRITE;
/*!40000 ALTER TABLE `archive_desc_overrides` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_desc_overrides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_double_data`
--

DROP TABLE IF EXISTS `archive_double_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_double_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` double NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  `unit_sig` bigint(20) DEFAULT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_double_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_double_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_double_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_double_data`
--

LOCK TABLES `archive_double_data` WRITE;
/*!40000 ALTER TABLE `archive_double_data` DISABLE KEYS */;
INSERT INTO `archive_double_data` VALUES (0,115,100,48,'FIX',0,1,-921068351),(0,115,100,48,'FIX',0,2,-921068351),(0,115,100,48,'FIX',0,3,-921068351),(0,120,100,814873,'FIX',0,1,-921068351),(0,123,101,0.08,'FIX',2,1,NULL),(0,124,101,0.97,'FIX',2,1,NULL),(0,125,101,0.84,'FIX',2,1,NULL),(0,125,101,0.84,'FIX',2,2,NULL),(0,123,101,0.08,'FIX',2,2,NULL),(0,124,101,0.97,'FIX',2,2,NULL),(0,146,100,161,'FIX',1,1,-921068351),(0,148,100,3939.76,'FIX',1,1,-921068351),(0,149,100,3481.16,'FIX',1,1,-921068351),(0,150,100,3237.96,'FIX',1,1,-921068351),(0,151,100,3136,'FIX',1,1,-921068351),(0,152,100,463,'FIX',1,1,-921068351),(0,153,100,4894,'FIX',1,1,-921068351),(0,123,101,0.08,'FIX',2,3,NULL),(0,124,101,0.97,'FIX',2,3,NULL),(0,125,101,0.84,'FIX',2,3,NULL),(0,123,101,0.08,'FIX',2,4,NULL),(0,124,101,0.97,'FIX',2,4,NULL),(0,125,101,0.84,'FIX',2,4,NULL),(0,123,101,0.08,'FIX',2,5,NULL),(0,124,101,0.97,'FIX',2,5,NULL),(0,124,101,0.97,'FIX',2,6,NULL),(0,125,101,0.84,'FIX',2,5,NULL),(0,123,101,0.08,'FIX',2,6,NULL),(0,123,101,0.08,'FIX',2,7,NULL),(0,124,101,0.97,'FIX',2,7,NULL),(0,125,101,0.84,'FIX',2,6,NULL),(0,125,101,0.84,'FIX',2,7,NULL),(0,125,101,0.84,'FIX',2,8,NULL),(0,124,101,0.97,'FIX',2,8,NULL),(0,123,101,0.08,'FIX',2,8,NULL),(0,124,101,0.97,'FIX',2,9,NULL),(0,124,101,0.97,'FIX',2,10,NULL),(0,359,101,0.3,'FIX',1,1,NULL),(0,359,101,0.3,'FIX',1,2,NULL),(0,364,101,0.3,'FIX',1,1,NULL),(0,364,101,0.3,'FIX',1,2,NULL);
/*!40000 ALTER TABLE `archive_double_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_entities`
--

DROP TABLE IF EXISTS `archive_entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_entities` (
  `id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` enum('RECORDTYPE','RECORD','FILE','DOMAIN','PROPERTY','DATATYPE','ROLE','QUERYTEMPLATE') COLLATE utf8_unicode_ci NOT NULL,
  `acl` int(10) unsigned DEFAULT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`_iversion`),
  KEY `acl` (`acl`),
  CONSTRAINT `archive_entities_ibfk_1` FOREIGN KEY (`id`, `_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE,
  CONSTRAINT `archive_entities_ibfk_2` FOREIGN KEY (`acl`) REFERENCES `entity_acl` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_entities`
--

LOCK TABLES `archive_entities` WRITE;
/*!40000 ALTER TABLE `archive_entities` DISABLE KEYS */;
INSERT INTO `archive_entities` VALUES (107,NULL,'RECORDTYPE',2,1),(109,NULL,'RECORDTYPE',2,1),(110,NULL,'RECORDTYPE',2,1),(111,NULL,'RECORDTYPE',2,1),(112,NULL,'RECORDTYPE',2,1),(112,NULL,'RECORDTYPE',2,2),(112,NULL,'RECORDTYPE',11,3),(112,NULL,'RECORDTYPE',11,4),(115,NULL,'RECORD',2,1),(115,NULL,'RECORD',2,2),(115,NULL,'RECORD',2,3),(116,NULL,'RECORD',2,1),(119,NULL,'RECORD',2,1),(120,NULL,'RECORD',2,1),(123,NULL,'RECORD',2,1),(123,NULL,'RECORD',2,2),(123,NULL,'RECORD',2,3),(123,NULL,'RECORD',2,4),(123,NULL,'RECORD',2,5),(123,NULL,'RECORD',2,6),(123,NULL,'RECORD',2,7),(123,NULL,'RECORD',7,8),(124,NULL,'RECORD',2,1),(124,NULL,'RECORD',2,2),(124,NULL,'RECORD',2,3),(124,NULL,'RECORD',2,4),(124,NULL,'RECORD',2,5),(124,NULL,'RECORD',2,6),(124,NULL,'RECORD',2,7),(124,NULL,'RECORD',7,8),(124,NULL,'RECORD',8,9),(124,NULL,'RECORD',10,10),(125,NULL,'RECORD',2,1),(125,NULL,'RECORD',2,2),(125,NULL,'RECORD',2,3),(125,NULL,'RECORD',2,4),(125,NULL,'RECORD',2,5),(125,NULL,'RECORD',2,6),(125,NULL,'RECORD',7,7),(125,NULL,'RECORD',8,8),(129,'a geographic coordinate','PROPERTY',2,1),(130,'a geographic coordinate','PROPERTY',2,1),(132,NULL,'FILE',2,1),(132,NULL,'FILE',2,2),(134,NULL,'FILE',2,1),(135,NULL,'FILE',2,1),(141,NULL,'RECORDTYPE',2,1),(146,'The Gibson J-160E was used by John Lennon of The Beatles','RECORD',2,1),(148,NULL,'RECORD',2,1),(149,NULL,'RECORD',2,1),(150,NULL,'RECORD',2,1),(151,NULL,'RECORD',2,1),(152,NULL,'RECORD',2,1),(153,NULL,'RECORD',2,1),(154,NULL,'RECORD',2,1),(155,NULL,'RECORD',2,1),(156,NULL,'RECORD',2,1),(157,NULL,'RECORD',2,1),(173,NULL,'FILE',2,1),(174,NULL,'FILE',2,1),(174,NULL,'FILE',2,2),(177,NULL,'FILE',2,1),(179,NULL,'FILE',2,1),(179,NULL,'FILE',2,2),(182,NULL,'FILE',2,1),(183,NULL,'FILE',2,1),(183,NULL,'FILE',2,2),(184,NULL,'FILE',2,1),(189,NULL,'FILE',2,1),(193,NULL,'FILE',2,1),(197,NULL,'FILE',2,1),(201,NULL,'FILE',2,1),(205,NULL,'FILE',2,1),(209,NULL,'FILE',2,1),(221,'Unpublished entries are only visible to the team and may be edited by any team member.','RECORD',4,1),(222,'Entries under review are not publicly available yet, but they can only be edited by the members of the publisher group.','RECORD',5,1),(313,NULL,'RECORD',2,1),(314,NULL,'RECORD',2,1),(315,NULL,'RECORD',2,1),(359,NULL,'RECORD',10,1),(359,NULL,'RECORD',10,2),(364,NULL,'RECORD',10,1),(364,NULL,'RECORD',10,2);
/*!40000 ALTER TABLE `archive_entities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_enum_data`
--

DROP TABLE IF EXISTS `archive_enum_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_enum_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` varbinary(255) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_enum_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_enum_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_enum_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_enum_data`
--

LOCK TABLES `archive_enum_data` WRITE;
/*!40000 ALTER TABLE `archive_enum_data` DISABLE KEYS */;
INSERT INTO `archive_enum_data` VALUES (0,115,106,'FALSE','FIX',1,1),(0,115,106,'FALSE','FIX',1,2),(0,115,106,'FALSE','FIX',1,3),(0,146,106,'TRUE','FIX',0,1),(0,148,147,'TRUE','FIX',0,1),(0,149,147,'TRUE','FIX',0,1),(0,150,147,'TRUE','FIX',0,1),(0,151,147,'TRUE','FIX',0,1),(0,151,106,'FALSE','FIX',2,1),(0,152,147,'TRUE','FIX',0,1),(0,152,106,'FALSE','FIX',2,1),(0,153,147,'TRUE','FIX',0,1),(0,153,106,'FALSE','FIX',2,1),(0,154,147,'TRUE','FIX',0,1),(0,155,147,'TRUE','FIX',0,1),(0,156,147,'TRUE','FIX',0,1),(0,157,147,'TRUE','FIX',0,1);
/*!40000 ALTER TABLE `archive_enum_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_files`
--

DROP TABLE IF EXISTS `archive_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_files` (
  `file_id` int(10) unsigned NOT NULL,
  `path` text COLLATE utf8_unicode_ci NOT NULL,
  `size` bigint(20) unsigned NOT NULL,
  `hash` binary(64) DEFAULT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  PRIMARY KEY (`file_id`,`_iversion`),
  CONSTRAINT `archive_files_ibfk_1` FOREIGN KEY (`file_id`, `_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_files`
--

LOCK TABLES `archive_files` WRITE;
/*!40000 ALTER TABLE `archive_files` DISABLE KEYS */;
INSERT INTO `archive_files` VALUES (132,'uploaded.by/PAM/admin/608cd1c3-fb9c-464c-818f-396e1e461cc3/guitar2.png',30874,'�XM�f;G�?}�$��*�b��R�������	�=��zp��k����k$2� �U�x渞���',1),(132,'images/john_lennon_guitar.png',30874,'�XM�f;G�?}�$��*�b��R�������	�=��zp��k����k$2� �U�x渞���',2),(134,'uploaded.by/PAM/admin/608cd1c3-fb9c-464c-818f-396e1e461cc3/violin2.png',24880,'#����Zg��	AG�r⨩��7̒-U�1�̴�S���<���^}��ߍf��F���j$w�~',1),(135,'uploaded.by/PAM/admin/608cd1c3-fb9c-464c-818f-396e1e461cc3/guitar1.png',30422,'�+�A\Z��aT�mf	�,��~�S4	�)�yD�n�>+<c�գ��n�b%�����Ã6 ��)�>',1),(173,'Analyses/2019-05-16_023/settings.xlsx',4939,'�Ҵ�������������T��=W�Մ�@�T���Q����o]��I��W��v	\r���itj�P',1),(174,'Analyses/2019-05-16_023/mode.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',1),(174,'Analyses/2019-05-16_023/mode.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',2),(177,'Analyses/2019-07-24_024/settings.csv',89,'�EC��!�Lqsr�Rc�[i�H0j��o��uD�t�2�D������(O�Kͺ�v��\nsf�n�̰�',1),(179,'Analyses/2019-07-24_024/mode3.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',1),(179,'Analyses/2019-07-24_024/mode3.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',2),(182,'Analyses/2019-09-09_025/settings2.csv',90,'vhM�;��rP��yw�EC_x��J0黈�i>=��@Nd_ٝ\r+>�\n���	�?�UW����\\',1),(183,'Analyses/2019-09-09_025/mode2.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',1),(183,'Analyses/2019-09-09_025/mode2.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',2),(184,'Analyses/2021-03-16_003/mode.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',1),(189,'Analyses/2021-03-22_004/mode.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',1),(193,'Analyses/2021-03-21_006/mode.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',1),(197,'Analyses/2021-03-20_007/mode.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',1),(201,'Analyses/2021-03-19_009/mode.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',1),(205,'Analyses/2021-03-20_012/mode.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',1),(209,'Analyses/2021-03-16_019/mode.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',1);
/*!40000 ALTER TABLE `archive_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_integer_data`
--

DROP TABLE IF EXISTS `archive_integer_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_integer_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` bigint(20) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  `unit_sig` bigint(20) DEFAULT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_integer_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_integer_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_integer_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_integer_data`
--

LOCK TABLES `archive_integer_data` WRITE;
/*!40000 ALTER TABLE `archive_integer_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_integer_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_isa`
--

DROP TABLE IF EXISTS `archive_isa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_isa` (
  `child` int(10) unsigned NOT NULL,
  `child_iversion` int(10) unsigned NOT NULL,
  `parent` int(10) unsigned NOT NULL,
  `direct` tinyint(1) DEFAULT 1,
  KEY `parent` (`parent`),
  KEY `child` (`child`,`child_iversion`),
  CONSTRAINT `archive_isa_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_isa_ibfk_2` FOREIGN KEY (`child`, `child_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_isa`
--

LOCK TABLES `archive_isa` WRITE;
/*!40000 ALTER TABLE `archive_isa` DISABLE KEYS */;
INSERT INTO `archive_isa` VALUES (119,1,107,1),(116,1,107,1),(115,1,108,0),(115,1,110,1),(115,2,108,0),(115,2,110,1),(115,3,108,0),(115,3,110,1),(120,1,108,0),(120,1,109,1),(123,1,112,1),(124,1,112,1),(125,1,112,1),(125,2,112,1),(123,2,112,1),(124,2,112,1),(146,1,108,0),(146,1,110,1),(148,1,108,0),(148,1,109,1),(149,1,108,0),(149,1,109,1),(150,1,108,0),(150,1,109,1),(151,1,108,0),(151,1,110,1),(152,1,108,0),(152,1,110,1),(153,1,108,0),(153,1,110,1),(154,1,111,1),(155,1,111,1),(156,1,111,1),(157,1,111,1),(123,3,112,1),(124,3,112,1),(125,3,112,1),(123,4,112,1),(124,4,112,1),(125,4,112,1),(123,5,112,1),(124,5,112,1),(124,6,112,1),(125,5,112,1),(123,6,112,1),(109,1,108,1),(110,1,108,1),(123,7,112,1),(124,7,112,1),(125,6,112,1),(125,7,112,1),(125,8,112,1),(124,8,112,1),(132,1,131,1),(132,2,131,1),(135,1,131,1),(134,1,131,1),(222,1,213,1),(221,1,213,1),(123,8,112,1),(124,9,112,1),(124,10,112,1),(313,1,253,1),(314,1,253,1),(315,1,253,1),(359,1,112,1),(359,2,112,1),(364,1,112,1),(364,2,112,1),(174,2,141,1),(179,2,141,1),(183,2,141,1);
/*!40000 ALTER TABLE `archive_isa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_name_data`
--

DROP TABLE IF EXISTS `archive_name_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_name_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `value` (`value`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_name_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_name_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_name_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_name_data`
--

LOCK TABLES `archive_name_data` WRITE;
/*!40000 ALTER TABLE `archive_name_data` DISABLE KEYS */;
INSERT INTO `archive_name_data` VALUES (0,119,20,'Antonio Stradivari','FIX',0,1),(0,116,20,'Gibson','FIX',0,1),(0,115,20,'My first guitar','FIX',0,1),(0,115,20,'My first guitar','FIX',0,2),(0,115,20,'My first guitar','FIX',0,3),(0,120,20,'Sherlock Holmes\' violin','FIX',0,1),(0,123,20,'G / 2019-023','FIX',0,1),(0,124,20,'G / 2019-024','FIX',0,1),(0,125,20,'V / 2019-025','FIX',0,1),(0,125,20,'V / 2019-025','FIX',0,2),(0,123,20,'G / 2019-023','FIX',0,2),(0,124,20,'G / 2019-024','FIX',0,2),(0,146,20,'John Lennon\'s guitar','FIX',0,1),(0,148,20,'Susan\'s Violin','FIX',0,1),(0,149,20,'Linda\'s Violin','FIX',0,1),(0,150,20,'Michael\'s Violin','FIX',0,1),(0,151,20,'James\'s Guitar','FIX',0,1),(0,152,20,'David\'s Guitar','FIX',0,1),(0,153,20,'Richard\'s Guitar','FIX',0,1),(0,154,20,'Non-destructive Harmonizer 3400','FIX',0,1),(0,155,20,'Holographic Stringalyzer alpha','FIX',0,1),(0,156,20,'Inexplicable SymphiTec 5','FIX',0,1),(0,157,20,'Relativistic MusiCorp 5','FIX',0,1),(0,123,20,'G / 2019-023','FIX',0,3),(0,124,20,'G / 2019-024','FIX',0,3),(0,125,20,'V / 2019-025','FIX',0,3),(0,123,20,'023','FIX',0,4),(0,124,20,'024','FIX',0,4),(0,125,20,'025','FIX',0,4),(0,174,20,'mode.npy','FIX',0,1),(0,173,20,'settings.xlsx','FIX',0,1),(0,179,20,'mode3.npy','FIX',0,1),(0,177,20,'settings.csv','FIX',0,1),(0,183,20,'mode2.npy','FIX',0,1),(0,182,20,'settings2.csv','FIX',0,1),(0,109,20,'Violin','FIX',0,1),(0,110,20,'Guitar','FIX',0,1),(0,111,20,'SoundQualityAnalyzer','FIX',0,1),(0,141,20,'RiffRecording','FIX',0,1),(0,112,20,'Analysis','FIX',0,1),(0,132,20,'guitar2.png','FIX',0,1),(0,132,20,'john_lennon_guitar.png','FIX',0,2),(0,135,20,'guitar1.png','FIX',0,1),(0,134,20,'violin2.png','FIX',0,1),(0,222,20,'Under Review','FIX',0,1),(0,221,20,'Unpublished','FIX',0,1),(0,112,20,'Analysis','FIX',0,2),(0,112,20,'Analysis','FIX',0,3),(0,112,20,'MusicalAnalysis','FIX',0,4),(0,107,20,'Manufacturer','FIX',0,1),(0,130,20,'latitude','FIX',0,1),(0,129,20,'longitude','FIX',0,1),(0,313,20,'Children\'s bike','FIX',0,1),(0,314,20,'Nice trekking bike','FIX',0,1),(0,315,20,'Lightweight MTB','FIX',0,1),(0,174,20,'mode.npy','FIX',0,2),(0,179,20,'mode3.npy','FIX',0,2),(0,183,20,'mode2.npy','FIX',0,2),(0,184,20,'mode.npy','FIX',0,1),(0,189,20,'mode.npy','FIX',0,1),(0,193,20,'mode.npy','FIX',0,1),(0,197,20,'mode.npy','FIX',0,1),(0,201,20,'mode.npy','FIX',0,1),(0,205,20,'mode.npy','FIX',0,1),(0,209,20,'mode.npy','FIX',0,1);
/*!40000 ALTER TABLE `archive_name_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_name_overrides`
--

DROP TABLE IF EXISTS `archive_name_overrides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_name_overrides` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  UNIQUE KEY `archive_name_overrides-d-e-p-v` (`domain_id`,`entity_id`,`property_id`,`_iversion`),
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_name_overrides_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_name_overrides_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_name_overrides_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_name_overrides`
--

LOCK TABLES `archive_name_overrides` WRITE;
/*!40000 ALTER TABLE `archive_name_overrides` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_name_overrides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_null_data`
--

DROP TABLE IF EXISTS `archive_null_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_null_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_null_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_null_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_null_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_null_data`
--

LOCK TABLES `archive_null_data` WRITE;
/*!40000 ALTER TABLE `archive_null_data` DISABLE KEYS */;
INSERT INTO `archive_null_data` VALUES (0,110,106,'RECOMMENDED',0,1),(0,112,101,'RECOMMENDED',0,1),(0,112,102,'RECOMMENDED',1,1),(0,112,105,'RECOMMENDED',2,1),(0,112,113,'RECOMMENDED',0,1),(0,112,114,'RECOMMENDED',0,1),(0,112,101,'RECOMMENDED',0,2),(0,112,102,'RECOMMENDED',1,2),(0,112,105,'RECOMMENDED',2,2),(0,112,113,'RECOMMENDED',0,2),(0,112,114,'RECOMMENDED',0,2),(0,112,101,'RECOMMENDED',0,3),(0,112,102,'RECOMMENDED',1,3),(0,112,105,'RECOMMENDED',2,3),(0,112,113,'RECOMMENDED',0,3),(0,112,114,'RECOMMENDED',0,3),(0,112,101,'RECOMMENDED',0,4),(0,112,102,'RECOMMENDED',1,4),(0,112,105,'RECOMMENDED',2,4),(0,112,113,'RECOMMENDED',0,4),(0,112,114,'RECOMMENDED',0,4);
/*!40000 ALTER TABLE `archive_null_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_query_template_def`
--

DROP TABLE IF EXISTS `archive_query_template_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_query_template_def` (
  `id` int(10) unsigned NOT NULL,
  `definition` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`_iversion`),
  CONSTRAINT `archive_query_template_def_ibfk_1` FOREIGN KEY (`id`, `_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_query_template_def`
--

LOCK TABLES `archive_query_template_def` WRITE;
/*!40000 ALTER TABLE `archive_query_template_def` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_query_template_def` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_reference_data`
--

DROP TABLE IF EXISTS `archive_reference_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_reference_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` int(10) unsigned NOT NULL,
  `value_iversion` int(10) unsigned DEFAULT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  KEY `value` (`value`),
  CONSTRAINT `archive_reference_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_reference_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_reference_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_reference_data_ibfk_4` FOREIGN KEY (`value`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_reference_data`
--

LOCK TABLES `archive_reference_data` WRITE;
/*!40000 ALTER TABLE `archive_reference_data` DISABLE KEYS */;
INSERT INTO `archive_reference_data` VALUES (0,115,131,113,NULL,'REPLACEMENT',2,2),(0,115,113,132,NULL,'FIX',0,2),(0,115,131,113,NULL,'REPLACEMENT',2,3),(0,115,113,132,NULL,'FIX',0,3),(0,115,131,114,NULL,'REPLACEMENT',3,3),(0,120,107,113,NULL,'REPLACEMENT',1,1),(0,120,113,119,NULL,'FIX',0,1),(0,123,108,113,NULL,'REPLACEMENT',0,1),(0,123,113,115,NULL,'FIX',0,1),(0,123,111,114,NULL,'REPLACEMENT',1,1),(0,123,114,121,NULL,'FIX',0,1),(0,124,108,113,NULL,'REPLACEMENT',0,1),(0,124,113,117,NULL,'FIX',0,1),(0,124,111,114,NULL,'REPLACEMENT',1,1),(0,124,114,122,NULL,'FIX',0,1),(0,125,108,113,NULL,'REPLACEMENT',0,1),(0,125,113,120,NULL,'FIX',0,1),(0,125,111,114,NULL,'REPLACEMENT',1,1),(0,125,114,121,NULL,'FIX',0,1),(0,125,108,113,NULL,'REPLACEMENT',0,2),(0,125,113,120,NULL,'FIX',0,2),(0,125,111,114,NULL,'REPLACEMENT',1,2),(0,125,114,121,NULL,'FIX',0,2),(0,125,136,138,NULL,'REPLACEMENT',5,2),(0,125,138,140,NULL,'FIX',0,2),(0,123,108,113,NULL,'REPLACEMENT',0,2),(0,123,113,115,NULL,'FIX',0,2),(0,123,111,114,NULL,'REPLACEMENT',1,2),(0,123,114,121,NULL,'FIX',0,2),(0,123,136,138,NULL,'REPLACEMENT',5,2),(0,123,138,137,NULL,'FIX',0,2),(0,124,108,113,NULL,'REPLACEMENT',0,2),(0,124,113,117,NULL,'FIX',0,2),(0,124,111,114,NULL,'REPLACEMENT',1,2),(0,124,114,122,NULL,'FIX',0,2),(0,124,136,138,NULL,'REPLACEMENT',5,2),(0,124,138,139,NULL,'FIX',0,2),(0,146,107,113,NULL,'REPLACEMENT',2,1),(0,146,113,116,NULL,'FIX',0,1),(0,146,131,114,NULL,'REPLACEMENT',3,1),(0,146,114,135,NULL,'FIX',0,1),(0,123,108,113,NULL,'REPLACEMENT',0,3),(0,123,113,115,NULL,'FIX',0,3),(0,123,111,114,NULL,'REPLACEMENT',1,3),(0,123,114,121,NULL,'FIX',0,3),(0,123,136,138,NULL,'REPLACEMENT',5,3),(0,123,138,137,NULL,'FIX',0,3),(0,123,141,143,NULL,'REPLACEMENT',6,3),(0,124,108,113,NULL,'REPLACEMENT',0,3),(0,124,113,117,NULL,'FIX',0,3),(0,124,111,114,NULL,'REPLACEMENT',1,3),(0,124,114,122,NULL,'FIX',0,3),(0,124,136,138,NULL,'REPLACEMENT',5,3),(0,124,138,139,NULL,'FIX',0,3),(0,124,141,143,NULL,'REPLACEMENT',6,3),(0,125,108,113,NULL,'REPLACEMENT',0,3),(0,125,113,120,NULL,'FIX',0,3),(0,125,111,114,NULL,'REPLACEMENT',1,3),(0,125,114,121,NULL,'FIX',0,3),(0,125,136,138,NULL,'REPLACEMENT',5,3),(0,125,138,140,NULL,'FIX',0,3),(0,125,141,143,NULL,'REPLACEMENT',6,3),(0,123,108,113,NULL,'REPLACEMENT',0,4),(0,123,113,115,NULL,'FIX',0,4),(0,123,111,114,NULL,'REPLACEMENT',1,4),(0,123,114,121,NULL,'FIX',0,4),(0,123,136,138,NULL,'REPLACEMENT',5,4),(0,123,138,137,NULL,'FIX',0,4),(0,123,141,143,NULL,'REPLACEMENT',6,4),(0,124,108,113,NULL,'REPLACEMENT',0,4),(0,124,113,117,NULL,'FIX',0,4),(0,124,111,114,NULL,'REPLACEMENT',1,4),(0,124,114,122,NULL,'FIX',0,4),(0,124,136,138,NULL,'REPLACEMENT',5,4),(0,124,138,139,NULL,'FIX',0,4),(0,124,141,143,NULL,'REPLACEMENT',6,4),(0,125,108,113,NULL,'REPLACEMENT',0,4),(0,125,113,120,NULL,'FIX',0,4),(0,125,111,114,NULL,'REPLACEMENT',1,4),(0,125,114,121,NULL,'FIX',0,4),(0,125,136,138,NULL,'REPLACEMENT',5,4),(0,125,138,140,NULL,'FIX',0,4),(0,125,141,143,NULL,'REPLACEMENT',6,4),(0,123,108,113,NULL,'REPLACEMENT',0,5),(0,123,113,115,NULL,'FIX',0,5),(0,123,111,114,NULL,'REPLACEMENT',1,5),(0,123,114,121,NULL,'FIX',0,5),(0,123,136,138,NULL,'REPLACEMENT',5,5),(0,123,138,137,NULL,'FIX',0,5),(0,123,141,143,NULL,'REPLACEMENT',6,5),(0,124,108,113,NULL,'REPLACEMENT',0,5),(0,124,113,117,NULL,'FIX',0,5),(0,124,111,114,NULL,'REPLACEMENT',1,5),(0,124,114,122,NULL,'FIX',0,5),(0,124,136,138,NULL,'REPLACEMENT',5,5),(0,124,138,139,NULL,'FIX',0,5),(0,124,141,143,NULL,'REPLACEMENT',6,5),(0,124,108,113,NULL,'REPLACEMENT',0,6),(0,124,113,117,NULL,'FIX',0,6),(0,124,111,114,NULL,'REPLACEMENT',1,6),(0,124,114,122,NULL,'FIX',0,6),(0,124,105,178,NULL,'FIX',4,6),(0,124,136,138,NULL,'REPLACEMENT',5,6),(0,124,138,139,NULL,'FIX',0,6),(0,124,141,143,NULL,'REPLACEMENT',6,6),(0,124,143,179,NULL,'FIX',0,6),(0,125,108,113,NULL,'REPLACEMENT',0,5),(0,125,113,120,NULL,'FIX',0,5),(0,125,111,114,NULL,'REPLACEMENT',1,5),(0,125,114,121,NULL,'FIX',0,5),(0,125,136,138,NULL,'REPLACEMENT',5,5),(0,125,138,140,NULL,'FIX',0,5),(0,125,141,143,NULL,'REPLACEMENT',6,5),(0,123,108,113,NULL,'REPLACEMENT',0,6),(0,123,113,115,NULL,'FIX',0,6),(0,123,111,114,NULL,'REPLACEMENT',1,6),(0,123,114,121,NULL,'FIX',0,6),(0,123,105,175,NULL,'FIX',4,6),(0,123,136,138,NULL,'REPLACEMENT',5,6),(0,123,138,137,NULL,'FIX',0,6),(0,123,141,143,NULL,'REPLACEMENT',6,6),(0,123,143,174,NULL,'FIX',0,6),(0,112,111,113,NULL,'REPLACEMENT',3,1),(0,112,108,114,NULL,'REPLACEMENT',4,1),(0,123,108,113,NULL,'REPLACEMENT',0,7),(0,123,113,115,NULL,'FIX',0,7),(0,123,111,114,NULL,'REPLACEMENT',1,7),(0,123,114,121,NULL,'FIX',0,7),(0,123,105,175,NULL,'FIX',4,7),(0,123,136,138,NULL,'REPLACEMENT',5,7),(0,123,138,173,NULL,'FIX',0,7),(0,123,141,143,NULL,'REPLACEMENT',6,7),(0,123,143,174,NULL,'FIX',0,7),(0,124,108,113,NULL,'REPLACEMENT',0,7),(0,124,113,117,NULL,'FIX',0,7),(0,124,111,114,NULL,'REPLACEMENT',1,7),(0,124,114,122,NULL,'FIX',0,7),(0,124,105,178,NULL,'FIX',4,7),(0,124,136,138,NULL,'REPLACEMENT',5,7),(0,124,138,177,NULL,'FIX',0,7),(0,124,141,143,NULL,'REPLACEMENT',6,7),(0,124,143,179,NULL,'FIX',0,7),(0,125,108,113,NULL,'REPLACEMENT',0,6),(0,125,113,120,NULL,'FIX',0,6),(0,125,111,114,NULL,'REPLACEMENT',1,6),(0,125,114,121,NULL,'FIX',0,6),(0,125,105,181,NULL,'FIX',4,6),(0,125,136,138,NULL,'REPLACEMENT',5,6),(0,125,138,182,NULL,'FIX',0,6),(0,125,141,143,NULL,'REPLACEMENT',6,6),(0,125,143,183,NULL,'FIX',0,6),(0,125,108,113,NULL,'REPLACEMENT',0,7),(0,125,113,120,NULL,'FIX',0,7),(0,125,111,114,NULL,'REPLACEMENT',1,7),(0,125,114,121,NULL,'FIX',0,7),(0,125,105,181,NULL,'FIX',4,7),(0,125,136,138,NULL,'REPLACEMENT',5,7),(0,125,138,182,NULL,'FIX',0,7),(0,125,141,143,NULL,'REPLACEMENT',6,7),(0,125,143,183,NULL,'FIX',0,7),(0,125,213,221,NULL,'FIX',0,7),(0,125,108,113,NULL,'REPLACEMENT',0,8),(0,125,113,120,NULL,'FIX',0,8),(0,125,111,114,NULL,'REPLACEMENT',1,8),(0,125,114,121,NULL,'FIX',0,8),(0,125,105,181,NULL,'FIX',4,8),(0,125,136,138,NULL,'REPLACEMENT',5,8),(0,125,138,182,NULL,'FIX',0,8),(0,125,141,143,NULL,'REPLACEMENT',6,8),(0,125,143,183,NULL,'FIX',0,8),(0,125,213,222,NULL,'FIX',0,8),(0,124,108,113,NULL,'REPLACEMENT',0,8),(0,124,113,117,NULL,'FIX',0,8),(0,124,111,114,NULL,'REPLACEMENT',1,8),(0,124,114,122,NULL,'FIX',0,8),(0,124,105,178,NULL,'FIX',4,8),(0,124,136,138,NULL,'REPLACEMENT',5,8),(0,124,138,177,NULL,'FIX',0,8),(0,124,141,143,NULL,'REPLACEMENT',6,8),(0,124,143,179,NULL,'FIX',0,8),(0,124,213,221,NULL,'FIX',0,8),(0,123,108,113,NULL,'REPLACEMENT',0,8),(0,123,113,115,NULL,'FIX',0,8),(0,123,111,114,NULL,'REPLACEMENT',1,8),(0,123,114,121,NULL,'FIX',0,8),(0,123,105,175,NULL,'FIX',4,8),(0,123,136,138,NULL,'REPLACEMENT',5,8),(0,123,138,173,NULL,'FIX',0,8),(0,123,141,143,NULL,'REPLACEMENT',6,8),(0,123,143,174,NULL,'FIX',0,8),(0,123,213,221,NULL,'FIX',0,8),(0,124,108,113,NULL,'REPLACEMENT',0,9),(0,124,113,117,NULL,'FIX',0,9),(0,124,111,114,NULL,'REPLACEMENT',1,9),(0,124,114,122,NULL,'FIX',0,9),(0,124,105,178,NULL,'FIX',4,9),(0,124,136,138,NULL,'REPLACEMENT',5,9),(0,124,138,177,NULL,'FIX',0,9),(0,124,141,143,NULL,'REPLACEMENT',6,9),(0,124,143,179,NULL,'FIX',0,9),(0,124,213,222,NULL,'FIX',0,9),(0,124,108,113,NULL,'REPLACEMENT',0,10),(0,124,113,117,NULL,'FIX',0,10),(0,124,111,114,NULL,'REPLACEMENT',1,10),(0,124,114,122,NULL,'FIX',0,10),(0,124,105,178,NULL,'FIX',4,10),(0,124,136,138,NULL,'REPLACEMENT',5,10),(0,124,138,177,NULL,'FIX',0,10),(0,124,141,143,NULL,'REPLACEMENT',6,10),(0,124,143,179,NULL,'FIX',0,10),(0,124,213,221,NULL,'FIX',0,10),(0,112,111,113,NULL,'REPLACEMENT',3,2),(0,112,108,114,NULL,'REPLACEMENT',4,2),(0,112,213,221,NULL,'FIX',0,2),(0,112,111,113,NULL,'REPLACEMENT',3,3),(0,112,108,114,NULL,'REPLACEMENT',4,3),(0,112,213,221,NULL,'FIX',0,3),(0,112,111,113,NULL,'REPLACEMENT',3,4),(0,112,108,114,NULL,'REPLACEMENT',4,4),(0,112,213,221,NULL,'FIX',0,4),(0,313,246,310,NULL,'FIX',1,1),(0,313,243,308,NULL,'FIX',2,1),(0,313,244,305,NULL,'FIX',3,1),(0,314,246,311,NULL,'FIX',1,1),(0,314,243,308,NULL,'FIX',2,1),(0,314,244,306,NULL,'FIX',3,1),(0,315,246,312,NULL,'FIX',1,1),(0,315,243,309,NULL,'FIX',2,1),(0,315,244,307,NULL,'FIX',3,1),(0,359,108,146,NULL,'FIX',0,1),(0,359,105,358,NULL,'FIX',3,1),(0,359,136,357,NULL,'FIX',4,1),(0,359,141,356,NULL,'FIX',5,1),(0,359,213,221,NULL,'FIX',0,1),(0,359,108,113,NULL,'REPLACEMENT',0,2),(0,359,113,146,NULL,'FIX',0,2),(0,359,105,358,NULL,'FIX',3,2),(0,359,136,114,NULL,'REPLACEMENT',4,2),(0,359,114,357,NULL,'FIX',0,2),(0,359,141,138,NULL,'REPLACEMENT',5,2),(0,359,138,356,NULL,'FIX',0,2),(0,359,213,222,NULL,'FIX',0,2),(0,364,108,117,NULL,'FIX',0,1),(0,364,105,363,NULL,'FIX',3,1),(0,364,136,362,NULL,'FIX',4,1),(0,364,141,361,NULL,'FIX',5,1),(0,364,213,221,NULL,'FIX',0,1),(0,364,108,113,NULL,'REPLACEMENT',0,2),(0,364,113,117,NULL,'FIX',0,2),(0,364,105,363,NULL,'FIX',3,2),(0,364,136,114,NULL,'REPLACEMENT',4,2),(0,364,114,362,NULL,'FIX',0,2),(0,364,141,138,NULL,'REPLACEMENT',5,2),(0,364,138,361,NULL,'FIX',0,2),(0,364,213,222,NULL,'FIX',0,2);
/*!40000 ALTER TABLE `archive_reference_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_text_data`
--

DROP TABLE IF EXISTS `archive_text_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_text_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_text_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_text_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_text_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_text_data`
--

LOCK TABLES `archive_text_data` WRITE;
/*!40000 ALTER TABLE `archive_text_data` DISABLE KEYS */;
INSERT INTO `archive_text_data` VALUES (115,100,21,'€','FIX',0,1),(115,100,21,'€','FIX',0,2),(115,100,21,'€','FIX',0,3),(120,100,21,'€','FIX',0,1),(146,100,21,'€','FIX',0,1),(148,100,21,'€','FIX',0,1),(149,100,21,'€','FIX',0,1),(150,100,21,'€','FIX',0,1),(151,100,21,'€','FIX',0,1),(152,100,21,'€','FIX',0,1),(153,100,21,'€','FIX',0,1),(0,222,220,'#FFCC33','FIX',0,1),(0,221,220,'#5bc0de','FIX',0,1),(0,130,21,'°','FIX',0,1),(0,129,21,'°','FIX',0,1),(0,313,254,'A3','FIX',0,1),(0,314,254,'B1','FIX',0,1),(0,315,254,'C4','FIX',0,1);
/*!40000 ALTER TABLE `archive_text_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collection_type`
--

DROP TABLE IF EXISTS `collection_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collection_type` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `collection` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `collection_type-d-e-p` (`domain_id`,`entity_id`,`property_id`),
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `collection_type_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `collection_type_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `collection_type_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collection_type`
--

LOCK TABLES `collection_type` WRITE;
/*!40000 ALTER TABLE `collection_type` DISABLE KEYS */;
INSERT INTO `collection_type` VALUES (0,229,113,'LIST'),(0,348,113,'LIST'),(0,349,113,'LIST'),(0,354,113,'LIST');
/*!40000 ALTER TABLE `collection_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_type`
--

DROP TABLE IF EXISTS `data_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_type` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `datatype` int(10) unsigned NOT NULL,
  UNIQUE KEY `datatype_ukey` (`domain_id`,`entity_id`,`property_id`),
  KEY `name_ov_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `datatype_forkey_ent` (`entity_id`),
  KEY `datatype_forkey_pro` (`property_id`),
  KEY `datatype_forkey_type` (`datatype`),
  CONSTRAINT `datatype_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `datatype_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `datatype_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `datatype_forkey_type` FOREIGN KEY (`datatype`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_type`
--

LOCK TABLES `data_type` WRITE;
/*!40000 ALTER TABLE `data_type` DISABLE KEYS */;
INSERT INTO `data_type` VALUES (0,0,252,3),(0,0,105,11),(0,0,231,11),(0,0,103,12),(0,0,242,12),(0,0,100,13),(0,0,101,13),(0,0,104,13),(0,0,129,13),(0,0,130,13),(0,0,245,13),(0,0,247,13),(0,0,263,13),(0,0,265,13),(0,0,268,13),(0,0,280,13),(0,0,281,13),(0,0,283,13),(0,0,284,13),(0,0,287,13),(0,0,288,13),(0,0,289,13),(0,0,297,13),(0,0,20,14),(0,0,21,14),(0,0,24,14),(0,0,220,14),(0,0,230,14),(0,0,234,14),(0,0,235,14),(0,0,238,14),(0,0,239,14),(0,0,254,14),(0,0,264,14),(0,0,271,14),(0,0,278,14),(0,0,102,15),(0,0,256,15),(0,0,260,15),(0,0,292,15),(0,0,293,15),(0,0,106,18),(0,0,147,18),(0,0,249,18),(0,0,250,18),(0,0,251,18),(0,108,113,107),(0,109,113,107),(0,110,113,107),(0,117,113,107),(0,120,113,107),(0,146,113,107),(0,112,114,108),(0,123,113,108),(0,124,113,108),(0,125,113,108),(0,359,113,108),(0,364,113,108),(0,112,113,111),(0,123,114,111),(0,124,114,111),(0,125,114,111),(0,115,113,131),(0,120,114,131),(0,146,114,131),(0,123,138,136),(0,124,138,136),(0,125,138,136),(0,359,114,136),(0,364,114,136),(0,123,143,141),(0,124,143,141),(0,125,143,141),(0,359,138,141),(0,364,138,141),(0,0,216,213),(0,0,217,213),(0,0,218,213),(0,0,219,213),(0,229,113,215),(0,0,240,237),(0,0,261,237),(0,0,277,237),(0,0,285,237),(0,0,294,237),(0,313,114,243),(0,314,114,243),(0,315,114,243),(0,313,138,244),(0,314,138,244),(0,315,138,244),(0,313,113,246),(0,314,113,246),(0,315,113,246),(0,348,113,273),(0,349,113,273),(0,354,113,290);
/*!40000 ALTER TABLE `data_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `date_data`
--

DROP TABLE IF EXISTS `date_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `date_data` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `value` int(11) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci DEFAULT NULL,
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  KEY `date_data_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `date_ov_forkey_ent` (`entity_id`),
  KEY `date_ov_forkey_pro` (`property_id`),
  CONSTRAINT `date_ov_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `date_ov_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `date_ov_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `date_data`
--

LOCK TABLES `date_data` WRITE;
/*!40000 ALTER TABLE `date_data` DISABLE KEYS */;
INSERT INTO `date_data` VALUES (0,125,102,20190909,'FIX',3),(0,123,102,20190516,'FIX',3),(0,124,102,20190724,'FIX',3),(0,316,256,20100102,'FIX',1),(0,317,256,20110201,'FIX',1),(0,318,260,20210414,'FIX',0),(0,319,260,20210415,'FIX',0),(0,320,260,20210416,'FIX',0),(0,336,256,20201202,'FIX',1),(0,337,256,20170809,'FIX',1),(0,338,256,20111013,'FIX',1),(0,354,292,20210301,'FIX',3),(0,354,293,20210430,'FIX',4),(0,359,102,20190520,'FIX',2),(0,364,102,20190724,'FIX',2);
/*!40000 ALTER TABLE `date_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datetime_data`
--

DROP TABLE IF EXISTS `datetime_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datetime_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  `value_ns` int(10) unsigned DEFAULT NULL,
  `value` bigint(20) NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `dat_entity_id_entity` (`entity_id`),
  KEY `dat_property_id_entity` (`property_id`),
  CONSTRAINT `dat_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `dat_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `dat_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datetime_data`
--

LOCK TABLES `datetime_data` WRITE;
/*!40000 ALTER TABLE `datetime_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `datetime_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `desc_overrides`
--

DROP TABLE IF EXISTS `desc_overrides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `desc_overrides` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `desc_ov_ukey` (`domain_id`,`entity_id`,`property_id`),
  KEY `desc_ov_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `desc_ov_forkey_ent` (`entity_id`),
  KEY `desc_ov_forkey_pro` (`property_id`),
  CONSTRAINT `desc_ov_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `desc_ov_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `desc_ov_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `desc_overrides`
--

LOCK TABLES `desc_overrides` WRITE;
/*!40000 ALTER TABLE `desc_overrides` DISABLE KEYS */;
/*!40000 ALTER TABLE `desc_overrides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `double_data`
--

DROP TABLE IF EXISTS `double_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `double_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `value` double NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  `unit_sig` bigint(20) DEFAULT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `dou_entity_id_entity` (`entity_id`),
  KEY `dou_property_id_entity` (`property_id`),
  CONSTRAINT `dou_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `dou_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `dou_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `double_data`
--

LOCK TABLES `double_data` WRITE;
/*!40000 ALTER TABLE `double_data` DISABLE KEYS */;
INSERT INTO `double_data` VALUES (0,117,100,2474,'FIX',0,-921068351),(0,119,130,45.07353,'FIX',0,-1941227323),(0,119,129,7.68315,'FIX',1,-1941227323),(0,116,130,37.4949,'FIX',0,-1941227323),(0,116,129,-86.5036,'FIX',1,-1941227323),(0,115,100,48,'FIX',0,-921068351),(0,120,100,814873,'FIX',0,-921068351),(0,146,100,2120000,'FIX',1,-921068351),(0,148,100,3939.76,'FIX',0,-921068351),(0,149,100,3481.16,'FIX',0,-921068351),(0,150,100,3237.96,'FIX',0,-921068351),(0,151,100,3136,'FIX',0,-921068351),(0,152,100,463,'FIX',0,-921068351),(0,153,100,4894,'FIX',0,-921068351),(0,125,101,0.84,'FIX',2,NULL),(0,123,101,0.08,'FIX',2,NULL),(0,124,101,0.97,'FIX',2,NULL),(0,301,129,126.4,'FIX',2,-1941227323),(0,301,130,-25,'FIX',3,-1941227323),(0,302,129,4,'FIX',2,-1941227323),(0,302,130,49.2,'FIX',3,-1941227323),(0,303,129,25.4,'FIX',2,-1941227323),(0,303,130,21.5,'FIX',3,-1941227323),(0,304,129,13,'FIX',2,-1941227323),(0,304,130,51.4,'FIX',3,-1941227323),(0,305,245,11,'FIX',2,-2509044298),(0,306,245,11,'FIX',2,-2509044298),(0,307,245,16,'FIX',2,-2509044298),(0,310,247,5,'FIX',1,4407456500),(0,311,247,10,'FIX',1,4407456500),(0,312,247,7.5,'FIX',1,4407456500),(0,318,263,0.5,'FIX',4,843971615),(0,318,265,23,'FIX',6,2467429176),(0,319,263,0.7,'FIX',4,843971615),(0,319,265,27,'FIX',6,2467429176),(0,320,263,0.5,'FIX',4,843971615),(0,320,265,23,'FIX',6,2467429176),(0,334,130,47.4,'FIX',2,-1941227323),(0,334,129,7.7,'FIX',3,-1941227323),(0,335,130,19.6,'FIX',2,-1941227323),(0,335,129,-98.5,'FIX',3,-1941227323),(0,341,280,320,'FIX',2,223047008),(0,342,281,1,'FIX',2,-92037305),(0,348,283,320,'FIX',3,223047008),(0,348,284,321.3,'FIX',4,223047008),(0,349,283,320,'FIX',3,223047008),(0,349,284,319.9,'FIX',4,223047008),(0,350,287,1,'FIX',3,-92037305),(0,350,288,1.007,'FIX',4,-92037305),(0,350,289,146.3,'FIX',6,572543270),(0,351,287,1,'FIX',3,-92037305),(0,351,288,0.999,'FIX',4,-92037305),(0,351,289,14630000,'FIX',6,572543270),(0,359,101,0.3,'FIX',1,NULL),(0,364,101,0.3,'FIX',1,NULL);
/*!40000 ALTER TABLE `double_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entities`
--

DROP TABLE IF EXISTS `entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier.',
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` enum('RECORDTYPE','RECORD','FILE','DOMAIN','PROPERTY','DATATYPE','ROLE','QUERYTEMPLATE') COLLATE utf8_unicode_ci NOT NULL,
  `acl` int(10) unsigned DEFAULT NULL COMMENT 'Access Control List for the entity.',
  PRIMARY KEY (`id`),
  KEY `entity_entity_acl` (`acl`),
  CONSTRAINT `entity_entity_acl` FOREIGN KEY (`acl`) REFERENCES `entity_acl` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=365 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entities`
--

LOCK TABLES `entities` WRITE;
/*!40000 ALTER TABLE `entities` DISABLE KEYS */;
INSERT INTO `entities` VALUES (0,'The default domain.','ROLE',0),(1,'The default recordtype.','ROLE',0),(2,'The default record.','ROLE',0),(3,'The default file.','ROLE',0),(4,'The default property.','ROLE',0),(7,'The default datatype.','ROLE',0),(8,'The QueryTemplate role.','ROLE',0),(11,'The default reference data type.','DATATYPE',0),(12,'The default integer data type.','DATATYPE',0),(13,'The default double data type.','DATATYPE',0),(14,'The default text data type.','DATATYPE',0),(15,'The default datetime data type.','DATATYPE',0),(16,'The default timespan data type.','DATATYPE',0),(17,'The default file reference data type.','DATATYPE',0),(18,'The defaulf boolean data type','DATATYPE',0),(20,'Name of an entity','PROPERTY',0),(21,'Unit of an entity.','PROPERTY',0),(24,'Description of an entity.','PROPERTY',0),(50,'The SQLite file data type.','DATATYPE',0),(99,NULL,'RECORDTYPE',0),(100,NULL,'PROPERTY',2),(101,NULL,'PROPERTY',2),(102,NULL,'PROPERTY',2),(103,NULL,'PROPERTY',2),(104,NULL,'PROPERTY',2),(105,NULL,'PROPERTY',2),(106,NULL,'PROPERTY',2),(107,'A generic manufacturer of all kinds of products','RECORDTYPE',2),(108,NULL,'RECORDTYPE',2),(109,NULL,'RECORDTYPE',2),(110,NULL,'RECORDTYPE',2),(111,'a device to measure the sound quality of musical instruments','RECORDTYPE',2),(112,NULL,'RECORDTYPE',12),(113,'Multipurpose subdomain','DOMAIN',0),(114,'Multipurpose subdomain','DOMAIN',0),(115,NULL,'RECORD',2),(116,NULL,'RECORD',2),(117,NULL,'RECORD',2),(119,NULL,'RECORD',2),(120,NULL,'RECORD',2),(121,NULL,'RECORD',2),(122,NULL,'RECORD',2),(123,NULL,'RECORD',10),(124,NULL,'RECORD',10),(125,NULL,'RECORD',6),(129,'a geographic coordinate','PROPERTY',2),(130,'a geographic coordinate','PROPERTY',2),(131,'A picture/photo','RECORDTYPE',2),(132,NULL,'FILE',2),(134,NULL,'FILE',2),(135,NULL,'FILE',2),(136,'the settings used in some device, e.g. some measurement instrument','RECORDTYPE',2),(137,NULL,'FILE',2),(138,NULL,'DOMAIN',0),(139,NULL,'FILE',2),(140,NULL,'FILE',2),(141,'a digital recording created by a SoundQualityAnalyzer','RECORDTYPE',2),(143,NULL,'DOMAIN',0),(146,'The Gibson J-160E was used by John Lennon of The Beatles','RECORD',2),(147,NULL,'PROPERTY',2),(148,NULL,'RECORD',2),(149,NULL,'RECORD',2),(150,NULL,'RECORD',2),(151,NULL,'RECORD',2),(152,NULL,'RECORD',2),(153,NULL,'RECORD',2),(154,NULL,'RECORD',2),(155,NULL,'RECORD',2),(156,NULL,'RECORD',2),(157,NULL,'RECORD',2),(172,NULL,'FILE',2),(173,NULL,'FILE',2),(174,NULL,'FILE',2),(175,NULL,'FILE',2),(176,NULL,'FILE',2),(177,NULL,'FILE',2),(178,NULL,'FILE',2),(179,NULL,'FILE',2),(180,NULL,'FILE',2),(181,NULL,'FILE',2),(182,NULL,'FILE',2),(183,NULL,'FILE',2),(184,NULL,'FILE',2),(185,NULL,'FILE',2),(186,NULL,'FILE',2),(187,NULL,'FILE',2),(188,NULL,'FILE',2),(189,NULL,'FILE',2),(190,NULL,'FILE',2),(191,NULL,'FILE',2),(192,NULL,'FILE',2),(193,NULL,'FILE',2),(194,NULL,'FILE',2),(195,NULL,'FILE',2),(196,NULL,'FILE',2),(197,NULL,'FILE',2),(198,NULL,'FILE',2),(199,NULL,'FILE',2),(200,NULL,'FILE',2),(201,NULL,'FILE',2),(202,NULL,'FILE',2),(203,NULL,'FILE',2),(204,NULL,'FILE',2),(205,NULL,'FILE',2),(206,NULL,'FILE',2),(207,NULL,'FILE',2),(208,NULL,'FILE',2),(209,NULL,'FILE',2),(210,NULL,'FILE',2),(211,NULL,'FILE',2),(212,NULL,'FILE',2),(213,NULL,'RECORDTYPE',3),(214,NULL,'RECORDTYPE',3),(215,NULL,'RECORDTYPE',3),(216,NULL,'PROPERTY',3),(217,NULL,'PROPERTY',3),(218,NULL,'PROPERTY',3),(219,NULL,'PROPERTY',3),(220,NULL,'PROPERTY',3),(221,'Unpublished entries are only visible to the team and may be edited by any team member.','RECORD',9),(222,'Entries under review are not publicly available yet, but they can only be edited by the members of the publisher group.','RECORD',9),(223,'Published entries are publicly available and cannot be edited unless they are unpublished again.','RECORD',6),(224,'This transitions denies the permissions to edit an entry for anyone but the members of the publisher group. However, the entry is not yet publicly available.','RECORD',2),(225,'Published entries are visible for the public and cannot be changed unless they are unpublished again. Only members of the publisher group can publish or unpublish entries.','RECORD',2),(226,'Unpublish this entry to hide it from the public. Unpublished entries can be edited by any team member.','RECORD',2),(227,'Reject the publishing of this entity.  Afterwards, the entity is editable for any team member again.','RECORD',2),(228,NULL,'RECORD',2),(229,'The publish life-cycle is a quality assurance tool. Database entries can be edited without being publicly available until the changes have been reviewed and explicitely published by an eligible user.','RECORD',2),(230,NULL,'PROPERTY',2),(231,NULL,'PROPERTY',2),(232,NULL,'RECORDTYPE',2),(233,NULL,'RECORDTYPE',2),(234,NULL,'PROPERTY',2),(235,NULL,'PROPERTY',2),(236,NULL,'RECORDTYPE',2),(237,NULL,'RECORDTYPE',2),(238,NULL,'PROPERTY',2),(239,NULL,'PROPERTY',2),(240,'The contact person at this manufacturer','PROPERTY',2),(241,NULL,'RECORDTYPE',2),(242,NULL,'PROPERTY',2),(243,NULL,'RECORDTYPE',2),(244,NULL,'RECORDTYPE',2),(245,NULL,'PROPERTY',2),(246,NULL,'RECORDTYPE',2),(247,NULL,'PROPERTY',2),(248,NULL,'RECORDTYPE',2),(249,NULL,'PROPERTY',2),(250,NULL,'PROPERTY',2),(251,NULL,'PROPERTY',2),(252,NULL,'PROPERTY',2),(253,NULL,'RECORDTYPE',2),(254,NULL,'PROPERTY',2),(255,NULL,'RECORDTYPE',2),(256,NULL,'PROPERTY',2),(257,NULL,'RECORDTYPE',2),(258,'A device to assess the quality of a product.','RECORDTYPE',2),(259,NULL,'RECORDTYPE',2),(260,NULL,'PROPERTY',2),(261,NULL,'PROPERTY',2),(262,'The assembly procedure of a bicycle.','RECORDTYPE',2),(263,'Number of products assembled per hour.','PROPERTY',2),(264,NULL,'PROPERTY',2),(265,NULL,'PROPERTY',2),(266,NULL,'RECORDTYPE',2),(267,NULL,'RECORDTYPE',2),(268,NULL,'PROPERTY',2),(269,NULL,'RECORDTYPE',2),(270,NULL,'RECORDTYPE',2),(271,NULL,'PROPERTY',2),(272,'Raw data of a tensile stress response','RECORDTYPE',2),(273,NULL,'RECORDTYPE',2),(274,'Produces samples from source materials','RECORDTYPE',2),(275,NULL,'RECORDTYPE',2),(276,'A Standard Operating Procedure','RECORDTYPE',2),(277,NULL,'PROPERTY',2),(278,NULL,'PROPERTY',2),(279,NULL,'RECORDTYPE',2),(280,NULL,'PROPERTY',2),(281,NULL,'PROPERTY',2),(282,NULL,'RECORDTYPE',2),(283,NULL,'PROPERTY',2),(284,NULL,'PROPERTY',2),(285,NULL,'PROPERTY',2),(286,NULL,'RECORDTYPE',2),(287,NULL,'PROPERTY',2),(288,NULL,'PROPERTY',2),(289,'... or yield stress: tensile stress at which a sample breaks.','PROPERTY',2),(290,NULL,'RECORDTYPE',2),(291,NULL,'RECORDTYPE',2),(292,NULL,'PROPERTY',2),(293,NULL,'PROPERTY',2),(294,NULL,'PROPERTY',2),(295,NULL,'RECORDTYPE',2),(296,NULL,'RECORDTYPE',2),(297,NULL,'PROPERTY',2),(298,NULL,'RECORD',2),(299,NULL,'RECORD',2),(300,NULL,'RECORD',2),(301,NULL,'RECORD',2),(302,NULL,'RECORD',2),(303,NULL,'RECORD',2),(304,NULL,'RECORD',2),(305,NULL,'RECORD',2),(306,NULL,'RECORD',2),(307,NULL,'RECORD',2),(308,NULL,'RECORD',2),(309,NULL,'RECORD',2),(310,NULL,'RECORD',2),(311,NULL,'RECORD',2),(312,NULL,'RECORD',2),(313,NULL,'RECORD',2),(314,NULL,'RECORD',2),(315,NULL,'RECORD',2),(316,NULL,'RECORD',2),(317,NULL,'RECORD',2),(318,NULL,'RECORD',2),(319,NULL,'RECORD',2),(320,NULL,'RECORD',2),(321,NULL,'FILE',2),(322,NULL,'FILE',2),(323,NULL,'FILE',2),(324,NULL,'RECORD',2),(325,NULL,'RECORD',2),(326,NULL,'RECORD',2),(327,NULL,'RECORD',2),(328,NULL,'RECORD',2),(329,NULL,'RECORD',2),(330,NULL,'RECORD',2),(331,NULL,'RECORD',2),(332,NULL,'RECORD',2),(333,NULL,'RECORD',2),(334,NULL,'RECORD',2),(335,NULL,'RECORD',2),(336,NULL,'RECORD',2),(337,NULL,'RECORD',2),(338,NULL,'RECORD',2),(339,NULL,'FILE',2),(340,NULL,'FILE',2),(341,'Create a biopolymer sample from source materials for later analysis.','RECORD',2),(342,'Measure the tensile stress response of a sample to an oscillatory strain.','RECORD',2),(343,'Compare the tensile stress response and the yield stress of different biopolymer samples.','RECORD',2),(344,NULL,'RECORD',2),(345,NULL,'RECORD',2),(346,NULL,'RECORD',2),(347,NULL,'RECORD',2),(348,NULL,'RECORD',2),(349,NULL,'RECORD',2),(350,NULL,'RECORD',2),(351,NULL,'RECORD',2),(352,NULL,'RECORD',2),(353,NULL,'RECORD',2),(354,'A comparison of the tensile stress response and the yield stress of different biopolymers.','RECORD',2),(355,NULL,'FILE',2),(356,NULL,'FILE',2),(357,NULL,'FILE',2),(358,NULL,'FILE',2),(359,NULL,'RECORD',6),(360,NULL,'FILE',2),(361,NULL,'FILE',2),(362,NULL,'FILE',2),(363,NULL,'FILE',2),(364,NULL,'RECORD',6);
/*!40000 ALTER TABLE `entities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entity_acl`
--

DROP TABLE IF EXISTS `entity_acl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity_acl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acl` varbinary(65525) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_acl_acl` (`acl`(3072))
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity_acl`
--

LOCK TABLES `entity_acl` WRITE;
/*!40000 ALTER TABLE `entity_acl` DISABLE KEYS */;
INSERT INTO `entity_acl` VALUES (0,''),(2,'[{\"realm\":\"PAM\",\"bitSet\":536608371,\"username\":\"admin\"}]'),(3,'[{\"role\":\"?OTHER?\",\"bitSet\":-9223372036318167437}]'),(4,'[{\"role\":\"?STATE?normal?\",\"bitSet\":284948496},{\"role\":\"?STATE?publisher?\",\"bitSet\":536608371}]'),(5,'[{\"role\":\"?STATE?normal?\",\"bitSet\":16},{\"role\":\"?STATE?publisher?\",\"bitSet\":536608371}]'),(6,'[]'),(7,'[{\"role\":\"normal\",\"bitSet\":284948496},{\"role\":\"publisher\",\"bitSet\":536608371}]'),(8,'[{\"role\":\"normal\",\"bitSet\":16},{\"role\":\"publisher\",\"bitSet\":536608371}]'),(9,'[{\"role\":\"?STATE??OTHER??\",\"bitSet\":-4611686017890779533}]'),(10,'[{\"role\":\"?OTHER?\",\"bitSet\":-4611686017890779533}]'),(11,'[{\"realm\":\"PAM\",\"bitSet\":251659891,\"username\":\"admin\"},{\"realm\":\"PAM\",\"bitSet\":4611686018712336384,\"username\":\"admin\"}]'),(12,'[{\"realm\":\"PAM\",\"bitSet\":251659891,\"username\":\"admin\"},{\"realm\":\"PAM\",\"bitSet\":-4611686018142439424,\"username\":\"admin\"}]');
/*!40000 ALTER TABLE `entity_acl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entity_version`
--

DROP TABLE IF EXISTS `entity_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity_version` (
  `entity_id` int(10) unsigned NOT NULL,
  `hash` varbinary(255) DEFAULT NULL,
  `version` varbinary(255) NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  `_ipparent` int(10) unsigned DEFAULT NULL,
  `srid` varbinary(255) NOT NULL,
  PRIMARY KEY (`entity_id`,`_iversion`),
  UNIQUE KEY `entity_version-e-v` (`entity_id`,`version`),
  KEY `srid` (`srid`),
  CONSTRAINT `entity_version_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entity_version_ibfk_2` FOREIGN KEY (`srid`) REFERENCES `transactions` (`srid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity_version`
--

LOCK TABLES `entity_version` WRITE;
/*!40000 ALTER TABLE `entity_version` DISABLE KEYS */;
INSERT INTO `entity_version` VALUES (0,NULL,'e3a644b6398122c9c9ac4556e60a5fc11ce109e7',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(1,NULL,'5f01a91c1c40b1d30c82b90e53cb67bd8a2c22a6',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(2,NULL,'d091d593d108b34296e16291c99f9e97d9cab1e5',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(3,NULL,'7d803a86084de127c28ce25ceeaa1604be38b2c6',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(4,NULL,'fdd53fefe4eb8eea5230ad7cba58116d82ac7fc0',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(7,NULL,'b73d6a2cc80130ed01b5741009f62cbc3f885c3e',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(8,NULL,'4634f2b81bd6083814f24f8ab0acd629b052e3e3',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(11,NULL,'5bbade6b8a6ce1ed531a7cefc4855e2c88b76df9',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(12,NULL,'6d80055ac4f34e80796a3347211b8c0fe60eb9c7',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(13,NULL,'64d1ab0a7b7640d7557faf9add7425868642a17e',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(14,NULL,'d7842adf1b815282ec61bdc8cc3ae3065ff4fca4',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(15,NULL,'b019a0d43f9d6771afeed251a7827170ed0b32f8',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(16,NULL,'958d5549381ed4894569a4042fed5836e93804bd',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(17,NULL,'2e54ff9ffa2971f2c7ef42726b909782bae266d2',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(18,NULL,'bec952b9447282419e4f41e9ac73ce21c76dd777',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(20,NULL,'b9d7574b90083eff6f4a851b4905a2a9082a3b3b',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(21,NULL,'9eea7c051a2c79c9f8e87b8d144e2d113196c71a',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(24,NULL,'42fe071006235d9658797c25160a522004f9315a',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(50,NULL,'f34266c1a4d6f5b029b70b7c7b0b2ca2252116bd',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(99,NULL,'9a425ba3a1acd81370928aab42e3f08ee740bde8',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(100,NULL,'8bf5c12820de1d2c9fb27f8aa8f43b8d54e337b7',1,NULL,'66fa0d88f7ecc7e0f783b979ce9f59e9c429dae17c7b186992a6d997f71d34df7c3f4ab40dcba91194bf51a850865490bfc8f1b3b7791b91c5aa8fb95bfcebd8'),(101,NULL,'ba35551f179a14c8c4e152d165019e58a3d4a636',1,NULL,'ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903'),(102,NULL,'fddc63296e8e3d4306283d4d9e07a575485d7beb',1,NULL,'ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903'),(103,NULL,'867267ab3fdd48709de8c4a1507346474a519039',1,NULL,'ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903'),(104,NULL,'2eda2bed8ed9b089966b4b83006e7cac44e6671f',1,NULL,'ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903'),(105,NULL,'896c3ce157a2ea0a92e07de266da44eba7c0705b',1,NULL,'ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903'),(106,NULL,'9c22bc7f822caed1ba33a6c2862ae34054f626d8',1,NULL,'ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903'),(107,NULL,'5780bc86ef9cc5779763b8e00275b5fad2d92e26',1,NULL,'ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903'),(107,NULL,'0bea8f7b17f0130fa5701a6c3849b9f8bfa0651b',2,1,'98693c91-20b3-477d-a653-e623c3687118'),(108,NULL,'8a92d9eab01cccea46f16c390968c064d9536eb6',1,NULL,'66fa0d88f7ecc7e0f783b979ce9f59e9c429dae17c7b186992a6d997f71d34df7c3f4ab40dcba91194bf51a850865490bfc8f1b3b7791b91c5aa8fb95bfcebd8'),(109,NULL,'5b10293da7264781f3a1c684502710d5b322abc4',1,NULL,'ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903'),(109,NULL,'41f12af91b375d351cecb29089e582e4ece38210',2,1,'20d8b95f-e86c-4f53-9503-44523e8786e7'),(110,NULL,'7d2f68f591257f231d86a186f931e705d1acbd9a',1,NULL,'ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903'),(110,NULL,'66d8719786b00c4a300198ffb0a2942d1480515f',2,1,'1d2ac525-3638-44fc-919b-6ee9b612220b'),(111,NULL,'0fc9bf4e74e6ede4a05ebad6c6062482efe72192',1,NULL,'ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903'),(111,NULL,'7c2daf407a9d50c1818e9f7851aca20e5225c512',2,1,'853d1263-8f56-4e6b-b013-bd9feefa3224'),(112,NULL,'0798391693aa05d4c89275d849cd6163454ac2ac',1,NULL,'66fa0d88f7ecc7e0f783b979ce9f59e9c429dae17c7b186992a6d997f71d34df7c3f4ab40dcba91194bf51a850865490bfc8f1b3b7791b91c5aa8fb95bfcebd8'),(112,NULL,'f1a9496cd95b4a5eb7b76fa45eedea4c1b44a1b0',2,1,'e89852bd-ec47-4a47-8871-eaf4cd65e839'),(112,NULL,'49353bc63264f1a5bc8e57714fe7ff84a0470b74',3,2,'335482dc-33e0-4ab3-9150-17a4257cb1ef'),(112,NULL,'6434980b561e91b6c904932242d956eefab8e887',4,3,'65e8fb64-d5e4-4fbc-bb52-46ca37fb16e9'),(112,NULL,'212dea0b0bd61cf1c3fd9fea43531a0c5257cb93',5,4,'7465b743-05ca-43a5-803e-3e695cc2c86f'),(115,NULL,'6993a99c4cbce5c6d12d91e12835347bf977535a',1,NULL,'19381df442d97ccce697803bda0164e6b3341999a3cb70aedd3a9e5496de3cbdc9aea5a090ce455994ccd197dfb375dacff0f41b20b6d34847f06dfa8ee36a1a'),(115,NULL,'bfe1a42cb37aae8ac625a757715d38814c274158',2,1,'57973bc2-0ee9-4ae4-b647-4a5cd391b379'),(115,NULL,'595403aae4bb593712b0aa653f70f6a8be0e0a42',3,2,'11dbd6cc-375a-4501-ae46-fe058c351964'),(115,NULL,'cf205bfafd852c1aba3a6eee8b502feeff89e320',4,3,'ba1aa66b-0e9c-4e66-91e1-9ef1cf10d896'),(116,NULL,'c2cc40116033f40fcf2cb7a82bb467394a834919',1,NULL,'ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903'),(116,NULL,'e0d7c28b956ade81c0252dd2e142f45ad2217311',2,1,'479cdf8e-aa78-42a3-be67-80181d91b2ab'),(117,NULL,'b80d22d44c77d326e8954bbef653679726cb5415',1,NULL,'19381df442d97ccce697803bda0164e6b3341999a3cb70aedd3a9e5496de3cbdc9aea5a090ce455994ccd197dfb375dacff0f41b20b6d34847f06dfa8ee36a1a'),(119,NULL,'e53aec78494ce9b2002555be39a1bd582d94e2a1',1,NULL,'ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903'),(119,NULL,'8ec52f52ae9d46845dc34e7c4825c1f1c49f554e',2,1,'f0d20c92-57fd-4924-8e2b-331a985e16b0'),(120,NULL,'5c7e1d34f27d3c95cd3b83af0f572febc1c92e1c',1,NULL,'19381df442d97ccce697803bda0164e6b3341999a3cb70aedd3a9e5496de3cbdc9aea5a090ce455994ccd197dfb375dacff0f41b20b6d34847f06dfa8ee36a1a'),(120,NULL,'8a24ed929cc8b02ae76ff647e7a24bdbb9c56388',2,1,'00a73ffe-9418-48de-a07c-cfdf17a6cf08'),(121,NULL,'eb7464a7fe259e70780b3b9d66e1ba2bb43e6188',1,NULL,'ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903'),(122,NULL,'ec0ccc40bfa110e3254e9b9a0ae152139f288c93',1,NULL,'ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903'),(123,NULL,'264d5375f634eb33f23971d2e51ce534d0af37df',1,NULL,'19381df442d97ccce697803bda0164e6b3341999a3cb70aedd3a9e5496de3cbdc9aea5a090ce455994ccd197dfb375dacff0f41b20b6d34847f06dfa8ee36a1a'),(123,NULL,'e7f9252867b82d4a7337bbb4ba7bd771c78be38a',2,1,'8c942acc-a887-4d47-8b18-b02b52bc6c24'),(123,NULL,'4f0bedbd0feecb9db6bdc6067e587ef8e99158db',3,2,'c2fee268-e8fe-4d37-a2b4-fb511446c3c0'),(123,NULL,'4fbc9bdcb0cec6a597feaa9fb11e889fb9da6c0e',4,3,'ff44a5c0-5b1d-4c0d-9dbd-d864c4873bc7'),(123,NULL,'847e59c4b88e1803df82508c843be244ddd987cf',5,4,'2b3c827b-8898-47fe-843a-729f90f06b2d'),(123,NULL,'70c943afe97f830ac3b060d9e357f2758c5a0127',6,5,'c3dcad2b-a835-4588-8c69-092dd9250d67'),(123,NULL,'a0525ad5cb66fdd6ba1e23e1d1f206a8f21e403b',7,6,'54aa82dc-9503-42cc-a240-94a958df7e68'),(123,NULL,'591e1d8172c4b5d57ede6695ae31b6369007ebd7',8,7,'1e398301-15f9-47e2-a61c-c0d6ce6ebaf4'),(123,NULL,'d6c94dd049abaf635d702ff5a934669e3eb3506c',9,8,'3dc130ba-9273-48af-aee9-0f15304a4cbd'),(124,NULL,'e02769e2eb4f39f7a68c29f453a1c7bc4a71ae02',1,NULL,'19381df442d97ccce697803bda0164e6b3341999a3cb70aedd3a9e5496de3cbdc9aea5a090ce455994ccd197dfb375dacff0f41b20b6d34847f06dfa8ee36a1a'),(124,NULL,'a31ec6a6e8ca398b79cd73d9e6bf5d3f9a923530',2,1,'5ee8fc76-bda7-4805-9f13-5a952e89cb18'),(124,NULL,'737fac2e5d69215ab224637d1dd398fd1be1bfcc',3,2,'ce08eacb-19cc-446f-907f-e73a66a4cfda'),(124,NULL,'0b322210f7bf136769a32bfd63b506b0eb7b21a7',4,3,'e4fb8283-1e48-4dce-b541-45e82d70dd2b'),(124,NULL,'f53a3c9b8a4ec1f42a78323df63a5ed01a84f634',5,4,'f2577a7b-8922-4e57-bdce-2a940ab8fc45'),(124,NULL,'6d4386f2e85893fd75128071042d8e0656e3044f',6,5,'3731b5ef-1c54-4fed-9132-2809f7785a8c'),(124,NULL,'3a5636828ad8bcc37553e1e4a86e014b13884f18',7,6,'90cebd81-59d0-44ad-a9e4-f548b09ba221'),(124,NULL,'2d47f35b12695e1dc2d7ffc84e5209a8a81b714d',8,7,'323f5c83-fa4b-4163-9d29-43e0aad7d446'),(124,NULL,'154a1168a2ec0df5d8bc0711b67d7ce53fccc7b7',9,8,'d9a65f6f-15d6-4326-a533-f69869b33b55'),(124,NULL,'5ab404eb3383808c0f60d38628d866921081fc31',10,9,'57f4497c-993f-418b-8fb4-84c175346be4'),(124,NULL,'ce23b0c81f78f286fcb11b4a26a10d533dd88958',11,10,'486cfa7f-333c-4d1e-8dcf-ab2e234589fc'),(125,NULL,'04033e07de470fe9eb13fcc4b1970f6e1976925e',1,NULL,'19381df442d97ccce697803bda0164e6b3341999a3cb70aedd3a9e5496de3cbdc9aea5a090ce455994ccd197dfb375dacff0f41b20b6d34847f06dfa8ee36a1a'),(125,NULL,'cc5fda8ba546145246124b0e6f207e9e61d384b6',2,1,'db05953e-2cd8-4041-b2e6-e94c58c8a92f'),(125,NULL,'684ec93a19f2a4c45c29a826767ecdf0c31610b7',3,2,'190ae7cd-4f97-4df1-ad9a-07c115e34641'),(125,NULL,'a0834ec49f18a8a3552d682068e77b1ca5923a0f',4,3,'4d988025-ef3a-4ea7-897c-4139dae37c5b'),(125,NULL,'007b3d7e9b5db1549b56a3cabc2a99904b9a2cc4',5,4,'b369f624-973c-4fec-8d78-32c204efd272'),(125,NULL,'4d0aaecf52d9b3d8dc845a241f439b1154648529',6,5,'f21ea47d-3c53-496b-80a7-df263953e7a9'),(125,NULL,'81b424b3bfd9e1bd6bbd50361125fc5c7e04f5a3',7,6,'f9ae6680-d438-4149-9b45-c4981267da57'),(125,NULL,'eccfa813bfa1933d51073e1877a616c00960bbd2',8,7,'d993bf99-78c0-4ae3-bbb8-8dc9609e43aa'),(125,NULL,'3b4071d71b2126290799178a922e4ca411d8055a',9,8,'8f95c4d6-785d-4421-a781-b8173122e792'),(129,NULL,'b0dbac9ff1ccb3586000ee09e7c93b9d3db2f92c',1,NULL,'1c277eea-380b-46a5-9a29-3d59ec24563b'),(129,NULL,'c2b7d53737b5255390b651bf180e9d05f0909b24',2,1,'98693c91-20b3-477d-a653-e623c3687118'),(130,NULL,'f3c0742655fd5bb15b091c5574322d05faee1549',1,NULL,'deabc67e-a5cf-475d-8852-9f85da0c2511'),(130,NULL,'33a2c90d79ceaee75cac82b0cda88810761c4ab6',2,1,'98693c91-20b3-477d-a653-e623c3687118'),(131,NULL,'e74ed8727d86db9e0e9a39153d6c24691fb87d75',1,NULL,'2c67b7f6-f55e-4c48-9376-ed833dcc8b2e'),(132,NULL,'9a5ce165a54f545a235fd65b4fe44953dc4bf459',1,NULL,'5351f282-bb02-402a-b21e-16e7ae6554ca'),(132,NULL,'639b7249a2810936d512de26a450b7632c87ef18',2,1,'d813871b-b3e7-4c23-9f80-8589be44bc0b'),(132,NULL,'1dcd4eaa1af8121587557f003163037fdda20aeb',3,2,'ce966936-7729-448e-8d88-0da83ddcacc6'),(134,NULL,'9f18fbfcd26435c1491133f8329c11d166a18ea0',1,NULL,'17330db7-1ff3-4240-9ede-f9730215af00'),(134,NULL,'837e697221d9b123bd02848396759583688fb633',2,1,'ed6d1ac3-76b9-4ee4-b1cd-57aa21ee454f'),(135,NULL,'d604a288944c2938e6937bdfb45d54ee09c15e28',1,NULL,'ec8dc845-9cb1-4dfc-a715-812265e6bebb'),(135,NULL,'efe7460846a63a010a30fa6f1f4ccfcd3b69ef81',2,1,'48f55467-121a-4dee-9adc-013da509135d'),(136,NULL,'284eefe2b692fe242af43a61ce355bfabb073af5',1,NULL,'71116cf5-e236-4b5c-95a7-bedb97c6d47d'),(137,NULL,'2646f5471a8e87237cfc821dfa7d5d2cb45a661d',1,NULL,'e66d6f26-48dd-420c-b648-08e627431818'),(139,NULL,'f4810ab8648e42a5f6b07bbe15d6165690ea4d64',1,NULL,'c93b7f56-0cfb-4bb1-9b1a-9b4108ad41d0'),(140,NULL,'8349d146e2a0bf3cf12008c85adcb895eea4e526',1,NULL,'2925c5d5-5c2a-473b-9888-3d3bcc3f6a3c'),(141,NULL,'4fd4712edd8b774787c5dca1bf86f45daffc5b2f',1,NULL,'81fa30a9-b7b2-48a8-9695-d508df39625f'),(141,NULL,'88a244c813a0f5bb10f8c358126c7a2ce5e146ca',2,1,'d2ef9a55-7933-4171-be98-5da4823ad913'),(146,NULL,'c017342c82b23ee897025c03ffa579480a68825b',1,NULL,'6cd43def-10be-498e-b75b-e572badadbed'),(146,NULL,'6e657c3931bffe06b830547abf033456b4d43404',2,1,'9062a8a2-5c38-450b-a624-2fb9d46b234e'),(147,NULL,'c986c9f2774b026deb8d1a781758d00dbd4ff2ff',1,NULL,'d12236ed-6962-4632-ae88-daf55af6e1bb'),(148,NULL,'15f3c552100dfcd0f946740b4dec07f09ee94350',1,NULL,'8f797262-f930-498b-9c1c-4f8b57651d73'),(148,NULL,'d4a10f9833bc998b1b3c76ed7a349618d3a21d57',2,1,'129f344e-73d0-46c3-bef8-caa2517c63e1'),(149,NULL,'bd1b61ab2afef720b34ca4e92fe5e5fd1343a1a1',1,NULL,'8f797262-f930-498b-9c1c-4f8b57651d73'),(149,NULL,'ea2435546bff27488dc50b0ef2387ee70a4b9195',2,1,'6c4050ee-309d-45aa-8717-d06419129348'),(150,NULL,'d4ddb6f1288b7d78b949cebf6ba0071d2a4c57a8',1,NULL,'8f797262-f930-498b-9c1c-4f8b57651d73'),(150,NULL,'9659aa9223efd2ee830b40dfb0aa80ac7178c6b1',2,1,'82e8842b-9fa7-4e14-a908-cde73b6d2d38'),(151,NULL,'87fd1a512c6cb3ded63009c4fdf8410ba0509ec5',1,NULL,'8f797262-f930-498b-9c1c-4f8b57651d73'),(151,NULL,'2e06d68b26d518de9b99ad04ac1634e89a8da203',2,1,'f292d60f-d18b-4b93-98ba-bd6fdd95b5f5'),(152,NULL,'908588d5dccaf9d5d64b50e20e856be3067a68cd',1,NULL,'8f797262-f930-498b-9c1c-4f8b57651d73'),(152,NULL,'85c9e0a0ed7f7f8bf1caa068a0363f8b243d7ba9',2,1,'5624c8eb-46ce-4cf2-93a9-a8f8f5ef05bc'),(153,NULL,'31799a02dae5c04397c4917a734da9b59dc31dd6',1,NULL,'8f797262-f930-498b-9c1c-4f8b57651d73'),(153,NULL,'e8996933a1dfe8bd5074e5b5a64049ce6a764063',2,1,'a19f418c-b5e7-428d-a087-9e87342ee71e'),(154,NULL,'efc2ebc522e1d9abb7b7e4fd667ce83a8566e180',1,NULL,'b60187f1-eeff-4a72-9958-65b7d4c3781a'),(154,NULL,'b01c0f028dfe6a4336a185f6a5096f0b21282866',2,1,'7f9e674f-aa91-48e8-adc7-6aa1c5405f53'),(155,NULL,'0fed3b676c9a39bd6c11712b76b8a091183d1786',1,NULL,'b60187f1-eeff-4a72-9958-65b7d4c3781a'),(155,NULL,'7059ca5f9d9b97bbcea3f80fd79b1c5f451c6ce3',2,1,'b964a674-aa5b-4f59-99cc-a81728d2b987'),(156,NULL,'f6524a385081a030ca8aaf039c73f7a9de0de7df',1,NULL,'b60187f1-eeff-4a72-9958-65b7d4c3781a'),(156,NULL,'b22d41f23cb207a193e840b353320abd48217bf0',2,1,'d1a18246-3c3c-4b9b-aca7-73f226c633f4'),(157,NULL,'3cbf22b3e2d5092087a36d27495d0b5e6a152bb5',1,NULL,'b60187f1-eeff-4a72-9958-65b7d4c3781a'),(157,NULL,'68946191a68ed9f7e43d1a26aab569392d54e9cf',2,1,'5e09057c-32b1-44d9-bd43-2673e95730fc'),(172,NULL,'3116566f53a598feb35e91a5adcc9ecb6bf36a43',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(173,NULL,'5b62fb823bc1ccd7f083458bad64a9c1e88f0317',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(173,NULL,'f51b03585c7c0568b20cb2ace533659562d42705',2,1,'b4dc07f5-11f5-4255-b3f1-cf7b067d7e2e'),(174,NULL,'87f9a7514f048c8b85dd4d97eb89f1e2d55092cc',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(174,NULL,'a919224097e701501a22c32c0bc378b8368c2097',2,1,'00963a37-5605-4761-a9ef-5a4cc81c5750'),(174,NULL,'d56969b4627dbbe24d1edc996b761a37e1bbd141',3,2,'26ae2cce-ad0d-4e93-ad88-8008a5398df6'),(175,NULL,'37c20c51136956f40c2905a263a6dd7f3d6dac77',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(176,NULL,'7d15b433c4b0e39b51d6501f64bb3ba6d899f56d',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(177,NULL,'9382a3d3309f669116e71ac423ada034ded29706',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(177,NULL,'d94df6a251e2be8c7f6f20ec0aec65ba7a150898',2,1,'85da6043-e893-48e6-80af-3189fa168db1'),(178,NULL,'692d5058b9163f3e79becd1a7c664702dccb48d9',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(179,NULL,'dcb9558d740893adc9b0766276c2d7bcc473acae',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(179,NULL,'0b2793fe3c8aa15c6e5eb6c84eb7609056bd4148',2,1,'630bb660-27fd-42d3-bafb-e0651c6c2769'),(179,NULL,'53f1e5288635d344a3526f1a803d43db5996e6d2',3,2,'d245250a-16e2-4693-b7e6-ca848afa33f7'),(180,NULL,'b16b0c7c61a08248b399c95d6f3e43dba7dbca70',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(181,NULL,'3639cc9fd51fe32c0a1b9e7ece629d72d53e01b9',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(182,NULL,'7e4e19d107fd84a0d2209dc50dcf1372dd068364',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(182,NULL,'cfd04742191b02c4fe5a52b0ab93cca03e5e9992',2,1,'a2cfcff0-7cff-4b22-8dfb-f874b04087d4'),(183,NULL,'d4a66065f14bb3b766f64f8a5cedaadcc0a18e96',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(183,NULL,'decc4225211ef46401c2929f3323c07832e8888f',2,1,'a7769102-5eb3-4421-ac5b-38499fa11faa'),(183,NULL,'b1463386815b61fc816cbd6b9b99273c032104a7',3,2,'3f8f27b4-3919-4a2d-84c6-cd95b483e41a'),(184,NULL,'712e45d043cda9eaac552a746ff36e0bf674d1ad',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(184,NULL,'14aecce8ec65ace518182f4cd4ae8ba1061f2a7e',2,1,'0ddf80a7-df7e-4baf-86cc-613d7213ebf5'),(185,NULL,'f35094a3e0fe4d47a428100e6e94c92ab5f794af',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(186,NULL,'cd4aa54bd320b75119be3f643877249d2587be9c',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(187,NULL,'ecd7a1f9f065410abc392dec94063b010a027922',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(188,NULL,'df8f85be0e0c3e5017d128bddb46d670e7822db7',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(189,NULL,'323eae83893968875c887a31af5cdbd3759bbd89',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(189,NULL,'93f53996c202410c31b36a25f3715feb6b5041f6',2,1,'07295cd4-afea-43d8-9fd2-1026a685ed23'),(190,NULL,'959466833db2a80351c25dafc60feb3aabce65cd',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(191,NULL,'b54b57c90b828e5dc61a70057eef16987e0fbe11',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(192,NULL,'3dfbb04ad3790726c75a27133b13662472d92090',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(193,NULL,'e06c5a5c94504803ace8f396c58bd2e50954d680',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(193,NULL,'41dab3a626e0c765ee49cc10130e6c369e6b0c97',2,1,'eb937b8b-25bc-496a-bbb1-33767dc3ff6b'),(194,NULL,'695c4d41022c4305e1de047e495a4aa80f10d6a6',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(195,NULL,'07eb5cb4523e312e12f636f9adfa61fe5ffa5b80',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(196,NULL,'7c267d8aa6e745cd993243bb3896dba7ea8adddc',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(197,NULL,'394f8b5c43b26369d40c36a3204c48a8ad18b5d2',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(197,NULL,'4058c04e302036ae892fc1fb388daa177378c0b1',2,1,'a2af374f-3a70-493a-a97d-052e0e4cff51'),(198,NULL,'cc27615d7c18bdf5908fc3a393e879d98c5df478',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(199,NULL,'e34b6078d253db843abde8cadb46f4e87b955116',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(200,NULL,'20194b728f9689e0ea108fc2e6a7665171284321',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(201,NULL,'b9d270c234960d81aa1863bbd96496be02a5c765',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(201,NULL,'97b586b129d4c0449d69d64bfdfda8ea1624971a',2,1,'bbd728b1-3c67-49f8-ab70-6635cf7bfac3'),(202,NULL,'4ac8c61e325d7ae6a5a24ab610e64990e2eaff17',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(203,NULL,'71cd332285e72ebbe5b985eeba1beabc26df59cd',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(204,NULL,'90d2e738697d43284ffebc4f440828d5f7594254',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(205,NULL,'de982d7a4dfe571f8cb2a17615069a976fcd1203',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(205,NULL,'18cd4a5aff2eed70ff869db4eb9ca856e035f6b9',2,1,'29488453-d9fd-415e-9617-a9e3e190fa7f'),(206,NULL,'9f36841ce248b89d81d48f0f259d963c967ea8dd',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(207,NULL,'bbd99ee55bdb8fabc0ff92c08a62054e44aad339',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(208,NULL,'c022191aa3f5fa72e28887c30fe7d1c07f250387',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(209,NULL,'33f3c4f6e9f5358acb91ee1fe2d7d222d93d0775',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(209,NULL,'86cb88cbd71dfd225d849101bd0d74450c894909',2,1,'5935f9f3-3e80-4b91-9f06-785003effe34'),(210,NULL,'e752c71feb937fccca147119422957bb79b7d088',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(211,NULL,'ef602743fe722b76c8f849b6c29585ab3d328a8d',1,NULL,'7f6457d9-b694-4fb6-babf-4e8e3c95ff79'),(212,NULL,'bc03dbdbe08a428fa7bf2d2abaf016ebf93530a0',1,NULL,'c9c24921-a24b-40c1-bf6c-c67c62e844ce'),(213,NULL,'08cf7ed840ea77df219935f07bd809c5c9a484bf',1,NULL,'f6e0593e-a3a4-4c2d-b837-70386c9307d0'),(214,NULL,'e0d2ab2e83068e79bab38d546a251fd7ef774886',1,NULL,'f344a7ec-90d2-498b-b473-7876f9b8ece5'),(215,NULL,'76e8435064423bdca2ca3e5fc662fb7c977b8cc1',1,NULL,'2b519105-7daf-4dcb-af35-5915b3425b33'),(216,NULL,'3103b59744bad3db7690fad230f8a386ea9a232c',1,NULL,'c7cfa8c1-575c-4916-b91d-5a9ae6e4301a'),(217,NULL,'3d42f9af2067d0e4b992c5fea861b5c22409e803',1,NULL,'21c08f5e-be47-4598-9f9f-51f3c08dfd7a'),(218,NULL,'14eec83f3bd511e5a808407a6db7559d3c961976',1,NULL,'dc59320b-4b53-47b7-b862-c06748616e27'),(219,NULL,'e2209fa79500ef6eeb51386c02270f8b4651bdfb',1,NULL,'428352a4-0346-4d57-9f0e-4ec6b17d436f'),(220,NULL,'677a7b345f450b2029b73b54f5191b1f5d677630',1,NULL,'f45f4a8e-666a-4be8-8392-0941c3263290'),(221,NULL,'d7839d53f04857f6b5ac33f120c9d3ba1297de0f',1,NULL,'d9678ec8-b904-4711-8e86-38bded3a9898'),(221,NULL,'dd0fe565866b977914df880dd1367fac75cb2e33',2,1,'55f5c517-610a-4672-b6cd-45e5af4bbb45'),(222,NULL,'2ab9e7b1c63494bf13c45cd9e3c56b233c93e984',1,NULL,'0e78ebf6-9dc2-4cf4-9a79-8d8a28d3f919'),(222,NULL,'3493bbfba72ecf1bcfcd428d7439a2dbbb7cfc5e',2,1,'76e9dc6e-bbae-414b-9da2-37f5a89099f7'),(223,NULL,'34f999b8a5196909bf92affc970ac6c3070e7425',1,NULL,'0ecdbfac-cf69-496d-a40a-774a56ef83f9'),(224,NULL,'f2be4684f8086bae9285b31d587182102408af2b',1,NULL,'70e888f2-b57b-4cd6-b31e-8f95f4f69347'),(225,NULL,'36ec3ba5617147f89bd6143be59ab2382422f18f',1,NULL,'ef5a2950-761b-4b9f-b104-b920c155f9ad'),(226,NULL,'3dcd5dd12cdb616aa5fdd580e90e23d8fa6c9393',1,NULL,'9fba326b-5308-4074-af85-510cc09ccb2c'),(227,NULL,'391eb15303d82142bba47a889f3fe0b7fba8e9ce',1,NULL,'c6fe4c18-7d90-4109-87eb-ec3c5903491f'),(228,NULL,'d18248e23bf1cfce9e756d9531ce9c6f638654da',1,NULL,'b099c7fe-0ec3-43a3-8bb3-7649df508e56'),(229,NULL,'41400f19ad92b34e1fa982a60ae5d57226e31085',1,NULL,'128f912c-ef86-4fd0-8072-262bef22a77c'),(230,NULL,'085e149e26d3fca394174527adf8608b62fa29b3',1,NULL,'60086f61-d7d3-4f86-a428-16d05e85c9fa'),(231,NULL,'7d301e4b1e34d74746fad204cafa101fc9445d8e',1,NULL,'2c88eb7f-9cdb-4af6-9d25-06a5bed193e5'),(232,NULL,'d18d44b8e07a3f68803a06668dc2e36c205017d1',1,NULL,'8cd3c732-9ccb-49af-919b-4d408c08dc2f'),(233,NULL,'1d5c9bbe942f7bd237c2f2277ba33937a0168ea6',1,NULL,'2bd0b828-6554-41fd-bc21-1778e1125eb3'),(234,NULL,'efa22e1e03ad1910331dd079602c9af488f18cce',1,NULL,'124bdb14-9577-419e-b3fd-14900ba6413d'),(235,NULL,'0d6a168014b9fd121119c3e0a5231f7765dda2ab',1,NULL,'124bdb14-9577-419e-b3fd-14900ba6413d'),(236,NULL,'354f6c4e1796f01aad7d8423ecec76b056a69623',1,NULL,'124bdb14-9577-419e-b3fd-14900ba6413d'),(237,NULL,'71bb67a548719cf2f802649aefe1e20bfb955048',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(238,NULL,'60a5bdbf90f695de4aac22cb0e55aae5a2fb4cf2',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(239,NULL,'9001e09883c20116274a2897e939b6fcf1b05278',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(240,NULL,'0072fe3e52332e01616225a43b164c3b164c4475',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(241,NULL,'9cc6b90e5bcbf07e528e8a7b21f51b6745f70d0b',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(242,NULL,'221de4b4739e24eb1cc605effd5b712b21e8ac15',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(243,NULL,'bfe5d722c8de8f11622312a75968c161707724ae',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(244,NULL,'a4a0e512f5a72d5a165ae48f692de841a6b63df6',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(245,NULL,'d41a40a6d97f26e39534c5b7aeb6d01b853f1cbf',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(246,NULL,'af7016fc7d87728bb4fe7ad1da1d2568568fb6ec',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(247,NULL,'010e1119db81a5ffb2fe752a9ea4e399629913fe',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(248,NULL,'9e575e03ecf97d1228e19778476ab90c297bd529',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(249,NULL,'3a1e6c3df552e95827eabe7e32cfcede9d3ad9ea',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(250,NULL,'cdc411ac328ca598579c88f0d9f035f671554163',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(251,NULL,'90766aec85a17218f3674c1525f507140ac1c8d5',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(252,NULL,'b59194f6899023e4c0e2f2c9aa6cc122d57de863',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(253,NULL,'bc2f14ee33f147daec27e50d375d94b6d71a22bf',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(254,NULL,'8f1fb89ae3789a5c60fb98ea357241edb4643a9c',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(255,NULL,'939219d2049e83c9e0c5a4353ad48e9632036db8',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(256,NULL,'3556e253c17a10de20e079e9b190f79483b7b87f',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(257,NULL,'5baa533e381ab7dedce08c0e202c42ce69cab1cb',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(258,NULL,'8d2564b25c2421c769d54752bda4ed6cd757fa34',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(259,NULL,'082510fecf0a5eed921cbdce948be85a85ee941c',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(260,NULL,'20198ddcdca0279f389f095fab370040c60e4ec1',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(261,NULL,'babb7350c534e3cee9ce5bd8d73cd28848600704',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(262,NULL,'6f26d19ad9215d58633fecaa2688403537d76053',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(263,NULL,'33eaf92ff2a3022ec0a59ad60653ff7860532449',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(264,NULL,'7e934e22919ebf1958f32f366dc7c54935d024a6',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(265,NULL,'8b8be94d5c612307b5ce2672f069b45d14df27b4',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(266,NULL,'07671970f1cf2e4d150c5d22b82889a40224dddf',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(267,NULL,'6e09ca512c74aeddc3e7c67953a574e04fc350e2',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(268,NULL,'d5e1c34dd1821e62ae47830bf6ea642f5dee3c38',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(269,NULL,'e9c40cd3bee26a42924ec662e4e9f931d67e84e5',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(270,NULL,'017f5497e2ce490b17a03988adb043206bba9446',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(271,NULL,'8afd0ca3120044dd2549c05be98e31247393c415',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(272,NULL,'1d72e2b7f4de21fa5b01e6f2348f637e45027e22',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(273,NULL,'db022b44b31ab1c92c52de6794720d5ba3b52ddd',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(274,NULL,'779f62c6e09ff9609802002904a495838e9d4252',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(275,NULL,'7f84c5766103a7780ce2e935ab6284d451e97a1c',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(276,NULL,'bcc91637880e4b542d3ef092e3640e9667a085f7',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(277,NULL,'a8b1e9231bf7268a6e98c37020a7f6b099acf361',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(278,NULL,'bc2799f06a96352b38b6c3f60d2f6a4f0149d33d',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(279,NULL,'7a7f7d3735dacc701a87211922f8ac17cb2155a8',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(280,NULL,'61e9e75ae6ab8c9bed2f593a74197e36d1bc871e',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(281,NULL,'983eecba31bbd8d4aaab5c8e2e26f90ecef53bf2',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(282,NULL,'e76071c86622d56674def6035340cd4aab4ddd4f',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(283,NULL,'3ab25c133e45cdef94b9d5ac713762f51f238b79',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(284,NULL,'c0b1356fc488e68e0884ebda99f5b51a12467a85',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(285,NULL,'927fc79f38acf3d3bd17db8df8dbea448f7d84f4',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(286,NULL,'8d27befde5090e46b8a08f705456a61154315b32',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(287,NULL,'4d52774b55acdfdff97c4996a7e15dc8292d6756',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(288,NULL,'bcc9f6a6b7ceef317e4e5d9a42775da6311c0a30',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(289,NULL,'cabf5189e713f60d5dff43383fe71e5462939301',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(290,NULL,'679b3501b4bd9bacd20c0fe1d2de50600d70323a',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(291,NULL,'53092320e583099b86e154dc52f872add2c95b88',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(292,NULL,'fc19e3163249428ead1e48d549f0647db7aaf4d1',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(293,NULL,'0dce091f4615a4960bc1849653253fe1dab1f1e2',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(294,NULL,'7806ee79df572d89e1a5cae7f765d5252ff30ed7',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(295,NULL,'26c955d62549a66325ed3a1a4a201ade04c6006d',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(296,NULL,'405a9aea111c1b8bd7f320e9cabfd65d467a767b',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(297,NULL,'0fe2afde53194afcfa3f15eece732d94431c3ff0',1,NULL,'44789515-9cd6-4cee-9914-ae8936954a89'),(298,NULL,'e53c6299c6f96a7a90cc87617d5ff52c711fc03c',1,NULL,'254693b4-a9d7-4d6b-a3f1-8c61717b700a'),(299,NULL,'64ba6c0adb7e44bd0c243adce69bd865d462066b',1,NULL,'254693b4-a9d7-4d6b-a3f1-8c61717b700a'),(300,NULL,'b587d313ce5bbe7bbb6831cb39263bf08e1d3ca6',1,NULL,'254693b4-a9d7-4d6b-a3f1-8c61717b700a'),(301,NULL,'852f91f610247fc653998b863d0b6cba5eeb11f6',1,NULL,'988294cb-6a61-47c2-991b-e24bf1f7d4d0'),(302,NULL,'fe07b15f889ebaa29b90534080c85d6f50262dde',1,NULL,'988294cb-6a61-47c2-991b-e24bf1f7d4d0'),(303,NULL,'46d3fe81c7eec7a395754acd468eff9410fe827c',1,NULL,'988294cb-6a61-47c2-991b-e24bf1f7d4d0'),(304,NULL,'dd942d5bbbc58cb1d3c0e2ae21e3ef121315642e',1,NULL,'988294cb-6a61-47c2-991b-e24bf1f7d4d0'),(305,NULL,'f8294aea204e49ec7a875c6806328fcbb2c3a485',1,NULL,'81b62a50-946d-488c-818c-5f58427db5e3'),(306,NULL,'c2c458c1166a3b064867f82f15edd49f92e143ba',1,NULL,'81b62a50-946d-488c-818c-5f58427db5e3'),(307,NULL,'1781b898794c7634be34d0ea816e59173a6880d5',1,NULL,'81b62a50-946d-488c-818c-5f58427db5e3'),(308,NULL,'7cdf2aade91eccb723c56e173a2d7a7e00e45998',1,NULL,'81b62a50-946d-488c-818c-5f58427db5e3'),(309,NULL,'dc2ccd9a4a055e37ef3d8529984edcbde5172c82',1,NULL,'81b62a50-946d-488c-818c-5f58427db5e3'),(310,NULL,'e165cb2ef8638a052e1e533eac83f58162f48656',1,NULL,'81b62a50-946d-488c-818c-5f58427db5e3'),(311,NULL,'e3cce280aaec371d998d9d50204af58cfa97e2b9',1,NULL,'81b62a50-946d-488c-818c-5f58427db5e3'),(312,NULL,'d55e335522fffbe30de935037b17ba9496feacb8',1,NULL,'81b62a50-946d-488c-818c-5f58427db5e3'),(313,NULL,'e599fb581636fa721f39324f70da3de446542e31',1,NULL,'8aa78367-62b5-48bf-a968-3a9be59a809c'),(313,NULL,'6e76175e527cd70a34adb3b3655a0aad9d030725',2,1,'de3494c7-6b51-4020-a091-58f3854ea880'),(314,NULL,'97e6889c4c11bec2797261bb55e02001b6e2728f',1,NULL,'8aa78367-62b5-48bf-a968-3a9be59a809c'),(314,NULL,'d806eb1d0e8d6c4d876a3b1af140379ba81bce89',2,1,'0c1f8efc-7668-49d0-96cb-2a58c7f38fcf'),(315,NULL,'7f945d59143ac8bb9b039c4bd1c3d8f4e820b7f5',1,NULL,'8aa78367-62b5-48bf-a968-3a9be59a809c'),(315,NULL,'a1960cdd0de52126c17f3fd971dc5f311d7c29a6',2,1,'773df73b-57e0-460b-a20a-ff5533d06c93'),(316,NULL,'ce0e592994a1fd83aa7bb0106caf82c725c284b0',1,NULL,'5f681463-ec81-4b97-a628-1605976ce32c'),(317,NULL,'abf1f1af2bed3637d67dacadd236abd9afcfae53',1,NULL,'5f681463-ec81-4b97-a628-1605976ce32c'),(318,NULL,'6fcbfe2865e7d5c390bcaf3c55db4f87447a8abf',1,NULL,'b77b1e36-0f12-49e2-8129-447330391e5e'),(319,NULL,'0fdbcdd6ea792fdc9b1cf7555e732b712c0e5908',1,NULL,'b77b1e36-0f12-49e2-8129-447330391e5e'),(320,NULL,'475468725e2a7939a4c30a3e6130cac2f8121f79',1,NULL,'b77b1e36-0f12-49e2-8129-447330391e5e'),(321,NULL,'51ae6cec1357f29b8722cce9ecce86b20cf99c72',1,NULL,'502c063e-db2c-42e4-bc6b-429aefcb0b41'),(322,NULL,'c6acf522a49fe096054303d44e1e3aa3f4e9598e',1,NULL,'502c063e-db2c-42e4-bc6b-429aefcb0b41'),(323,NULL,'d1d05d25f17fede4d75d430cc2016b333091b953',1,NULL,'502c063e-db2c-42e4-bc6b-429aefcb0b41'),(324,NULL,'a9992d86cd7975f481b2a4e7e08adcd9660050cd',1,NULL,'b58a0cac-2a78-4e3e-9859-9bf843c5df46'),(325,NULL,'69a41a11a8e015dd13367b8114d5865268adb70c',1,NULL,'b58a0cac-2a78-4e3e-9859-9bf843c5df46'),(326,NULL,'bc0fa2c27854520311b32617726a3f716a158662',1,NULL,'b58a0cac-2a78-4e3e-9859-9bf843c5df46'),(327,NULL,'407d953a12297e7f695e81e49412ca07a3e1a4b1',1,NULL,'a45acde2-b6ef-4a31-8af8-f2dd9f515d00'),(328,NULL,'087de21637f448360ecc354bd15357483d71f54b',1,NULL,'a45acde2-b6ef-4a31-8af8-f2dd9f515d00'),(329,NULL,'982c784797cb5a8246eb542849620cd062909078',1,NULL,'a45acde2-b6ef-4a31-8af8-f2dd9f515d00'),(330,NULL,'14909e64105a6a11fb5717846d212c803ff0e82c',1,NULL,'08cae94c-ef46-4d3c-ba70-0c94cff2188d'),(331,NULL,'ec74367b381a4f0236df27dca714e708c4fabb8c',1,NULL,'08cae94c-ef46-4d3c-ba70-0c94cff2188d'),(332,NULL,'3930ccf9a83429214ded5b47d7107c399ea1e2e8',1,NULL,'08cae94c-ef46-4d3c-ba70-0c94cff2188d'),(333,NULL,'f897608a03911fb6423bf5e7c9d131dbae2a82be',1,NULL,'08cae94c-ef46-4d3c-ba70-0c94cff2188d'),(334,NULL,'bd56ff337e69c5a37230df1d4ee77f3460ceee2e',1,NULL,'0877faea-9870-44c2-8440-b6747bfbf364'),(335,NULL,'a4ba3218d4130c6d3c4bf5186b027d7fd3cb382f',1,NULL,'0877faea-9870-44c2-8440-b6747bfbf364'),(336,NULL,'70da3e769bd95fc3bdc040b921c771738a7f1f87',1,NULL,'1cb1907d-e5dd-463c-b10e-d4ff85c3d7b8'),(337,NULL,'0caccf291901431f86ad002cd630a70b7e986aa4',1,NULL,'1cb1907d-e5dd-463c-b10e-d4ff85c3d7b8'),(338,NULL,'4b2a0fedb799783212c6496da822ddceca4116e6',1,NULL,'1cb1907d-e5dd-463c-b10e-d4ff85c3d7b8'),(339,NULL,'42fad7a25ec6210021618284e087861b027596f2',1,NULL,'d516afc2-9e06-44a0-95f4-931f69003802'),(340,NULL,'004f038e9714051d959ee73b59a72b41f223cca1',1,NULL,'d516afc2-9e06-44a0-95f4-931f69003802'),(341,NULL,'7092350d2eca2e983c33c6b541fbf59350aef259',1,NULL,'17eb6c42-de9d-4835-af10-8083ca5645c0'),(342,NULL,'0d18d2d1f084759e5dd0bf4dd7ba68ab06dd473b',1,NULL,'17eb6c42-de9d-4835-af10-8083ca5645c0'),(343,NULL,'7f19c610ec00bb63b122964ad7806ab99c10b82e',1,NULL,'17eb6c42-de9d-4835-af10-8083ca5645c0'),(344,NULL,'d43bc992035c8e69568d52f2fb23a9a63862c99c',1,NULL,'a1788322-11e3-4445-9023-2e46e7371f28'),(345,NULL,'76d76d3d819a055a7451684cfa402011a2429ca9',1,NULL,'a1788322-11e3-4445-9023-2e46e7371f28'),(346,NULL,'23e01fa0ebc16f911135b8b52d78211cd403377b',1,NULL,'a1788322-11e3-4445-9023-2e46e7371f28'),(347,NULL,'99d2a431277d0afac0dd02bbd0298f9709e58c61',1,NULL,'a1788322-11e3-4445-9023-2e46e7371f28'),(348,NULL,'226ad4ecdcda3b96d9238a1aff49ae2404f9a2c9',1,NULL,'b9aec957-956d-45e3-91cc-dc8ac3f6e11e'),(349,NULL,'2e2245ebeb8831602aee22b9c76358235d80d864',1,NULL,'b9aec957-956d-45e3-91cc-dc8ac3f6e11e'),(350,NULL,'72bb52f49815cf43b83f27b069c4031bc7c08dec',1,NULL,'d2d70958-fe90-4273-b3df-99963c4998c7'),(351,NULL,'c83e65182f73834eddcedd20a1271d7155d5d010',1,NULL,'d2d70958-fe90-4273-b3df-99963c4998c7'),(352,NULL,'bbaf4cca8f7e49cfb907e764f71ee87a36fc07e5',1,NULL,'1cf0d719-f5c0-452d-b91e-fe968662073a'),(353,NULL,'8feee91c253f61db567085b92d81bac194216d8f',1,NULL,'1cf0d719-f5c0-452d-b91e-fe968662073a'),(354,NULL,'3db14c9475acb2a7998fb98baf25237bb4cce5b2',1,NULL,'9c44d8d4-eb03-4820-bdba-b40ecf1c4e76'),(355,NULL,'ce64692c0fdcf563d820146f8845b9b7f114f51b',1,NULL,'4ce82e39-21ff-49c7-95bc-a59b91cfead2'),(356,NULL,'3e7fcd81d15d05e295f2eb9a470aeedf86acf5a3',1,NULL,'4ce82e39-21ff-49c7-95bc-a59b91cfead2'),(357,NULL,'a29714fc4987e47b0b00f281f0bff941f77f9167',1,NULL,'4ce82e39-21ff-49c7-95bc-a59b91cfead2'),(358,NULL,'b7e22eb74c2f30b483af1adc10edc76b480adfc7',1,NULL,'4ce82e39-21ff-49c7-95bc-a59b91cfead2'),(359,NULL,'fd1bc83a4fbe07897573062e6f91f93a888e3765',1,NULL,'4ce82e39-21ff-49c7-95bc-a59b91cfead2'),(359,NULL,'ca596838987d2378dd1b8482756f5da182f6c937',2,1,'5dbbd5e4-4677-48c9-9967-56b6e2597743'),(359,NULL,'177e893f6a4603e1005976f79cb6eb23cdf7a9fb',3,2,'eb5c3f84-d3f1-4315-b599-9632cbcd764c'),(360,NULL,'d05d6864c717b2388729ef97cdc45f5cecc59297',1,NULL,'4ce82e39-21ff-49c7-95bc-a59b91cfead2'),(361,NULL,'d83a6315d070026a850d479bb55e998bee5474e3',1,NULL,'4ce82e39-21ff-49c7-95bc-a59b91cfead2'),(362,NULL,'c818d7dc838d9a1016a6e84ead9fee4306c8aca2',1,NULL,'4ce82e39-21ff-49c7-95bc-a59b91cfead2'),(363,NULL,'ef06d8cd32f8f1406a9a252800da0c02ec300912',1,NULL,'4ce82e39-21ff-49c7-95bc-a59b91cfead2'),(364,NULL,'9ec5301921916356571a5cbab2953d3af0fac5bd',1,NULL,'4ce82e39-21ff-49c7-95bc-a59b91cfead2'),(364,NULL,'5a88ab134d4e62f2aa2951718ac2c316fca193d2',2,1,'0f8609ab-34a7-4119-8955-b0a2f678e31c'),(364,NULL,'01357e218f3cbe5e7cd0879a09c2f6eaae303c33',3,2,'8ef3ab91-641e-4443-a666-8bb547c3ed37');
/*!40000 ALTER TABLE `entity_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enum_data`
--

DROP TABLE IF EXISTS `enum_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enum_data` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `value` varbinary(255) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci DEFAULT NULL,
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  KEY `enum_ov_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `enum_ov_forkey_ent` (`entity_id`),
  KEY `enum_ov_forkey_pro` (`property_id`),
  CONSTRAINT `enum_ov_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `enum_ov_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `enum_ov_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enum_data`
--

LOCK TABLES `enum_data` WRITE;
/*!40000 ALTER TABLE `enum_data` DISABLE KEYS */;
INSERT INTO `enum_data` VALUES (0,117,106,'TRUE','FIX',1),(0,115,106,'FALSE','FIX',1),(0,146,106,'TRUE','FIX',0),(0,151,106,'FALSE','FIX',1),(0,152,106,'FALSE','FIX',1),(0,153,106,'FALSE','FIX',1),(0,324,249,'TRUE','FIX',0),(0,324,250,'TRUE','FIX',1),(0,324,251,'TRUE','FIX',2),(0,325,249,'FALSE','FIX',0),(0,325,250,'TRUE','FIX',1),(0,325,251,'FALSE','FIX',2),(0,326,249,'TRUE','FIX',0),(0,326,250,'TRUE','FIX',1),(0,326,251,'TRUE','FIX',2);
/*!40000 ALTER TABLE `enum_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feature_config`
--

DROP TABLE IF EXISTS `feature_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feature_config` (
  `_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feature_config`
--

LOCK TABLES `feature_config` WRITE;
/*!40000 ALTER TABLE `feature_config` DISABLE KEYS */;
INSERT INTO `feature_config` VALUES ('ENTITY_VERSIONING','ENABLED');
/*!40000 ALTER TABLE `feature_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `file_id` int(10) unsigned NOT NULL COMMENT 'The file''s ID.',
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Directory of the file.',
  `size` bigint(20) unsigned NOT NULL COMMENT 'Size in kB (oktet bytes).',
  `hash` binary(64) DEFAULT NULL,
  `checked_timestamp` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`file_id`),
  CONSTRAINT `fil_file_id_entity` FOREIGN KEY (`file_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (132,'images/my_first_guitar.png',30874,'�XM�f;G�?}�$��*�b��R�������	�=��zp��k����k$2� �U�x渞���',0),(134,'images/sherlock_holmes_violin.png',24880,'#����Zg��	AG�r⨩��7̒-U�1�̴�S���<���^}��ߍf��F���j$w�~',0),(135,'images/john_lennon_guitar.png',30422,'�+�A\Z��aT�mf	�,��~�S4	�)�yD�n�>+<c�գ��n�b%�����Ã6 ��)�>',0),(137,'uploaded.by/PAM/admin/ffff1361-3130-43fb-b52f-50d7082cb1f7/settings.xlsx',4939,'�Ҵ�������������T��=W�Մ�@�T���Q����o]��I��W��v	\r���itj�P',0),(139,'uploaded.by/PAM/admin/ffff1361-3130-43fb-b52f-50d7082cb1f7/settings.csv',89,'�EC��!�Lqsr�Rc�[i�H0j��o��uD�t�2�D������(O�Kͺ�v��\nsf�n�̰�',0),(140,'uploaded.by/PAM/admin/ffff1361-3130-43fb-b52f-50d7082cb1f7/settings2.csv',90,'vhM�;��rP��yw�EC_x��J0黈�i>=��@Nd_ٝ\r+>�\n���	�?�UW����\\',0),(172,'Analyses/2019-05-16_023/info.yaml',107,'�u}����Ǿ��ZLT4f)��&�v%OE��4���HooҲ��۸g{A���/��c� ',0),(173,'Analyses/2019-05-16_023/settings.xlsx',4939,'�Ҵ�������������T��=W�Մ�@�T���Q����o]��I��W��v	\r���itj�P',0),(174,'Analyses/2019-05-16_023/mode.npy',120128,'����%mJ�R@dm�6X����8>m�#�uH��`x�=�\nm+�F3Ǒ��i����x3�Cu',0),(175,'Analyses/2019-05-16_023/report_g_2019-023.pdf',38130,'@v^x���8��Q&a�,����w]��B�3�	w�Wϊt���G�K�\n(\"g���6�A��=���',0),(176,'Analyses/2019-07-24_024/info.yaml',92,'w�~M��+�-�iN�E�&���U�ߜg\".���.p��e��n��ni�|� �C���NG��w',0),(177,'Analyses/2019-07-24_024/settings.csv',89,'�EC��!�Lqsr�Rc�[i�H0j��o��uD�t�2�D������(O�Kͺ�v��\nsf�n�̰�',0),(178,'Analyses/2019-07-24_024/report_g_2019-024.pdf',39123,'ciR2и��j;C�\0#�f�Ӳ�^k��b$�.�V\'�R��W�8Żl}g�jmZ���5ӊ��(β��',0),(179,'Analyses/2019-07-24_024/mode3.npy',120128,'���G����I��)�z�|�j�\"D��%�����(ԁ)������p�r�Vb�����&�l!�<',0),(180,'Analyses/2019-09-09_025/info.yaml',115,'TW�-�.\r��GTt�,��X�5p͋HO����(�qLT�)DAQY_EE\r��C���p�4P��',0),(181,'Analyses/2019-09-09_025/report_v_2019-025.pdf',39544,'�p̠.�9��[ת�C��vi�D��[�������W��ǘ������M�g����UZc7Ԕ\r\\�',0),(182,'Analyses/2019-09-09_025/settings2.csv',90,'vhM�;��rP��yw�EC_x��J0黈�i>=��@Nd_ٝ\r+>�\n���	�?�UW����\\',0),(183,'Analyses/2019-09-09_025/mode2.npy',120128,'�A��.�%.*Y��K��d��<�=�?>��<Q61������Eo��}��C�/�g;o�j7B',0),(184,'Analyses/2021-03-16_003/mode.npy',120128,'\0/��DR̅r\Z�w���ϐyG�F�\0��Ŧ�git�k\0����)�м>H׬3�E�/��f�;',0),(185,'Analyses/2021-03-16_003/settings.csv',90,'vhM�;��rP��yw�EC_x��J0黈�i>=��@Nd_ٝ\r+>�\n���	�?�UW����\\',0),(186,'Analyses/2021-03-16_003/info.yaml',114,'�6ʻ���iDҳ�ׄ[�v��#�6؝�@PtnaY�A\rws�b	��J����vg|�9!,�\n�',0),(187,'Analyses/2021-03-16_003/report.pdf',18134,'/a�]�85PZc?ށs{�D�z�{��;=+\\�\'���J���f�o�0�u>z\"�!�.2�&��Y ѝJi�',0),(188,'Analyses/2021-03-22_004/settings.csv',90,'vhM�;��rP��yw�EC_x��J0黈�i>=��@Nd_ٝ\r+>�\n���	�?�UW����\\',0),(189,'Analyses/2021-03-22_004/mode.npy',120128,'\r�=���T�Av���iU ��BI�\0�l�$��SA\r*�� ��G`����uc�M�Fv�538x���+',0),(190,'Analyses/2021-03-22_004/info.yaml',96,'<�h\n<Y�%?^�t��6A��m����n&d�>�G��w4/0�	��]L���W-���&��0=',0),(191,'Analyses/2021-03-22_004/report.pdf',17761,'�ޛ*��z��~\n1�ˏ����]�m�=�i*��~p�Y��.#�\n���{�*d�N���H?��x',0),(192,'Analyses/2021-03-21_006/settings.csv',90,'vhM�;��rP��yw�EC_x��J0黈�i>=��@Nd_ٝ\r+>�\n���	�?�UW����\\',0),(193,'Analyses/2021-03-21_006/mode.npy',120128,'�Ϣ�ܰ���}]՘I*>�b�2�j�a����D|&S�o�P�yA�qBeq,r�L�o����� ',0),(194,'Analyses/2021-03-21_006/info.yaml',103,'�\nP�c6��k$�A6������	�Tae�h�Cp�@l�;ޔ�I������ě,=��ᗓ/',0),(195,'Analyses/2021-03-21_006/report.pdf',15549,'�At�<���~KXӘ[A-g�MI�NYA\0(ip��j~h�\r�ʴ#E�m�<j���c����y_���',0),(196,'Analyses/2021-03-20_007/settings.csv',90,'vhM�;��rP��yw�EC_x��J0黈�i>=��@Nd_ٝ\r+>�\n���	�?�UW����\\',0),(197,'Analyses/2021-03-20_007/mode.npy',120128,'F��5�N��MSJU�b�S gv.Z�����E��n@\Z��*�\"}��4�$���*�tǿ',0),(198,'Analyses/2021-03-20_007/info.yaml',101,'��A[c`:\Z1�Sd�a�1\0�J�E@̦�z�_�j���F������^$��+���߂q',0),(199,'Analyses/2021-03-20_007/report.pdf',17952,' ��\\w�Ʒ��H�{Ո ���fz`_�sf���V�^D��0�,\r@�$�a�b�,�',0),(200,'Analyses/2021-03-19_009/settings.csv',90,'vhM�;��rP��yw�EC_x��J0黈�i>=��@Nd_ٝ\r+>�\n���	�?�UW����\\',0),(201,'Analyses/2021-03-19_009/mode.npy',120128,'���ヌA$/��y����3X�jN/�gW�+xYJ�|��v��\Z��@��ʲ�����1',0),(202,'Analyses/2021-03-19_009/info.yaml',114,'t�Jk���˟V���Y�/w\'p(x�h��3�&V~�(}o�����}�\0Ǫ�P�9*,��5�\0',0),(203,'Analyses/2021-03-19_009/report.pdf',17902,'�|Cv�]��@`�\Z\Zr�48VK�\\#,�	m��|Ѭ2	�U\"�d��̾lM&0B?˝$cu�',0),(204,'Analyses/2021-03-20_012/settings.csv',90,'vhM�;��rP��yw�EC_x��J0黈�i>=��@Nd_ٝ\r+>�\n���	�?�UW����\\',0),(205,'Analyses/2021-03-20_012/mode.npy',120128,'��(��ir��؉~�=a�7h�!I�xkn�������x��:��Unc0w��3�[~��\' ��+',0),(206,'Analyses/2021-03-20_012/info.yaml',105,'Qb5@��c�~�^.O�(�r���e֢ʦ��R��䢗��ì�yi(=����C�\\�z���(��',0),(207,'Analyses/2021-03-20_012/report.pdf',16988,']ӳ��s\0��&�AKt�1�\\�i�;�Ӭz�`�����8G%K���AJJ��e>葝\0Dx�N����br',0),(208,'Analyses/2021-03-16_019/settings.csv',90,'vhM�;��rP��yw�EC_x��J0黈�i>=��@Nd_ٝ\r+>�\n���	�?�UW����\\',0),(209,'Analyses/2021-03-16_019/mode.npy',120128,'��8���]�+I}�s�6��s���^��f�l�㒜��(l0����Ñ�bIr1��n�i�oADz',0),(210,'Analyses/2021-03-16_019/info.yaml',106,'���w��.�s��Q�(D���;=K��$ ��A���\Z�7�qQ���j�d��OvO�){d��l!V2',0),(211,'Analyses/2021-03-16_019/report.pdf',16350,'�>]�3(K|4�u�2�=��=����:���p�8}�.D�@����T����7�A!4�c��#',0),(212,'Analyses/unknown_file.txt',14,'%��~��^�f莠���@/Q-��wTL��yDZ�V���w����U�ou��OÜ7F%�ȯJ����c',0),(321,'Bikes/2021-04-21_qc_report.csv',60,'R��ہ�1Ɋ+ɯ~53l*5i��~�����wHS�T�|�?R�lR�_,�ܚ/����Y�6E`8���',0),(322,'Bikes/2021-04-22_qc_report.csv',62,'�����K��k*�@eK����M��i1$�X\ZIkn�O|��xD��\"�4���C�wՃ��]',0),(323,'Bikes/2021-04-23_qc_report.csv',60,'R��ہ�1Ɋ+ɯ~53l*5i��~�����wHS�T�|�?R�lR�_,�ܚ/����Y�6E`8���',0),(339,'Tensile Stress Analyses/soft_sample.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',0),(340,'Tensile Stress Analyses/hard_sample.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',0),(355,'Analyses/2019-05-20_021/info.yaml',102,'����Z��6������z��Y�\0#��tsѴ>Co�Z\0��S#swyDA��#9F�{���%',0),(356,'Analyses/2019-05-20_021/mode.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',0),(357,'Analyses/2019-05-20_021/settings.csv',89,'�EC��!�Lqsr�Rc�[i�H0j��o��uD�t�2�D������(O�Kͺ�v��\nsf�n�̰�',0),(358,'Analyses/2019-05-20_021/report_g_2019-021.pdf',38130,'@v^x���8��Q&a�,����w]��B�3�	w�Wϊt���G�K�\n(\"g���6�A��=���',0),(360,'Analyses/2019-07-24_022/info.yaml',101,'�0m1����\n�@��1�R����Ni:�O��/#z}>	�����D�+W4ǵ�r��<�(�~�g*�k�',0),(361,'Analyses/2019-07-24_022/mode.npy',120128,'�b�H��K���>�W�@(���)i�j�fr���ON\'@Z-7���תr@wp�*��(Z�(�',0),(362,'Analyses/2019-07-24_022/settings.csv',89,'�EC��!�Lqsr�Rc�[i�H0j��o��uD�t�2�D������(O�Kͺ�v��\nsf�n�̰�',0),(363,'Analyses/2019-07-24_022/report_g_2019-022.pdf',39123,'ciR2и��j;C�\0#�f�Ӳ�^k��b$�.�V\'�R��W�8Żl}g�jmZ���5ӊ��(β��',0);
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `group_id_entities_id` (`group_id`),
  CONSTRAINT `group_id_entities_id` FOREIGN KEY (`group_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `user_id_entities_id` FOREIGN KEY (`user_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `integer_data`
--

DROP TABLE IF EXISTS `integer_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integer_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `value` bigint(20) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  `unit_sig` bigint(20) DEFAULT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `int_entity_id_entity` (`entity_id`),
  KEY `int_property_id_entity` (`property_id`),
  CONSTRAINT `int_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `int_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `int_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `integer_data`
--

LOCK TABLES `integer_data` WRITE;
/*!40000 ALTER TABLE `integer_data` DISABLE KEYS */;
INSERT INTO `integer_data` VALUES (0,305,242,7,'FIX',1,NULL),(0,306,242,10,'FIX',1,NULL),(0,307,242,12,'FIX',1,NULL),(0,308,242,3,'FIX',1,NULL),(0,309,242,1,'FIX',1,NULL);
/*!40000 ALTER TABLE `integer_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `isa`
--

DROP TABLE IF EXISTS `isa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `isa` (
  `child` int(10) unsigned NOT NULL COMMENT 'Child',
  `parent` int(10) unsigned NOT NULL COMMENT 'Parent',
  `type` enum('INHERITANCE','SUBTYPING') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Type of is-a relation.',
  UNIQUE KEY `child` (`child`,`parent`),
  KEY `parent_entity` (`parent`),
  CONSTRAINT `child_entity` FOREIGN KEY (`child`) REFERENCES `entities` (`id`),
  CONSTRAINT `parent_entity` FOREIGN KEY (`parent`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `isa`
--

LOCK TABLES `isa` WRITE;
/*!40000 ALTER TABLE `isa` DISABLE KEYS */;
/*!40000 ALTER TABLE `isa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `isa_cache`
--

DROP TABLE IF EXISTS `isa_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `isa_cache` (
  `child` int(10) unsigned NOT NULL,
  `parent` int(10) unsigned NOT NULL,
  `rpath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`child`,`parent`,`rpath`),
  KEY `isa_cache_parent_entity` (`parent`),
  CONSTRAINT `isa_cache_child_entity` FOREIGN KEY (`child`) REFERENCES `entities` (`id`),
  CONSTRAINT `isa_cache_parent_entity` FOREIGN KEY (`parent`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `isa_cache`
--

LOCK TABLES `isa_cache` WRITE;
/*!40000 ALTER TABLE `isa_cache` DISABLE KEYS */;
INSERT INTO `isa_cache` VALUES (109,108,'109'),(110,108,'110'),(115,108,'110'),(115,110,'115'),(116,107,'116'),(117,108,'110'),(117,110,'117'),(119,107,'119'),(120,108,'109'),(120,109,'120'),(121,111,'121'),(122,111,'122'),(123,112,'123'),(124,112,'124'),(125,112,'125'),(132,131,'132'),(134,131,'134'),(135,131,'135'),(137,136,'137'),(139,136,'139'),(140,136,'140'),(146,108,'110'),(146,110,'146'),(148,108,'109'),(148,109,'148'),(149,108,'109'),(149,109,'149'),(150,108,'109'),(150,109,'150'),(151,108,'110'),(151,110,'151'),(152,108,'110'),(152,110,'152'),(153,108,'110'),(153,110,'153'),(154,111,'154'),(155,111,'155'),(156,111,'156'),(157,111,'157'),(173,136,'173'),(174,141,'174'),(177,136,'177'),(179,141,'179'),(182,136,'182'),(183,141,'183'),(184,141,'184'),(189,141,'189'),(193,141,'193'),(197,141,'197'),(201,141,'201'),(205,141,'205'),(209,141,'209'),(221,213,'221'),(222,213,'222'),(223,213,'223'),(224,215,'224'),(225,215,'225'),(226,215,'226'),(227,215,'227'),(228,215,'228'),(229,214,'229'),(233,232,'233'),(243,241,'243'),(244,241,'244'),(257,255,'257'),(258,255,'258'),(262,259,'262'),(269,255,'269'),(270,259,'270'),(274,255,'274'),(275,255,'275'),(279,276,'279'),(295,255,'295'),(296,276,'296'),(298,237,'298'),(299,237,'299'),(300,237,'300'),(301,107,'301'),(302,107,'302'),(303,107,'303'),(304,107,'304'),(305,241,'244'),(305,244,'305'),(306,241,'244'),(306,244,'306'),(307,241,'244'),(307,244,'307'),(308,241,'243'),(308,243,'308'),(309,241,'243'),(309,243,'309'),(310,246,'310'),(311,246,'311'),(312,246,'312'),(313,253,'313'),(314,253,'314'),(315,253,'315'),(316,255,'257'),(316,257,'316'),(317,255,'258'),(317,258,'317'),(318,259,'262'),(318,262,'318'),(319,259,'262'),(319,262,'319'),(320,259,'262'),(320,262,'320'),(324,248,'324'),(325,248,'325'),(326,248,'326'),(327,266,'327'),(328,266,'328'),(329,266,'329'),(330,237,'330'),(331,237,'331'),(332,237,'332'),(333,237,'333'),(334,107,'334'),(335,107,'335'),(336,255,'274'),(336,274,'336'),(337,255,'274'),(337,274,'337'),(338,255,'275'),(338,275,'338'),(339,272,'339'),(340,272,'340'),(341,276,'279'),(341,279,'341'),(342,276,'279'),(342,279,'342'),(343,276,'343'),(344,273,'344'),(345,273,'345'),(346,273,'346'),(347,273,'347'),(348,282,'348'),(349,282,'349'),(350,286,'350'),(351,286,'351'),(352,290,'352'),(353,290,'353'),(354,291,'354'),(356,141,'356'),(357,136,'357'),(359,112,'359'),(361,141,'361'),(362,136,'362'),(364,112,'364');
/*!40000 ALTER TABLE `isa_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logging`
--

DROP TABLE IF EXISTS `logging`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logging` (
  `level` int(11) NOT NULL,
  `logger` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `millis` bigint(20) NOT NULL,
  `logRecord` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logging`
--

LOCK TABLES `logging` WRITE;
/*!40000 ALTER TABLE `logging` DISABLE KEYS */;
/*!40000 ALTER TABLE `logging` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `name_data`
--

DROP TABLE IF EXISTS `name_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `name_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  UNIQUE KEY `domain_id_2` (`domain_id`,`entity_id`,`property_id`),
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  KEY `value` (`value`),
  CONSTRAINT `name_data_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `name_data_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `name_data_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `name_data`
--

LOCK TABLES `name_data` WRITE;
/*!40000 ALTER TABLE `name_data` DISABLE KEYS */;
INSERT INTO `name_data` VALUES (0,0,20,'DOMAIN','FIX',0),(0,1,20,'RECORDTYPE','FIX',0),(0,2,20,'RECORD','FIX',0),(0,3,20,'FILE','FIX',0),(0,4,20,'PROPERTY','FIX',0),(0,7,20,'DATATYPE','FIX',0),(0,8,20,'QUERYTEMPLATE','FIX',0),(0,11,20,'REFERENCE','FIX',0),(0,12,20,'INTEGER','FIX',0),(0,13,20,'DOUBLE','FIX',0),(0,14,20,'TEXT','FIX',0),(0,15,20,'DATETIME','FIX',0),(0,16,20,'TIMESPAN','FIX',0),(0,17,20,'FILE','FIX',0),(0,18,20,'BOOLEAN','FIX',0),(0,20,20,'name','FIX',0),(0,21,20,'unit','FIX',0),(0,24,20,'description','FIX',0),(0,50,20,'SQLITE','FIX',0),(0,100,20,'price','FIX',0),(0,101,20,'quality_factor','FIX',0),(0,102,20,'date','FIX',0),(0,103,20,'visitors','FIX',0),(0,104,20,'ticket_sales','FIX',0),(0,105,20,'report','FIX',0),(0,106,20,'electric','FIX',0),(0,107,20,'Manufacturer','FIX',0),(0,108,20,'MusicalInstrument','FIX',0),(0,109,20,'Violin','FIX',0),(0,110,20,'Guitar','FIX',0),(0,111,20,'SoundQualityAnalyzer','FIX',0),(0,112,20,'MusicalAnalysis','FIX',0),(0,115,20,'My first guitar','FIX',0),(0,116,20,'Gibson','FIX',0),(0,117,20,'Nice guitar','FIX',0),(0,119,20,'Antonio Stradivari','FIX',0),(0,120,20,'Sherlock Holmes\' violin','FIX',0),(0,121,20,'Harmonics Scanner HS 142','FIX',0),(0,122,20,'Riff-o-Tron 4','FIX',0),(0,129,20,'longitude','FIX',0),(0,130,20,'latitude','FIX',0),(0,131,20,'Photograph','FIX',0),(0,132,20,'my_first_guitar.png','FIX',0),(0,134,20,'sherlock_holmes_violin.png','FIX',0),(0,135,20,'john_lennon_guitar.png','FIX',0),(0,136,20,'SettingsTable','FIX',0),(0,137,20,'settings.xlsx','FIX',0),(0,139,20,'settings.csv','FIX',0),(0,140,20,'settings2.csv','FIX',0),(0,141,20,'RiffRecording','FIX',0),(0,146,20,'John Lennon\'s guitar','FIX',0),(0,147,20,'generated','FIX',0),(0,148,20,'Susan\'s Violin','FIX',0),(0,149,20,'Linda\'s Violin','FIX',0),(0,150,20,'Michael\'s Violin','FIX',0),(0,151,20,'James\'s Guitar','FIX',0),(0,152,20,'David\'s Guitar','FIX',0),(0,153,20,'Richard\'s Guitar','FIX',0),(0,154,20,'Non-destructive Harmonizer 3400','FIX',0),(0,155,20,'Holographic Stringalyzer alpha','FIX',0),(0,156,20,'Inexplicable SymphiTec 5','FIX',0),(0,157,20,'Relativistic MusiCorp 5','FIX',0),(0,172,20,'info.yaml','FIX',0),(0,173,20,'settings.xlsx','FIX',0),(0,174,20,'mode.npy','FIX',0),(0,175,20,'report_g_2019-023.pdf','FIX',0),(0,176,20,'info.yaml','FIX',0),(0,177,20,'settings.csv','FIX',0),(0,178,20,'report_g_2019-024.pdf','FIX',0),(0,179,20,'mode3.npy','FIX',0),(0,180,20,'info.yaml','FIX',0),(0,181,20,'report_v_2019-025.pdf','FIX',0),(0,182,20,'settings2.csv','FIX',0),(0,183,20,'mode2.npy','FIX',0),(0,184,20,'mode.npy','FIX',0),(0,185,20,'settings.csv','FIX',0),(0,186,20,'info.yaml','FIX',0),(0,187,20,'report.pdf','FIX',0),(0,188,20,'settings.csv','FIX',0),(0,189,20,'mode.npy','FIX',0),(0,190,20,'info.yaml','FIX',0),(0,191,20,'report.pdf','FIX',0),(0,192,20,'settings.csv','FIX',0),(0,193,20,'mode.npy','FIX',0),(0,194,20,'info.yaml','FIX',0),(0,195,20,'report.pdf','FIX',0),(0,196,20,'settings.csv','FIX',0),(0,197,20,'mode.npy','FIX',0),(0,198,20,'info.yaml','FIX',0),(0,199,20,'report.pdf','FIX',0),(0,200,20,'settings.csv','FIX',0),(0,201,20,'mode.npy','FIX',0),(0,202,20,'info.yaml','FIX',0),(0,203,20,'report.pdf','FIX',0),(0,204,20,'settings.csv','FIX',0),(0,205,20,'mode.npy','FIX',0),(0,206,20,'info.yaml','FIX',0),(0,207,20,'report.pdf','FIX',0),(0,208,20,'settings.csv','FIX',0),(0,209,20,'mode.npy','FIX',0),(0,210,20,'info.yaml','FIX',0),(0,211,20,'report.pdf','FIX',0),(0,212,20,'unknown_file.txt','FIX',0),(0,213,20,'State','FIX',0),(0,214,20,'StateModel','FIX',0),(0,215,20,'Transition','FIX',0),(0,216,20,'from','FIX',0),(0,217,20,'to','FIX',0),(0,218,20,'initial','FIX',0),(0,219,20,'final','FIX',0),(0,220,20,'color','FIX',0),(0,221,20,'Unpublished','FIX',0),(0,222,20,'Under Review','FIX',0),(0,223,20,'Published','FIX',0),(0,224,20,'Start Review','FIX',0),(0,225,20,'Publish','FIX',0),(0,226,20,'Unpublish','FIX',0),(0,227,20,'Reject','FIX',0),(0,228,20,'Edit','FIX',0),(0,229,20,'Publish Life-cycle','FIX',0),(0,230,20,'comment','FIX',0),(0,231,20,'annotationOf','FIX',0),(0,232,20,'Annotation','FIX',0),(0,233,20,'CommentAnnotation','FIX',0),(0,234,20,'Query','FIX',0),(0,235,20,'templateDescription','FIX',0),(0,236,20,'UserTemplate','FIX',0),(0,237,20,'Person','FIX',0),(0,238,20,'email_address','FIX',0),(0,239,20,'address','FIX',0),(0,240,20,'contact','FIX',0),(0,241,20,'Gear','FIX',0),(0,242,20,'sprockets','FIX',0),(0,243,20,'FrontGear','FIX',0),(0,244,20,'BackGear','FIX',0),(0,245,20,'diameter','FIX',0),(0,246,20,'Frame','FIX',0),(0,247,20,'weight','FIX',0),(0,248,20,'QCResult','FIX',0),(0,249,20,'passed','FIX',0),(0,250,20,'front_gear_ok','FIX',0),(0,251,20,'back_gear_ok','FIX',0),(0,252,20,'qc_report','FIX',0),(0,253,20,'Bicycle','FIX',0),(0,254,20,'serial_no','FIX',0),(0,255,20,'Machine','FIX',0),(0,256,20,'production_date','FIX',0),(0,257,20,'BikeAssemblyMachine','FIX',0),(0,258,20,'QCDevice','FIX',0),(0,259,20,'Assembly','FIX',0),(0,260,20,'assembly_date','FIX',0),(0,261,20,'operator','FIX',0),(0,262,20,'BikeAssembly','FIX',0),(0,263,20,'assembly_speed','FIX',0),(0,264,20,'firmware_version','FIX',0),(0,265,20,'temperature','FIX',0),(0,266,20,'QualityControl','FIX',0),(0,267,20,'Motor','FIX',0),(0,268,20,'power','FIX',0),(0,269,20,'EBikeAssemblyMachine','FIX',0),(0,270,20,'EBikeAssembly','FIX',0),(0,271,20,'motor_firmware_version','FIX',0),(0,272,20,'RawTensileAnalysisData','FIX',0),(0,273,20,'SourceMaterial','FIX',0),(0,274,20,'ProductionMachine','FIX',0),(0,275,20,'TensileAnalysisDevice','FIX',0),(0,276,20,'SOP','FIX',0),(0,277,20,'author','FIX',0),(0,278,20,'procedure','FIX',0),(0,279,20,'SimpleProcedure','FIX',0),(0,280,20,'preparation_temperature','FIX',0),(0,281,20,'analysis_frequency','FIX',0),(0,282,20,'Preparation','FIX',0),(0,283,20,'temperature_set','FIX',0),(0,284,20,'temperature_measured','FIX',0),(0,285,20,'supervisor','FIX',0),(0,286,20,'TensileAnalysis','FIX',0),(0,287,20,'frequency_set','FIX',0),(0,288,20,'frequency_measured','FIX',0),(0,289,20,'tensile_strength','FIX',0),(0,290,20,'Sample','FIX',0),(0,291,20,'Study','FIX',0),(0,292,20,'start_date','FIX',0),(0,293,20,'end_date','FIX',0),(0,294,20,'responsible','FIX',0),(0,295,20,'ImprovedTensileAnalysisDevice','FIX',0),(0,296,20,'ImprovedProcedure','FIX',0),(0,297,20,'analysis_orientation','FIX',0),(0,298,20,'Frauke Fahrrad','FIX',0),(0,299,20,'Manfred Manufacturer','FIX',0),(0,300,20,'Valentin Vélo','FIX',0),(0,301,20,'Budget Bikes','FIX',0),(0,302,20,'Top Mountainbikes Ltd.','FIX',0),(0,303,20,'Easy Assemblies','FIX',0),(0,304,20,'Bike QC AG','FIX',0),(0,305,20,'Simple back','FIX',0),(0,306,20,'Trekking back','FIX',0),(0,307,20,'MTB 12 speed','FIX',0),(0,308,20,'simple front','FIX',0),(0,309,20,'MTB front','FIX',0),(0,310,20,'Small frame','FIX',0),(0,311,20,'Trekking frame','FIX',0),(0,312,20,'MTB frame','FIX',0),(0,313,20,'Children\'s bike','FIX',0),(0,314,20,'Nice trekking bike','FIX',0),(0,315,20,'Lightweight MTB','FIX',0),(0,316,20,'Easy Assemblies Assemblebot 9000','FIX',0),(0,317,20,'BQC S123','FIX',0),(0,321,20,'Report: children\'s bike','FIX',0),(0,322,20,'Report: nice trekking bike','FIX',0),(0,323,20,'Report: lightweight MTB','FIX',0),(0,330,20,'Simone Scientist','FIX',0),(0,331,20,'Sebastian Sample','FIX',0),(0,332,20,'Lucas Labassistant','FIX',0),(0,333,20,'Dorothee Device','FIX',0),(0,334,20,'Prima Proben AG','FIX',0),(0,335,20,'Scientific Devices Ltd','FIX',0),(0,336,20,'Sampl-o-Tron 5000','FIX',0),(0,337,20,'TT Shak-o-matic','FIX',0),(0,338,20,'RheoTens 500','FIX',0),(0,339,20,'soft_sample.npy','FIX',0),(0,340,20,'hard_sample.npy','FIX',0),(0,341,20,'Biopolymer preparation procedure','FIX',0),(0,342,20,'Oscillatory tensile stress test','FIX',0),(0,343,20,'Tensile stress response of biopolymers','FIX',0),(0,344,20,'PBS','FIX',0),(0,345,20,'NaOH','FIX',0),(0,346,20,'Bovine Collagen I','FIX',0),(0,347,20,'P-Fibrin','FIX',0),(0,348,20,'SoftSamplePreparation','FIX',0),(0,349,20,'HardSamplePreparation','FIX',0),(0,350,20,'SoftSampleAnalysis','FIX',0),(0,351,20,'HardSampleAnalysis','FIX',0),(0,352,20,'SoftSample','FIX',0),(0,353,20,'HardSample','FIX',0),(0,354,20,'Yield-stress study','FIX',0);
/*!40000 ALTER TABLE `name_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `name_overrides`
--

DROP TABLE IF EXISTS `name_overrides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `name_overrides` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `name_ov_ukey` (`domain_id`,`entity_id`,`property_id`),
  KEY `name_ov_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `name_ov_forkey_ent` (`entity_id`),
  KEY `name_ov_forkey_pro` (`property_id`),
  CONSTRAINT `name_ov_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `name_ov_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `name_ov_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `name_overrides`
--

LOCK TABLES `name_overrides` WRITE;
/*!40000 ALTER TABLE `name_overrides` DISABLE KEYS */;
INSERT INTO `name_overrides` VALUES (0,301,113,'Contact'),(0,302,113,'Contact'),(0,303,113,'Contact'),(0,304,113,'Contact');
/*!40000 ALTER TABLE `name_overrides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `null_data`
--

DROP TABLE IF EXISTS `null_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `null_data` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci DEFAULT NULL,
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  KEY `null_data_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `null_forkey_ent` (`entity_id`),
  KEY `null_forkey_pro` (`property_id`),
  CONSTRAINT `null_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `null_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `null_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `null_data`
--

LOCK TABLES `null_data` WRITE;
/*!40000 ALTER TABLE `null_data` DISABLE KEYS */;
INSERT INTO `null_data` VALUES (0,108,100,'RECOMMENDED',0),(0,108,113,'RECOMMENDED',0),(0,109,100,'RECOMMENDED',0),(0,109,113,'RECOMMENDED',0),(0,110,106,'RECOMMENDED',0),(0,110,100,'RECOMMENDED',1),(0,110,113,'RECOMMENDED',0),(0,232,231,'OBLIGATORY',0),(0,233,230,'OBLIGATORY',0),(0,233,231,'OBLIGATORY',0),(0,236,234,'OBLIGATORY',0),(0,236,235,'OBLIGATORY',1),(0,112,101,'RECOMMENDED',0),(0,112,102,'RECOMMENDED',1),(0,112,105,'RECOMMENDED',2),(0,112,113,'RECOMMENDED',0),(0,112,114,'RECOMMENDED',0),(0,237,238,'RECOMMENDED',0),(0,241,107,'RECOMMENDED',0),(0,241,242,'RECOMMENDED',1),(0,244,245,'RECOMMENDED',0),(0,246,107,'RECOMMENDED',0),(0,246,247,'RECOMMENDED',1),(0,248,249,'RECOMMENDED',0),(0,248,250,'RECOMMENDED',1),(0,248,251,'RECOMMENDED',2),(0,248,252,'RECOMMENDED',3),(0,253,254,'RECOMMENDED',0),(0,253,248,'RECOMMENDED',1),(0,253,246,'RECOMMENDED',2),(0,253,243,'RECOMMENDED',3),(0,253,244,'RECOMMENDED',4),(0,255,107,'RECOMMENDED',0),(0,255,256,'RECOMMENDED',1),(0,259,260,'RECOMMENDED',0),(0,259,261,'RECOMMENDED',1),(0,259,253,'RECOMMENDED',2),(0,262,257,'RECOMMENDED',0),(0,262,263,'RECOMMENDED',1),(0,262,264,'RECOMMENDED',2),(0,262,265,'RECOMMENDED',3),(0,262,246,'RECOMMENDED',4),(0,262,243,'RECOMMENDED',5),(0,262,244,'RECOMMENDED',6),(0,266,258,'RECOMMENDED',0),(0,266,264,'RECOMMENDED',1),(0,266,261,'RECOMMENDED',2),(0,266,253,'RECOMMENDED',3),(0,266,248,'RECOMMENDED',4),(0,267,107,'RECOMMENDED',0),(0,267,247,'RECOMMENDED',1),(0,267,268,'RECOMMENDED',2),(0,270,257,'RECOMMENDED',0),(0,270,263,'RECOMMENDED',1),(0,270,264,'RECOMMENDED',2),(0,270,265,'RECOMMENDED',3),(0,270,246,'RECOMMENDED',4),(0,270,267,'RECOMMENDED',5),(0,270,271,'RECOMMENDED',6),(0,270,244,'RECOMMENDED',7),(0,273,107,'RECOMMENDED',0),(0,273,254,'RECOMMENDED',1),(0,276,277,'RECOMMENDED',0),(0,276,278,'RECOMMENDED',1),(0,279,280,'RECOMMENDED',0),(0,279,281,'RECOMMENDED',1),(0,282,273,'RECOMMENDED',0),(0,282,276,'RECOMMENDED',1),(0,282,274,'RECOMMENDED',2),(0,282,283,'RECOMMENDED',3),(0,282,284,'RECOMMENDED',4),(0,282,285,'RECOMMENDED',5),(0,286,276,'RECOMMENDED',0),(0,286,275,'RECOMMENDED',1),(0,286,285,'RECOMMENDED',2),(0,286,287,'RECOMMENDED',3),(0,286,288,'RECOMMENDED',4),(0,286,220,'RECOMMENDED',5),(0,286,289,'RECOMMENDED',6),(0,286,272,'RECOMMENDED',7),(0,290,282,'RECOMMENDED',0),(0,290,286,'RECOMMENDED',1),(0,291,292,'RECOMMENDED',0),(0,291,293,'RECOMMENDED',1),(0,291,276,'RECOMMENDED',2),(0,291,294,'RECOMMENDED',3),(0,291,290,'RECOMMENDED',4),(0,296,280,'RECOMMENDED',0),(0,296,297,'RECOMMENDED',1),(0,107,239,'RECOMMENDED',0),(0,107,240,'RECOMMENDED',1),(0,107,130,'RECOMMENDED',2),(0,107,129,'RECOMMENDED',3);
/*!40000 ALTER TABLE `null_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passwd`
--

DROP TABLE IF EXISTS `passwd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passwd` (
  `principal` varbinary(255) NOT NULL,
  `hash` varbinary(255) NOT NULL,
  `alg` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SHA-512',
  `it` int(10) unsigned DEFAULT 5000,
  `salt` varbinary(255) NOT NULL,
  PRIMARY KEY (`principal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passwd`
--

LOCK TABLES `passwd` WRITE;
/*!40000 ALTER TABLE `passwd` DISABLE KEYS */;
/*!40000 ALTER TABLE `passwd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passwords`
--

DROP TABLE IF EXISTS `passwords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passwords` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'User ID.',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Password.',
  PRIMARY KEY (`entity_id`),
  CONSTRAINT `use_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passwords`
--

LOCK TABLES `passwords` WRITE;
/*!40000 ALTER TABLE `passwords` DISABLE KEYS */;
INSERT INTO `passwords` VALUES (98,'37d7bd8a833261b4e4653644ee0a065f522b92b3738ca9ae2cb43a83844bf352c4a59c386a44965997a508c61988c9484c093775027425091d6d3d435c3c0e0c'),(99,'37d7bd8a833261b4e4653644ee0a065f522b92b3738ca9ae2cb43a83844bf352c4a59c386a44965997a508c61988c9484c093775027425091d6d3d435c3c0e0c');
/*!40000 ALTER TABLE `passwords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `role` varbinary(255) NOT NULL,
  `permissions` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`role`),
  CONSTRAINT `perm_name_roles` FOREIGN KEY (`role`) REFERENCES `roles` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES ('administration','[{\"grant\":\"true\",\"priority\":\"true\",\"permission\":\"*\"}]'),('anonymous','[{\"permission\":\"SCRIPTING:EXECUTE:ext_table_preview:pandas_table_preview.py\",\"priority\":\"false\",\"grant\":\"true\"},{\"permission\":\"SCRIPTING:EXECUTE:showcase.py\",\"priority\":\"false\",\"grant\":\"true\"}]');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `query_template_def`
--

DROP TABLE IF EXISTS `query_template_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `query_template_def` (
  `id` int(10) unsigned NOT NULL,
  `definition` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `query_template_def_ibfk_1` FOREIGN KEY (`id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `query_template_def`
--

LOCK TABLES `query_template_def` WRITE;
/*!40000 ALTER TABLE `query_template_def` DISABLE KEYS */;
/*!40000 ALTER TABLE `query_template_def` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reference_data`
--

DROP TABLE IF EXISTS `reference_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reference_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `value` int(10) unsigned NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  `value_iversion` int(10) unsigned DEFAULT NULL,
  KEY `entity_id` (`entity_id`,`property_id`),
  KEY `ref_domain_id_entity` (`domain_id`),
  KEY `ref_property_id_entity` (`property_id`),
  KEY `ref_value_entity` (`value`),
  KEY `value` (`value`,`value_iversion`),
  CONSTRAINT `ref_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `ref_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `ref_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `ref_value_entity` FOREIGN KEY (`value`) REFERENCES `entities` (`id`),
  CONSTRAINT `reference_data_ibfk_1` FOREIGN KEY (`value`, `value_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reference_data`
--

LOCK TABLES `reference_data` WRITE;
/*!40000 ALTER TABLE `reference_data` DISABLE KEYS */;
INSERT INTO `reference_data` VALUES (0,108,107,113,'REPLACEMENT',1,NULL),(0,117,107,113,'REPLACEMENT',2,NULL),(0,117,113,116,'FIX',0,NULL),(0,115,131,113,'REPLACEMENT',2,NULL),(0,115,113,132,'FIX',0,NULL),(0,120,107,113,'REPLACEMENT',1,NULL),(0,120,113,119,'FIX',0,NULL),(0,120,131,114,'REPLACEMENT',2,NULL),(0,120,114,134,'FIX',0,NULL),(0,146,107,113,'REPLACEMENT',2,NULL),(0,146,113,116,'FIX',0,NULL),(0,146,131,114,'REPLACEMENT',3,NULL),(0,146,114,135,'FIX',0,NULL),(0,109,107,113,'REPLACEMENT',1,NULL),(0,110,107,113,'REPLACEMENT',2,NULL),(0,224,216,221,'FIX',0,NULL),(0,224,217,222,'FIX',1,NULL),(0,225,216,222,'FIX',0,NULL),(0,225,217,223,'FIX',1,NULL),(0,226,216,223,'FIX',0,NULL),(0,226,217,221,'FIX',1,NULL),(0,227,216,222,'FIX',0,NULL),(0,227,217,221,'FIX',1,NULL),(0,228,216,221,'FIX',0,NULL),(0,228,217,221,'FIX',1,NULL),(0,229,215,113,'REPLACEMENT',0,NULL),(0,229,113,224,'FIX',1,NULL),(0,229,113,227,'FIX',2,NULL),(0,229,113,225,'FIX',3,NULL),(0,229,113,226,'FIX',4,NULL),(0,229,113,228,'FIX',0,NULL),(0,229,218,221,'FIX',1,NULL),(0,229,219,221,'FIX',2,NULL),(0,125,108,113,'REPLACEMENT',0,NULL),(0,125,113,120,'FIX',0,NULL),(0,125,111,114,'REPLACEMENT',1,NULL),(0,125,114,121,'FIX',0,NULL),(0,125,105,181,'FIX',4,NULL),(0,125,136,138,'REPLACEMENT',5,NULL),(0,125,138,182,'FIX',0,NULL),(0,125,141,143,'REPLACEMENT',6,NULL),(0,125,143,183,'FIX',0,NULL),(0,125,213,223,'FIX',0,NULL),(0,123,108,113,'REPLACEMENT',0,NULL),(0,123,113,115,'FIX',0,NULL),(0,123,111,114,'REPLACEMENT',1,NULL),(0,123,114,121,'FIX',0,NULL),(0,123,105,175,'FIX',4,NULL),(0,123,136,138,'REPLACEMENT',5,NULL),(0,123,138,173,'FIX',0,NULL),(0,123,141,143,'REPLACEMENT',6,NULL),(0,123,143,174,'FIX',0,NULL),(0,123,213,221,'FIX',0,NULL),(0,124,108,113,'REPLACEMENT',0,NULL),(0,124,113,117,'FIX',0,NULL),(0,124,111,114,'REPLACEMENT',1,NULL),(0,124,114,122,'FIX',0,NULL),(0,124,105,178,'FIX',4,NULL),(0,124,136,138,'REPLACEMENT',5,NULL),(0,124,138,177,'FIX',0,NULL),(0,124,141,143,'REPLACEMENT',6,NULL),(0,124,143,179,'FIX',0,NULL),(0,124,213,222,'FIX',0,NULL),(0,112,111,113,'REPLACEMENT',3,NULL),(0,112,108,114,'REPLACEMENT',4,NULL),(0,112,213,221,'FIX',0,NULL),(0,301,240,113,'REPLACEMENT',1,NULL),(0,301,113,299,'FIX',0,NULL),(0,302,240,113,'REPLACEMENT',1,NULL),(0,302,113,300,'FIX',0,NULL),(0,303,240,113,'REPLACEMENT',1,NULL),(0,303,113,299,'FIX',0,NULL),(0,304,240,113,'REPLACEMENT',1,NULL),(0,304,113,298,'FIX',0,NULL),(0,305,107,301,'FIX',0,NULL),(0,306,107,301,'FIX',0,NULL),(0,307,107,302,'FIX',0,NULL),(0,308,107,301,'FIX',0,NULL),(0,309,107,302,'FIX',0,NULL),(0,310,107,301,'FIX',0,NULL),(0,311,107,301,'FIX',0,NULL),(0,312,107,302,'FIX',0,NULL),(0,316,107,303,'FIX',0,NULL),(0,317,107,304,'FIX',0,NULL),(0,318,261,298,'FIX',1,NULL),(0,318,253,313,'FIX',2,NULL),(0,318,257,316,'FIX',3,NULL),(0,318,246,310,'FIX',7,NULL),(0,318,243,308,'FIX',8,NULL),(0,318,244,305,'FIX',9,NULL),(0,319,261,298,'FIX',1,NULL),(0,319,253,314,'FIX',2,NULL),(0,319,257,316,'FIX',3,NULL),(0,319,246,311,'FIX',7,NULL),(0,319,243,308,'FIX',8,NULL),(0,319,244,306,'FIX',9,NULL),(0,320,261,298,'FIX',1,NULL),(0,320,253,315,'FIX',2,NULL),(0,320,257,316,'FIX',3,NULL),(0,320,246,312,'FIX',7,NULL),(0,320,243,309,'FIX',8,NULL),(0,320,244,307,'FIX',9,NULL),(0,324,252,321,'FIX',3,NULL),(0,325,252,322,'FIX',3,NULL),(0,326,252,323,'FIX',3,NULL),(0,313,246,113,'REPLACEMENT',1,NULL),(0,313,113,310,'FIX',0,NULL),(0,313,243,114,'REPLACEMENT',2,NULL),(0,313,114,308,'FIX',0,NULL),(0,313,244,138,'REPLACEMENT',3,NULL),(0,313,138,305,'FIX',0,NULL),(0,313,248,324,'FIX',4,NULL),(0,314,246,113,'REPLACEMENT',1,NULL),(0,314,113,311,'FIX',0,NULL),(0,314,243,114,'REPLACEMENT',2,NULL),(0,314,114,308,'FIX',0,NULL),(0,314,244,138,'REPLACEMENT',3,NULL),(0,314,138,306,'FIX',0,NULL),(0,314,248,325,'FIX',4,NULL),(0,315,246,113,'REPLACEMENT',1,NULL),(0,315,113,312,'FIX',0,NULL),(0,315,243,114,'REPLACEMENT',2,NULL),(0,315,114,309,'FIX',0,NULL),(0,315,244,138,'REPLACEMENT',3,NULL),(0,315,138,307,'FIX',0,NULL),(0,315,248,326,'FIX',4,NULL),(0,327,258,317,'FIX',0,NULL),(0,327,261,298,'FIX',2,NULL),(0,327,253,313,'FIX',3,NULL),(0,327,248,324,'FIX',4,NULL),(0,328,258,317,'FIX',0,NULL),(0,328,261,298,'FIX',2,NULL),(0,328,253,314,'FIX',3,NULL),(0,328,248,325,'FIX',4,NULL),(0,329,258,317,'FIX',0,NULL),(0,329,261,298,'FIX',2,NULL),(0,329,253,315,'FIX',3,NULL),(0,329,248,326,'FIX',4,NULL),(0,334,240,331,'FIX',1,NULL),(0,335,240,333,'FIX',1,NULL),(0,336,107,335,'FIX',0,NULL),(0,337,107,335,'FIX',0,NULL),(0,338,107,335,'FIX',0,NULL),(0,341,277,332,'FIX',0,NULL),(0,342,277,332,'FIX',0,NULL),(0,343,277,330,'FIX',1,NULL),(0,344,107,334,'FIX',0,NULL),(0,345,107,334,'FIX',0,NULL),(0,346,107,334,'FIX',0,NULL),(0,347,107,334,'FIX',0,NULL),(0,348,273,113,'REPLACEMENT',0,NULL),(0,348,113,345,'FIX',1,NULL),(0,348,113,346,'FIX',2,NULL),(0,348,113,344,'FIX',0,NULL),(0,348,276,341,'FIX',1,NULL),(0,348,274,337,'FIX',2,NULL),(0,348,285,332,'FIX',5,NULL),(0,349,273,113,'REPLACEMENT',0,NULL),(0,349,113,345,'FIX',1,NULL),(0,349,113,347,'FIX',2,NULL),(0,349,113,344,'FIX',0,NULL),(0,349,276,341,'FIX',1,NULL),(0,349,274,336,'FIX',2,NULL),(0,349,285,332,'FIX',5,NULL),(0,350,276,342,'FIX',0,NULL),(0,350,275,338,'FIX',1,NULL),(0,350,285,330,'FIX',2,NULL),(0,350,272,339,'FIX',7,NULL),(0,351,276,342,'FIX',0,NULL),(0,351,275,338,'FIX',1,NULL),(0,351,285,330,'FIX',2,NULL),(0,351,272,340,'FIX',7,NULL),(0,352,282,348,'FIX',0,NULL),(0,352,286,350,'FIX',1,NULL),(0,353,282,349,'FIX',0,NULL),(0,353,286,351,'FIX',1,NULL),(0,354,276,343,'FIX',0,NULL),(0,354,294,330,'FIX',1,NULL),(0,354,290,113,'REPLACEMENT',2,NULL),(0,354,113,353,'FIX',1,NULL),(0,354,113,352,'FIX',0,NULL),(0,359,108,113,'REPLACEMENT',0,NULL),(0,359,113,146,'FIX',0,NULL),(0,359,105,358,'FIX',3,NULL),(0,359,136,114,'REPLACEMENT',4,NULL),(0,359,114,357,'FIX',0,NULL),(0,359,141,138,'REPLACEMENT',5,NULL),(0,359,138,356,'FIX',0,NULL),(0,359,213,223,'FIX',0,NULL),(0,364,108,113,'REPLACEMENT',0,NULL),(0,364,113,117,'FIX',0,NULL),(0,364,105,363,'FIX',3,NULL),(0,364,136,114,'REPLACEMENT',4,NULL),(0,364,114,362,'FIX',0,NULL),(0,364,141,138,'REPLACEMENT',5,NULL),(0,364,138,361,'FIX',0,NULL),(0,364,213,223,'FIX',0,NULL);
/*!40000 ALTER TABLE `reference_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `name` varbinary(255) NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES ('administration','Users with this role have unrestricted permissions.'),('anonymous','Users who did not authenticate themselves.');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats`
--

DROP TABLE IF EXISTS `stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` blob DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats`
--

LOCK TABLES `stats` WRITE;
/*!40000 ALTER TABLE `stats` DISABLE KEYS */;
INSERT INTO `stats` VALUES ('RootBenchmark','��\0sr\0-org.caosdb.server.database.misc.RootBenchmark����Qk]\0\0xr\04org.caosdb.server.database.misc.TransactionBenchmark����Qk]\0J\0sinceL\0measurementst\0Ljava/util/Map;[\0stackTraceElementst\0[Ljava/lang/StackTraceElement;L\0\rsubBenchmarksq\0~\0xp\0\0}v\"�sr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xur\0[Ljava.lang.StackTraceElement;F*<<�\"9\0\0xp\0\0\0sr\0java.lang.StackTraceElementa	Ś&6݅\0B\0formatI\0\nlineNumberL\0classLoaderNamet\0Ljava/lang/String;L\0declaringClassq\0~\0\nL\0fileNameq\0~\0\nL\0\nmethodNameq\0~\0\nL\0\nmoduleNameq\0~\0\nL\0\rmoduleVersionq\0~\0\nxp\0\0Bpt\0java.lang.Threadt\0Thread.javat\0\rgetStackTracet\0	java.baset\011.0.13sq\0~\0	\0\0 t\0appt\04org.caosdb.server.database.misc.TransactionBenchmarkt\0TransactionBenchmark.javat\0<init>ppsq\0~\0	\0\0\0�q\0~\0t\0-org.caosdb.server.database.misc.RootBenchmarkq\0~\0q\0~\0ppsq\0~\0	\0\0q\0~\0q\0~\0q\0~\0t\0<clinit>ppsq\0~\0	\0\0<q\0~\0t\0org.caosdb.server.CaosDBServert\0CaosDBServer.javat\0initBackendppsq\0~\0	\0\0\0�q\0~\0q\0~\0q\0~\0t\0mainppsq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0t\0Infosr\0,org.caosdb.server.database.misc.SubBenchmark����Qk]\0L\0nameq\0~\0\nxq\0~\0\0\0}v\"�sq\0~\0?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xuq\0~\0\0\0\0\nsq\0~\0	\0\0Bpq\0~\0q\0~\0\rq\0~\0q\0~\0q\0~\0sq\0~\0	\0\0 q\0~\0q\0~\0q\0~\0q\0~\0ppsq\0~\0	\0\0\0�q\0~\0t\0,org.caosdb.server.database.misc.SubBenchmarkq\0~\0q\0~\0ppsq\0~\0	\0\0�q\0~\0q\0~\0q\0~\0t\0getBenchmarkppsq\0~\0	\0\0�q\0~\0q\0~\0q\0~\0q\0~\0+ppsq\0~\0	\0\0\0#q\0~\0t\02org.caosdb.server.transaction.TransactionInterfacet\0TransactionInterface.javat\0getTransactionBenchmarkppsq\0~\0	\0\0\0/q\0~\0q\0~\0.q\0~\0/t\0executeppsq\0~\0	\0\0\0�q\0~\0t\0org.caosdb.server.utils.Infot\0	Info.javat\0syncDatabaseppsq\0~\0	\0\0\0�q\0~\0t\0/org.caosdb.server.database.misc.RootBenchmark$1q\0~\0t\0runppsq\0~\0	\0\0=pq\0~\0q\0~\0\rq\0~\09q\0~\0q\0~\0sq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0t\0	SyncStatssq\0~\0\"\0\0}v\"�sq\0~\0?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xuq\0~\0\0\0\0	sq\0~\0	\0\0Bpq\0~\0q\0~\0\rq\0~\0q\0~\0q\0~\0sq\0~\0	\0\0 q\0~\0q\0~\0q\0~\0q\0~\0ppsq\0~\0	\0\0\0�q\0~\0q\0~\0)q\0~\0q\0~\0ppsq\0~\0	\0\0�q\0~\0q\0~\0q\0~\0q\0~\0+ppsq\0~\0	\0\0�q\0~\0q\0~\0q\0~\0q\0~\0+ppsq\0~\0	\0\0\0/q\0~\0q\0~\0.q\0~\0/q\0~\02ppsq\0~\0	\0\0\0�q\0~\0q\0~\04q\0~\05q\0~\06ppsq\0~\0	\0\0\0�q\0~\0q\0~\08q\0~\0q\0~\09ppsq\0~\0	\0\0=pq\0~\0q\0~\0\rq\0~\09q\0~\0q\0~\0sq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0t\0MySQLSyncStatssq\0~\0\"\0\0}v\"�sq\0~\0?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xuq\0~\0\0\0\0sq\0~\0	\0\0Bpq\0~\0q\0~\0\rq\0~\0q\0~\0q\0~\0sq\0~\0	\0\0 q\0~\0q\0~\0q\0~\0q\0~\0ppsq\0~\0	\0\0\0�q\0~\0q\0~\0)q\0~\0q\0~\0ppsq\0~\0	\0\0�q\0~\0q\0~\0q\0~\0q\0~\0+ppsq\0~\0	\0\0�q\0~\0q\0~\0q\0~\0q\0~\0+ppsq\0~\0	\0\0\0�q\0~\0t\0-org.caosdb.server.database.BackendTransactiont\0BackendTransaction.javat\0getImplementationppsq\0~\0	\0\0\0+q\0~\0t\08org.caosdb.server.database.backend.transaction.SyncStatst\0SyncStats.javaq\0~\02ppsq\0~\0	\0\0\0�q\0~\0q\0~\0Tq\0~\0Ut\0executeTransactionppsq\0~\0	\0\0\00q\0~\0q\0~\0.q\0~\0/q\0~\02ppsq\0~\0	\0\0\0�q\0~\0q\0~\04q\0~\05q\0~\06ppsq\0~\0	\0\0\0�q\0~\0q\0~\08q\0~\0q\0~\09ppsq\0~\0	\0\0=pq\0~\0q\0~\0\rq\0~\09q\0~\0q\0~\0sq\0~\0?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xq\0~\0Jxq\0~\0<xq\0~\0!x'),('TransactionBenchmark','��\0sr\00caosdb.server.database.misc.TransactionBenchmark�Cl=���E\0J\0sinceL\0acct\0Ljava/util/HashMap;L\0countsq\0~\0xp\0\0mݐsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0�w\0\0\0\0\0\0�t\0Retrieve.commitsr\0java.lang.Long;��̏#�\0J\0valuexr\0java.lang.Number������\0\0xp\0\0\0\0\0\0\0t\0Retrieve.checksq\0~\0\0\0\0\0\0\0\0\nt\0GetUpdateableChecksumssq\0~\0\0\0\0\0\0\0\0t\0RuleLoader (cached=true)sq\0~\0\0\0\0\0\0\0\0t\07RetrieveFullEntity -> RetrieveProperties (cached=false)sq\0~\0\0\0\0\0\0\0;t\0\rUpdate.commitsq\0~\0\0\0\0\0\0\0\0t\0*RetrieveSparseEntityByPath.pre_transactionsq\0~\0\0\0\0\0\0\0\0\0t\0CheckDatatypePresentsq\0~\0\0\0\0\0\0\0\0(t\0Inheritanceq\0~\0t\0CheckParOblPropPresentsq\0~\0\0\0\0\0\0\0\0#t\0CheckRefidValidsq\0~\0\0\0\0\0\0\0\0t\0Retrieve.pre_checkq\0~\0t\0CheckParPresentq\0~\0t\0%RetrieveSparseEntityByPath.post_checkq\0~\0t\0Historyq\0~\0t\0#RetrieveSparseEntity (cached=false)sq\0~\0\0\0\0\0\0\0\0t\0!RetrieveSparseEntityByPath.commitq\0~\0t\0\rAccessControlsq\0~\0\0\0\0\0\0\0�t\0+RetrieveSparseEntityByPath.post_transactionq\0~\0t\0Insert.transactionsq\0~\0\0\0\0\0\0\0\0�t\0.Query.POV.initPOVPropertiesTable()#getChildrensq\0~\0\0\0\0\0\0\0\0Ct\0Insert.checksq\0~\0\0\0\0\0\0\0\0�t\0Insert.post_transactionsq\0~\0\0\0\0\0\0\0\0t\0CheckUnitPresentq\0~\0-t\0Query.POV.initPOVRefidsTable()sq\0~\0\0\0\0\0\0\0m�t\0\rRetrieve.initsq\0~\0\0\0\0\0\0rt\0RetrieveFullEntitysq\0~\0\0\0\0\0\0\0�t\0RetrieveSparseEntityByPath.initq\0~\0t\0\"RetrieveSparseEntityByPath.cleanupq\0~\0t\08RetrieveFullEntity -> RetrieveSparseEntity (cached=true)sq\0~\0\0\0\0\0\0\0\0t\0\\RetrieveFullEntity -> RetrieveProperties (cached=true) -> RetrieveSparseEntity (cached=true)q\0~\08t\0&InsertEntity -> InsertEntityPropertiessq\0~\0\0\0\0\0\0\0\0\\t\0;UpdateEntity -> InsertEntityProperties -> RegisterSubDomainsq\0~\0\0\0\0\0\0\0\0t\0RetrieveAllJobsq\0~\0\0\0\0\0\0\0\0.t\0\nUniqueNamesq\0~\0\0\0\0\0\0\0\0t\0;InsertEntity -> InsertEntityProperties -> RegisterSubDomainsq\0~\0\0\0\0\0\0\0\0t\0RetrieveDatatypessq\0~\0\0\0\0\0\0\0\0\rt\0Update.post_checkq\0~\0t\0\rMatchFilePropq\0~\0t\0Update.cleanupsq\0~\0\0\0\0\0\0\0\0t\0Retrieve.post_checkq\0~\0\nt\0Update.pre_transactionq\0~\0-t\0CheckPropValidsq\0~\0\0\0\0\0\0\0\0$t\0	Query.POVsq\0~\0\0\0\0\0\0\0��t\0#FileSystemResource.checkPermissionsq\0~\0t\0\rCheckParValidsq\0~\0\0\0\0\0\0\0\0t\0RetrieveAllq\0~\0?t\0Insert.cleanupq\0~\0t\0ResolveNamesq\0~\0=t\0Retrieve.pre_transactionsq\0~\0\0\0\0\0\0\0\0	t\0\rInsert.commitsq\0~\0\0\0\0\0\0\0\0t\0Update.checksq\0~\0\0\0\0\0\0\0\0<t\0RemoveDuplicatesq\0~\0It\02Query.POV.initPOVPropertiesTable()#addReplacementsq\0~\0-t\0\ZQuery.SubProperty.IDFiltersq\0~\0\0\0\0\0\0\0\'t\03RetrieveFullEntity -> RetrieveParents (cached=true)q\0~\0!t\0<Query.POV.initPOVPropertiesTable()#initPropertiesTableByNamesq\0~\0\0\0\0\0\0\0\0Vt\0Update.initsq\0~\0\0\0\0\0\0\0\0Zt\0	IsSubTypeq\0~\0Rt\06RetrieveFullEntity -> RetrieveProperties (cached=true)q\0~\0bt\0\ZRetrieveRole (cached=true)sq\0~\0\0\0\0\0\0\0\0t\0Query.POV.applyQueryTemplates()sq\0~\0\0\0\0\0\0\0-t\0Insert.pre_checkq\0~\0=t\0Query.RoleFiltersq\0~\0\0\0\0\0\0\0\0�t\0InsertEntity -> InsertParentsq\0~\0Ct\0Insert.historyq\0~\0t\0^RetrieveFullEntity -> RetrieveProperties (cached=false) -> RetrieveSparseEntity (cached=false)q\0~\0At\0$RetrieveSparseEntityByPath.pre_checkq\0~\0t\0GetIDByNamesq\0~\0\0\0\0\0\0\0\0t\0Retrieve.cleanupsq\0~\0\0\0\0\0\0\0\0�t\0CheckPropPresentq\0~\0-t\0Insert.initsq\0~\0\0\0\0\0\0\0\0\Zt\0CheckRefidPresentq\0~\0t\0CheckEntityACLRolessq\0~\0\0\0\0\0\0\0\09t\0UpdateEntity -> InsertParentsq\0~\0=t\0&UpdateEntity -> InsertEntityPropertiesq\0~\0xt\0\"RetrieveSparseEntity (cached=true)q\0~\0=t\0FileSystemResourceq\0~\0t\0Update.historyq\0~\0It\09RetrieveFullEntity -> RetrieveSparseEntity (cached=false)sq\0~\0\0\0\0\0\0\0\0�t\0UpdateUnitConvertersq\0~\0t\0FileSystemResource.getEntityq\0~\0It\0	ParseUnitq\0~\0Rt\0CheckValueParsableq\0~\0Et\0InsertEntitysq\0~\0\0\0\0\0\0\0\0�t\0\"GetFileRecordByPath (cached=false)q\0~\0-t\0%RetrievePermissionRules (cached=true)q\0~\0!t\0UpdateEntitysq\0~\0\0\0\0\0\0\0\00t\0]RetrieveFullEntity -> RetrieveProperties (cached=false) -> RetrieveSparseEntity (cached=true)q\0~\0t\0Pagingq\0~\0-t\0\nAWIBoxLoanq\0~\0t\03Query.POV.initPOVPropertiesTable()#findReplacementssq\0~\0\0\0\0\0\0\0\0Kt\0InsertTransactionHistorysq\0~\0\0\0\0\0\0\0\0\"t\0\"InsertEntity -> InsertSparseEntityq\0~\0�t\0CheckTargetPathValidq\0~\0-t\0:Query.POV.initPOVPropertiesTable()#initPropertiesTableByIdq\0~\0-t\0CheckRefidIsaParRefidq\0~\0!t\0 RetrieveSparseEntityByPath.checkq\0~\0t\0ExecuteQuerysq\0~\0\0\0\0\0\0t\0ProcessNamePropertiesq\0~\0t\0&UpdateEntity -> DeleteEntityPropertiesq\0~\0\nt\0GetInfosq\0~\0\0\0\0\0\0\0�t\0\"Query.POV.initPOVPropertiesTable()sq\0~\0\0\0\0\0\0\0�t\0LoadContainerFlagJobssq\0~\0\0\0\0\0\0\0\0%t\0Retrieve.post_transactionsq\0~\0\0\0\0\0\0\0\0�t\0SetImpToFixq\0~\0t\0Insert.pre_transactionq\0~\0It\0RetrieveUser (cached=false)q\0~\0It\0$InsertEntity -> InsertEntityDatatypeq\0~\0It\0\ZInsertEntity -> InsertFilesq\0~\0\0\0\0\0\0\0\0t\0&RetrieveSparseEntityByPath.transactionq\0~\0It\0Update.transactionsq\0~\0\0\0\0\0\0\0\01t\0&RetrievePermissionRules (cached=false)q\0~\0t\0Atomicq\0~\0t\0Update.pre_checkq\0~\0t\0Retrieve.historyq\0~\0t\0SetImpToRecByDefaultq\0~\0t\0RetrieveRole (cached=false)q\0~\0Yt\0CheckNamePresentq\0~\0t\0Retrieve.transactionsq\0~\0\0\0\0\0\0\0Ft\0RetrievePasswordValidatorq\0~\0It\0Query.SubPropertysq\0~\0\0\0\0\0\0\04t\0	SyncStatssq\0~\0\0\0\0\0\0\0Ht\04RetrieveFullEntity -> RetrieveParents (cached=false)q\0~\0�t\0!UpdateEntity -> InsertEntityValueq\0~\0t\0\"RetrieveSparseEntityByPath.historyq\0~\0t\0Query.POV.applySubProperty()sq\0~\0\0\0\0\0\0\04t\0Update.post_transactionq\0~\0t\0RuleLoader (cached=false)sq\0~\0\0\0\0\0\0\0\0t\0\"UpdateEntity -> UpdateSparseEntityq\0~\0t\0Query.POV.executeStmt()sq\0~\0\0\0\0\0\0\0t\0Insert.post_checkq\0~\0xsq\0~\0?@\0\0\0\0\0�w\0\0\0\0\0\0�q\0~\0sr\0java.lang.Integer⠤���8\0I\0valuexq\0~\0\0\0{q\0~\0	sq\0~\0�\0\0{q\0~\0sq\0~\0�\0\0\0q\0~\0\rsq\0~\0�\0\0�q\0~\0sq\0~\0�\0\0\0Kq\0~\0sq\0~\0�\0\0\0q\0~\0q\0~\0�q\0~\0sq\0~\0�\0\0\0Gq\0~\0sq\0~\0�\0\0\0q\0~\0sq\0~\0�\0\0\0pq\0~\0\Zsq\0~\0�\0\0\0<q\0~\0sq\0~\0�\0\0{q\0~\0sq\0~\0�\0\0\0q\0~\0q\0~\0�q\0~\0sq\0~\0�\0\0�q\0~\0 sq\0~\0�\0\0\0\nq\0~\0\"q\0~\0�q\0~\0#sq\0~\0�\0\0q\0~\0%q\0~\0�q\0~\0&sq\0~\0�\0\0\0q\0~\0(sq\0~\0�\0\0\0q\0~\0*q\0~\0�q\0~\0,q\0~\0�q\0~\0.sq\0~\0�\0\0\0.q\0~\0/sq\0~\0�\0\0�q\0~\01sq\0~\0�\0\0{q\0~\03sq\0~\0�\0\0�q\0~\05q\0~\0�q\0~\06q\0~\0�q\0~\07sq\0~\0�\0\0�q\0~\09sq\0~\0�\0\0	tq\0~\0:sq\0~\0�\0\0\0q\0~\0<q\0~\0�q\0~\0>sq\0~\0�\0\0\0!q\0~\0@sq\0~\0�\0\0\0q\0~\0Bq\0~\0�q\0~\0Dq\0~\0�q\0~\0Fq\0~\0�q\0~\0Gq\0~\0�q\0~\0Hq\0~\0�q\0~\0Jsq\0~\0�\0\0{q\0~\0Kq\0~\0�q\0~\0Lq\0~\0�q\0~\0Nsq\0~\0�\0\0�q\0~\0Pq\0~\0�q\0~\0Qq\0~\0�q\0~\0Sq\0~\0�q\0~\0Tq\0~\0�q\0~\0Usq\0~\0�\0\0{q\0~\0Vsq\0~\0�\0\0{q\0~\0Xq\0~\0�q\0~\0Zq\0~\0�q\0~\0\\sq\0~\0�\0\0{q\0~\0]q\0~\0�q\0~\0^sq\0~\0�\0\0�q\0~\0`sq\0~\0�\0\0�q\0~\0aq\0~\0�q\0~\0cq\0~\0�q\0~\0eq\0~\0�q\0~\0fsq\0~\0�\0\0�q\0~\0gsq\0~\0�\0\0)�q\0~\0isq\0~\0�\0\0�q\0~\0kq\0~\0�q\0~\0lsq\0~\0�\0\0xq\0~\0nq\0~\0�q\0~\0oq\0~\0�q\0~\0psq\0~\0�\0\0\0q\0~\0qq\0~\0�q\0~\0rsq\0~\0�\0\0\02q\0~\0tsq\0~\0�\0\0{q\0~\0vsq\0~\0�\0\0\0\Zq\0~\0wq\0~\0�q\0~\0ysq\0~\0�\0\0\0q\0~\0zsq\0~\0�\0\0q\0~\0|q\0~\0�q\0~\0}q\0~\0�q\0~\0~sq\0~\0�\0\0\0�q\0~\0q\0~\0�q\0~\0�q\0~\0�q\0~\0�sq\0~\0�\0\0\0@q\0~\0�sq\0~\0�\0\0\0q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�sq\0~\0�\0\0\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�sq\0~\0�\0\0(�q\0~\0�q\0~\0�q\0~\0�sq\0~\0�\0\0\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�sq\0~\0�\0\0*q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�sq\0~\0�\0\0�q\0~\0�sq\0~\0�\0\0�q\0~\0�sq\0~\0�\0\0q\0~\0�sq\0~\0�\0\0{q\0~\0�sq\0~\0�\0\0\0\\q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�sq\0~\0�\0\0{q\0~\0�sq\0~\0�\0\0\0q\0~\0�sq\0~\0�\0\0\0:q\0~\0�sq\0~\0�\0\0\0dq\0~\0�sq\0~\0�\0\0{q\0~\0�q\0~\0�q\0~\0�sq\0~\0�\0\0�q\0~\0�sq\0~\0�\0\0\0q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�sq\0~\0�\0\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�q\0~\0�sq\0~\0�\0\0�q\0~\0�q\0~\0�x');
/*!40000 ALTER TABLE `stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_data`
--

DROP TABLE IF EXISTS `text_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `str_entity_id_entity` (`entity_id`),
  KEY `str_property_id_entity` (`property_id`),
  CONSTRAINT `str_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `str_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `str_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_data`
--

LOCK TABLES `text_data` WRITE;
/*!40000 ALTER TABLE `text_data` DISABLE KEYS */;
INSERT INTO `text_data` VALUES (0,100,21,'€','FIX',0),(108,100,21,'€','FIX',0),(117,100,21,'€','FIX',0),(119,130,21,'°','FIX',0),(119,129,21,'°','FIX',0),(116,130,21,'°','FIX',0),(116,129,21,'°','FIX',0),(115,100,21,'€','FIX',0),(120,100,21,'€','FIX',0),(146,100,21,'€','FIX',0),(148,100,21,'€','FIX',0),(149,100,21,'€','FIX',0),(150,100,21,'€','FIX',0),(151,100,21,'€','FIX',0),(152,100,21,'€','FIX',0),(153,100,21,'€','FIX',0),(109,100,21,'€','FIX',0),(110,100,21,'€','FIX',0),(0,223,220,'#333333','FIX',0),(0,224,220,'#FFCC33','FIX',2),(0,225,220,'red','FIX',2),(0,222,220,'#FFCC33','FIX',0),(0,221,220,'#5bc0de','FIX',0),(244,245,21,'cm','FIX',0),(0,245,21,'cm','FIX',0),(246,247,21,'kg','FIX',0),(0,247,21,'kg','FIX',0),(262,263,21,'1/h','FIX',0),(262,265,21,'°C','FIX',0),(0,263,21,'1/h','FIX',0),(0,265,21,'°C','FIX',0),(267,247,21,'kg','FIX',0),(267,268,21,'W','FIX',0),(0,268,21,'W','FIX',0),(270,263,21,'1/h','FIX',0),(270,265,21,'°C','FIX',0),(279,280,21,'K','FIX',0),(279,281,21,'Hz','FIX',0),(0,280,21,'K','FIX',0),(0,281,21,'Hz','FIX',0),(282,283,21,'K','FIX',0),(282,284,21,'K','FIX',0),(0,283,21,'K','FIX',0),(0,284,21,'K','FIX',0),(286,287,21,'Hz','FIX',0),(286,288,21,'Hz','FIX',0),(286,289,21,'Pa','FIX',0),(0,287,21,'Hz','FIX',0),(0,288,21,'Hz','FIX',0),(0,289,21,'Pa','FIX',0),(296,280,21,'K','FIX',0),(107,130,21,'°','FIX',0),(107,129,21,'°','FIX',0),(0,298,238,'frauke@fahrad.de','FIX',0),(0,299,238,'manfred@manufacturer.com','FIX',0),(0,300,238,'valentin@velo.fr','FIX',0),(0,301,239,'123 Bike Street, 45678 Bicycletown','FIX',0),(301,129,21,'°','FIX',0),(301,130,21,'°','FIX',0),(0,302,239,'Rue de Cycle, 1234 Véloville','FIX',0),(302,129,21,'°','FIX',0),(302,130,21,'°','FIX',0),(0,303,239,'987 Forest Drive, 6543 Fairmountains','FIX',0),(303,129,21,'°','FIX',0),(303,130,21,'°','FIX',0),(0,304,239,'Fahrradweg 1, 23456 Raddorf','FIX',0),(304,129,21,'°','FIX',0),(304,130,21,'°','FIX',0),(305,245,21,'cm','FIX',0),(306,245,21,'cm','FIX',0),(307,245,21,'cm','FIX',0),(310,247,21,'kg','FIX',0),(311,247,21,'kg','FIX',0),(312,247,21,'kg','FIX',0),(0,318,264,'3.14','FIX',5),(318,263,21,'1/h','FIX',0),(318,265,21,'°C','FIX',0),(0,319,264,'3.15','FIX',5),(319,263,21,'1/h','FIX',0),(319,265,21,'°C','FIX',0),(0,320,264,'3.14','FIX',5),(320,263,21,'1/h','FIX',0),(320,265,21,'°C','FIX',0),(0,313,254,'A3','FIX',0),(0,314,254,'B1','FIX',0),(0,315,254,'C4','FIX',0),(0,327,264,'1.1','FIX',1),(0,328,264,'1.1','FIX',1),(0,329,264,'1.1a','FIX',1),(0,330,238,'s.scientist@lab.edu','FIX',0),(0,331,238,'sebastian@sample.com','FIX',0),(0,332,238,'l.labassistant@lab.edu','FIX',0),(0,333,238,'dorothee@device.org','FIX',0),(0,334,239,'Probenstraße 1, 98765 Probstadt','FIX',0),(334,130,21,'°','FIX',0),(334,129,21,'°','FIX',0),(0,335,239,'24 Science Street, 681012 Labbington','FIX',0),(335,130,21,'°','FIX',0),(335,129,21,'°','FIX',0),(0,341,278,'Mix the source materials, then let the gel solidify for 5 hours.','FIX',1),(341,280,21,'K','FIX',0),(0,342,278,'Apply oscillatory strain at a fixed frequency and measure the tensile stress response with increasing amplitude.','FIX',1),(342,281,21,'Hz','FIX',0),(0,343,278,'After sample preparation, apply oscillatory tensile strain with increasing amplitudes and record the stress response. Compare tensile stress and yield stress of different samples.','FIX',0),(0,344,254,'2020ABC','FIX',1),(0,345,254,'naoh_123','FIX',1),(0,346,254,'21BC456','FIX',1),(0,347,254,'21PF789','FIX',1),(348,283,21,'K','FIX',0),(348,284,21,'K','FIX',0),(349,283,21,'K','FIX',0),(349,284,21,'K','FIX',0),(0,350,220,'white','FIX',5),(350,287,21,'Hz','FIX',0),(350,288,21,'Hz','FIX',0),(350,289,21,'Pa','FIX',0),(0,351,220,'grey','FIX',5),(351,287,21,'Hz','FIX',0),(351,288,21,'Hz','FIX',0),(351,289,21,'Pa','FIX',0);
/*!40000 ALTER TABLE `text_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_log`
--

DROP TABLE IF EXISTS `transaction_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_log` (
  `transaction` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Transaction.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID.',
  `username` varbinary(255) NOT NULL,
  `seconds` bigint(20) unsigned NOT NULL DEFAULT 0,
  `nanos` int(10) unsigned NOT NULL DEFAULT 0,
  `realm` varbinary(255) NOT NULL,
  KEY `entity_id` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_log`
--

LOCK TABLES `transaction_log` WRITE;
/*!40000 ALTER TABLE `transaction_log` DISABLE KEYS */;
INSERT INTO `transaction_log` VALUES ('Insert',100,'admin',1568207645,242000000,'PAM'),('Insert',101,'admin',1568207645,242000000,'PAM'),('Insert',102,'admin',1568207645,242000000,'PAM'),('Insert',103,'admin',1568207645,242000000,'PAM'),('Insert',104,'admin',1568207645,242000000,'PAM'),('Insert',105,'admin',1568207645,242000000,'PAM'),('Insert',106,'admin',1568207645,242000000,'PAM'),('Insert',107,'admin',1568207645,242000000,'PAM'),('Insert',108,'admin',1568207645,242000000,'PAM'),('Insert',109,'admin',1568207645,242000000,'PAM'),('Insert',110,'admin',1568207645,242000000,'PAM'),('Insert',111,'admin',1568207645,242000000,'PAM'),('Insert',112,'admin',1568207645,242000000,'PAM'),('Insert',115,'admin',1568207645,552000000,'PAM'),('Insert',116,'admin',1568207645,552000000,'PAM'),('Insert',117,'admin',1568207645,552000000,'PAM'),('Insert',118,'admin',1568207645,552000000,'PAM'),('Insert',119,'admin',1568207645,552000000,'PAM'),('Insert',120,'admin',1568207645,552000000,'PAM'),('Insert',121,'admin',1568207645,552000000,'PAM'),('Insert',122,'admin',1568207645,552000000,'PAM'),('Insert',123,'admin',1568207645,552000000,'PAM'),('Insert',124,'admin',1568207645,552000000,'PAM'),('Insert',125,'admin',1568207645,552000000,'PAM'),('Insert',126,'admin',1568207645,811000000,'PAM'),('Insert',127,'admin',1568207645,811000000,'PAM'),('Insert',128,'admin',1568207645,811000000,'PAM'),('Update',101,'admin',1568207645,925000000,'PAM'),('Update',102,'admin',1568207645,925000000,'PAM'),('Update',103,'admin',1568207645,925000000,'PAM'),('Update',104,'admin',1568207645,925000000,'PAM'),('Update',105,'admin',1568207645,925000000,'PAM'),('Update',106,'admin',1568207645,925000000,'PAM'),('Update',107,'admin',1568207645,925000000,'PAM'),('Update',109,'admin',1568207645,925000000,'PAM'),('Update',110,'admin',1568207645,925000000,'PAM'),('Update',111,'admin',1568207645,925000000,'PAM'),('Update',116,'admin',1568207645,925000000,'PAM'),('Update',119,'admin',1568207645,925000000,'PAM'),('Update',121,'admin',1568207645,925000000,'PAM'),('Update',122,'admin',1568207645,925000000,'PAM'),('Update',126,'admin',1568207645,925000000,'PAM'),('Update',127,'admin',1568207645,925000000,'PAM'),('Update',128,'admin',1568207645,925000000,'PAM'),('Insert',129,'admin',1615910392,952000000,'PAM'),('Insert',130,'admin',1615910416,304000000,'PAM'),('Insert',131,'admin',1615974072,292000000,'PAM'),('Insert',132,'admin',1615974179,283000000,'PAM'),('Insert',133,'admin',1615974310,763000000,'PAM'),('Delete',133,'admin',1615974441,9000000,'PAM'),('Insert',134,'admin',1615974554,741000000,'PAM'),('Insert',135,'admin',1615974604,972000000,'PAM'),('Insert',136,'admin',1615975368,353000000,'PAM'),('Insert',137,'admin',1615975570,370000000,'PAM'),('Insert',139,'admin',1615975670,987000000,'PAM'),('Insert',140,'admin',1615975802,654000000,'PAM'),('Insert',141,'admin',1615980914,63000000,'PAM'),('Insert',142,'admin',1615980934,661000000,'PAM'),('Insert',144,'admin',1615980952,91000000,'PAM'),('Insert',145,'admin',1615980977,743000000,'PAM'),('Delete',118,'admin',1616448435,12000000,'PAM'),('Insert',146,'admin',1616448439,993000000,'PAM'),('Insert',147,'admin',1616576682,430000000,'PAM'),('Insert',148,'admin',1616576682,674000000,'PAM'),('Insert',149,'admin',1616576682,674000000,'PAM'),('Insert',150,'admin',1616576682,674000000,'PAM'),('Insert',151,'admin',1616576682,674000000,'PAM'),('Insert',152,'admin',1616576682,674000000,'PAM'),('Insert',153,'admin',1616576682,674000000,'PAM'),('Insert',154,'admin',1616576682,850000000,'PAM'),('Insert',155,'admin',1616576682,850000000,'PAM'),('Insert',156,'admin',1616576682,850000000,'PAM'),('Insert',157,'admin',1616576682,850000000,'PAM'),('Insert',158,'admin',1616576683,453000000,'PAM'),('Insert',159,'admin',1616576683,602000000,'PAM'),('Insert',160,'admin',1616576683,753000000,'PAM'),('Insert',161,'admin',1616576683,893000000,'PAM'),('Insert',162,'admin',1616576684,22000000,'PAM'),('Insert',163,'admin',1616576684,154000000,'PAM'),('Insert',164,'admin',1616576684,292000000,'PAM'),('Insert',165,'admin',1616576684,346000000,'PAM'),('Insert',166,'admin',1616576684,346000000,'PAM'),('Insert',167,'admin',1616576684,346000000,'PAM'),('Insert',168,'admin',1616576684,346000000,'PAM'),('Insert',169,'admin',1616576684,346000000,'PAM'),('Insert',170,'admin',1616576684,346000000,'PAM'),('Insert',171,'admin',1616576684,346000000,'PAM'),('Insert',172,'admin',1616584989,485000000,'PAM'),('Insert',173,'admin',1616584989,485000000,'PAM'),('Insert',174,'admin',1616584989,485000000,'PAM'),('Insert',175,'admin',1616584989,485000000,'PAM'),('Insert',176,'admin',1616584989,485000000,'PAM'),('Insert',177,'admin',1616584989,485000000,'PAM'),('Insert',178,'admin',1616584989,485000000,'PAM'),('Insert',179,'admin',1616584989,485000000,'PAM'),('Insert',180,'admin',1616584989,485000000,'PAM'),('Insert',181,'admin',1616584989,485000000,'PAM'),('Insert',182,'admin',1616584989,485000000,'PAM'),('Insert',183,'admin',1616584989,485000000,'PAM'),('Insert',184,'admin',1616584989,485000000,'PAM'),('Insert',185,'admin',1616584989,485000000,'PAM'),('Insert',186,'admin',1616584989,485000000,'PAM'),('Insert',187,'admin',1616584989,485000000,'PAM'),('Insert',188,'admin',1616584989,485000000,'PAM'),('Insert',189,'admin',1616584989,485000000,'PAM'),('Insert',190,'admin',1616584989,485000000,'PAM'),('Insert',191,'admin',1616584989,485000000,'PAM'),('Insert',192,'admin',1616584989,485000000,'PAM'),('Insert',193,'admin',1616584989,485000000,'PAM'),('Insert',194,'admin',1616584989,485000000,'PAM'),('Insert',195,'admin',1616584989,485000000,'PAM'),('Insert',196,'admin',1616584989,485000000,'PAM'),('Insert',197,'admin',1616584989,485000000,'PAM'),('Insert',198,'admin',1616584989,485000000,'PAM'),('Insert',199,'admin',1616584989,485000000,'PAM'),('Insert',200,'admin',1616584989,485000000,'PAM'),('Insert',201,'admin',1616584989,485000000,'PAM'),('Insert',202,'admin',1616584989,485000000,'PAM'),('Insert',203,'admin',1616584989,485000000,'PAM'),('Insert',204,'admin',1616584989,485000000,'PAM'),('Insert',205,'admin',1616584989,485000000,'PAM'),('Insert',206,'admin',1616584989,485000000,'PAM'),('Insert',207,'admin',1616584989,485000000,'PAM'),('Insert',208,'admin',1616584989,485000000,'PAM'),('Insert',209,'admin',1616584989,485000000,'PAM'),('Insert',210,'admin',1616584989,485000000,'PAM'),('Insert',211,'admin',1616584989,485000000,'PAM'),('Delete',158,'admin',1616587819,326000000,'PAM'),('Delete',159,'admin',1616587819,326000000,'PAM'),('Delete',160,'admin',1616587819,326000000,'PAM'),('Delete',161,'admin',1616587819,326000000,'PAM'),('Delete',162,'admin',1616587819,326000000,'PAM'),('Delete',163,'admin',1616587819,326000000,'PAM'),('Delete',164,'admin',1616587819,326000000,'PAM'),('Delete',165,'admin',1616587819,326000000,'PAM'),('Delete',166,'admin',1616587819,326000000,'PAM'),('Delete',167,'admin',1616587819,326000000,'PAM'),('Delete',168,'admin',1616587819,326000000,'PAM'),('Delete',169,'admin',1616587819,326000000,'PAM'),('Delete',170,'admin',1616587819,326000000,'PAM'),('Delete',171,'admin',1616587819,326000000,'PAM'),('Insert',212,'admin',1616780803,609000000,'PAM'),('Insert',213,'admin',1617193824,132000000,'PAM'),('Insert',214,'admin',1617193824,275000000,'PAM'),('Insert',215,'admin',1617193824,360000000,'PAM'),('Insert',216,'admin',1617193824,431000000,'PAM'),('Insert',217,'admin',1617193824,526000000,'PAM'),('Insert',218,'admin',1617193824,602000000,'PAM'),('Insert',219,'admin',1617193824,676000000,'PAM'),('Insert',220,'admin',1617193824,749000000,'PAM'),('Insert',221,'admin',1617193824,820000000,'PAM'),('Insert',222,'admin',1617193824,941000000,'PAM'),('Insert',223,'admin',1617193825,21000000,'PAM'),('Insert',224,'admin',1617193825,79000000,'PAM'),('Insert',225,'admin',1617193825,178000000,'PAM'),('Insert',226,'admin',1617193825,231000000,'PAM'),('Insert',227,'admin',1617193825,287000000,'PAM'),('Insert',228,'admin',1617193825,344000000,'PAM'),('Insert',229,'admin',1617193825,399000000,'PAM'),('Delete',126,'admin',1617279111,166000000,'PAM'),('Delete',127,'admin',1617279133,683000000,'PAM'),('Delete',128,'admin',1617279137,14000000,'PAM'),('Insert',230,'admin',1617975392,701000000,'PAM'),('Insert',231,'admin',1617975392,755000000,'PAM'),('Insert',232,'admin',1617975392,806000000,'PAM'),('Insert',233,'admin',1617975392,875000000,'PAM'),('Insert',234,'admin',1619422732,644000000,'PAM'),('Insert',235,'admin',1619422732,644000000,'PAM'),('Insert',236,'admin',1619422732,644000000,'PAM'),('Insert',237,'admin',1619796558,83000000,'PAM'),('Insert',238,'admin',1619796558,83000000,'PAM'),('Insert',239,'admin',1619796558,83000000,'PAM'),('Insert',240,'admin',1619796558,83000000,'PAM'),('Insert',241,'admin',1619796558,83000000,'PAM'),('Insert',242,'admin',1619796558,83000000,'PAM'),('Insert',243,'admin',1619796558,83000000,'PAM'),('Insert',244,'admin',1619796558,83000000,'PAM'),('Insert',245,'admin',1619796558,83000000,'PAM'),('Insert',246,'admin',1619796558,83000000,'PAM'),('Insert',247,'admin',1619796558,83000000,'PAM'),('Insert',248,'admin',1619796558,83000000,'PAM'),('Insert',249,'admin',1619796558,83000000,'PAM'),('Insert',250,'admin',1619796558,83000000,'PAM'),('Insert',251,'admin',1619796558,83000000,'PAM'),('Insert',252,'admin',1619796558,83000000,'PAM'),('Insert',253,'admin',1619796558,83000000,'PAM'),('Insert',254,'admin',1619796558,83000000,'PAM'),('Insert',255,'admin',1619796558,83000000,'PAM'),('Insert',256,'admin',1619796558,83000000,'PAM'),('Insert',257,'admin',1619796558,83000000,'PAM'),('Insert',258,'admin',1619796558,83000000,'PAM'),('Insert',259,'admin',1619796558,83000000,'PAM'),('Insert',260,'admin',1619796558,83000000,'PAM'),('Insert',261,'admin',1619796558,83000000,'PAM'),('Insert',262,'admin',1619796558,83000000,'PAM'),('Insert',263,'admin',1619796558,83000000,'PAM'),('Insert',264,'admin',1619796558,83000000,'PAM'),('Insert',265,'admin',1619796558,83000000,'PAM'),('Insert',266,'admin',1619796558,83000000,'PAM'),('Insert',267,'admin',1619796558,83000000,'PAM'),('Insert',268,'admin',1619796558,83000000,'PAM'),('Insert',269,'admin',1619796558,83000000,'PAM'),('Insert',270,'admin',1619796558,83000000,'PAM'),('Insert',271,'admin',1619796558,83000000,'PAM'),('Insert',272,'admin',1619796558,83000000,'PAM'),('Insert',273,'admin',1619796558,83000000,'PAM'),('Insert',274,'admin',1619796558,83000000,'PAM'),('Insert',275,'admin',1619796558,83000000,'PAM'),('Insert',276,'admin',1619796558,83000000,'PAM'),('Insert',277,'admin',1619796558,83000000,'PAM'),('Insert',278,'admin',1619796558,83000000,'PAM'),('Insert',279,'admin',1619796558,83000000,'PAM'),('Insert',280,'admin',1619796558,83000000,'PAM'),('Insert',281,'admin',1619796558,83000000,'PAM'),('Insert',282,'admin',1619796558,83000000,'PAM'),('Insert',283,'admin',1619796558,83000000,'PAM'),('Insert',284,'admin',1619796558,83000000,'PAM'),('Insert',285,'admin',1619796558,83000000,'PAM'),('Insert',286,'admin',1619796558,83000000,'PAM'),('Insert',287,'admin',1619796558,83000000,'PAM'),('Insert',288,'admin',1619796558,83000000,'PAM'),('Insert',289,'admin',1619796558,83000000,'PAM'),('Insert',290,'admin',1619796558,83000000,'PAM'),('Insert',291,'admin',1619796558,83000000,'PAM'),('Insert',292,'admin',1619796558,83000000,'PAM'),('Insert',293,'admin',1619796558,83000000,'PAM'),('Insert',294,'admin',1619796558,83000000,'PAM'),('Insert',295,'admin',1619796558,83000000,'PAM'),('Insert',296,'admin',1619796558,83000000,'PAM'),('Insert',297,'admin',1619796558,83000000,'PAM'),('Update',220,'admin',1619796559,243000000,'PAM'),('Insert',298,'admin',1619796559,774000000,'PAM'),('Insert',299,'admin',1619796559,774000000,'PAM'),('Insert',300,'admin',1619796559,774000000,'PAM'),('Insert',301,'admin',1619796559,888000000,'PAM'),('Insert',302,'admin',1619796559,888000000,'PAM'),('Insert',303,'admin',1619796559,888000000,'PAM'),('Insert',304,'admin',1619796559,888000000,'PAM'),('Insert',305,'admin',1619796560,162000000,'PAM'),('Insert',306,'admin',1619796560,162000000,'PAM'),('Insert',307,'admin',1619796560,162000000,'PAM'),('Insert',308,'admin',1619796560,162000000,'PAM'),('Insert',309,'admin',1619796560,162000000,'PAM'),('Insert',310,'admin',1619796560,162000000,'PAM'),('Insert',311,'admin',1619796560,162000000,'PAM'),('Insert',312,'admin',1619796560,162000000,'PAM'),('Insert',313,'admin',1619796560,355000000,'PAM'),('Insert',314,'admin',1619796560,355000000,'PAM'),('Insert',315,'admin',1619796560,355000000,'PAM'),('Insert',316,'admin',1619796560,523000000,'PAM'),('Insert',317,'admin',1619796560,523000000,'PAM'),('Insert',318,'admin',1619796560,670000000,'PAM'),('Insert',319,'admin',1619796560,670000000,'PAM'),('Insert',320,'admin',1619796560,670000000,'PAM'),('Insert',321,'admin',1619796560,817000000,'PAM'),('Insert',322,'admin',1619796560,817000000,'PAM'),('Insert',323,'admin',1619796560,817000000,'PAM'),('Insert',324,'admin',1619796560,955000000,'PAM'),('Insert',325,'admin',1619796560,955000000,'PAM'),('Insert',326,'admin',1619796560,955000000,'PAM'),('Insert',327,'admin',1619796561,307000000,'PAM'),('Insert',328,'admin',1619796561,307000000,'PAM'),('Insert',329,'admin',1619796561,307000000,'PAM'),('Insert',330,'admin',1619796561,777000000,'PAM'),('Insert',331,'admin',1619796561,777000000,'PAM'),('Insert',332,'admin',1619796561,777000000,'PAM'),('Insert',333,'admin',1619796561,777000000,'PAM'),('Insert',334,'admin',1619796561,869000000,'PAM'),('Insert',335,'admin',1619796561,869000000,'PAM'),('Insert',336,'admin',1619796561,976000000,'PAM'),('Insert',337,'admin',1619796561,976000000,'PAM'),('Insert',338,'admin',1619796561,976000000,'PAM'),('Insert',339,'admin',1619796562,82000000,'PAM'),('Insert',340,'admin',1619796562,82000000,'PAM'),('Insert',341,'admin',1619796562,235000000,'PAM'),('Insert',342,'admin',1619796562,235000000,'PAM'),('Insert',343,'admin',1619796562,235000000,'PAM'),('Insert',344,'admin',1619796562,340000000,'PAM'),('Insert',345,'admin',1619796562,340000000,'PAM'),('Insert',346,'admin',1619796562,340000000,'PAM'),('Insert',347,'admin',1619796562,340000000,'PAM'),('Insert',348,'admin',1619796562,473000000,'PAM'),('Insert',349,'admin',1619796562,473000000,'PAM'),('Insert',350,'admin',1619796562,633000000,'PAM'),('Insert',351,'admin',1619796562,633000000,'PAM'),('Insert',352,'admin',1619796562,750000000,'PAM'),('Insert',353,'admin',1619796562,750000000,'PAM'),('Insert',354,'admin',1619796562,843000000,'PAM'),('Insert',355,'admin',1620992909,768000000,'PAM'),('Insert',356,'admin',1620992909,768000000,'PAM'),('Insert',357,'admin',1620992909,768000000,'PAM'),('Insert',358,'admin',1620992909,768000000,'PAM'),('Insert',359,'admin',1620992909,768000000,'PAM'),('Insert',360,'admin',1620992909,768000000,'PAM'),('Insert',361,'admin',1620992909,768000000,'PAM'),('Insert',362,'admin',1620992909,768000000,'PAM'),('Insert',363,'admin',1620992909,768000000,'PAM'),('Insert',364,'admin',1620992909,768000000,'PAM'),('Delete',142,'admin',1638363716,323000000,'PAM'),('Delete',144,'admin',1638363718,73000000,'PAM'),('Delete',145,'admin',1638363719,645000000,'PAM'),('Update',174,'admin',1638364010,156000000,'PAM'),('Update',179,'admin',1638364010,226000000,'PAM'),('Update',183,'admin',1638364010,266000000,'PAM'),('Update',184,'admin',1638364010,333000000,'PAM'),('Update',189,'admin',1638364010,379000000,'PAM'),('Update',193,'admin',1638364010,426000000,'PAM'),('Update',197,'admin',1638364010,473000000,'PAM'),('Update',201,'admin',1638364010,516000000,'PAM'),('Update',205,'admin',1638364010,556000000,'PAM'),('Update',209,'admin',1638364010,596000000,'PAM');
/*!40000 ALTER TABLE `transaction_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `srid` varbinary(255) NOT NULL,
  `username` varbinary(255) NOT NULL,
  `realm` varbinary(255) NOT NULL,
  `seconds` bigint(20) unsigned NOT NULL,
  `nanos` int(10) unsigned NOT NULL,
  PRIMARY KEY (`srid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES ('00963a37-5605-4761-a9ef-5a4cc81c5750','admin','PAM',1616780433,928000000),('00a73ffe-9418-48de-a07c-cfdf17a6cf08','admin','PAM',1615974579,906000000),('05b5fac2-15da-445c-950d-427a683f3283','admin','PAM',1617279137,14000000),('07295cd4-afea-43d8-9fd2-1026a685ed23','admin','PAM',1638364010,379000000),('0877faea-9870-44c2-8440-b6747bfbf364','admin','PAM',1619796561,869000000),('08cae94c-ef46-4d3c-ba70-0c94cff2188d','admin','PAM',1619796561,777000000),('0c1f8efc-7668-49d0-96cb-2a58c7f38fcf','admin','PAM',1619796561,131000000),('0ddf80a7-df7e-4baf-86cc-613d7213ebf5','admin','PAM',1638364010,333000000),('0e78ebf6-9dc2-4cf4-9a79-8d8a28d3f919','admin','PAM',1617193824,941000000),('0ecdbfac-cf69-496d-a40a-774a56ef83f9','admin','PAM',1617193825,21000000),('0f8609ab-34a7-4119-8955-b0a2f678e31c','admin','PAM',1620992910,112000000),('11dbd6cc-375a-4501-ae46-fe058c351964','admin','PAM',1615974312,933000000),('124bdb14-9577-419e-b3fd-14900ba6413d','admin','PAM',1619422732,644000000),('128f912c-ef86-4fd0-8072-262bef22a77c','admin','PAM',1617193825,399000000),('129f344e-73d0-46c3-bef8-caa2517c63e1','admin','PAM',1616577970,21000000),('16eed374-ca46-4747-954d-7e8d9065e053','admin','PAM',1616576683,602000000),('17330db7-1ff3-4240-9ede-f9730215af00','admin','PAM',1615974554,741000000),('17eb6c42-de9d-4835-af10-8083ca5645c0','admin','PAM',1619796562,235000000),('190ae7cd-4f97-4df1-ad9a-07c115e34641','admin','PAM',1615980936,403000000),('19381df442d97ccce697803bda0164e6b3341999a3cb70aedd3a9e5496de3cbdc9aea5a090ce455994ccd197dfb375dacff0f41b20b6d34847f06dfa8ee36a1a','admin','PAM',1568207645,552000000),('1bb0370e-0ebd-4559-9f9a-89fb809a278f','admin','PAM',1616576683,893000000),('1c277eea-380b-46a5-9a29-3d59ec24563b','admin','PAM',1615910392,952000000),('1cb1907d-e5dd-463c-b10e-d4ff85c3d7b8','admin','PAM',1619796561,976000000),('1cf0d719-f5c0-452d-b91e-fe968662073a','admin','PAM',1619796562,750000000),('1d2ac525-3638-44fc-919b-6ee9b612220b','admin','PAM',1617178692,383000000),('1e398301-15f9-47e2-a61c-c0d6ce6ebaf4','admin','PAM',1617194028,15000000),('205d50e9-f9af-4d97-9774-26a55cd81b02','admin','PAM',1616576684,154000000),('20d8b95f-e86c-4f53-9503-44523e8786e7','admin','PAM',1617178679,821000000),('21c08f5e-be47-4598-9f9f-51f3c08dfd7a','admin','PAM',1617193824,526000000),('254693b4-a9d7-4d6b-a3f1-8c61717b700a','admin','PAM',1619796559,774000000),('26ae2cce-ad0d-4e93-ad88-8008a5398df6','admin','PAM',1638364010,156000000),('29209b6e-358c-4245-aa98-a2a7609fd88c','admin','PAM',1615980977,743000000),('2925c5d5-5c2a-473b-9888-3d3bcc3f6a3c','admin','PAM',1615975802,654000000),('29488453-d9fd-415e-9617-a9e3e190fa7f','admin','PAM',1638364010,556000000),('2b3c827b-8898-47fe-843a-729f90f06b2d','admin','PAM',1616778748,277000000),('2b519105-7daf-4dcb-af35-5915b3425b33','admin','PAM',1617193824,360000000),('2bd0b828-6554-41fd-bc21-1778e1125eb3','admin','PAM',1617975392,875000000),('2c67b7f6-f55e-4c48-9376-ed833dcc8b2e','admin','PAM',1615974072,292000000),('2c88eb7f-9cdb-4af6-9d25-06a5bed193e5','admin','PAM',1617975392,755000000),('323f5c83-fa4b-4163-9d29-43e0aad7d446','admin','PAM',1617194028,162000000),('335482dc-33e0-4ab3-9150-17a4257cb1ef','admin','PAM',1619534080,809000000),('33f8ab9d-f2ad-4f67-b157-ff70994c9173','admin','PAM',1638363718,73000000),('3731b5ef-1c54-4fed-9132-2809f7785a8c','admin','PAM',1616780623,679000000),('3dc130ba-9273-48af-aee9-0f15304a4cbd','admin','PAM',1618214679,799000000),('3f8f27b4-3919-4a2d-84c6-cd95b483e41a','admin','PAM',1638364010,266000000),('428352a4-0346-4d57-9f0e-4ec6b17d436f','admin','PAM',1617193824,676000000),('44789515-9cd6-4cee-9914-ae8936954a89','admin','PAM',1619796558,83000000),('479cdf8e-aa78-42a3-be67-80181d91b2ab','admin','PAM',1615910639,63000000),('486cfa7f-333c-4d1e-8dcf-ab2e234589fc','admin','PAM',1618214680,166000000),('48f55467-121a-4dee-9adc-013da509135d','admin','PAM',1617279312,368000000),('4ce82e39-21ff-49c7-95bc-a59b91cfead2','admin','PAM',1620992909,768000000),('4d988025-ef3a-4ea7-897c-4139dae37c5b','admin','PAM',1616587726,198000000),('502c063e-db2c-42e4-bc6b-429aefcb0b41','admin','PAM',1619796560,817000000),('5351f282-bb02-402a-b21e-16e7ae6554ca','admin','PAM',1615974179,283000000),('54aa82dc-9503-42cc-a240-94a958df7e68','admin','PAM',1616781094,659000000),('55f5c517-610a-4672-b6cd-45e5af4bbb45','admin','PAM',1618214679,341000000),('5624c8eb-46ce-4cf2-93a9-a8f8f5ef05bc','admin','PAM',1616577991,160000000),('563f3a71-614a-406c-b756-74691e452718','admin','PAM',1638363719,645000000),('57973bc2-0ee9-4ae4-b647-4a5cd391b379','admin','PAM',1615974186,683000000),('57f4497c-993f-418b-8fb4-84c175346be4','admin','PAM',1618214680,72000000),('5935f9f3-3e80-4b91-9f06-785003effe34','admin','PAM',1638364010,596000000),('5d5b7cac-3c7f-4de7-82ed-a87de62d240c','admin','PAM',1616576684,346000000),('5dbbd5e4-4677-48c9-9967-56b6e2597743','admin','PAM',1620992909,967000000),('5e09057c-32b1-44d9-bd43-2673e95730fc','admin','PAM',1616578029,268000000),('5ee8fc76-bda7-4805-9f13-5a952e89cb18','admin','PAM',1615975676,306000000),('5f681463-ec81-4b97-a628-1605976ce32c','admin','PAM',1619796560,523000000),('60086f61-d7d3-4f86-a428-16d05e85c9fa','admin','PAM',1617975392,701000000),('60527664-cf39-45ba-9b21-09e4f2db1b74','admin','PAM',1615974310,763000000),('630bb660-27fd-42d3-bafb-e0651c6c2769','admin','PAM',1616780590,687000000),('65e8fb64-d5e4-4fbc-bb52-46ca37fb16e9','admin','PAM',1619534121,343000000),('66fa0d88f7ecc7e0f783b979ce9f59e9c429dae17c7b186992a6d997f71d34df7c3f4ab40dcba91194bf51a850865490bfc8f1b3b7791b91c5aa8fb95bfcebd8','admin','PAM',1568207645,242000000),('6c4050ee-309d-45aa-8717-d06419129348','admin','PAM',1616577974,906000000),('6cd43def-10be-498e-b75b-e572badadbed','admin','PAM',1616448439,993000000),('70e888f2-b57b-4cd6-b31e-8f95f4f69347','admin','PAM',1617193825,79000000),('71116cf5-e236-4b5c-95a7-bedb97c6d47d','admin','PAM',1615975368,353000000),('74077064-042e-4c93-87b3-8d48b38d1bf6','admin','PAM',1616576683,453000000),('7431c6d6-bf95-4de9-b761-7fe1aab19fef','admin','PAM',1616576684,292000000),('7465b743-05ca-43a5-803e-3e695cc2c86f','admin','PAM',1619534207,251000000),('74d2ffd8-b540-44f0-870c-5b2f0331879c','admin','PAM',1617279111,166000000),('75c6ce6f-c26b-4f5a-879d-6a96281637c6','admin','PAM',1615974606,744000000),('76e9dc6e-bbae-414b-9da2-37f5a89099f7','admin','PAM',1618214679,78000000),('773df73b-57e0-460b-a20a-ff5533d06c93','admin','PAM',1619796561,197000000),('7f6457d9-b694-4fb6-babf-4e8e3c95ff79','admin','PAM',1616584989,485000000),('7f9e674f-aa91-48e8-adc7-6aa1c5405f53','admin','PAM',1616578008,848000000),('81b62a50-946d-488c-818c-5f58427db5e3','admin','PAM',1619796560,162000000),('81fa30a9-b7b2-48a8-9695-d508df39625f','admin','PAM',1615980914,63000000),('82e8842b-9fa7-4e14-a908-cde73b6d2d38','admin','PAM',1616577978,807000000),('844d202a-a311-4cac-b16b-8c53603b3a1b','admin','PAM',1616576684,22000000),('84fd696c-ec51-4433-a4e1-cd01270e9247','admin','PAM',1619534238,767000000),('853d1263-8f56-4e6b-b013-bd9feefa3224','admin','PAM',1617178742,999000000),('85da6043-e893-48e6-80af-3189fa168db1','admin','PAM',1616780645,594000000),('884bf059-a5ef-40f9-a548-7364e391dc90','admin','PAM',1615974402,854000000),('8aa78367-62b5-48bf-a968-3a9be59a809c','admin','PAM',1619796560,355000000),('8c942acc-a887-4d47-8b18-b02b52bc6c24','admin','PAM',1615975574,88000000),('8cd3c732-9ccb-49af-919b-4d408c08dc2f','admin','PAM',1617975392,806000000),('8ef3ab91-641e-4443-a666-8bb547c3ed37','admin','PAM',1620992910,179000000),('8f797262-f930-498b-9c1c-4f8b57651d73','admin','PAM',1616576682,674000000),('8f95c4d6-785d-4421-a781-b8173122e792','anonymous','anonymous',1617195837,134000000),('90207e34-c259-4810-8c8e-63e463a9d46a','admin','PAM',1615974441,9000000),('9062a8a2-5c38-450b-a624-2fb9d46b234e','admin','PAM',1616448466,106000000),('90cebd81-59d0-44ad-a9e4-f548b09ba221','admin','PAM',1616780672,764000000),('94de2b16-9a3a-43b1-b8c7-a3a2f4460481','admin','PAM',1619533700,787000000),('96d4411f-737e-453d-90f1-fce402ed9409','admin','PAM',1638363976,397000000),('98693c91-20b3-477d-a653-e623c3687118','admin','PAM',1619796559,243000000),('988294cb-6a61-47c2-991b-e24bf1f7d4d0','admin','PAM',1619796559,888000000),('997394de-0140-4f73-af08-46453eabe091','admin','PAM',1617279133,683000000),('9c44d8d4-eb03-4820-bdba-b40ecf1c4e76','admin','PAM',1619796562,843000000),('9fba326b-5308-4074-af85-510cc09ccb2c','admin','PAM',1617193825,231000000),('a1788322-11e3-4445-9023-2e46e7371f28','admin','PAM',1619796562,340000000),('a19f418c-b5e7-428d-a087-9e87342ee71e','admin','PAM',1616578003,174000000),('a2af374f-3a70-493a-a97d-052e0e4cff51','admin','PAM',1638364010,473000000),('a2cfcff0-7cff-4b22-8dfb-f874b04087d4','admin','PAM',1616780710,799000000),('a45acde2-b6ef-4a31-8af8-f2dd9f515d00','admin','PAM',1619796561,307000000),('a5cc41b8-e44e-4ffb-9bb4-1e390556e576','admin','PAM',1615980952,91000000),('a7769102-5eb3-4421-ac5b-38499fa11faa','admin','PAM',1616780694,126000000),('ac54aec11c940e9b1d979a9ee19c9d5153ec26c5253023ddb80cb8b133b9cdd2c28709c1bb1f0f723c2c4cf3c3bdc2ae4bd47e172f75f7fa0b6f85e67dab0903','admin','PAM',1568207645,925000000),('b099c7fe-0ec3-43a3-8bb3-7649df508e56','admin','PAM',1617193825,344000000),('b369f624-973c-4fec-8d78-32c204efd272','admin','PAM',1616778763,622000000),('b4dc07f5-11f5-4255-b3f1-cf7b067d7e2e','admin','PAM',1616780550,962000000),('b58a0cac-2a78-4e3e-9859-9bf843c5df46','admin','PAM',1619796560,955000000),('b60187f1-eeff-4a72-9958-65b7d4c3781a','admin','PAM',1616576682,850000000),('b77b1e36-0f12-49e2-8129-447330391e5e','admin','PAM',1619796560,670000000),('b964a674-aa5b-4f59-99cc-a81728d2b987','admin','PAM',1616578014,558000000),('b9aec957-956d-45e3-91cc-dc8ac3f6e11e','admin','PAM',1619796562,473000000),('ba1aa66b-0e9c-4e66-91e1-9ef1cf10d896','admin','PAM',1615974373,179000000),('bbd728b1-3c67-49f8-ab70-6635cf7bfac3','admin','PAM',1638364010,516000000),('c2fee268-e8fe-4d37-a2b4-fb511446c3c0','admin','PAM',1615980953,973000000),('c3dcad2b-a835-4588-8c69-092dd9250d67','admin','PAM',1616780529,433000000),('c6fe4c18-7d90-4109-87eb-ec3c5903491f','admin','PAM',1617193825,287000000),('c7cfa8c1-575c-4916-b91d-5a9ae6e4301a','admin','PAM',1617193824,431000000),('c93b7f56-0cfb-4bb1-9b1a-9b4108ad41d0','admin','PAM',1615975670,987000000),('c9c24921-a24b-40c1-bf6c-c67c62e844ce','admin','PAM',1616780803,609000000),('cc56ed5b-231f-447e-9322-fa7da654fae8','admin','PAM',1619532818,674000000),('ce08eacb-19cc-446f-907f-e73a66a4cfda','admin','PAM',1615980980,276000000),('ce966936-7729-448e-8d88-0da83ddcacc6','admin','PAM',1617279275,27000000),('cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e','administration','CaosDB',0,0),('cfd0704d-74ad-4c9a-a726-5867f42841c0','admin','PAM',1616448435,12000000),('d10afd02-0a9c-47bb-9f84-e7084a3af4b8','admin','PAM',1619532859,762000000),('d12236ed-6962-4632-ae88-daf55af6e1bb','admin','PAM',1616576682,430000000),('d1a18246-3c3c-4b9b-aca7-73f226c633f4','admin','PAM',1616578019,221000000),('d245250a-16e2-4693-b7e6-ca848afa33f7','admin','PAM',1638364010,226000000),('d2d70958-fe90-4273-b3df-99963c4998c7','admin','PAM',1619796562,633000000),('d2ef9a55-7933-4171-be98-5da4823ad913','admin','PAM',1617178790,882000000),('d3e28596-fea5-43dc-93db-327bd7fca05a','admin','PAM',1615975705,381000000),('d516afc2-9e06-44a0-95f4-931f69003802','admin','PAM',1619796562,82000000),('d6426749-5745-4778-8768-49caf056d2fc','admin','PAM',1638363716,323000000),('d813871b-b3e7-4c23-9f80-8589be44bc0b','admin','PAM',1617279221,681000000),('d9678ec8-b904-4711-8e86-38bded3a9898','admin','PAM',1617193824,820000000),('d993bf99-78c0-4ae3-bbb8-8dc9609e43aa','anonymous','anonymous',1617195833,36000000),('d9a65f6f-15d6-4326-a533-f69869b33b55','anonymous','anonymous',1617195844,893000000),('da288cc5-3320-4a07-b0ce-6e504ca4817a','admin','PAM',1615980934,661000000),('db05953e-2cd8-4041-b2e6-e94c58c8a92f','admin','PAM',1615975804,756000000),('dc59320b-4b53-47b7-b862-c06748616e27','admin','PAM',1617193824,602000000),('de3494c7-6b51-4020-a091-58f3854ea880','admin','PAM',1619796561,45000000),('deabc67e-a5cf-475d-8852-9f85da0c2511','admin','PAM',1615910416,304000000),('e06caa81-5870-4484-aeec-e461d7d22199','admin','PAM',1616587819,326000000),('e4fb8283-1e48-4dce-b541-45e82d70dd2b','admin','PAM',1616587718,967000000),('e66d6f26-48dd-420c-b648-08e627431818','admin','PAM',1615975570,370000000),('e89852bd-ec47-4a47-8871-eaf4cd65e839','admin','PAM',1617193965,196000000),('eb5c3f84-d3f1-4315-b599-9632cbcd764c','admin','PAM',1620992910,50000000),('eb937b8b-25bc-496a-bbb1-33767dc3ff6b','admin','PAM',1638364010,426000000),('ec8dc845-9cb1-4dfc-a715-812265e6bebb','admin','PAM',1615974604,972000000),('ed6d1ac3-76b9-4ee4-b1cd-57aa21ee454f','admin','PAM',1617279362,35000000),('ef5a2950-761b-4b9f-b104-b920c155f9ad','admin','PAM',1617193825,178000000),('f0d20c92-57fd-4924-8e2b-331a985e16b0','admin','PAM',1615910524,996000000),('f21ea47d-3c53-496b-80a7-df263953e7a9','admin','PAM',1616780739,984000000),('f2577a7b-8922-4e57-bdce-2a940ab8fc45','admin','PAM',1616778755,948000000),('f292d60f-d18b-4b93-98ba-bd6fdd95b5f5','admin','PAM',1616577985,656000000),('f344a7ec-90d2-498b-b473-7876f9b8ece5','admin','PAM',1617193824,275000000),('f35b0ee2-ee51-495a-a0d8-da5692a5d9f4','admin','PAM',1616576683,753000000),('f45f4a8e-666a-4be8-8392-0941c3263290','admin','PAM',1617193824,749000000),('f6e0593e-a3a4-4c2d-b837-70386c9307d0','admin','PAM',1617193824,132000000),('f9ae6680-d438-4149-9b45-c4981267da57','admin','PAM',1617194028,244000000),('ff44a5c0-5b1d-4c0d-9dbd-d864c4873bc7','admin','PAM',1616587705,893000000);
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units_lin_con`
--

DROP TABLE IF EXISTS `units_lin_con`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units_lin_con` (
  `signature_from` bigint(20) NOT NULL,
  `signature_to` bigint(20) NOT NULL,
  `a` decimal(65,30) NOT NULL,
  `b_dividend` int(11) NOT NULL,
  `b_divisor` int(11) NOT NULL,
  `c` decimal(65,30) NOT NULL,
  PRIMARY KEY (`signature_from`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units_lin_con`
--

LOCK TABLES `units_lin_con` WRITE;
/*!40000 ALTER TABLE `units_lin_con` DISABLE KEYS */;
INSERT INTO `units_lin_con` VALUES (-2509044298,-1100862590,0.000000000000000000000000000000,1,100,0.000000000000000000000000000000),(2467429176,223047008,273.150000000000000000000000000000,1,1,0.000000000000000000000000000000),(4407456500,1153929596,0.000000000000000000000000000000,1000,1,0.000000000000000000000000000000);
/*!40000 ALTER TABLE `units_lin_con` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info` (
  `realm` varbinary(255) NOT NULL,
  `name` varbinary(255) NOT NULL,
  `email` varbinary(255) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `entity` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`realm`,`name`),
  KEY `subject_entity` (`entity`),
  CONSTRAINT `subjects_ibfk_1` FOREIGN KEY (`entity`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_info`
--

LOCK TABLES `user_info` WRITE;
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `realm` varbinary(255) NOT NULL,
  `user` varbinary(255) NOT NULL,
  `role` varbinary(255) NOT NULL,
  PRIMARY KEY (`realm`,`user`,`role`),
  KEY `user_roles_ibfk_1` (`role`),
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`role`) REFERENCES `roles` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'caosdb'
--
/*!50003 DROP FUNCTION IF EXISTS `CaosDBVersion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `CaosDBVersion`() RETURNS varchar(255) CHARSET utf8 COLLATE utf8_unicode_ci
    DETERMINISTIC
RETURN 'v5.0.0' ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `constructDateTimeWhereClauseForColumn` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `constructDateTimeWhereClauseForColumn`(seconds_col VARCHAR(255), nanos_col VARCHAR(255), vDateTimeSecLow VARCHAR(255), vDateTimeNSLow VARCHAR(255), vDateTimeSecUpp VARCHAR(255), vDateTimeNSUpp VARCHAR(255), operator CHAR(4)) RETURNS varchar(20000) CHARSET utf8 COLLATE utf8_unicode_ci
    DETERMINISTIC
BEGIN

    DECLARE isInterval BOOLEAN DEFAULT vDateTimeSecUpp IS NOT NULL or vDateTimeNSUpp IS NOT NULL;
    DECLARE operator_prefix CHAR(1) DEFAULT LEFT(operator,1);

    IF isInterval THEN
        IF operator = '=' THEN
            RETURN " 0=1";
        ELSEIF operator = '!=' THEN
            RETURN " 0=1";
        ELSEIF operator = '>' or operator = '<=' THEN
            RETURN CONCAT(" ", seconds_col, operator_prefix, vDateTimeSecUpp);
        ELSEIF operator = '<' or operator = '>=' THEN
            RETURN CONCAT(" ", seconds_col, operator_prefix, vDateTimeSecLow);
        ELSEIF operator = "(" THEN
            RETURN CONCAT(" ", seconds_col, ">=", vDateTimeSecLow, " AND ",seconds_col, "<", vDateTimeSecUpp);
        ELSEIF operator = "!(" THEN
            RETURN CONCAT(" ", seconds_col, "<", vDateTimeSecLow, " OR ", seconds_col, ">=", vDateTimeSecUpp);
        END IF;
    ELSE
        IF operator = '=' THEN
            RETURN CONCAT(" ",
                seconds_col,
                "=", vDateTimeSecLow, IF(vDateTimeNSLow IS NULL, CONCAT(' AND ', nanos_col, ' IS NULL'), CONCAT(' AND ',
                    nanos_col,
                '=', vDateTimeNSLow)));
        ELSEIF operator = '!=' THEN
            RETURN CONCAT(" ",
                seconds_col,
                "!=", vDateTimeSecLow, IF(vDateTimeNSLow IS NULL, '', CONCAT(' OR ',
                        nanos_col,
                        '!=', vDateTimeNSLow)));
        ELSEIF operator = '>' or operator = '<' THEN
            RETURN CONCAT(" ",
                seconds_col, operator, vDateTimeSecLow, IF(vDateTimeNSLow IS NULL, '', CONCAT(' OR (',seconds_col,'=', vDateTimeSecLow, ' AND ',nanos_col, operator, vDateTimeNSLow, ')')));
        ELSEIF operator = '>=' or operator = '<=' THEN
            RETURN CONCAT(
                " ", seconds_col, operator, vDateTimeSecLow,
                IF(vDateTimeNSLow IS NULL,
                    '',
                    CONCAT(
                        ' AND (', seconds_col, operator_prefix, vDateTimeSecLow,
                        ' OR ', nanos_col, operator, vDateTimeNSLow,
                        ' OR ', nanos_col, ' IS NULL)')));
        ELSEIF operator = "(" THEN
            RETURN IF(vDateTimeNSLow IS NULL,CONCAT(" ",seconds_col,"=", vDateTimeSecLow),CONCAT(" ",seconds_col,"=",vDateTimeSecLow," AND ",nanos_col,"=",vDateTimeNSLow));
        ELSEIF operator = "!(" THEN
            RETURN IF(vDateTimeNSLow IS NULL,CONCAT(" ",seconds_col,"!=",vDateTimeSecLow, ""),CONCAT(" ",seconds_col,"!=",vDateTimeSecLow," OR ",nanos_col, " IS NULL OR ", nanos_col, "!=",vDateTimeNSLow));
        END IF;
    END IF;
    return ' 0=1';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `convert_unit` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `convert_unit`(unit_sig BIGINT, value DECIMAL(65,30)) RETURNS decimal(65,30)
    DETERMINISTIC
BEGIN
    DECLARE ret DECIMAL(65,30) DEFAULT value;

    SELECT (((value+a)*b_dividend)/b_divisor+c) INTO ret FROM units_lin_con WHERE signature_from=unit_sig;
    RETURN ret;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getAggValueWhereClause` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `getAggValueWhereClause`(entities VARCHAR(255), properties VARCHAR(255)) RETURNS varchar(20000) CHARSET utf8 COLLATE utf8_unicode_ci
    DETERMINISTIC
BEGIN
        RETURN CONCAT(" EXISTS (SELECT 1 FROM `", entities, "` AS ent WHERE ent.id = subdata.entity_id LIMIT 1)", IF(properties IS NOT NULL AND properties != '', CONCAT(" AND EXISTS (SELECT 1 FROM `", properties, "` as props WHERE props.id = subdata.property_id LIMIT 1)"),''));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getDateTimeWhereClause` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `getDateTimeWhereClause`(vDateTime VARCHAR(255), operator CHAR(4)) RETURNS varchar(20000) CHARSET utf8 COLLATE utf8_unicode_ci
    DETERMINISTIC
BEGIN
    DECLARE sep_loc INTEGER DEFAULT LOCATE('--',vDateTime);
    DECLARE vDateTimeLow VARCHAR(255) DEFAULT IF(sep_loc != 0, SUBSTRING_INDEX(vDateTime, '--',1), vDateTime);
    DECLARE vDateTimeUpp VARCHAR(255) DEFAULT IF(sep_loc != 0, SUBSTRING_INDEX(vDateTime, '--',-1), NULL);

    DECLARE vDateTimeSecLow VARCHAR(255) DEFAULT SUBSTRING_INDEX(vDateTimeLow, 'UTC', 1);
    DECLARE vDateTimeNSLow VARCHAR(255) DEFAULT IF(SUBSTRING_INDEX(vDateTimeLow, 'UTC', -1)='',NULL,SUBSTRING_INDEX(vDateTimeLow, 'UTC', -1));

    DECLARE vDateTimeSecUpp VARCHAR(255) DEFAULT IF(sep_loc != 0, SUBSTRING_INDEX(vDateTimeUpp, 'UTC', 1), NULL);
    DECLARE vDateTimeNSUpp VARCHAR(255) DEFAULT IF(sep_loc != 0 AND SUBSTRING_INDEX(vDateTimeUpp, 'UTC', -1)!='',SUBSTRING_INDEX(vDateTimeUpp, 'UTC', -1),NULL);


    RETURN constructDateTimeWhereClauseForColumn("subdata.value", "subdata.value_ns", vDateTimeSecLow, vDateTimeNSLow, vDateTimeSecUpp, vDateTimeNSUpp, operator);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getDateWhereClause` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `getDateWhereClause`(vDateTimeDotNotation VARCHAR(255), operator CHAR(4)) RETURNS varchar(20000) CHARSET utf8 COLLATE utf8_unicode_ci
    DETERMINISTIC
BEGIN
    DECLARE isInterval INTEGER DEFAULT LOCATE('--',vDateTimeDotNotation);
    
    DECLARE vILB VARCHAR(255) DEFAULT IF(isInterval != 0, SUBSTRING_INDEX(vDateTimeDotNotation, '--', 1), vDateTimeDotNotation);
    
    DECLARE vEUB VARCHAR(255) DEFAULT IF(isInterval != 0, SUBSTRING_INDEX(vDateTimeDotNotation, '--', -1), NULL);
    DECLARE vILB_Date INTEGER DEFAULT SUBSTRING_INDEX(vILB, '.', 1);
    DECLARE vEUB_Date INTEGER DEFAULT SUBSTRING_INDEX(vEUB, '.', 1);
    
    DECLARE hasTime INTEGER DEFAULT LOCATE('.NULL.NULL',vILB);
    
    DECLARE dom INTEGER DEFAULT vILB_Date % 100;
    
    DECLARE mon INTEGER DEFAULT ((vILB_Date % 10000) - dom) / 100;
    
    DECLARE yea INTEGER DEFAULT (vILB_Date - (vILB_Date % 10000)) / 10000;

    IF operator = '=' and hasTime != 0 THEN
        RETURN CONCAT(" subdata.value=", vILB_Date);
    ELSEIF operator = "!=" and hasTime != 0 THEN
        IF mon != 0  and dom != 0 THEN
            RETURN CONCAT(" subdata.value!=", vILB_Date, " and subdata.value%100!=0");
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" subdata.value!=", vILB_Date, " and subdata.value%100=0 and subdata.value%10000!=0");
        ELSE
            RETURN CONCAT(" subdata.value!=", vILB_Date, " and subdata.value%10000=0");
        END IF;
    ELSEIF operator = "(" and hasTime != 0 THEN
        IF mon != 0 and dom != 0 THEN
            RETURN CONCAT(" subdata.value=", vILB_Date);
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" subdata.value=",vILB_Date," OR (subdata.value>", vILB_Date, " and subdata.value<", vEUB_Date, " and subdata.value%10000!=0)");
        ELSE
            RETURN CONCAT(" subdata.value=",vILB_Date," OR (subdata.value>", vILB_Date, " and subdata.value<", vEUB_Date,")");
        END IF;
    ELSEIF operator = "!(" THEN
        IF hasTime = 0 THEN
            RETURN " 0=0";
        END IF;
        IF mon != 0 and dom != 0 THEN
            RETURN CONCAT(" subdata.value!=",vILB_Date);
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" (subdata.value!=",vILB_Date, " AND subdata.value%100=0) OR ((subdata.value<", vILB_Date, " or subdata.value>", vEUB_Date, ") and subdata.value%100!=0)");
        ELSE
            RETURN CONCAT(" (subdata.value!=",vILB_Date, " AND subdata.value%10000=0) OR ((subdata.value<", vILB_Date, " or subdata.value>=", vEUB_Date, ") and subdata.value%10000!=0)");
        END IF;
    ELSEIF operator = "<" THEN
        IF mon != 0 and dom != 0 THEN
            RETURN CONCAT(" subdata.value<", vILB_Date, " and (subdata.value%100!=0 or (subdata.value<", yea*10000+mon*100, " and subdata.value%10000!=0) or (subdata.value<", yea*10000, " and subdata.value%10000=0))");
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" subdata.value<", vILB_Date, " and (subdata.value%10000!=0 or (subdata.value<", yea*10000, "))");
        ELSE
            RETURN CONCAT(" subdata.value<", vILB_Date);
        END IF;
    ELSEIF operator = ">" THEN
        IF mon != 0 and dom != 0 THEN
            RETURN CONCAT(" subdata.value>", vILB_Date);
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" subdata.value>=",vEUB_Date);
        ELSE
            RETURN CONCAT(" subdata.value>=",vEUB_Date);
        END IF;
    ELSEIF operator = "<=" THEN
        IF mon != 0 and dom != 0 THEN
            
            RETURN CONCAT(" subdata.value<=", vILB_Date,
                          " or (subdata.value<=", yea*10000 + mon*100, " and subdata.value%100=0)");
        ELSEIF mon != 0 THEN
            
            RETURN CONCAT(" subdata.value<", vEUB_Date);
        ELSE
            
            RETURN CONCAT(" subdata.value<", vEUB_Date);
        END IF;
    ELSEIF operator = ">=" THEN
        IF mon != 0 and dom != 0 THEN
            
            RETURN CONCAT(" subdata.value>=", vILB_Date,
                          " or (subdata.value>=", yea*10000 + mon*100, " and subdata.value%100=0)",
                          " or (subdata.value>=", yea*10000, " and subdata.value%10000=0)");
        ELSEIF mon != 0 THEN
            
            RETURN CONCAT(" subdata.value>=", yea*10000 + mon*100,
                          " or (subdata.value>=", yea*10000, " and subdata.value%10000=0)");
        ELSE
            
            RETURN CONCAT(" subdata.value>=", yea*10000);
        END IF;
    END IF;

    return ' 0=1';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getDoubleWhereClause` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `getDoubleWhereClause`(value DOUBLE, unit_sig BIGINT, valueStdUnit DECIMAL(65,30), stdUnit_sig BIGINT, o CHAR(4)) RETURNS varchar(20000) CHARSET utf8 COLLATE utf8_unicode_ci
    DETERMINISTIC
BEGIN
    RETURN IF(unit_sig IS NULL AND value IS NOT NULL, 
        CONCAT('subdata.value ', o, ' \'', value, '\''), 
        CONCAT(
            IF(value IS NULL, '', 
                CONCAT('(subdata.unit_sig=', unit_sig, ' AND subdata.value ', o, ' \'', value, '\') OR ')), 
        	IF(unit_sig = stdUnit_sig,'',CONCAT('(subdata.unit_sig=', stdUnit_sig,' AND subdata.value ', o, ' \'', valueStdUnit, '\') OR ')),'(standard_unit(subdata.unit_sig)=', stdUnit_sig,' AND convert_unit(subdata.unit_sig,subdata.value) ', o, ' ', valueStdUnit, ')')); 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_head_relative` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `get_head_relative`(EntityID INT UNSIGNED,
    HeadOffset INT UNSIGNED) RETURNS varbinary(255)
    READS SQL DATA
BEGIN
    
    
    
    
    RETURN (
        SELECT e.version
            FROM entity_version AS e
            WHERE e.entity_id = EntityID
            ORDER BY e._iversion DESC
            LIMIT 1 OFFSET HeadOffset
        );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_head_version` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `get_head_version`(EntityID INT UNSIGNED) RETURNS varbinary(255)
    READS SQL DATA
BEGIN
    RETURN get_head_relative(EntityID, 0);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_iversion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `get_iversion`(EntityID INT UNSIGNED,
    Version VARBINARY(255)) RETURNS int(10) unsigned
    READS SQL DATA
BEGIN
    RETURN (
        SELECT e._iversion
            FROM entity_version AS e
            WHERE e.entity_id = EntityID
                AND e.version = Version
        );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_primary_parent_version` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `get_primary_parent_version`(EntityID INT UNSIGNED,
    Version VARBINARY(255)) RETURNS varbinary(255)
    READS SQL DATA
BEGIN
    RETURN (
        SELECT p.version
            FROM entity_version AS e INNER JOIN entity_version AS p
                ON (e._ipparent = p._iversion
                    AND e.entity_id = p.entity_id)
            WHERE e.entity_id = EntityID
                AND e.version = Version
        );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_version_timestamp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `get_version_timestamp`(EntityID INT UNSIGNED,
    Version VARBINARY(255)) RETURNS varchar(255) CHARSET utf8 COLLATE utf8_unicode_ci
    READS SQL DATA
BEGIN
    RETURN (
        SELECT concat(t.seconds, '.', t.nanos)
            FROM entity_version AS e INNER JOIN transactions AS t
                ON ( e.srid = t.srid )
            WHERE e.entity_id = EntityID
            AND e.version = Version
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `is_feature_config` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `is_feature_config`(_Key VARCHAR(255),
    Expected VARCHAR(255)) RETURNS tinyint(1)
    READS SQL DATA
BEGIN
    RETURN (
        SELECT f._value = Expected FROM feature_config as f WHERE f._key = _Key
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `makeStmt` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `makeStmt`(sourceSet VARCHAR(255), targetSet VARCHAR(255), data VARCHAR(20000),
                                properties VARCHAR(20000), versioned BOOLEAN) RETURNS varchar(20000) CHARSET utf8 COLLATE utf8_unicode_ci
    NO SQL
BEGIN
        IF sourceSet = "entities" AND versioned THEN
            RETURN CONCAT('INSERT IGNORE INTO `',
                targetSet,
                '` (id, _iversion) SELECT entity_id, _iversion FROM ',
                data,
                IF(properties IS NULL, '',
                    CONCAT(' AS data JOIN `', properties, '` AS prop ON (data.property_id = prop.id) WHERE ',
                           'data.entity_id = prop.id2 OR prop.id2 = 0')));
        END IF;
        RETURN CONCAT(
            IF(targetSet IS NULL,
                CONCAT('DELETE FROM `',sourceSet,'` WHERE NOT EXISTS (SELECT 1 FROM '), 
                CONCAT('INSERT IGNORE INTO `',targetSet,'` (id) SELECT id FROM `',sourceSet,'` ',
                       'WHERE EXISTS (SELECT 1 FROM ')),
            IF(properties IS NULL,
                CONCAT(data,' as data WHERE '),
                CONCAT('`',properties,'` as prop JOIN ',data,' as data ON (data.property_id=prop.id) WHERE ',
                       '(data.entity_id=prop.id2 OR prop.id2=0) AND ')),
            'data.entity_id=`', sourceSet, '`.`id` LIMIT 1)'
        );

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ms` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `ms`(ps bigint(20) unsigned  ) RETURNS double
return  TRUNCATE(ps/1000000000,3) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `standard_unit` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `standard_unit`(unit_sig BIGINT) RETURNS bigint(20)
    DETERMINISTIC
BEGIN
    DECLARE ret BIGINT DEFAULT unit_sig;

    SELECT signature_to INTO ret FROM units_lin_con WHERE signature_from=unit_sig;
    RETURN ret;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `_get_head_iversion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `_get_head_iversion`(EntityID INT UNSIGNED) RETURNS int(10) unsigned
    READS SQL DATA
BEGIN
    
    
    
    
    RETURN (
        SELECT e._iversion
            FROM entity_version AS e
            WHERE e.entity_id = EntityID
            ORDER BY e._iversion DESC
            LIMIT 1
        );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `_get_version` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` FUNCTION `_get_version`(EntityID INT UNSIGNED,
    IVersion INT UNSIGNED) RETURNS varbinary(255)
    READS SQL DATA
BEGIN
    RETURN (
        SELECT version FROM entity_version
            WHERE entity_id = EntityID
            AND _iversion = IVersion
        );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `applyBackReference` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `applyBackReference`(in sourceSet VARCHAR(255), targetSet VARCHAR(255),
    in propertiesTable VARCHAR(255), in entitiesTable VARCHAR(255), in subQuery BOOLEAN,
    in versioned BOOLEAN)
BEGIN
    DECLARE newTableName VARCHAR(255) DEFAULT NULL;


    IF subQuery IS TRUE THEN
        call registerTempTableName(newTableName);

        SET @createBackRefSubQueryTableStr = CONCAT('CREATE TEMPORARY TABLE `',newTableName,'` ( entity_id INT UNSIGNED NOT NULL, id INT UNSIGNED NOT NULL, CONSTRAINT `',newTableName,'PK` PRIMARY KEY (id, entity_id))');

        PREPARE createBackRefSubQueryTable FROM @createBackRefSubQueryTableStr;
        EXECUTE createBackRefSubQueryTable;
        DEALLOCATE PREPARE createBackRefSubQueryTable;

        SET @backRefSubResultSetStmtStr = CONCAT('INSERT IGNORE INTO `',
            newTableName,
            '` (id,entity_id) SELECT entity_id AS id, value AS entity_id FROM `reference_data` AS data ',
            'WHERE EXISTS (SELECT 1 FROM `',
                sourceSet,
                '` AS source WHERE source.id=data.value LIMIT 1)',
            IF(propertiesTable IS NULL,
                '',
                CONCAT(' AND EXISTS (SELECT 1 FROM `',
                    propertiesTable,
                    '` AS p WHERE p.id=data.property_id LIMIT 1)')),
            IF(entitiesTable IS NULL,
                '',
                CONCAT(' AND EXISTS (SELECT 1 FROM `',
                    entitiesTable,
                    '` AS e WHERE e.id=data.entity_id LIMIT 1)'))
        );

        PREPARE backRefSubResultSetStmt FROM @backRefSubResultSetStmtStr;
        EXECUTE backRefSubResultSetStmt;
        DEALLOCATE PREPARE backRefSubResultSetStmt;

        SELECT newTableName as list;
    ELSE
        IF versioned THEN
            IF sourceSet = "entities" THEN
                
                SET @stmtBackRefStr = CONCAT('INSERT IGNORE INTO `',
                    targetSet,
                    '` (id, _iversion) SELECT source.id, _get_head_iversion(source.id)',
                    
                    ' FROM entities AS source WHERE EXISTS (',
                        'SELECT 1 FROM `reference_data` AS data WHERE data.value=source.id AND (',
                            'data.value_iversion IS NULL OR data.value_iversion=_get_head_iversion(source.id))',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),
                    ') UNION ALL ',
                    
                    'SELECT source.id, source._iversion FROM archive_entities AS source WHERE EXISTS (',
                        'SELECT 1 FROM `reference_data` AS data WHERE data.value=source.id AND ',
                          '(data.value_iversion IS NULL OR data.value_iversion=source._iversion)',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),

                    ')');
            ELSEIF targetSet IS NULL OR sourceSet = targetSet THEN
                SET @stmtBackRefStr = CONCAT('DELETE FROM `',
                    sourceSet,
                    '` WHERE NOT EXISTS (SELECT 1 FROM `reference_data` AS data WHERE data.value=`',
                    sourceSet,
                    '`.`id` AND ( data.value_iversion IS NULL OR data.value_iversion=`',
                    sourceSet,
                    '`._iversion)',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),
                    ')');
            ELSE
                SET @stmtBackRefStr = CONCAT('INSERT IGNORE INTO `',
                    targetSet,
                    '` (id, _iversion) SELECT source.id, source._iversion FROM `',
                    sourceSet,
                    '` AS source WHERE EXISTS (',
                    'SELECT 1 FROM `reference_data` AS data WHERE data.value=source.id AND',
                    ' (data.value_iversion IS NULL OR data.value_iversion=source._iversion)',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),

                    ')');
            END IF;
        ELSE
            
            IF targetSet IS NULL OR sourceSet = targetSet THEN
                
                SET @stmtBackRefStr = CONCAT('DELETE FROM `',
                    sourceSet,
                    '` WHERE NOT EXISTS (SELECT 1 FROM `reference_data` AS data WHERE data.value=`',
                    sourceSet,
                    '`.`id`',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT('
                            AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT('
                            AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),
                    ')');
            ELSE
                
                SET @stmtBackRefStr = CONCAT('INSERT IGNORE INTO `',
                    targetSet,
                    '` (id) SELECT id FROM `',
                    sourceSet,
                    '` AS source WHERE EXISTS (SELECT 1 FROM `reference_data` AS data WHERE data.value=source.id',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),
                    ')');
            END IF;
        END IF;

        PREPARE stmtBackRef FROM @stmtBackRefStr;
        EXECUTE stmtBackRef;
        DEALLOCATE PREPARE stmtBackRef;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `applyIDFilter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `applyIDFilter`(in sourceSet VARCHAR(255), in targetSet VARCHAR(255),
    in o CHAR(2), in vInt BIGINT, in agg CHAR(3), in versioned BOOLEAN)
IDFILTER_LABEL: BEGIN
DECLARE data VARCHAR(20000) DEFAULT NULL;
DECLARE aggVal VARCHAR(255) DEFAULT NULL;


IF agg IS NOT NULL THEN
    IF versioned THEN
        
        SELECT 1 FROM id_agg_with_versioning_not_implemented;
    END IF;
    SET @stmtIDAggValStr = CONCAT(
        "SELECT ",
        agg,
        "(id) INTO @sAggVal FROM `",
        sourceSet,
        "`");
    PREPARE stmtIDAggVal FROM @stmtIDAggValStr;
    EXECUTE stmtIDAggVal;
    DEALLOCATE PREPARE stmtIDAggVal;
    SET aggVal = @sAggVal;
END IF;


IF targetSet IS NULL OR targetSet = sourceSet THEN
    SET data = CONCAT(
        "DELETE FROM `",
        sourceSet,
        "` WHERE ",
        IF(o IS NULL OR vInt IS NULL,
            "1=1",
            CONCAT("NOT id",
                o,
                vInt)),
        IF(aggVal IS NULL,
            "",
            CONCAT(" AND id!=",
                aggVal)));
ELSEIF versioned AND sourceSet = "entities" THEN

    
    SET data = CONCAT(
        "INSERT IGNORE INTO `",
        targetSet,
        '` (id, _iversion) SELECT id, _get_head_iversion(id) FROM `entities` WHERE ',
        IF(o IS NULL OR vInt IS NULL,
            "1=1",
            CONCAT("id",
                o,
                vInt)),
        IF(aggVal IS NULL,
            "",
            CONCAT(" AND id=",
                aggVal)),
        ' UNION SELECT id, _iversion FROM `archive_entities` WHERE ',
        IF(o IS NULL OR vInt IS NULL,
            "1=1",
            CONCAT("id",
                o,
                vInt)),
        IF(aggVal IS NULL,
            "",
            CONCAT(" AND id=",
                aggVal)));
    

ELSE
    SET data = CONCAT(
        "INSERT IGNORE INTO `",
        targetSet,
        IF(versioned,
            '` (id, _iversion) SELECT data.id, data._iversion FROM `',
            '` (id) SELECT data.id FROM `'),
        sourceSet,
        "` AS data WHERE ",
        IF(o IS NULL OR vInt IS NULL,
            "1=1",
            CONCAT("data.id",
                o,
                vInt)),
        IF(aggVal IS NULL,
            "",
            CONCAT(" AND data.id=",
                aggVal)));
END IF;

Set @stmtIDFilterStr = data;
PREPARE stmtIDFilter FROM @stmtIDFilterStr;
EXECUTE stmtIDFilter;
DEALLOCATE PREPARE stmtIDFilter;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `applyPOV` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `applyPOV`(in sourceSet VARCHAR(255), 
                                 in targetSet VARCHAR(255), 
                                 in propertiesTable VARCHAR(255),
                                 in refIdsTable VARCHAR(255),
                                 in o CHAR(4), 
                                 in vText VARCHAR(255),  
                                 in vInt INT,  
                                 in vDouble DOUBLE,  
                                 in unit_sig BIGINT,
                                 in vDoubleStdUnit DOUBLE,
                                 in stdUnit_sig BIGINT,
                                 in vDateTime VARCHAR(255),
                                 in vDateTimeDotNotation VARCHAR(255),
                                 in agg CHAR(3), 
                                 in pname VARCHAR(255), 
                                 in versioned BOOLEAN)
POV_LABEL: BEGIN
    DECLARE data TEXT DEFAULT NULL; 
    DECLARE sTextData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sNameData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sEnumData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sIntData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sDoubleData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sDatetimeData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sNullData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sDateData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sRefData VARCHAR(20000) DEFAULT NULL; 
    DECLARE aggValue VARCHAR(255) DEFAULT NULL;
    DECLARE aggValueWhereClause VARCHAR(20000) DEFAULT NULL;
    DECLARE distinctUnits INT DEFAULT 0;
    DECLARE usedStdUnit BIGINT DEFAULT NULL;
    DECLARE keepTabl VARCHAR(255) DEFAULT NULL;
    DECLARE existence_op VARCHAR(255) DEFAULT "EXISTS";

    IF o = '->' THEN
        
        call applyRefPOV(sourceSet,targetSet, propertiesTable, refIdsTable, versioned);
        LEAVE POV_LABEL;
    ELSEIF o = '0' THEN
        
        
        SET vText = NULL;
        SET sTextData = 'SELECT domain_id, entity_id, property_id FROM `null_data` AS subdata';

        

    ELSEIF o = '!0' THEN
        
        
        SET vText = NULL;
        
        SET sTextData = CONCAT(
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `text_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL UNION ALL ',
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `name_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL UNION ALL ',
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `enum_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL UNION ALL ',
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `integer_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL UNION ALL ',
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `double_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL UNION ALL ',
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `date_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL UNION ALL ',
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `datetime_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL UNION ALL ',
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `reference_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL');

    ELSEIF o = "(" or o = "!(" THEN  
        IF versioned THEN
            SET sTextData = IF(vText IS NULL,
            CONCAT(
                ' SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) ',
                'AS _iversion, property_id FROM `date_data` UNION ALL ',
                'SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_date_data`'),
            IF(vDateTimeDotNotation IS NULL, NULL,  
                CONCAT(' SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) ',
                       'AS _iversion, property_id FROM `date_data` AS subdata WHERE ',
                       getDateWhereClause(vDateTimeDotNotation, o), ' UNION ALL ',
                       'SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_date_data` ',
                       'AS subdata WHERE ', getDateWhereClause(vDateTimeDotNotation, o))));
            SET sDatetimeData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `datetime_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_datetime_data`',
                                                  IF(vDateTime IS NULL, NULL,
                                                                        CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `datetime_data` AS subdata WHERE ', getDateTimeWhereClause(vDateTime, o), ' UNION ALL SELECT DISTINCT domain_id, entity_id,_iversion, property_id FROM `archive_datetime_data` AS subdata WHERE ', getDateTimeWhereClause(vDateTime, o))));
        ELSE  
            SET sTextData = IF(vText IS NULL,
                ' SELECT DISTINCT domain_id, entity_id, property_id FROM `date_data`',
                IF(vDateTimeDotNotation IS NULL, NULL,
                    CONCAT(' SELECT DISTINCT domain_id, entity_id, property_id FROM `date_data` AS subdata WHERE ',
                             getDateWhereClause(vDateTimeDotNotation, o))));
            SET sDatetimeData = IF(vText IS NULL,
                ' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `datetime_data`',
                IF(vDateTime IS NULL, NULL,
                   CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `datetime_data` ',
                           'AS subdata WHERE ', getDateTimeWhereClause(vDateTime, o))));
        END IF;
        SET vText = NULL;
    ELSEIF agg IS NOT NULL THEN
        IF versioned THEN
            SELECT 1 FROM versioned_agg_pov_filter_not_implemented;
        END IF;
        

        
        SET aggValueWhereClause = CONCAT(getDoubleWhereClause(vDouble, unit_sig, vDoubleStdUnit, stdUnit_sig, o), ' AND ');
        SET aggValueWhereClause = CONCAT(IF(aggValueWhereClause IS NULL, '', aggValueWhereClause), getAggValueWhereClause(sourceSet, propertiesTable));

        
        SET @aggValueStmtStr = CONCAT('SELECT ',agg,'(subdata.value), ', agg, '(convert_unit(subdata.unit_sig,subdata.value)), COUNT(DISTINCT standard_unit(subdata.unit_sig)), max(standard_unit(subdata.unit_sig)) INTO @sAggValue, @sAggValueConvert, @distinctUnits, @StdUnitSig FROM (SELECT entity_id, property_id, value, unit_sig FROM `integer_data` UNION ALL SELECT entity_id, property_id, value, unit_sig FROM `double_data`) AS subdata WHERE ', aggValueWhereClause);

        
        PREPARE stmtAggValueStmt FROM @aggValueStmtStr;
        EXECUTE stmtAggValueStmt;
        DEALLOCATE PREPARE stmtAggValueStmt;

        SET distinctUnits = @distinctUnits;
        SET aggValue = @sAggValue;

        
        IF distinctUnits = 1 THEN
            SET aggValue = @sAggValueConvert;
            SET usedStdUnit = @StdUnitSig;
        ELSE
            call raiseWarning(CONCAT("The filter POV(",IF(pname IS NULL, 'NULL', pname),",",IF(o IS NULL, 'NULL', o),",",IF(vText IS NULL, 'NULL', vText),") with the aggregate function '", agg, "' could not match the values against each other with their units. The values had different base units. Only their numric value had been taken into account." ));
        END IF;

        IF aggValue IS NULL THEN
            SET sTextData = 'SELECT NULL as domain_id, NULL as entity_id, NULL as property_id';
        ELSE
            SET sTextData = '';
            SET sIntData = CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `integer_data` as subdata WHERE ', getDoubleWhereClause(aggValue, usedStdUnit, aggValue, usedStdUnit, '='));
            SET sDoubleData = CONCAT(' SELECT DISTINCT domain_id, entity_id, property_id FROM `double_data` as subdata WHERE ', getDoubleWhereClause(aggValue, usedStdUnit, aggValue, usedStdUnit, '='));
        END IF;

        SET vText = NULL;
    ELSE
        
        IF versioned THEN
            SET sTextData = IF(vText IS NULL,
            'SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `text_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_text_data` ',
            CONCAT(
            'SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id ',
              'FROM `text_data` AS subdata WHERE subdata.value ', o,' ? ',
            'UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id ',
              'FROM `archive_text_data` AS subdata WHERE subdata.value ', o, '?'
            ));
            SET sNameData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `name_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_name_data` ', CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `name_data` AS subdata WHERE subdata.value ', o, ' ? UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_name_data` AS subdata WHERE subdata.value ', o, '?'));
            SET sEnumData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `enum_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_enum_data` ', CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `enum_data` AS subdata WHERE subdata.value ', o, ' ? UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_enum_data` AS subdata WHERE subdata.value ', o, '?'));
            IF o = "!=" AND refIdsTable IS NOT NULL THEN
                SET existence_op = "NOT EXISTS";
            END IF;
            SET sRefData = IF(vText IS NULL,
                ' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `reference_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_reference_data`',
                IF(refIdsTable IS NULL,
                    NULL,
                    CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `reference_data` AS subdata WHERE ', existence_op, ' (SELECT 1 FROM `', refIdsTable, '` AS refIdsTable WHERE subdata.value=refIdsTable.id LIMIT 1) AND subdata.status != "REPLACEMENT" UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_reference_data` AS subdata WHERE ', existence_op, ' (SELECT 1 FROM `', refIdsTable, '` AS refIdsTable WHERE subdata.value=refIdsTable.id LIMIT 1) AND subdata.status != "REPLACEMENT"')));
            SET sDoubleData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT subdata.domain_id, subdata.entity_id, _get_head_iversion(subdata.entity_id) AS _iversion, subdata.property_id FROM `double_data` AS subdata UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_double_data` ', IF(vDouble IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id), property_id FROM `double_data` AS subdata WHERE ', getDoubleWhereClause(vDouble,unit_sig,vDoubleStdUnit,stdUnit_sig,o), ' UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_double_data` AS subdata WHERE ', getDoubleWhereClause(vDouble, unit_sig, vDoubleStdUnit, stdUnit_sig, o))));
            SET sIntData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT subdata.domain_id, subdata.entity_id, _get_head_iversion(subdata.entity_id) AS _iversion, subdata.property_id FROM `integer_data` AS subdata UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_integer_data`', IF(vInt IS NULL AND vDoubleStdUnit IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `integer_data` AS subdata WHERE ', getDoubleWhereClause(vInt, unit_sig, vDoubleStdUnit, stdUnit_sig, o), ' UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_integer_data` AS subdata WHERE ', getDoubleWhereClause(vInt, unit_sig, vDoubleStdUnit, stdUnit_sig, o))));
            SET sDatetimeData = IF(vText IS NULL,' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `datetime_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_datetime_data`', IF(vDateTime IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `datetime_data` AS subdata WHERE ',getDateTimeWhereClause(vDateTime,o), ' UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_datetime_data` AS subdata WHERE ',getDateTimeWhereClause(vDateTime,o))));
            SET sDateData = IF(vText IS NULL,' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `date_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_date_data`', IF(vDateTimeDotNotation IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `date_data` AS subdata WHERE ', getDateWhereClause(vDateTimeDotNotation,o), ' UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_date_data` AS subdata WHERE ', getDateWhereClause(vDateTimeDotNotation,o))));
            SET sNullData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `null_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_null_data`', NULL);
        
        ELSE
            SET sTextData = IF(vText IS NULL, 'SELECT DISTINCT domain_id, entity_id, property_id FROM `text_data`', CONCAT('SELECT DISTINCT domain_id, entity_id, property_id FROM `text_data` AS subdata WHERE subdata.value ',o,' ?'));
            SET sNameData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `name_data`', CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `name_data` AS subdata WHERE subdata.value ', o, ' ?'));
            SET sEnumData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `enum_data`', CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `enum_data` AS subdata WHERE subdata.value ', o, ' ?'));
            IF o = "!=" AND refIdsTable IS NOT NULL THEN
                SET existence_op = "NOT EXISTS";
            END IF;
            SET sRefData = IF(vText IS NULL,
                ' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `reference_data`',
                IF(refIdsTable IS NULL,
                    NULL,
                    CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `reference_data` AS subdata WHERE ',existence_op ,' (SELECT 1 FROM `', refIdsTable, '` AS refIdsTable WHERE subdata.value=refIdsTable.id LIMIT 1) AND subdata.status != "REPLACEMENT"')));
            SET sDoubleData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT subdata.domain_id, subdata.entity_id, subdata.property_id FROM `double_data` AS subdata', IF(vDouble IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `double_data` AS subdata WHERE ', getDoubleWhereClause(vDouble,unit_sig,vDoubleStdUnit,stdUnit_sig,o))));
            SET sIntData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT subdata.domain_id, subdata.entity_id, subdata.property_id FROM `integer_data` AS subdata', IF(vInt IS NULL AND vDoubleStdUnit IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `integer_data` AS subdata WHERE ', getDoubleWhereClause(vInt, unit_sig, vDoubleStdUnit, stdUnit_sig, o))));
            SET sDatetimeData = IF(vText IS NULL,' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `datetime_data`', IF(vDateTime IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `datetime_data` AS subdata WHERE ',getDateTimeWhereClause(vDateTime,o))));
            SET sDateData = IF(vText IS NULL,' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `date_data`', IF(vDateTimeDotNotation IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `date_data` AS subdata WHERE ',getDateWhereClause(vDateTimeDotNotation,o))));
            SET sNullData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `null_data`', NULL);
        END IF;

    END IF;

    SET data = CONCAT('(',sTextData,
                IF(sNameData IS NULL, '', sNameData),
                IF(sEnumData IS NULL, '', sEnumData),
                IF(sDoubleData IS NULL, '', sDoubleData),
                IF(sIntData IS NULL, '', sIntData),
                IF(sDatetimeData IS NULL, '', sDatetimeData),
                IF(sDateData IS NULL, '', sDateData),
                IF(sRefData IS NULL, '', sRefData),
                IF(sNullData IS NULL, '', sNullData),
                ')'
            );


    call createTmpTable(keepTabl, versioned);
    IF versioned THEN
        
        SET @stmtPOVkeepTblStr = CONCAT(
            'INSERT IGNORE INTO `', keepTabl, '` (id, _iversion) SELECT entity_id AS id, _iversion FROM ', data,
            ' as data', IF(propertiesTable IS NULL, '', CONCAT(
                ' WHERE EXISTS (Select 1 from `', propertiesTable, '` AS prop ',
                  'WHERE prop.id = data.property_id AND (prop.id2=data.entity_id OR prop.id2=0))')));

        IF targetSet IS NOT NULL THEN
            SET @stmtPOVStr = CONCAT('INSERT IGNORE INTO `',
                    targetSet,
                    '` (id, _iversion) SELECT source.id, source._iversion FROM `',
                    keepTabl,
                    '` AS source');
        ELSE
        
            SET @stmtPOVStr = CONCAT('DELETE FROM `',
                    sourceSet,
                    '` WHERE NOT EXISTS (SELECT 1 FROM `',
                    keepTabl,
                    '` AS data WHERE data.id=`',
                    sourceSet,
                    '`.`id` AND data._iversion=`',
                    sourceSet,
                    '`._iversion LIMIT 1)');

        END IF;

        
        PREPARE stmt3 FROM @stmtPOVStr;
        PREPARE stmtPOVkeepTbl FROM @stmtPOVkeepTblStr;
        IF vText IS NULL THEN
            EXECUTE stmtPOVkeepTbl;
        ELSE
            SET @vText = vText;
            EXECUTE stmtPOVkeepTbl USING @vText, @vText, @vText, @vText, @vText, @vText;
        END IF;
        EXECUTE stmt3;
        DEALLOCATE PREPARE stmt3;
        DEALLOCATE PREPARE stmtPOVkeepTbl;
    ELSE
        
        SET @stmtPOVkeepTblStr = CONCAT('INSERT IGNORE INTO `', keepTabl, '` (id) SELECT DISTINCT entity_id AS id FROM ', data, ' as data', IF(propertiesTable IS NULL, '', CONCAT(' WHERE EXISTS (Select 1 from `', propertiesTable, '` AS prop WHERE prop.id = data.property_id AND (prop.id2=data.entity_id OR prop.id2=0))')));

        SET @stmtPOVStr = CONCAT(
                IF(targetSet IS NULL,
                    CONCAT('DELETE FROM `',
                        sourceSet,
                        '` WHERE NOT EXISTS (SELECT 1 FROM `'),
                    CONCAT('INSERT IGNORE INTO `',
                        targetSet,
                        '` (id) SELECT id FROM `',
                        sourceSet,
                        '` WHERE EXISTS (SELECT 1 FROM `')),
                keepTabl,
                '` AS data WHERE data.id=`',
                sourceSet,
                '`.`id` LIMIT 1)'
            );

        
        PREPARE stmt3 FROM @stmtPOVStr;
        PREPARE stmtPOVkeepTbl FROM @stmtPOVkeepTblStr;
        IF vText IS NULL THEN
            EXECUTE stmtPOVkeepTbl;
        ELSE
            SET @vText = vText;
            EXECUTE stmtPOVkeepTbl USING @vText, @vText, @vText;
        END IF;
        EXECUTE stmt3;
        DEALLOCATE PREPARE stmt3;
        DEALLOCATE PREPARE stmtPOVkeepTbl;
    END IF;

    SELECT @stmtPOVkeepTblStr as applyPOVStmt1, @stmtPOVStr as applyPOVStmt2, keepTabl as applyPOVIntermediateResultSet;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `applyRefPOV` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `applyRefPOV`(in sourceSet VARCHAR(255), in targetSet VARCHAR(255),
                                    in properties VARCHAR(255), in refs VARCHAR(255),
                                    in versioned BOOLEAN)
BEGIN
    DECLARE data VARCHAR(20000) DEFAULT CONCAT(
        '(SELECT domain_id, entity_id, property_id FROM `reference_data` AS subdata ',
        'WHERE EXISTS (SELECT 1 FROM `', refs, '` AS refs WHERE subdata.value=refs.id LIMIT 1))');

    IF versioned THEN
        SET data = CONCAT(
            '(SELECT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id ',
                'FROM `reference_data` AS subdata WHERE EXISTS (',
                    'SELECT 1 FROM `', refs, '` AS refs WHERE subdata.value=refs.id LIMIT 1) ',
            'UNION ALL SELECT domain_id, entity_id, _iversion, property_id ',
                'FROM `archive_reference_data` AS subdata WHERE EXISTS (',
                    'SELECT 1 FROM `', refs, '` AS refs WHERE subdata.value=refs.id LIMIT 1))');
    END IF;
    SET @stmtRefPOVStr = makeStmt(sourceSet,targetSet,data,properties, versioned);

    PREPARE stmt4 FROM @stmtRefPOVStr;
    EXECUTE stmt4;

    SELECT @stmtRefPOVstr as applyRefPOVStmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `applySAT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `applySAT`(in sourceSet VARCHAR(255), in targetSet VARCHAR(255), in loc MEDIUMTEXT, in op CHAR(5))
BEGIN

    IF targetSet IS NULL OR sourceSet = targetSet THEN
        SET @stmtSATString = CONCAT('DELETE FROM `', sourceSet, '` WHERE id NOT IN (SELECT file_id FROM files WHERE path ', op, ' ?)');  
    ELSE
        SET @stmtSATString = CONCAT('INSERT INTO `', targetSet, '` (id) SELECT data.id FROM `',sourceSet,'` as data WHERE EXISTS (SELECT 1 FROM `files` as f WHERE f.file_id=data.id AND f.path ', op, ' ?)');
    END IF;
    PREPARE stmtSAT FROM @stmtSATString;
	SET @loc = loc;
    EXECUTE stmtSAT USING @loc;
    DEALLOCATE PREPARE stmtSAT;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `applyTransactionFilter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `applyTransactionFilter`(in sourceSet VARCHAR(255), targetSet VARCHAR(255), in transaction VARCHAR(255), in operator_u CHAR(2), in realm VARCHAR(255), in userName VARCHAR(255), in ilb BIGINT, in ilb_nanos INT UNSIGNED, in eub BIGINT, in eub_nanos INT UNSIGNED, in operator_t CHAR(2))
BEGIN
	DECLARE data TEXT default CONCAT('(SELECT entity_id FROM transaction_log AS t WHERE t.transaction=\'', 
		transaction, 
		'\'',
		IF(userName IS NOT NULL, 
			CONCAT(' AND t.realm', operator_u, '? AND t.username', operator_u, '?'),
			'' 
		),
		IF(ilb IS NOT NULL, 
			CONCAT(" AND", constructDateTimeWhereClauseForColumn("t.seconds", "t.nanos", ilb, ilb_nanos, eub, eub_nanos, operator_t)),
			""
		),
		')'
	);

	SET @stmtTransactionStr = makeStmt(sourceSet, targetSet, data, NULL, FALSE);
	PREPARE stmtTransactionFilter from @stmtTransactionStr;
	IF userName IS NOT NULL THEN
		SET @userName = userName;
		SET @realm = realm;
		EXECUTE stmtTransactionFilter USING @realm, @userName;
	ELSE
		EXECUTE stmtTransactionFilter;
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calcComplementUnion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `calcComplementUnion`(in targetSet VARCHAR(255), in subResultSet VARCHAR(255), in universe VARCHAR(255), in versioned BOOLEAN)
BEGIN
    IF versioned AND universe = "entities" THEN
        SET @stmtComplementUnionStr = CONCAT(
            'INSERT IGNORE INTO `', targetSet,
            '` SELECT e.id, _get_head_iversion(e.id) FROM entities as e WHERE NOT EXISTS ( SELECT 1 FROM `',
            subResultSet,
            '` AS diff WHERE diff.id=e.id AND diff._iversion = _get_head_iversion(e.id)) UNION ALL SELECT e.id, e._iversion FROM archive_entities AS e WHERE NOT EXISTS ( SELECT 1 FROM `',
            subResultSet,
            '` as diff WHERE e.id = diff.id AND e._iversion = diff._iversion)');
    ELSEIF versioned THEN
        SET @stmtComplementUnionStr = CONCAT(
            'INSERT IGNORE INTO `', targetSet,
            '` SELECT id FROM `',universe,
            '` AS universe WHERE NOT EXISTS ( SELECT 1 FROM `',
                subResultSet,'`
                AS diff WHERE diff.id=universe.id AND diff._iversion = universe.id_version)');
    ELSE
        SET @stmtComplementUnionStr = CONCAT('INSERT IGNORE INTO `', targetSet, '` SELECT id FROM `',universe, '` AS universe WHERE NOT EXISTS ( SELECT 1 FROM `', subResultSet,'` AS diff WHERE diff.id=universe.id)');
    END IF;
    PREPARE stmtComplementUnion FROM @stmtComplementUnionStr;
    EXECUTE stmtComplementUnion;
    DEALLOCATE PREPARE stmtComplementUnion;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calcDifference` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `calcDifference`(in resultSetTable VARCHAR(255), in diff VARCHAR(255), in versioned BOOLEAN)
BEGIN
    IF versioned THEN
        SET @diffStmtStr = CONCAT('DELETE FROM `', resultSetTable, '` WHERE EXISTS ( SELECT 1 FROM `', diff,'` AS diff WHERE diff.id=`',resultSetTable,'`.`id` AND diff._iversion=`', resultSetTable, '`.`_iversion`)');
    ELSE
        SET @diffStmtStr = CONCAT('DELETE FROM `', resultSetTable, '` WHERE EXISTS ( SELECT 1 FROM `', diff,'` AS diff WHERE diff.id=`',resultSetTable,'`.`id`)');
    END IF;
    PREPARE diffStmt FROM @diffStmtStr;
    EXECUTE diffStmt;
    DEALLOCATE PREPARE diffStmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calcIntersection` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `calcIntersection`(in resultSetTable VARCHAR(255), in intersectWith VARCHAR(255), in versioned BOOLEAN)
BEGIN
    IF versioned THEN
        SET @diffStmtStr = CONCAT('DELETE FROM `',
            resultSetTable,
            '` WHERE NOT EXISTS ( SELECT 1 FROM `',
            intersectWith,
            '` AS diff WHERE diff.id=`',
            resultSetTable,
            '`.`id` AND diff._iversion=`',
            resultSetTable,
            '`.`_iversion`)');
    ELSE
        SET @diffStmtStr = CONCAT('DELETE FROM `', resultSetTable, '` WHERE NOT EXISTS ( SELECT 1 FROM `', intersectWith,'` AS diff WHERE diff.id=`',resultSetTable,'`.`id`)');
    END IF;
    PREPARE diffStmt FROM @diffStmtStr;
    EXECUTE diffStmt;

    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calcUnion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `calcUnion`(in targetSet VARCHAR(255), in sourceSet VARCHAR(255))
BEGIN
    SET @diffStmtStr = CONCAT('INSERT IGNORE INTO `', targetSet, '` SELECT * FROM `',sourceSet,'`');
    PREPARE diffStmt FROM @diffStmtStr;
    EXECUTE diffStmt;
    DEALLOCATE PREPARE diffStmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cleanUpLinCon` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `cleanUpLinCon`()
BEGIN

    DELETE FROM units_lin_con WHERE NOT EXISTS (SELECT '1' FROM double_data WHERE unit_sig=signature_from) AND NOT EXISTS (SELECT '1' FROM integer_data WHERE unit_sig=signature_from);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cleanUpQuery` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `cleanUpQuery`()
BEGIN
    CREATE TEMPORARY TABLE IF NOT EXISTS warnings (warning TEXT NOT NULL);
    SELECT * from warnings;

    SET @pstmtstr = CONCAT('DROP TEMPORARY TABLE IF EXISTS `warnings`',
        IF(@tempTableList IS NULL, '', CONCAT(',',@tempTableList)));
    PREPARE pstmt FROM @pstmtstr;
    EXECUTE pstmt;

    SET @tempTableList = NULL;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `copyTable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `copyTable`(in fromTable VARCHAR(255), in toTable VARCHAR(255))
BEGIN
    SET @copyTableStmtStr = CONCAT('INSERT IGNORE INTO `', toTable, '` (id) SELECT id FROM `', fromTable, '`');
    PREPARE copyTableStmt FROM @copyTableStmtStr;
    EXECUTE copyTableStmt;
    DEALLOCATE PREPARE copyTableStmt;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `createTmpTable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `createTmpTable`(out newTableName VARCHAR(255), in versioned BOOLEAN)
BEGIN
    call registerTempTableName(newTableName);

    IF versioned THEN
        SET @createTableStmtStr = CONCAT('CREATE TEMPORARY TABLE `', newTableName,
            '` ( id INT UNSIGNED, _iversion INT UNSIGNED, PRIMARY KEY (id, _iversion))' );
    ELSE
        SET @createTableStmtStr = CONCAT('CREATE TEMPORARY TABLE `', newTableName,'` ( id INT UNSIGNED PRIMARY KEY)' );
    END IF;

    PREPARE createTableStmt FROM @createTableStmtStr; 
    EXECUTE createTableStmt;
    DEALLOCATE PREPARE createTableStmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `createTmpTable2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `createTmpTable2`(out newTableName VARCHAR(255))
BEGIN
    call registerTempTableName(newTableName);
    SET @createTableStmtStr = CONCAT('CREATE TEMPORARY TABLE `', newTableName,
        '` ( id INT UNSIGNED, id2 INT UNSIGNED, domain INT UNSIGNED, CONSTRAINT `',
        newTableName,'PK` PRIMARY KEY (id,id2,domain) )' );

    PREPARE createTableStmt FROM @createTableStmtStr; 
    EXECUTE createTableStmt;
    DEALLOCATE PREPARE createTableStmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteEntity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `deleteEntity`(in EntityID INT UNSIGNED)
BEGIN

    
    DELETE FROM files where file_id=EntityID;

    
    DELETE FROM data_type
        WHERE ( domain_id = 0
            AND entity_id = 0
            AND property_id = EntityID )
        OR datatype = EntityID;
    DELETE FROM collection_type
        WHERE domain_id = 0
        AND entity_id = 0
        AND property_id = EntityID;

    
    DELETE FROM name_data
        WHERE domain_id = 0
        AND entity_id = EntityID
        AND property_id = 20;

    DELETE FROM entities where id=EntityID;

    
    DELETE FROM entity_acl
        WHERE NOT EXISTS (
            SELECT 1 FROM entities
            WHERE entities.acl = entity_acl.id LIMIT 1)
        AND NOT EXISTS (
            SELECT 1 FROM archive_entities
            WHERE archive_entities.acl = entity_acl.id LIMIT 1);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteEntityProperties` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `deleteEntityProperties`(in EntityID INT UNSIGNED)
BEGIN
    DECLARE IVersion INT UNSIGNED DEFAULT NULL;

    CALL deleteIsa(EntityID);

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        SELECT max(e._iversion) INTO IVersion 
            FROM entity_version AS e
            WHERE e.entity_id = EntityID;

        
        INSERT INTO archive_reference_data (domain_id, entity_id,
                property_id, value, value_iversion, status, pidx, _iversion)
            SELECT domain_id, entity_id, property_id, value, value_iversion,
                status, pidx, IVersion AS _iversion
            FROM reference_data
            WHERE (domain_id = 0 AND entity_id = EntityID)
            OR domain_id = EntityID;

        INSERT INTO archive_null_data (domain_id, entity_id,
                property_id, status, pidx, _iversion)
            SELECT domain_id, entity_id, property_id, status,
                pidx, IVersion AS _iversion
            FROM null_data
            WHERE (domain_id = 0 AND entity_id = EntityID)
            OR domain_id = EntityID;

        INSERT INTO archive_text_data (domain_id, entity_id,
                property_id, value, status, pidx, _iversion)
            SELECT domain_id, entity_id, property_id, value, status,
                pidx, IVersion AS _iversion
            FROM text_data
            WHERE (domain_id = 0 AND entity_id = EntityID)
            OR domain_id = EntityID;

        INSERT INTO archive_name_data (domain_id, entity_id,
                property_id, value, status, pidx, _iversion)
            SELECT domain_id, entity_id, property_id, value, status,
                pidx, IVersion AS _iversion
            FROM name_data
            WHERE (domain_id = 0 AND entity_id = EntityID)
            OR domain_id = EntityID;

        INSERT INTO archive_enum_data (domain_id, entity_id,
                property_id, value, status, pidx, _iversion)
            SELECT domain_id, entity_id, property_id, value, status,
                pidx, IVersion AS _iversion
            FROM enum_data
            WHERE (domain_id = 0 AND entity_id = EntityID)
            OR domain_id = EntityID;

        INSERT INTO archive_integer_data (domain_id, entity_id,
                property_id, value, status, pidx, _iversion, unit_sig)
            SELECT domain_id, entity_id, property_id, value, status,
                pidx, IVersion AS _iversion, unit_sig
            FROM integer_data
            WHERE (domain_id = 0 AND entity_id = EntityID)
            OR domain_id = EntityID;

        INSERT INTO archive_double_data (domain_id, entity_id,
                property_id, value, status, pidx, _iversion, unit_sig)
            SELECT domain_id, entity_id, property_id, value, status,
                pidx, IVersion AS _iversion, unit_sig
            FROM double_data
            WHERE (domain_id = 0 AND entity_id = EntityID)
            OR domain_id = EntityID;

        INSERT INTO archive_datetime_data (domain_id, entity_id,
                property_id, value, value_ns, status, pidx, _iversion)
            SELECT domain_id, entity_id, property_id, value, value_ns,
                status, pidx, IVersion AS _iversion
            FROM datetime_data
            WHERE (domain_id = 0 AND entity_id = EntityID)
            OR domain_id = EntityID;

        INSERT INTO archive_date_data (domain_id, entity_id,
                property_id, value, status, pidx, _iversion)
            SELECT domain_id, entity_id, property_id, value, status,
                pidx, IVersion AS _iversion
            FROM date_data
            WHERE (domain_id = 0 AND entity_id = EntityID)
            OR domain_id = EntityID;

        INSERT INTO archive_name_overrides (domain_id, entity_id,
                property_id, name, _iversion)
            SELECT domain_id, entity_id, property_id, name,
                IVersion AS _iversion
            FROM name_overrides
            WHERE (domain_id = 0 AND entity_id = EntityID)
            OR domain_id = EntityID;

        INSERT INTO archive_desc_overrides (domain_id, entity_id,
                property_id, description, _iversion)
            SELECT domain_id, entity_id, property_id, description,
                IVersion AS _iversion
            FROM desc_overrides
            WHERE (domain_id = 0 AND entity_id = EntityID)
            OR domain_id = EntityID;

        INSERT INTO archive_data_type (domain_id, entity_id,
                property_id, datatype, _iversion)
            SELECT domain_id, entity_id, property_id, datatype,
                IVersion AS _iversion
            FROM data_type
            WHERE (domain_id = 0 AND entity_id = EntityID)
            OR domain_id = EntityID;

        INSERT INTO archive_collection_type (domain_id, entity_id,
                property_id, collection, _iversion)
            SELECT domain_id, entity_id, property_id, collection,
                IVersion AS _iversion
            FROM collection_type
            WHERE (domain_id = 0 AND entity_id = EntityID)
            OR domain_id = EntityID;

        INSERT INTO archive_query_template_def (id, definition, _iversion)
            SELECT id, definition, IVersion AS _iversion
            FROM query_template_def
            WHERE id = EntityID;

    END IF;

    DELETE FROM reference_data
    where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
    DELETE FROM null_data
    where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
    DELETE FROM text_data
    where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
    DELETE FROM name_data
    where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
    DELETE FROM enum_data
    where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
    DELETE FROM integer_data
    where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
    DELETE FROM double_data
    where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
    DELETE FROM datetime_data
    where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
    DELETE FROM date_data
    where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;

    DELETE FROM name_overrides
    WHERE (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
    DELETE FROM desc_overrides
    WHERE (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;

    DELETE FROM data_type
    WHERE (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
    DELETE FROM collection_type
    WHERE (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;

    DELETE FROM query_template_def WHERE id=EntityID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteIsa` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `deleteIsa`(IN EntityID INT UNSIGNED)
BEGIN
    DECLARE IVersion INT UNSIGNED DEFAULT NULL;

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        SELECT max(_iversion) INTO IVersion
            FROM entity_version
            WHERE entity_id = EntityID;

        
        INSERT IGNORE INTO archive_isa (child, child_iversion, parent, direct)
            SELECT e.child, IVersion AS child_iversion, e.parent, rpath = EntityID
            FROM isa_cache AS e
            WHERE e.child = EntityID;
    END IF;

    DELETE FROM isa_cache
        WHERE child = EntityID
        OR rpath = EntityID
        OR rpath LIKE concat('%>', EntityID)
        OR rpath LIKE concat('%>', EntityID, '>%');

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteLinCon` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `deleteLinCon`(in sig BIGINT)
BEGIN

    DELETE FROM units_lin_con WHERE signature_from=sig;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_all_entity_versions` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `delete_all_entity_versions`(
    in EntityID INT UNSIGNED)
BEGIN

    DELETE FROM entity_version WHERE entity_id = EntityID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `entityACL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `entityACL`(out ACLID INT UNSIGNED, in ACLSTR VARBINARY(65525))
BEGIN
   SELECT id INTO ACLID FROM entity_acl as t WHERE t.acl=ACLSTR LIMIT 1;
   IF ACLID IS NULL THEN
        INSERT INTO entity_acl (acl) VALUES (ACLSTR);
        SET ACLID = LAST_INSERT_ID();
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `finishNegationFilter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `finishNegationFilter`(in resultSetTable VARCHAR(255), in diff VARCHAR(255))
BEGIN
    call calcDifference(resultSetTable, diff);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `finishSubProperty` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `finishSubProperty`(in sourceSet VARCHAR(255),in targetSet VARCHAR(255),
                                          in list VARCHAR(255), in versioned BOOLEAN)
BEGIN
    DECLARE data VARCHAR(20000) DEFAULT CONCAT('`',list,'`');
    SET @finishSubPropertyStmtStr = makeStmt(sourceSet, targetSet, data, NULL, versioned);

    PREPARE finishSubPropertyStmt FROM @finishSubPropertyStmtStr;
    EXECUTE finishSubPropertyStmt;
    DEALLOCATE PREPARE finishSubPropertyStmt;

    SELECT @finishSubPropertyStmtStr AS finishSubPropertyStmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getChildren` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `getChildren`(in tableName varchar(255), in versioned BOOLEAN)
BEGIN
    DECLARE found_children INT UNSIGNED DEFAULT 0;

    DROP TEMPORARY TABLE IF EXISTS dependTemp;
    CREATE TEMPORARY TABLE dependTemp (id INT UNSIGNED, _iversion INT UNSIGNED, PRIMARY KEY(id, _iversion));


    SET @initDepend = CONCAT(
        'INSERT IGNORE INTO dependTemp (id, _iversion) SELECT i.child, ',
        IF(versioned,
            '_get_head_iversion(i.child)',
            '0'),
        ' FROM isa_cache AS i INNER JOIN `',
        tableName,
        '` AS t ON (i.parent=t.id);');
    PREPARE initDependStmt FROM @initDepend;

    EXECUTE initDependStmt;
    SET found_children = found_children + ROW_COUNT();

    

    IF versioned IS TRUE THEN
        SET @initDepend = CONCAT(
            'INSERT IGNORE INTO dependTemp (id, _iversion) ',
            'SELECT i.child, i.child_iversion FROM archive_isa AS i INNER JOIN `',
            tableName,
            '` AS t ON (i.parent=t.id);');
        PREPARE initDependStmt FROM @initDepend;

        EXECUTE initDependStmt;
        SET found_children = found_children + ROW_COUNT();
    END IF;

    


    IF found_children != 0 THEN
        SET @transfer = CONCAT(
            'INSERT IGNORE INTO `',
            tableName,
            IF(versioned,
                '` (id, _iversion) SELECT id, _iversion FROM dependTemp',
                '` (id) SELECT id FROM dependTemp'));
        PREPARE transferstmt FROM @transfer;
        EXECUTE transferstmt;
        DEALLOCATE PREPARE transferstmt;
    END IF;


    DEALLOCATE PREPARE initDependStmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getDependentEntities` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `getDependentEntities`(in EntityID INT UNSIGNED)
BEGIN

DROP TEMPORARY TABLE IF EXISTS refering;		
CREATE TEMPORARY TABLE refering (
id INT UNSIGNED UNIQUE
);

INSERT IGNORE INTO refering (id) SELECT entity_id FROM reference_data WHERE (value=EntityID OR property_id=EntityID) AND domain_id=0 AND entity_id!=EntityID;
INSERT IGNORE INTO refering (id) SELECT domain_id FROM reference_data WHERE (value=EntityID OR property_id=EntityID) AND domain_id!=EntityID AND entity_id!=EntityID AND domain_id!=0; 

INSERT IGNORE INTO refering (id) SELECT entity_id FROM text_data WHERE property_id=EntityID AND domain_id=0 AND entity_id!=EntityID;
INSERT IGNORE INTO refering (id) SELECT domain_id FROM text_data WHERE property_id=EntityID AND domain_id!=EntityID AND entity_id!=EntityID AND domain_id!=0; 

INSERT IGNORE INTO refering (id) SELECT entity_id FROM enum_data WHERE property_id=EntityID AND domain_id=0 AND entity_id!=EntityID;
INSERT IGNORE INTO refering (id) SELECT domain_id FROM enum_data WHERE property_id=EntityID AND domain_id!=EntityID AND entity_id!=EntityID AND domain_id!=0; 

INSERT IGNORE INTO refering (id) SELECT entity_id FROM name_data WHERE property_id=EntityID AND domain_id=0 AND entity_id!=EntityID;
INSERT IGNORE INTO refering (id) SELECT domain_id FROM name_data WHERE property_id=EntityID AND domain_id!=EntityID AND entity_id!=EntityID AND domain_id!=0; 

INSERT IGNORE INTO refering (id) SELECT entity_id FROM integer_data WHERE property_id=EntityID AND domain_id=0 AND entity_id!=EntityID;
INSERT IGNORE INTO refering (id) SELECT domain_id FROM integer_data WHERE property_id=EntityID AND domain_id!=EntityID AND entity_id!=EntityID AND domain_id!=0; 

INSERT IGNORE INTO refering (id) SELECT entity_id FROM double_data WHERE property_id=EntityID AND domain_id=0 AND entity_id!=EntityID;
INSERT IGNORE INTO refering (id) SELECT domain_id FROM double_data WHERE property_id=EntityID AND domain_id!=EntityID AND entity_id!=EntityID AND domain_id!=0; 

INSERT IGNORE INTO refering (id) SELECT entity_id FROM datetime_data WHERE property_id=EntityID AND domain_id=0 AND entity_id!=EntityID;
INSERT IGNORE INTO refering (id) SELECT domain_id FROM datetime_data WHERE property_id=EntityID AND domain_id!=EntityID AND entity_id!=EntityID AND domain_id!=0; 

INSERT IGNORE INTO refering (id) SELECT entity_id FROM date_data WHERE property_id=EntityID AND domain_id=0 AND entity_id!=EntityID;
INSERT IGNORE INTO refering (id) SELECT domain_id FROM date_data WHERE property_id=EntityID AND domain_id!=EntityID AND entity_id!=EntityID AND domain_id!=0; 

INSERT IGNORE INTO refering (id) SELECT entity_id FROM null_data WHERE property_id=EntityID AND domain_id=0 AND entity_id!=EntityID;
INSERT IGNORE INTO refering (id) SELECT domain_id FROM null_data WHERE property_id=EntityID AND domain_id!=EntityID AND entity_id!=EntityID AND domain_id!=0; 

INSERT IGNORE INTO refering (id) SELECT entity_id from data_type WHERE datatype=EntityID AND domain_id=0 AND entity_id!=EntityID;
INSERT IGNORE INTO refering (id) SELECT domain_id from data_type WHERE datatype=EntityID;


Select id from refering WHERE id!=0 and id!=EntityID;

DROP TEMPORARY TABLE refering;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getFile` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `getFile`(in FileID INT)
BEGIN 

Select name, description, role into @name, @description, @role from entities where id=FileID LIMIT 1;

IF @role = 'file' Then
		Select path, hash, size into @FilePath, @FileHash, @FileSize from files where file_id=FileID LIMIT 1;
		Select timestamp, user_id, user_agent into @FileCreated, @FileCreator, @FileGenerator from history where entity_id=FileID AND event='insertion' LIMIT 1;

Select 
FileID as FileID,
@FilePath as FilePath,
@FileSize as FileSize,
@FileHash as FileHash,
@FileDescription as FileDescription,
@FileCreated as FileCreated,
@FileCreator as FileCreator,
@FileGenerator as FileGenerator,
NULL	as FileOwner,
NULL as FilePermission,
NULL as FileChecksum;

END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getFileIdByPath` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `getFileIdByPath`(in FilePath VARCHAR(255))
BEGIN 

Select file_id as FileID from files where path=FilePath LIMIT 1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getRole` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `getRole`(in RoleName VARCHAR(255))
BEGIN

Select e.id INTO @RoleID from entities e where e.name=RoleName AND e.role=RoleName LIMIT 1;

call retrieveEntity(@RoleID);




END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getRules` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `getRules`(in DomainID INT UNSIGNED, in EntityID INT UNSIGNED, in TransType VARCHAR(255))
BEGIN

		
		
		
SELECT rules.transaction, rules.criterion, rules.modus from rules where if(DomainID is null, rules.domain_id=0,rules.domain_id=DomainID) AND if(EntityID is null, rules.entity_id=0,rules.entity_id=EntityID) AND if(TransType is null,true=true,rules.transaction=TransType);




END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_version_history` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `get_version_history`(
    in EntityID INT UNSIGNED)
BEGIN
    
    SELECT c.version AS child,
            NULL as parent,
            t.seconds AS child_seconds,
            t.nanos AS child_nanos,
            t.username AS child_username,
            t.realm AS child_realm
        FROM entity_version AS c INNER JOIN transactions as t
        ON ( c.srid = t.srid )
        WHERE c.entity_id = EntityID
        AND c._ipparent is Null

    
    
    

    
    UNION SELECT c.version AS child,
            p.version AS parent,
            t.seconds AS child_seconds,
            t.nanos AS child_nanos,
            t.username AS child_username,
            t.realm AS child_realm
        FROM entity_version AS p
            INNER JOIN entity_version as c
            INNER JOIN transactions AS t
            ON (c._ipparent = p._iversion
                AND c.entity_id = p.entity_id
                AND t.srid = c.srid)
        WHERE p.entity_id = EntityID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initAutoIncrement` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `initAutoIncrement`()
BEGIN

    SELECT @max := MAX(entity_id)+ 1 FROM transaction_log; 
    IF @max IS NOT NULL THEN
        SET @stmtStr = CONCAT('ALTER TABLE entities AUTO_INCREMENT=',@max);
        PREPARE stmt FROM @stmtStr;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initBackReference` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `initBackReference`(in pid INT UNSIGNED, in pname VARCHAR(255), in entity_id INT UNSIGNED, in ename VARCHAR(255))
BEGIN
    DECLARE propertiesTable VARCHAR(255) DEFAULT NULL;
    DECLARE entitiesTable VARCHAR(255) DEFAULT NULL;

    IF pname IS NOT NULL THEN
        
        call createTmpTable(propertiesTable, FALSE);
        call initSubEntity(pid, pname, propertiesTable);
    END IF;

    IF ename IS NOT NULL THEN
        
        call createTmpTable(entitiesTable, FALSE);
        call initSubEntity(entity_id, ename, entitiesTable);
    END IF;

    SELECT propertiesTable, entitiesTable;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initConjunctionFilter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `initConjunctionFilter`(in sourceSet VARCHAR(255))
BEGIN
    DECLARE newTableName VARCHAR(255) DEFAULT NULL;
    call createTmpTable(newTableName);
    call copyTable(sourceSet, newTableName);
    SELECT newTableName AS newTableName;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initDisjunctionFilter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `initDisjunctionFilter`(in versioned BOOLEAN)
BEGIN
    call initEmptyTargetSet(NULL, versioned);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initEmptyTargetSet` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `initEmptyTargetSet`(in targetSet VARCHAR(255), in versioned BOOLEAN)
BEGIN
    DECLARE newTableName VARCHAR(255) DEFAULT targetSet;
    IF targetSet IS NOT NULL THEN
        SET @isNotEmptyVar = NULL; 
        SET @isEmptyStmtStr = CONCAT("SELECT 1 INTO @isNotEmptyVar FROM `",targetSet,"` LIMIT 1");
        PREPARE stmtIsNotEmpty FROM @isEmptyStmtStr;
        EXECUTE stmtIsNotEmpty;
        DEALLOCATE PREPARE stmtIsNotEmpty;
        IF @isNotEmptyVar IS NOT NULL THEN 
            call createTmpTable(newTableName, versioned);
        END IF;
    ELSE
        call createTmpTable(newTableName, versioned);
    END IF;
    SELECT newTableName AS newTableName;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initEntity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `initEntity`(in eid INT UNSIGNED, in ename VARCHAR(255),
                                   in enameLike VARCHAR(255), in enameRegexp VARCHAR(255),
                                   in resultset VARCHAR(255), in versioned BOOLEAN)
initEntityLabel: BEGIN
    DECLARE select_columns VARCHAR(255) DEFAULT '` (id) SELECT entity_id FROM name_data ';
    SET @initEntityStmtStr = NULL;

    
    
    IF versioned IS TRUE THEN
        SET select_columns = '` (id, _iversion) SELECT entity_id, _get_head_iversion(entity_id) FROM name_data ';
    END IF;
    IF ename IS NOT NULL THEN
        SET @initEntityStmtStr = CONCAT(
            'INSERT IGNORE INTO `',
            resultset,
            select_columns,
            'WHERE value=?; ');
        SET @query_param = ename;
    ELSEIF enameLike IS NOT NULL THEN
        SET @initEntityStmtStr = CONCAT(
            'INSERT IGNORE INTO `',
            resultset,
            select_columns,
            'WHERE value LIKE ?;');
        SET @query_param = enameLike;
    ELSEIF enameRegexp IS NOT NULL THEN 
        SET @initEntityStmtStr = CONCAT(
            'INSERT IGNORE INTO `',
            resultset,
            select_columns,
            'WHERE value REGEXP ?;');
        SET @query_param = enameRegexp;
    END IF;

    
    IF @initEntityStmtStr IS NOT NULL THEN
        PREPARE initEntityStmt FROM @initEntityStmtStr;
        EXECUTE initEntityStmt USING @query_param;
        DEALLOCATE PREPARE initEntityStmt;
    END IF;

    IF eid IS NOT NULL THEN
        
        SET @initEntityStmtStr = CONCAT(
            'INSERT IGNORE INTO `',
            resultset,
            IF(versioned,
                '` (id, _iversion) SELECT id, _get_head_iversion(id) ',
                '` (id) SELECT id '),
            'FROM entities WHERE id=',eid,';');
        PREPARE initEntityStmt FROM @initEntityStmtStr;
        EXECUTE initEntityStmt;
        DEALLOCATE PREPARE initEntityStmt;
    END IF;


    
    
    IF versioned IS TRUE THEN
        SET select_columns = '` (id, _iversion) SELECT entity_id, _iversion FROM archive_name_data ';
        IF ename IS NOT NULL THEN
            SET @initEntityStmtStr = CONCAT(
                'INSERT IGNORE INTO `',
                resultset,
                select_columns,
                'WHERE value=?; ');
            SET @query_param = ename;
        ELSEIF enameLike IS NOT NULL THEN
            SET @initEntityStmtStr = CONCAT(
                'INSERT IGNORE INTO `',
                resultset,
                select_columns,
                'WHERE value LIKE ?;');
            SET @query_param = enameLike;
        ELSEIF enameRegexp IS NOT NULL THEN
            SET @initEntityStmtStr = CONCAT(
                'INSERT IGNORE INTO `',
                resultset,
                'WHERE value REGEXP ?;');
            SET @query_param = enameRegexp;
        END IF;

        
        IF @initEntityStmtStr IS NOT NULL THEN
            PREPARE initEntityStmt FROM @initEntityStmtStr;
            EXECUTE initEntityStmt USING @query_param;
            DEALLOCATE PREPARE initEntityStmt;
        END IF;
    END IF;
    


    IF @initEntityStmtStr IS NOT NULL THEN
        call getChildren(resultset, versioned);
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initNegationFilter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `initNegationFilter`(in sourceSet VARCHAR(255))
BEGIN
    DECLARE newTableName VARCHAR(255) DEFAULT NULL;
    call createTmpTable(newTableName);
    call copyTable(sourceSet, newTableName);
    SELECT newTableName AS newTableName;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initPOVPropertiesTable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `initPOVPropertiesTable`(in pid INT UNSIGNED, in pname VARCHAR(255), in sourceSet VARCHAR(255))
BEGIN
    DECLARE propertiesTable VARCHAR(255) DEFAULT NULL; 
    DECLARE replTbl VARCHAR(255) DEFAULT NULL;
    DECLARE ecount INT DEFAULT 0;
    DECLARE t1 BIGINT DEFAULT 0;
    DECLARE t2 BIGINT DEFAULT 0;
    DECLARE t3 BIGINT DEFAULT 0;
    DECLARE t4 BIGINT DEFAULT 0;
    DECLARE t5 BIGINT DEFAULT 0;
    DECLARE t6 BIGINT DEFAULT 0;


    IF pname is NOT NULL THEN 
        SELECT conv( concat( substring(uid,16,3), substring(uid,10,4), substring(uid,1,8)),16,10) div 10000 - (141427 * 24 * 60 * 60 * 1000) as current_mills INTO t1 from (select uuid() uid) as alias;
        call createTmpTable2(propertiesTable);

        
        SET @initPOVPropertiesTableStmt1 = CONCAT('INSERT IGNORE INTO `', propertiesTable, '` (id, id2, domain) SELECT property_id, entity_id, domain_id from name_overrides WHERE name = ? UNION ALL SELECT entity_id, domain_id, 0 FROM name_data WHERE value = ?;');
        PREPARE stmt FROM @initPOVPropertiesTableStmt1;
        SET @pname = pname;
        EXECUTE stmt USING @pname, @pname;
        SET ecount = ROW_COUNT();

        
        SELECT conv( concat( substring(uid,16,3), substring(uid,10,4), substring(uid,1,8)),16,10) div 10000 - (141427 * 24 * 60 * 60 * 1000) as current_mills INTO t2 from (select uuid() uid) as alias;
        IF pid IS NOT NULL THEN
            SET @initPOVPropertiesTableStmt2 = CONCAT('INSERT IGNORE INTO `', propertiesTable, '` (id, id2, domain) VALUES (?, 0, 0)');
            PREPARE stmt FROM @initPOVPropertiesTableStmt2;
            SET @pid = pid;
            EXECUTE stmt USING @pid;
            SET ecount = ecount + ROW_COUNT();
        END IF;

        
        SELECT conv( concat( substring(uid,16,3), substring(uid,10,4), substring(uid,1,8)),16,10) div 10000 - (141427 * 24 * 60 * 60 * 1000) as current_mills INTO t3 from (select uuid() uid) as alias;
        IF ecount > 0 THEN
            
            call getChildren(propertiesTable, False);
        END IF;

        
        SELECT conv( concat( substring(uid,16,3), substring(uid,10,4), substring(uid,1,8)),16,10) div 10000 - (141427 * 24 * 60 * 60 * 1000) as current_mills INTO t4 from (select uuid() uid) as alias;
        IF ecount > 0 THEN
            call createTmpTable2(replTbl);
            SET @replTblStmt1 := CONCAT('INSERT IGNORE INTO `',replTbl, '` (id, id2, domain) SELECT r.value as id, r.entity_id as id2, 0 as domain_id FROM reference_data AS r WHERE status="REPLACEMENT" AND domain_id=0 AND EXISTS (SELECT * FROM `', sourceSet, '` AS s WHERE s.id=r.entity_id) AND EXISTS (SELECT * FROM `', propertiesTable, '` AS p WHERE p.domain = 0 AND p.id2=0 AND p.id=r.property_id);');
            PREPARE replStmt1 FROM @replTblStmt1;
            EXECUTE replStmt1;
            DEALLOCATE PREPARE replStmt1;
            SELECT conv( concat( substring(uid,16,3), substring(uid,10,4), substring(uid,1,8)),16,10) div 10000 - (141427 * 24 * 60 * 60 * 1000) as current_mills INTO t5 from (select uuid() uid) as alias;

            SET @replTblStmt2 := CONCAT('INSERT IGNORE INTO `', propertiesTable, '` SELECT id, id2, domain FROM `', replTbl, '`;');
            PREPARE replStmt2 FROM @replTblStmt2;
            EXECUTE replStmt2;
            DEALLOCATE PREPARE replStmt2;
            SELECT conv( concat( substring(uid,16,3), substring(uid,10,4), substring(uid,1,8)),16,10) div 10000 - (141427 * 24 * 60 * 60 * 1000) as current_mills INTO t6 from (select uuid() uid) as alias;
        END IF;
    END IF;
    SELECT propertiesTable, t1, t2, t3, t4, t5, t6, @initPOVPropertiesTableStmt1 as initPOVPropertiesTableStmt1, @initPOVPropertiesTableStmt2 as initPOVPropertiesTableStmt2, @replTblStmt1 as replTblStmt1, @replTblStmt2 as replTblStmt2;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initPOVRefidsTable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `initPOVRefidsTable`(in vInt INT UNSIGNED, in vText VARCHAR(255))
BEGIN
    DECLARE refIdsTable VARCHAR(255) DEFAULT NULL; 

    
    IF vText IS NOT NULL THEN
        
        call createTmpTable(refIdsTable, FALSE);
        call initSubEntity(vInt, vText, refIdsTable);
        
    END IF;
    SELECT refIdsTable;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initQuery` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `initQuery`(in versioned BOOLEAN)
BEGIN
    CREATE TEMPORARY TABLE IF NOT EXISTS warnings (warning TEXT NOT NULL);

    call createTmpTable(@resultSet, versioned);
    SELECT @resultSet as tablename;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initSubEntity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `initSubEntity`(in e_id INT UNSIGNED, in ename VARCHAR(255), in tableName VARCHAR(255))
BEGIN
    DECLARE ecount INT DEFAULT 0;
    DECLARE op VARCHAR(255) DEFAULT '=';

    IF LOCATE("%", ename) > 0 THEN
        SET op = "LIKE";
    END IF;

    SET @stmtStr = CONCAT('INSERT IGNORE INTO `',
        tableName,
        '` (id) SELECT entity_id FROM name_data WHERE value ',
        op,
        ' ? AND domain_id=0;');

    PREPARE stmt FROM @stmtStr;
    SET @ename = ename;
    EXECUTE stmt USING @ename;
    SET ecount = ROW_COUNT();
    DEALLOCATE PREPARE stmt;

    IF e_id IS NOT NULL THEN
        SET @stmtStr = CONCAT('INSERT IGNORE INTO `', tableName, '` (id) VALUES (', e_id, ')');
        PREPARE stmt FROM @stmtStr;
        EXECUTE stmt;
        SET ecount = ecount + ROW_COUNT();
        DEALLOCATE PREPARE stmt;
    END IF;

    IF ecount > 0 THEN
        
        call getChildren(tableName, False);
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initSubProperty` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `initSubProperty`(in sourceSet VARCHAR(255), in propertiesTable VARCHAR(255), in refIdsTable VARCHAR(255))
BEGIN
DECLARE newTableName VARCHAR(255) DEFAULT NULL;
    call registerTempTableName(newTableName);	
    
    SET @createSubPropertyListTableStr = CONCAT('CREATE TEMPORARY TABLE `', newTableName,'` ( entity_id INT UNSIGNED NOT NULL, id INT UNSIGNED NOT NULL, domain INT UNSIGNED NOT NULL, CONSTRAINT `',newTableName,'PK` PRIMARY KEY (entity_id, id, domain)) ' );
    
    PREPARE createSubPropertyListTable FROM @createSubPropertyListTableStr; 
    EXECUTE createSubPropertyListTable;
    DEALLOCATE PREPARE createSubPropertyListTable;

	SET @subResultSetStmtStr = CONCAT('INSERT IGNORE INTO `', newTableName, '` (domain, entity_id, id) 
            SELECT data1.domain_id as domain, data1.entity_id as entity_id, data1.value as id 
                FROM reference_data as data1 JOIN reference_data as data2 
                    ON (data1.domain_id=0 
                        AND data1.domain_id=data2.domain_id 
                        AND data2.entity_id=data1.entity_id 
                        AND (
                            (data1.property_id=data2.value AND data2.status="REPLACEMENT")
                            OR
                            (data1.property_id!=data2.value AND data2.status!="REPLACEMENT" AND data1.status!="REPLACEMENT" AND data1.property_id=data2.property_id)
                        )
                        AND EXISTS (SELECT 1 FROM `', sourceSet, '` as source WHERE source.id=data1.entity_id LIMIT 1)',
                        IF(propertiesTable IS NULL, '', CONCAT(' AND EXISTS (SELECT 1 FROM `', propertiesTable, '` as props WHERE props.id=data2.property_id LIMIT 1)')),
                        IF(refIdsTable IS NULL, '', CONCAT(' AND EXISTS (SELECT 1 FROM `', refIdsTable, '` as refs WHERE refs.id=data1.value LIMIT 1)')),	
		')'
        );


	PREPARE subResultSetStmt FROM @subResultSetStmtStr;
	EXECUTE subResultSetStmt;
    DEALLOCATE PREPARE subResultSetStmt;

	SELECT newTableName as list;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertEntity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `insertEntity`(in EntityName VARCHAR(255), in EntityDesc TEXT, in EntityRole VARCHAR(255), in ACL VARBINARY(65525))
BEGIN
    DECLARE NewEntityID INT UNSIGNED DEFAULT NULL;
    DECLARE NewACLID INT UNSIGNED DEFAULT NULL;
    DECLARE Hash VARBINARY(255) DEFAULT NULL;
    DECLARE Version VARBINARY(255) DEFAULT NULL;
    DECLARE Transaction VARBINARY(255) DEFAULT NULL;

    
    
    call entityACL(NewACLID, ACL);

    
    INSERT INTO entities (description, role, acl)
        VALUES (EntityDesc, EntityRole, NewACLID);

    
    SET NewEntityID = LAST_INSERT_ID();

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        
        SET Transaction = @SRID;
        SET Version = SHA1(UUID());
        CALL insert_single_child_version(NewEntityID, Hash, Version, Null, Transaction);
    END IF;

    
    
    IF EntityName IS NOT NULL THEN
        INSERT INTO name_data
            (domain_id, entity_id, property_id, value, status, pidx)
            VALUES (0, NewEntityID, 20, EntityName, "FIX", 0);
    END IF;

    SELECT NewEntityID as EntityID, Version as Version;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertEntityProperty` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `insertEntityProperty`(
    in DomainID INT UNSIGNED,
    in EntityID INT UNSIGNED,
    in PropertyID INT UNSIGNED,
    in Datatable VARCHAR(255),
    in PropertyValue TEXT,
    in PropertyUnitSig BIGINT,
    in PropertyStatus VARCHAR(255),
    in NameOverride VARCHAR(255),
    in DescOverride TEXT,
    in DatatypeOverride INT UNSIGNED,
    in Collection VARCHAR(255),
    in PropertyIndex INT UNSIGNED)
BEGIN
    DECLARE ReferenceValueIVersion INT UNSIGNED DEFAULT NULL;
    DECLARE ReferenceValue INT UNSIGNED DEFAULT NULL;
    DECLARE AT_PRESENT INTEGER DEFAULT NULL;

    CASE Datatable
    WHEN 'double_data' THEN
        INSERT INTO double_data
        (domain_id, entity_id, property_id, value, unit_sig, status, pidx)
        VALUES
        (DomainID, EntityID, PropertyID, PropertyValue, PropertyUnitSig, PropertyStatus, PropertyIndex);
    WHEN 'integer_data' THEN
        INSERT INTO integer_data
        (domain_id, entity_id, property_id, value, unit_sig, status, pidx)
        VALUES
        (DomainID, EntityID, PropertyID, PropertyValue, PropertyUnitSig, PropertyStatus, PropertyIndex);
    WHEN 'datetime_data' THEN
        INSERT INTO datetime_data
        (domain_id, entity_id, property_id, value, value_ns, status, pidx)
        VALUES
        (DomainID, EntityID, PropertyID, SUBSTRING_INDEX(PropertyValue, 'UTC', 1), IF(SUBSTRING_INDEX(PropertyValue, 'UTC', -1)='',NULL,SUBSTRING_INDEX(PropertyValue, 'UTC', -1)), PropertyStatus, PropertyIndex);
    WHEN 'reference_data' THEN

        
        SET AT_PRESENT=LOCATE("@", PropertyValue);
        IF is_feature_config("ENTITY_VERSIONING", "ENABLED") AND AT_PRESENT > 0 THEN
            SET ReferenceValue = SUBSTRING_INDEX(PropertyValue, '@', 1);
            SET ReferenceValueIVersion = get_iversion(ReferenceValue,
                SUBSTRING_INDEX(PropertyValue, '@', -1));
            
            IF ReferenceValueIVersion IS NULL THEN
                SELECT 0 from `ReferenceValueIVersion_WAS_NULL`;
            END IF;

        ELSE
            SET ReferenceValue = PropertyValue;
        END IF;

        INSERT INTO reference_data
            (domain_id, entity_id, property_id, value, value_iversion, status,
                pidx)
        VALUES
            (DomainID, EntityID, PropertyID, ReferenceValue,
                ReferenceValueIVersion, PropertyStatus, PropertyIndex);
    WHEN 'enum_data' THEN
        INSERT INTO enum_data
        (domain_id, entity_id, property_id, value, status, pidx)
        VALUES
        (DomainID, EntityID, PropertyID, PropertyValue, PropertyStatus, PropertyIndex);
    WHEN 'date_data' THEN
        INSERT INTO date_data
        (domain_id, entity_id, property_id, value, status, pidx)
        VALUES
        (DomainID, EntityID, PropertyID, SUBSTRING_INDEX(PropertyValue, '.', 1), PropertyStatus, PropertyIndex);
    WHEN 'text_data' THEN
        INSERT INTO text_data
        (domain_id, entity_id, property_id, value, status, pidx)
        VALUES
        (DomainID, EntityID, PropertyID, PropertyValue, PropertyStatus, PropertyIndex);
    WHEN 'null_data' THEN
        INSERT INTO null_data
        (domain_id, entity_id, property_id, status, pidx)
        VALUES
        (DomainID, EntityID, PropertyID, PropertyStatus, PropertyIndex);
    WHEN 'name_data' THEN
        INSERT INTO name_data
        (domain_id, entity_id, property_id, value, status, pidx)
        VALUES
        (DomainID, EntityID, PropertyID, PropertyValue, PropertyStatus, PropertyIndex);

    ELSE
        SELECT * FROM table_does_not_exist;
    END CASE;

    IF DatatypeOverride IS NOT NULL THEN
        call overrideType(DomainID, EntityID, PropertyID, DatatypeOverride);
        IF Collection IS NOT NULL THEN
            INSERT INTO collection_type (domain_id, entity_id, property_id, collection) VALUES (DomainID, EntityID, PropertyID, Collection);
        END IF;
    END IF;

    IF NameOverride IS NOT NULL THEN
        call overrideName(DomainID, EntityID, PropertyID, NameOverride);
    END IF;

    IF DescOverride IS NOT NULL THEN
        call overrideDesc(DomainID, EntityID, PropertyID, DescOverride);
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertIsa` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `insertIsa`(IN c INT UNSIGNED, IN p INT UNSIGNED)
insert_is_a_proc: BEGIN

    INSERT INTO isa_cache (child, parent, rpath) VALUES (c, p, c);

    IF p = c THEN
        
        LEAVE insert_is_a_proc;
    END IF;
    

    
    
    
    INSERT IGNORE INTO isa_cache SELECT
        c
            AS child,   
        i.parent
            AS parent,  
        IF(p=i.rpath or i.rpath=parent,  
           p,                            
           concat(p, ">", i.rpath))      
            AS rpath
        FROM isa_cache AS i WHERE i.child = p AND i.child != i.parent;  

    
    
    INSERT IGNORE INTO isa_cache SELECT
        l.child,    
        r.parent,   
        if(l.rpath=l.child and r.rpath=c,  
           c,                              
           concat(if(l.rpath=l.child,        
                     c,                         
                     concat(l.rpath, '>', c)),  
                  if(r.rpath=c,              
                     '',                        
                     concat('>', r.rpath))))    
            AS rpath
        FROM
            isa_cache as l INNER JOIN isa_cache as r
            ON (l.parent = c AND c = r.child AND l.child != l.parent); 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertLinCon` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `insertLinCon`(in signature_from BIGINT, in signature_to BIGINT, in a DECIMAL(65,30), in b_dividend BIGINT, in b_divisor BIGINT, in c DECIMAL(65,30))
BEGIN

    INSERT IGNORE INTO units_lin_con (signature_from, signature_to, a, b_dividend, b_divisor, c) VALUES (signature_from, signature_to, a, b_dividend, b_divisor, c);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `insertUser`(in Name VARCHAR(255), in Password VARCHAR(255))
BEGIN 


INSERT INTO entities (name, role, acl) VALUES (Name, 'USER', 0);

SET @LAST_UserID = LAST_INSERT_ID();

INSERT INTO passwords VALUES (@LAST_UserID, Password);

Select @LAST_UserID as UserID; 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_single_child_version` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `insert_single_child_version`(
    in EntityID INT UNSIGNED,
    in Hash VARBINARY(255),
    in Version VARBINARY(255),
    in Parent VARBINARY(255),
    in Transaction VARBINARY(255))
BEGIN
    DECLARE newiversion INT UNSIGNED DEFAULT NULL;
    DECLARE newipparent INT UNSIGNED DEFAULT NULL;

    
    IF Parent IS NOT NULL THEN
        SELECT e._iversion INTO newipparent
            FROM entity_version AS e
            WHERE e.entity_id = EntityID
            AND e.version = Parent;
        IF newipparent IS NULL THEN
            
            SELECT concat("This parent does not exists: ", Parent)
            FROM nonexisting;
        END IF;
    END IF;


    
    SELECT max(e._iversion)+1 INTO newiversion
        FROM entity_version AS e
        WHERE e.entity_id=EntityID;
    IF newiversion IS NULL THEN
        SET newiversion = 1;
    END IF;

    INSERT INTO entity_version
        (entity_id, hash, version, _iversion, _ipparent, srid)
        VALUES
        (EntityID, Hash, Version, newiversion, newipparent, Transaction);



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `intersectTable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `intersectTable`(in resultSetTable VARCHAR(255), in diff VARCHAR(255))
BEGIN
    SET @diffStmtStr = CONCAT('DELETE FROM `', resultSetTable, '` WHERE id NOT IN ( SELECT id FROM `', diff,'`)');
    PREPARE diffStmt FROM @diffStmtStr;
    EXECUTE diffStmt;

    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `isSubtype` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `isSubtype`(in c INT UNSIGNED, in p INT UNSIGNED)
BEGIN
	DECLARE ret BOOLEAN DEFAULT FALSE;
	SELECT TRUE INTO ret FROM isa_cache AS i WHERE i.child=c AND i.parent=p LIMIT 1;
    SELECT ret as ISA;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `overrideDesc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `overrideDesc`(in DomainID INT UNSIGNED, in EntityID INT UNSIGNED, in PropertyID INT UNSIGNED, in Description TEXT)
BEGIN
	INSERT INTO desc_overrides (domain_id, entity_id, property_id, description) VALUES (DomainID, EntityID, PropertyID, Description);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `overrideName` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `overrideName`(in DomainID INT UNSIGNED, in EntityID INT UNSIGNED, in PropertyID INT UNSIGNED, in Name VARCHAR(255))
BEGIN
	INSERT INTO name_overrides (domain_id, entity_id, property_id, name) VALUES (DomainID, EntityID, PropertyID, Name);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `overrideType` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `overrideType`(in DomainID INT UNSIGNED, in EntityID INT UNSIGNED, in PropertyID INT UNSIGNED, in Datatype INT UNSIGNED)
BEGIN
	INSERT INTO data_type (domain_id, entity_id, property_id, datatype) VALUES (DomainID, EntityID, PropertyID, Datatype);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `raiseWarning` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `raiseWarning`(in str VARCHAR(20000))
BEGIN
    INSERT INTO warnings VALUES (str);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registerSubdomain` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `registerSubdomain`(in amount INT UNSIGNED)
BEGIN
    DECLARE ED INTEGER DEFAULT NULL;

    SELECT COUNT(id) INTO ED FROM entities WHERE Role='DOMAIN' AND id!=0;

    WHILE ED < amount DO
        INSERT INTO entities (description, role, acl) VALUES
            (NULL, 'DOMAIN', 0);
        SET ED = ED + 1;
    END WHILE;

    SELECT id as DomainID FROM entities WHERE Role='DOMAIN' and id!=0;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registerTempTableName` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `registerTempTableName`(out newTableName VARCHAR(255))
BEGIN
    SET newTableName = md5(CONCAT(RAND(),CURRENT_TIMESTAMP()));
    SET @tempTableList = IF(@tempTableList IS NULL,
        CONCAT('`',newTableName,'`'),
        CONCAT(@tempTableList, ',`', newTableName, '`')
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reset_stats` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `reset_stats`()
BEGIN
	truncate table performance_schema.events_statements_summary_by_digest;
	truncate table performance_schema.events_statements_history_long;
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `retrieveDatatype` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `retrieveDatatype`(in DatatypeName VARCHAR(255))
BEGIN

Select e.id INTO @DatatypeID from entities e where e.name=DatatypeName AND e.role='DATATYPE' LIMIT 1;

call retrieveEntity(@DatatypeID);




END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `retrieveEntity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `retrieveEntity`(
    in EntityID INT UNSIGNED,
    in Version VARBINARY(255))
retrieveEntityBody: BEGIN
    DECLARE FilePath VARCHAR(255) DEFAULT NULL;
    DECLARE FileSize VARCHAR(255) DEFAULT NULL;
    DECLARE FileHash VARCHAR(255) DEFAULT NULL;
    DECLARE DatatypeID INT UNSIGNED DEFAULT NULL;
    DECLARE CollectionName VARCHAR(255) DEFAULT NULL;
    DECLARE IsHead BOOLEAN DEFAULT TRUE;
    DECLARE IVersion INT UNSIGNED DEFAULT NULL;


    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        
        IF Version IS NULL OR UPPER(Version) = "HEAD" THEN
            SET Version = get_head_version(EntityID);
        ELSEIF UPPER(LEFT(Version, 5)) = "HEAD~" THEN
            SET IsHead = FALSE;
            SET Version = get_head_relative(EntityID, SUBSTR(Version, 6));
        ELSE
            SELECT get_head_version(EntityID) = Version INTO IsHead;
        END IF;

        IF IsHead IS FALSE THEN
            SET IVersion=get_iversion(EntityID, Version);

            IF IVersion IS NULL THEN
                
                SELECT 0 FROM entities WHERE 0 = 1;
                LEAVE retrieveEntityBody;
            END IF;

            SELECT path, size, HEX(hash)
                INTO FilePath, FileSize, FileHash
                FROM archive_files
                WHERE file_id = EntityID
                AND _iversion = IVersion
                LIMIT 1;

            SELECT datatype
                INTO DatatypeID
                FROM archive_data_type
                WHERE domain_id = 0
                AND entity_id = 0
                AND property_id = EntityID
                AND _iversion = IVersion
                LIMIT 1;

            SELECT collection
                INTO CollectionName
                FROM archive_collection_type
                WHERE domain_id = 0
                AND entity_id = 0
                AND property_id = EntityID
                AND _iversion = IVersion
                LIMIT 1;

            
            SELECT
                ( SELECT value FROM
                    ( SELECT value FROM name_data
                        WHERE domain_id = 0
                        AND entity_ID = DatatypeID
                        AND property_id = 20
                        UNION SELECT DatatypeID AS value
                    ) AS tmp LIMIT 1 ) AS Datatype,
                CollectionName AS Collection,
                EntityID AS EntityID,
                ( SELECT value FROM archive_name_data
                    WHERE domain_id = 0
                    AND entity_ID = EntityID
                    AND property_id = 20
                    AND _iversion = IVersion
                    
                    ) AS EntityName,
                e.description AS EntityDesc,
                e.role AS EntityRole,
                FileSize AS FileSize,
                FilePath AS FilePath,
                FileHash AS FileHash,
                (SELECT acl FROM entity_acl AS a WHERE a.id = e.acl) AS ACL,
                Version AS Version
            FROM archive_entities AS e
            WHERE e.id = EntityID
            AND e._iversion = IVersion
            LIMIT 1;

            
            LEAVE retrieveEntityBody;

        END IF;
    END IF;

    SELECT path, size, hex(hash)
        INTO FilePath, FileSize, FileHash
        FROM files
        WHERE file_id = EntityID
        LIMIT 1;

    SELECT datatype INTO DatatypeID
        FROM data_type
        WHERE domain_id=0
        AND entity_id=0
        AND property_id=EntityID
        LIMIT 1;

    SELECT collection INTO CollectionName
        FROM collection_type
        WHERE domain_id=0
        AND entity_id=0
        AND property_id=EntityID
        LIMIT 1;

    SELECT
        ( SELECT value FROM name_data
            WHERE domain_id = 0
            AND entity_ID = DatatypeID
            AND property_id = 20 LIMIT 1 ) AS Datatype,
        CollectionName AS Collection,
        EntityID AS EntityID,
        ( SELECT value FROM name_data
            WHERE domain_id = 0
            AND entity_ID = EntityID
            AND property_id = 20 LIMIT 1) AS EntityName,
        e.description AS EntityDesc,
        e.role AS EntityRole,
        FileSize AS FileSize,
        FilePath AS FilePath,
        FileHash AS FileHash,
        (SELECT acl FROM entity_acl AS a WHERE a.id = e.acl) AS ACL,
        Version AS Version
    FROM entities e WHERE id = EntityID LIMIT 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `retrieveEntityParents` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `retrieveEntityParents`(
    in EntityID INT UNSIGNED,
    in Version VARBINARY(255))
retrieveEntityParentsBody: BEGIN

    DECLARE IVersion INT UNSIGNED DEFAULT NULL;
    DECLARE IsHead BOOLEAN DEFAULT TRUE;

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        IF Version IS NOT NULL THEN
            SELECT get_head_version(EntityID) = Version INTO IsHead;
        END IF;

        IF IsHead IS FALSE THEN
            SELECT e._iversion INTO IVersion
                FROM entity_version as e
                WHERE e.entity_id = EntityID
                AND e.version = Version;

            IF IVersion IS NULL THEN
                
                LEAVE retrieveEntityParentsBody;
            END IF;

            SELECT
                i.parent AS ParentID,
                ( SELECT value FROM name_data
                    WHERE domain_id = 0
                    AND entity_id = ParentID
                    AND property_id = 20
                ) AS ParentName, 
                                 
                                 
                                 
                                 
                e.description AS ParentDescription,
                e.role AS ParentRole,
                (SELECT acl FROM entity_acl AS a WHERE a.id = e.acl) AS ACL
                FROM archive_isa AS i JOIN entities AS e
                    ON (i.parent = e.id)
                WHERE i.child = EntityID
                AND i.child_iversion = IVersion
                AND i.direct IS TRUE
                ;

            LEAVE retrieveEntityParentsBody;
        END IF;
    END IF;

    SELECT
        i.parent AS ParentID,
        ( SELECT value FROM name_data
            WHERE domain_id = 0
            AND entity_id = ParentID
            AND property_id = 20 ) AS ParentName,
        e.description AS ParentDescription,
        e.role AS ParentRole,
        (SELECT acl FROM entity_acl AS a WHERE a.id = e.acl) AS ACL
        FROM isa_cache AS i JOIN entities AS e
            ON (i.parent = e.id)
        WHERE i.child = EntityID
        AND i.rpath = EntityID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `retrieveEntityProperties` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `retrieveEntityProperties`(
    in DomainID INT UNSIGNED,
    in EntityID INT UNSIGNED,
    in Version VARBINARY(255))
retrieveEntityPropertiesBody: BEGIN

    DECLARE IVersion INT UNSIGNED DEFAULT NULL;
    DECLARE IsHead BOOLEAN DEFAULT TRUE;

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        IF Version IS NOT NULL THEN
            IF DomainID = 0 THEN
                SELECT get_head_version(EntityID) = Version INTO IsHead;
            ELSE
                SELECT get_head_version(DomainID) = Version INTO IsHead;
            END IF;

        END IF;

        IF IsHead IS FALSE THEN
            SELECT e._iversion INTO IVersion
                FROM entity_version as e
                WHERE ((e.entity_id = EntityID AND DomainID = 0)
                    OR (e.entity_id = DomainID))
                AND e.version = Version;

            IF IVersion IS NULL THEN
                
                LEAVE retrieveEntityPropertiesBody;
            END IF;

            
            SELECT
                property_id AS PropertyID,
                value AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_double_data
            WHERE domain_id = DomainID
            AND entity_id = EntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                property_id AS PropertyID,
                value AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_integer_data
            WHERE domain_id = DomainID
            AND entity_id = EntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                property_id AS PropertyID,
                CONCAT(value, '.NULL.NULL') AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_date_data
            WHERE domain_id = DomainID
            AND entity_id = EntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                property_id AS PropertyID,
                CONCAT(value, 'UTC', IF(value_ns IS NULL, '', value_ns))
                    AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_datetime_data
            WHERE domain_id = DomainID
            AND entity_id = EntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                property_id AS PropertyID,
                value AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_text_data
            WHERE domain_id = DomainID
            AND entity_id = EntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                property_id AS PropertyID,
                value AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_enum_data
            WHERE domain_id = DomainID
            AND entity_id = EntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                property_id AS PropertyID,
                IF(value_iversion IS NULL, value,
                    
                    CONCAT(value, "@", _get_version(value, value_iversion)))
                    AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_reference_data
            WHERE domain_id = DomainID
            AND entity_id = EntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                property_id AS PropertyID,
                NULL AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_null_data
            WHERE domain_id = DomainID
            AND entity_id = EntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                property_id AS PropertyID,
                value AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_name_data
            WHERE domain_id = DomainID
            AND entity_id = EntityID
            AND property_id != 20
            AND _iversion = IVersion;

            LEAVE retrieveEntityPropertiesBody;
        END IF;
    END IF;

    
    SELECT
        property_id AS PropertyID,
        value AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM double_data
    WHERE domain_id = DomainID
    AND entity_id = EntityID

    UNION ALL

    
    SELECT
        property_id AS PropertyID,
        value AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM integer_data
    WHERE domain_id = DomainID
    AND entity_id = EntityID

    UNION ALL

    
    SELECT
        property_id AS PropertyID,
        CONCAT(value, '.NULL.NULL') AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM date_data
    WHERE domain_id = DomainID
    AND entity_id = EntityID

    UNION ALL

    
    SELECT
        property_id AS PropertyID,
        CONCAT(value, 'UTC', IF(value_ns IS NULL, '', value_ns))
            AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM datetime_data
    WHERE domain_id = DomainID
    AND entity_id = EntityID

    UNION ALL

    
    SELECT
        property_id AS PropertyID,
        value AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM text_data
    WHERE domain_id = DomainID
    AND entity_id = EntityID

    UNION ALL

    
    SELECT
        property_id AS PropertyID,
        value AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM enum_data
    WHERE domain_id = DomainID
    AND entity_id = EntityID

    UNION ALL

    
    SELECT
        property_id AS PropertyID,
        IF(value_iversion IS NULL, value,
            CONCAT(value, "@", _get_version(value, value_iversion)))
            AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM reference_data
    WHERE domain_id = DomainID
    AND entity_id = EntityID

    UNION ALL

    
    SELECT
        property_id AS PropertyID,
        NULL AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM null_data
    WHERE domain_id = DomainID
    AND entity_id = EntityID

    UNION ALL

    
    SELECT
        property_id AS PropertyID,
        value AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM name_data
    WHERE domain_id = DomainID
    AND entity_id = EntityID
    AND property_id != 20;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `retrieveOverrides` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `retrieveOverrides`(
    in DomainID INT UNSIGNED,
    in EntityID INT UNSIGNED,
    in Version VARBINARY(255))
retrieveOverridesBody: BEGIN

    DECLARE IVersion INT UNSIGNED DEFAULT NULL;
    DECLARE IsHead BOOLEAN DEFAULT TRUE;

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        IF Version IS NOT NULL THEN
            IF DomainID = 0 THEN
                SELECT get_head_version(EntityID) = Version INTO IsHead;
            ELSE
                SELECT get_head_version(DomainID) = Version INTO IsHead;
            END IF;
        END IF;

        IF IsHead IS FALSE THEN
            SELECT e._iversion INTO IVersion
                FROM entity_version as e
                WHERE ((e.entity_id = EntityID AND DomainID = 0)
                    OR (e.entity_id = DomainID))
                AND e.version = Version;

            IF IVersion IS NULL THEN
                
                LEAVE retrieveOverridesBody;
            END IF;

            
            SELECT
                NULL AS collection_override,
                name AS name_override,
                NULL AS desc_override,
                NULL AS type_override,
                entity_id,
                property_id
            FROM archive_name_overrides
            WHERE domain_id = DomainID
            AND entity_id = EntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                NULL AS collection_override,
                NULL AS name_override,
                description AS desc_override,
                NULL AS type_override,
                entity_id,
                property_id
            FROM archive_desc_overrides
            WHERE domain_id = DomainID
            AND entity_id = EntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                NULL AS collection_override,
                NULL AS name_override,
                NULL AS desc_override,
                IFNULL((SELECT value FROM name_data
                    WHERE domain_id = 0
                    AND entity_id = datatype
                    AND property_id = 20
                    LIMIT 1), datatype) AS type_override,
                entity_id,
                property_id
            FROM archive_data_type
            WHERE domain_id = DomainID
            AND entity_id = EntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                collection AS collection_override,
                NULL AS name_override,
                NULL AS desc_override,
                NULL AS type_override,
                entity_id,
                property_id
            FROM archive_collection_type
            WHERE domain_id = DomainID
            AND entity_id = EntityID
            AND _iversion = IVersion;

            LEAVE retrieveOverridesBody;
        END IF;
    END IF;

    SELECT
        NULL AS collection_override,
        name AS name_override,
        NULL AS desc_override,
        NULL AS type_override,
        entity_id,
        property_id
    FROM name_overrides
    WHERE domain_id = DomainID
    AND entity_id = EntityID

    UNION ALL

    SELECT
        NULL AS collection_override,
        NULL AS name_override,
        description AS desc_override,
        NULL AS type_override,
        entity_id,
        property_id
    FROM desc_overrides
    WHERE domain_id = DomainID
    AND entity_id = EntityID

    UNION ALL

    SELECT
        NULL AS collection_override,
        NULL AS name_override,
        NULL AS desc_override,
        IFNULL((SELECT value FROM name_data
            WHERE domain_id = 0
            AND entity_ID = datatype
            AND property_id = 20 LIMIT 1), datatype) AS type_override,
        entity_id,
        property_id
    FROM data_type
    WHERE domain_id = DomainID
    AND entity_id = EntityID

    UNION ALL

    SELECT
        collection AS collection_override,
        NULL AS name_override,
        NULL AS desc_override,
        NULL AS type_override,
        entity_id,
        property_id
    FROM collection_type
    WHERE domain_id = DomainID
    AND entity_id = EntityID;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `retrieveQueryTemplateDef` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `retrieveQueryTemplateDef`(
    in EntityID INT UNSIGNED,
    in Version VARBINARY(255))
retrieveQueryTemplateDefBody: BEGIN

    DECLARE IVersion INT UNSIGNED DEFAULT NULL;
    DECLARE IsHead BOOLEAN DEFAULT TRUE;

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        
        IF Version IS NOT NULL THEN
            SELECT get_head_version(EntityID) = Version INTO IsHead;
        END IF;

        IF IsHead IS FALSE THEN
            
            SELECT e._iversion INTO IVersion
                FROM entity_version as e
                WHERE e.entity_id = EntityID
                AND e.version = Version;

            IF IVersion IS NULL THEN
                
                LEAVE retrieveQueryTemplateDefBody;
            END IF;

            SELECT definition
            FROM archive_query_template_def
            WHERE id = EntityID
            AND _iversion = IVersion;

            LEAVE retrieveQueryTemplateDefBody;
        END IF;
    END IF;

    SELECT definition
    FROM query_template_def
    WHERE id = EntityID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `setFileProperties` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `setFileProperties`(
    in EntityID INT UNSIGNED,
    in FilePath TEXT,
    in FileSize BIGINT UNSIGNED,
    in FileHash VARCHAR(255)
)
BEGIN
    DECLARE IVersion INT UNSIGNED DEFAULT NULL;
    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        SELECT max(e._iversion) INTO IVersion
            FROM entity_version AS e
            WHERE e.entity_id = EntityID;

        INSERT INTO archive_files (file_id, path, size, hash,
                _iversion)
            SELECT file_id, path, size, hash, IVersion AS _iversion
            FROM files
            WHERE file_id = EntityID;
    END IF;

    DELETE FROM files WHERE file_id = EntityID;

    IF FilePath IS NOT NULL THEN
        INSERT INTO files (file_id, path, size, hash)
            VALUES (EntityID, FilePath, FileSize, unhex(FileHash));
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `setPassword` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `setPassword`(in EntityID INT UNSIGNED, in NewPassword VARCHAR(255))
BEGIN


	DELETE FROM passwords where entity_id=EntityID;
	INSERT INTO passwords (entity_id, password) VALUES (EntityID, NewPassword);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `set_transaction` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `set_transaction`(
    srid VARBINARY(255),
    username VARCHAR(255),
    realm VARCHAR(255),
    seconds BIGINT UNSIGNED,
    nanos INT(10) UNSIGNED)
BEGIN

    SET @SRID = srid;  
    INSERT INTO transactions (srid, username, realm, seconds, nanos)
        VALUES (srid, username, realm, seconds, nanos);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `showEntityAutoIncr` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `showEntityAutoIncr`()
BEGIN
SELECT `AUTO_INCREMENT`
FROM  INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'caosdb'
AND   TABLE_NAME   = 'entities';


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateEntity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `updateEntity`(
    in EntityID INT UNSIGNED,
    in EntityName VARCHAR(255),
    in EntityDescription TEXT,
    in EntityRole VARCHAR(255),
    in Datatype VARCHAR(255),
    in Collection VARCHAR(255),
    in ACL VARBINARY(65525))
BEGIN
    DECLARE ACLID INT UNSIGNED DEFAULT NULL;
    DECLARE Hash VARBINARY(255) DEFAULT NULL;
    DECLARE Version VARBINARY(255) DEFAULT SHA1(UUID());
    DECLARE ParentVersion VARBINARY(255) DEFAULT NULL;
    DECLARE Transaction VARBINARY(255) DEFAULT NULL;
    DECLARE OldIVersion INT UNSIGNED DEFAULT NULL;

    call entityACL(ACLID, ACL);

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        SELECT max(_iversion) INTO OldIVersion
            FROM entity_version
            WHERE entity_id = EntityID;

        
        INSERT INTO archive_entities (id, description, role,
                acl, _iversion)
            SELECT e.id, e.description, e.role, e.acl, OldIVersion
            FROM entities AS e
            WHERE e.id = EntityID;

        INSERT INTO archive_data_type (domain_id, entity_id, property_id,
                datatype, _iversion)
            SELECT e.domain_id, e.entity_id, e.property_id, e.datatype,
                OldIVersion
            FROM data_type AS e
            WHERE e.domain_id = 0
            AND e.entity_id = 0
            AND e.property_id = EntityID;

        INSERT INTO archive_collection_type (domain_id, entity_id, property_id,
                collection, _iversion)
            SELECT e.domain_id, e.entity_id, e.property_id, e.collection,
                OldIVersion
            FROM collection_type as e
            WHERE e.domain_id = 0
            AND e.entity_id = 0
            AND e.property_id = EntityID;


        SET Transaction = @SRID;
        SELECT e.version INTO ParentVersion
            FROM entity_version as e
            WHERE e.entity_id = EntityID
            AND e._iversion = OldIVersion;
        CALL insert_single_child_version(
            EntityID, Hash, Version,
            ParentVersion, Transaction);
    END IF;

    UPDATE entities e
        SET e.description = EntityDescription,
            e.role=EntityRole,
            e.acl = ACLID
        WHERE e.id = EntityID;

    
    
    DELETE FROM name_data
        WHERE domain_id = 0 AND entity_id = EntityID AND property_id = 20;
    IF EntityName IS NOT NULL THEN
        INSERT INTO name_data
                (domain_id, entity_id, property_id, value, status, pidx)
            VALUES (0, EntityID, 20, EntityName, "FIX", 0);
    END IF;

    DELETE FROM data_type
        WHERE domain_id=0 AND entity_id=0 AND property_id=EntityID;

    DELETE FROM collection_type
        WHERE domain_id=0 AND entity_id=0 AND property_id=EntityID;

    IF Datatype IS NOT NULL THEN
        INSERT INTO data_type (domain_id, entity_id, property_id, datatype)
            SELECT 0, 0, EntityID,
                ( SELECT entity_id FROM name_data WHERE domain_id = 0
                    AND property_id = 20 AND value = Datatype LIMIT 1 );

        IF Collection IS NOT NULL THEN
            INSERT INTO collection_type (domain_id, entity_id, property_id,
                    collection)
                SELECT 0, 0, EntityID, Collection;
        END IF;
    END IF;

    Select Version as Version;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateLinCon` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `updateLinCon`(in sig_from BIGINT, in sig_to BIGINT, in new_a DECIMAL(65,30), in new_b_dividend BIGINT, in new_b_divisor BIGINT, in new_c DECIMAL(65,30))
BEGIN
    UPDATE units_lin_con SET signature_to=sig_to, a=new_a, b_dividend=new_b_dividend, b_divisor=new_b_divisor, c=new_c where signature_from=sig_from;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-01 13:08:46
