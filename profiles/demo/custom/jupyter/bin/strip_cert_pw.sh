#!/bin/bash 
INFILE="/opt/cert/caosdb.key.pem"
OUTFILE="/tmp/ssl/notebook/notebook.key"
env
mkdir -p /tmp/ssl/notebook/
KEYPW="${KEYPW:-CaosDBSecret}" openssl rsa -in ${INFILE}  -out  ${OUTFILE} \
         -passin env:KEYPW 
