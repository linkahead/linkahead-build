default:
  # Optionally, specify a base compose file. The default base compose file is
  # located in ./compose/docker-compose-default.yml. A different compose file
  # may be useful when other services shall be included, e.g. nginx or django.
  # base_compose_file: "compose/docker-compose-default.yml"

  # Directories (list of strings) (or single directory (string, deprecated))
  # with customization files.
  # Directories listed later in the list take precedence over earlier ones.
  # If you change this, you need to include the full list of directories
  # (including possibly directories contained in the default setting).
  # custom:
    # Standard directory for customizations
    # - "./custom"                 # included by default
    # LinkAhead Theme for the web interface
    # - "./theme-linkahead"        # included by default
    # - "./included_customization" # since this is later in the list this takes precedence

  # Paths to be mounted into Docker, all entries are optional.
  paths:
    # extroot: From where files are copied/symlinked. This is a
    # list of `NAME: PATH` pairs or a single path.
    extroot:
      "": "paths/extroot"
    #
    #  "base": "/path/to/base/dir"
    #  "other": "/path/to/other"
    #
    # dropoffbox: (Soon to be deprecated.) Files can be written here, but note that you may need to
    # become root to remove this directory later. The corresponding server property is DROP_OFF_BOX.
    # dropoffbox: "/path/to/dropoffbox"

  # Docker building configuration
  # References can be either hashes of commits, branch names or tags.
  refs:
    # SERVER:            main
    # PYLIB:             main
    # MYSQLBACKEND:      main
    # WEBUI:             main
    # ADVANCEDUSERTOOLS: main

  # General configuration options
  conf:
    # Shall the SQL & caosroot dumps at custom/other/restore/ be used?
    restore: true
    # uncomment to disable tls (ssl). This might be insecure!
    # no_tls: false
    # Shall the local users be imported as LinkAhead users?
    # local_users: false
    # Shall NIS be used for authentication?
    # nis: false
    # Shall LDAP be used for autentication? If `true`, an `nslcd.conf` with the
    # configuration for the LDAP client must exist at the profile's
    # `custom/other/` directory."
    # ldap: false
    # Shall a mail server be used?
    # mail: false
    # You can provide the path to an non-standard sendmail executable
    # sendmail: /usr/sbin/sendmail
    # sendmail: /usr/local/bin/sendmail_to_file
    # Shall the server run in debug mode?
    # This will bind-mount the following directories from custom into the Docker
    # container:
    # - debug-authtoken :: Authentication tokens will be stored here.
    # - debug-scripting-bin :: Used as the server-side scripting bin dir.
    # debug: false
    # URL of the docker registry.  Set to "" to look locally and at the default locations.
    # registry_server: ""
    # The account for accessing the registry server.  "" means no account.
    # registry_account: ""
    # The secret token for accessing the registry server.  "" means no token.
    # registry_token: ""
    # Name of the docker image
    # image: "indiscale/linkahead"
    # Tag of the docker image
    # tag: "latest"
    # Name of the main Docker container.  Set to "" to use an auto-generated
    # name, which is necessary for running multiple instances
    # container_name: "linkahead"
    # The time zone for the server, format: posix identifier; something that
    # is available in Debian under /usr/share/zoneinfo
    # timezone: "UTC"

    # You can set labels for the docker container here
    # labels:
    #   label_1_key: label_1_value
    #   label_2_key: label_2_value

    # User/Group of the server, either numeric or names.
    # user_group: 999:999
    # Enable the default envoy config. Will start an envoy image together with
    # LinkAhead that serves routing to ACM and WIKI modules by default (not
    # included, have to be provided separately as of now)
    envoy_enabled: true
    # Path to a custom envoy configuration in case the defaults aren't
    # sufficient. Ignored if envoy isn't enabled.
    # envoy_custom_config: path/to/envoy/config
    # User Id that is used for the envoy config
    # envoy_user_id: 987
    # Envoy connection timeout (same for all clusters)
    # envoy_connect_timeout: 5s
    # Envoy routing timeout (same for all virtual hosts)
    # envoy_route_timeout: 1800s

    # The user will be part of these additional groups
    # additional_groups:
    #  - group1
    #  - "11121"

    # Proxy settings. Will be entered in the corresponding environment
    # variables within the docker container. Defaults are null since
    # we need to distinguish from an empty string.
    # http_proxy: null
    # https_proxy: null
    # ftp_proxy: null
    # no_proxy: null

    # Network settings.
    network:
      # The subnet for the Docker containers
      # default: auto selected by docker
      # You can set it with:
      # subnet: 10.3.128.0/17
      # Port for accessing LinkAhead via HTTPS
      # port_ssl: 10443
      # Port for accessing LinkAhead via plain HTTP (not recommended when
      # accessible from untrusted networks, but ok for testing or when behind
      # a proxy)
      # port_plain: 8000
      # Port for GRPC end-point via HTTPS
      # port_grpc_ssl: 8443
      # Port for GRPC end-point via plain HTTP
      # port_grpc_plain: 8080
      # Port for debugging the LinkAhead JVM
      # port_debug: 9000
      # Port for profiling the LinkAhead JVM via JMX
      # port_profiler: 9090
      # listen to ip address ("" means any)
      # bind_ip: "127.0.0.1"
      # Only relevant together with envoy:
      # envoy_ports:
        # Ports under which the plain WebUI is served by envoy with SSL.
        # port_envoy_ssl: 10444
        # Ports under which the plain WebUI is served by envoy without SSL.
        # port_envoy_plain: 8081

    server:
      # All the keys of conf are set as environment variables in the server
      # container before the server start. This overrides the server.conf
      # settings in any other files, even the settings from
      # `custom/caosdb-server/conf/ext/server.conf.d/`
      # Check out conf/core/server.conf in the caosdb-server repository for
      # options.
      #
      # When the conf variables are unset, the server uses its default values
      # or the values from the `server.conf.d` directory.
      conf:
        # uncomment to enable the anonymous user
        auth_optional: TRUE
        # uncomment to use the your custom authtoken config. See
        # `conf/core/authtoken.example.yaml` for examples.
        # Note: The path is relative to the caosdb server's root directory.
        # authtoken_config: conf/core/authtoken.yaml

        # HTTPS port of the grpc end-point
        # grpc_server_port_https: 8443
        # HTTP port of the grpc end-point
        # grpc_server_port_http: 8080

  # Development configuration options
  # devel:
    # Copy the caosdb-server jar from this location into the Docker container.
    # Note that this is implemented by copying the file to
    # custom/caosdb-server/target/, any file there will be overwritten.
    # jar: /var/build/caosdb-server/0123abcd/target/caosdb-server-<version>-jar-with-dependencies.jar

  # The following is for the very specific case of server-side scripts
  # requiring additional Python packages that are not installed during
  # the regular build process of LinkAhead. If additional packages are
  # needed, list them below. Mind that only packages that can be
  # installed by pip are supported.

  # scripting:
    # packages:

      # Packages can be installed from PyPI or external git
      # repositories. In this case, `mode: "pip"` has to be
      # provided. `package` can be the package name in PyPI (possibly
      # with a version specification, i.e., `my_package>=1.0`, or it
      # can be the URL of a git repository of a Python
      # package. Essentially, the command `pip3 install
      # package_string` will be executed within LinkAhead.

      # <package1_key>:
        # mode: "pip"
        # package: "<package_string>"

      # Alternatively, local packages can be copied into LinkAhead and then be
      # installed using pip. Here, `mode: "copy"` has to be provided. `path`
      # specifies the path to the Python package on the host system (either
      # absolute or relative to the profile directory). `package` is the name of
      # the destination directory within the LinkAhead container into which the
      # local package will be copied. After copying, a `pip3 install .` is run
      # from within that directory.

      # <package2_key>:
        # mode: "copy"
        # path: "/path/to/local/python/package"
        # package: "<package_string>"
