-- MariaDB dump 10.19  Distrib 10.11.6-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: sqldb    Database: caosdb
-- ------------------------------------------------------
-- Server version	10.5.25-MariaDB-ubu2004

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `archive_collection_type`
--

DROP TABLE IF EXISTS `archive_collection_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_collection_type` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `collection` varchar(255) NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  UNIQUE KEY `archive_collection_type-d-e-p-v` (`domain_id`,`entity_id`,`property_id`,`_iversion`),
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_collection_type_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_collection_type_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_collection_type_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_collection_type`
--

LOCK TABLES `archive_collection_type` WRITE;
/*!40000 ALTER TABLE `archive_collection_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_collection_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_data_type`
--

DROP TABLE IF EXISTS `archive_data_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_data_type` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `datatype` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  UNIQUE KEY `archive_data_type-d-e-p-v` (`domain_id`,`entity_id`,`property_id`,`_iversion`),
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  KEY `datatype` (`datatype`),
  CONSTRAINT `archive_data_type_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_data_type_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_data_type_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_data_type_ibfk_4` FOREIGN KEY (`datatype`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_data_type`
--

LOCK TABLES `archive_data_type` WRITE;
/*!40000 ALTER TABLE `archive_data_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_data_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_date_data`
--

DROP TABLE IF EXISTS `archive_date_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_date_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` int(11) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_date_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_date_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_date_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_date_data`
--

LOCK TABLES `archive_date_data` WRITE;
/*!40000 ALTER TABLE `archive_date_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_date_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_datetime_data`
--

DROP TABLE IF EXISTS `archive_datetime_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_datetime_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` bigint(20) NOT NULL,
  `value_ns` int(10) unsigned DEFAULT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_datetime_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_datetime_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_datetime_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_datetime_data`
--

LOCK TABLES `archive_datetime_data` WRITE;
/*!40000 ALTER TABLE `archive_datetime_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_datetime_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_desc_overrides`
--

DROP TABLE IF EXISTS `archive_desc_overrides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_desc_overrides` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  UNIQUE KEY `archive_desc_overrides-d-e-p-v` (`domain_id`,`entity_id`,`property_id`,`_iversion`),
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_desc_overrides_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_desc_overrides_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_desc_overrides_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_desc_overrides`
--

LOCK TABLES `archive_desc_overrides` WRITE;
/*!40000 ALTER TABLE `archive_desc_overrides` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_desc_overrides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_double_data`
--

DROP TABLE IF EXISTS `archive_double_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_double_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` double NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  `unit_sig` bigint(20) DEFAULT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_double_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_double_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_double_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_double_data`
--

LOCK TABLES `archive_double_data` WRITE;
/*!40000 ALTER TABLE `archive_double_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_double_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_entities`
--

DROP TABLE IF EXISTS `archive_entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_entities` (
  `id` int(10) unsigned NOT NULL,
  `description` text DEFAULT NULL,
  `role` enum('RECORDTYPE','RECORD','FILE','_REPLACEMENT','PROPERTY','DATATYPE','ROLE','QUERYTEMPLATE') NOT NULL,
  `acl` int(10) unsigned DEFAULT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`_iversion`),
  KEY `acl` (`acl`),
  CONSTRAINT `archive_entities_ibfk_1` FOREIGN KEY (`id`, `_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE,
  CONSTRAINT `archive_entities_ibfk_2` FOREIGN KEY (`acl`) REFERENCES `entity_acl` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_entities`
--

LOCK TABLES `archive_entities` WRITE;
/*!40000 ALTER TABLE `archive_entities` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_entities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_enum_data`
--

DROP TABLE IF EXISTS `archive_enum_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_enum_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` varbinary(255) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_enum_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_enum_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_enum_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_enum_data`
--

LOCK TABLES `archive_enum_data` WRITE;
/*!40000 ALTER TABLE `archive_enum_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_enum_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_files`
--

DROP TABLE IF EXISTS `archive_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_files` (
  `file_id` int(10) unsigned NOT NULL,
  `path` text NOT NULL,
  `size` bigint(20) unsigned NOT NULL,
  `hash` binary(64) DEFAULT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  PRIMARY KEY (`file_id`,`_iversion`),
  CONSTRAINT `archive_files_ibfk_1` FOREIGN KEY (`file_id`, `_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_files`
--

LOCK TABLES `archive_files` WRITE;
/*!40000 ALTER TABLE `archive_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_integer_data`
--

DROP TABLE IF EXISTS `archive_integer_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_integer_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` bigint(20) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  `unit_sig` bigint(20) DEFAULT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_integer_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_integer_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_integer_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_integer_data`
--

LOCK TABLES `archive_integer_data` WRITE;
/*!40000 ALTER TABLE `archive_integer_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_integer_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_isa`
--

DROP TABLE IF EXISTS `archive_isa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_isa` (
  `child` int(10) unsigned NOT NULL,
  `child_iversion` int(10) unsigned NOT NULL,
  `parent` int(10) unsigned NOT NULL,
  `direct` tinyint(1) DEFAULT 1,
  KEY `parent` (`parent`),
  KEY `child` (`child`,`child_iversion`),
  CONSTRAINT `archive_isa_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_isa_ibfk_2` FOREIGN KEY (`child`, `child_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_isa`
--

LOCK TABLES `archive_isa` WRITE;
/*!40000 ALTER TABLE `archive_isa` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_isa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_name_data`
--

DROP TABLE IF EXISTS `archive_name_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_name_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` varchar(255) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `value` (`value`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_name_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_name_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_name_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_name_data`
--

LOCK TABLES `archive_name_data` WRITE;
/*!40000 ALTER TABLE `archive_name_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_name_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_name_overrides`
--

DROP TABLE IF EXISTS `archive_name_overrides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_name_overrides` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  UNIQUE KEY `archive_name_overrides-d-e-p-v` (`domain_id`,`entity_id`,`property_id`,`_iversion`),
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_name_overrides_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_name_overrides_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_name_overrides_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_name_overrides`
--

LOCK TABLES `archive_name_overrides` WRITE;
/*!40000 ALTER TABLE `archive_name_overrides` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_name_overrides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_null_data`
--

DROP TABLE IF EXISTS `archive_null_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_null_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_null_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_null_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_null_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_null_data`
--

LOCK TABLES `archive_null_data` WRITE;
/*!40000 ALTER TABLE `archive_null_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_null_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_query_template_def`
--

DROP TABLE IF EXISTS `archive_query_template_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_query_template_def` (
  `id` int(10) unsigned NOT NULL,
  `definition` mediumtext NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`_iversion`),
  CONSTRAINT `archive_query_template_def_ibfk_1` FOREIGN KEY (`id`, `_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_query_template_def`
--

LOCK TABLES `archive_query_template_def` WRITE;
/*!40000 ALTER TABLE `archive_query_template_def` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_query_template_def` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_reference_data`
--

DROP TABLE IF EXISTS `archive_reference_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_reference_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` int(10) unsigned NOT NULL,
  `value_iversion` int(10) unsigned DEFAULT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  KEY `value` (`value`),
  CONSTRAINT `archive_reference_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_reference_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_reference_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_reference_data_ibfk_4` FOREIGN KEY (`value`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_reference_data`
--

LOCK TABLES `archive_reference_data` WRITE;
/*!40000 ALTER TABLE `archive_reference_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_reference_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_text_data`
--

DROP TABLE IF EXISTS `archive_text_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_text_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` text NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_text_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_text_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_text_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_text_data`
--

LOCK TABLES `archive_text_data` WRITE;
/*!40000 ALTER TABLE `archive_text_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_text_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collection_type`
--

DROP TABLE IF EXISTS `collection_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collection_type` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `collection` varchar(255) NOT NULL,
  UNIQUE KEY `collection_type-d-e-p` (`domain_id`,`entity_id`,`property_id`),
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `collection_type_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `collection_type_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `collection_type_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collection_type`
--

LOCK TABLES `collection_type` WRITE;
/*!40000 ALTER TABLE `collection_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `collection_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_type`
--

DROP TABLE IF EXISTS `data_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_type` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `datatype` int(10) unsigned NOT NULL,
  UNIQUE KEY `datatype_ukey` (`domain_id`,`entity_id`,`property_id`),
  KEY `name_ov_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `datatype_forkey_ent` (`entity_id`),
  KEY `datatype_forkey_pro` (`property_id`),
  KEY `datatype_forkey_type` (`datatype`),
  CONSTRAINT `datatype_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `datatype_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `datatype_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `datatype_forkey_type` FOREIGN KEY (`datatype`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_type`
--

LOCK TABLES `data_type` WRITE;
/*!40000 ALTER TABLE `data_type` DISABLE KEYS */;
INSERT INTO `data_type` VALUES
(0,0,20,14),
(0,0,21,14),
(0,0,24,14);
/*!40000 ALTER TABLE `data_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `date_data`
--

DROP TABLE IF EXISTS `date_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `date_data` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `value` int(11) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') DEFAULT NULL,
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  KEY `date_data_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `date_ov_forkey_ent` (`entity_id`),
  KEY `date_ov_forkey_pro` (`property_id`),
  CONSTRAINT `date_ov_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `date_ov_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `date_ov_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `date_data`
--

LOCK TABLES `date_data` WRITE;
/*!40000 ALTER TABLE `date_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `date_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datetime_data`
--

DROP TABLE IF EXISTS `datetime_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datetime_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  `value_ns` int(10) unsigned DEFAULT NULL,
  `value` bigint(20) NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `dat_entity_id_entity` (`entity_id`),
  KEY `dat_property_id_entity` (`property_id`),
  CONSTRAINT `dat_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `dat_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `dat_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datetime_data`
--

LOCK TABLES `datetime_data` WRITE;
/*!40000 ALTER TABLE `datetime_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `datetime_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `desc_overrides`
--

DROP TABLE IF EXISTS `desc_overrides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `desc_overrides` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `description` text DEFAULT NULL,
  UNIQUE KEY `desc_ov_ukey` (`domain_id`,`entity_id`,`property_id`),
  KEY `desc_ov_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `desc_ov_forkey_ent` (`entity_id`),
  KEY `desc_ov_forkey_pro` (`property_id`),
  CONSTRAINT `desc_ov_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `desc_ov_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `desc_ov_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `desc_overrides`
--

LOCK TABLES `desc_overrides` WRITE;
/*!40000 ALTER TABLE `desc_overrides` DISABLE KEYS */;
/*!40000 ALTER TABLE `desc_overrides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `double_data`
--

DROP TABLE IF EXISTS `double_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `double_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `value` double NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  `unit_sig` bigint(20) DEFAULT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `dou_entity_id_entity` (`entity_id`),
  KEY `dou_property_id_entity` (`property_id`),
  CONSTRAINT `dou_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `dou_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `dou_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `double_data`
--

LOCK TABLES `double_data` WRITE;
/*!40000 ALTER TABLE `double_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `double_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entities`
--

DROP TABLE IF EXISTS `entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier.',
  `description` text DEFAULT NULL,
  `role` enum('RECORDTYPE','RECORD','FILE','_REPLACEMENT','PROPERTY','DATATYPE','ROLE','QUERYTEMPLATE') NOT NULL,
  `acl` int(10) unsigned DEFAULT NULL COMMENT 'Access Control List for the entity.',
  PRIMARY KEY (`id`),
  KEY `entity_entity_acl` (`acl`),
  CONSTRAINT `entity_entity_acl` FOREIGN KEY (`acl`) REFERENCES `entity_acl` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entities`
--

LOCK TABLES `entities` WRITE;
/*!40000 ALTER TABLE `entities` DISABLE KEYS */;
INSERT INTO `entities` VALUES
(0,'The default domain.','ROLE',0),
(1,'The default recordtype.','ROLE',0),
(2,'The default record.','ROLE',0),
(3,'The default file.','ROLE',0),
(4,'The default property.','ROLE',0),
(7,'The default datatype.','ROLE',0),
(8,'The QueryTemplate role.','ROLE',0),
(11,'The default reference data type.','DATATYPE',0),
(12,'The default integer data type.','DATATYPE',0),
(13,'The default double data type.','DATATYPE',0),
(14,'The default text data type.','DATATYPE',0),
(15,'The default datetime data type.','DATATYPE',0),
(16,'The default timespan data type.','DATATYPE',0),
(17,'The default file reference data type.','DATATYPE',0),
(18,'The defaulf boolean data type','DATATYPE',0),
(20,'Name of an entity','PROPERTY',0),
(21,'Unit of an entity.','PROPERTY',0),
(24,'Description of an entity.','PROPERTY',0);
/*!40000 ALTER TABLE `entities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entity_acl`
--

DROP TABLE IF EXISTS `entity_acl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity_acl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acl` varbinary(65525) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_acl_acl` (`acl`(3072))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity_acl`
--

LOCK TABLES `entity_acl` WRITE;
/*!40000 ALTER TABLE `entity_acl` DISABLE KEYS */;
INSERT INTO `entity_acl` VALUES
(0,'');
/*!40000 ALTER TABLE `entity_acl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entity_ids`
--

DROP TABLE IF EXISTS `entity_ids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity_ids` (
  `id` varchar(255) NOT NULL,
  `internal_id` int(10) unsigned NOT NULL COMMENT 'Internal ID of an entity. This id is used internally in the *_data tables and elsewhere. This ID is never exposed via the CaosDB API.',
  PRIMARY KEY (`id`),
  KEY `entity_ids_internal_id` (`internal_id`),
  CONSTRAINT `entity_ids_internal_id` FOREIGN KEY (`internal_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity_ids`
--

LOCK TABLES `entity_ids` WRITE;
/*!40000 ALTER TABLE `entity_ids` DISABLE KEYS */;
INSERT INTO `entity_ids` VALUES
('1',1),
('2',2),
('3',3),
('4',4),
('7',7),
('8',8),
('11',11),
('12',12),
('13',13),
('14',14),
('15',15),
('16',16),
('17',17),
('18',18),
('20',20),
('21',21),
('24',24);
/*!40000 ALTER TABLE `entity_ids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entity_version`
--

DROP TABLE IF EXISTS `entity_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity_version` (
  `entity_id` int(10) unsigned NOT NULL,
  `hash` varbinary(255) DEFAULT NULL,
  `version` varbinary(255) NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  `_ipparent` int(10) unsigned DEFAULT NULL,
  `srid` varbinary(255) NOT NULL,
  PRIMARY KEY (`entity_id`,`_iversion`),
  UNIQUE KEY `entity_version-e-v` (`entity_id`,`version`),
  KEY `srid` (`srid`),
  CONSTRAINT `entity_version_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entity_version_ibfk_2` FOREIGN KEY (`srid`) REFERENCES `transactions` (`srid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity_version`
--

LOCK TABLES `entity_version` WRITE;
/*!40000 ALTER TABLE `entity_version` DISABLE KEYS */;
INSERT INTO `entity_version` VALUES
(0,NULL,'a22e13cc0a0d6abc63a520b038ffb11f937f2fe5',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(1,NULL,'8206d5a37ccedf6e2101274cacabf613c464963e',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(2,NULL,'5ff5012d83e097bd915bde08d50ef1570b868bba',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(3,NULL,'03f4ee27316f17c90e421cd474cc0004defddf47',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(4,NULL,'8dfef42117275fbc475b607f95b19810f4de2de5',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(7,NULL,'0a283af5f5b162a36744b2043dc6caeabb881e25',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(8,NULL,'8acd25b0ce6fbdb9841bb214f60a06f5dc31bc3f',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(11,NULL,'be63da8ec865803eabcdb6bba4d2156cbaee524a',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(12,NULL,'87f06890501ab9a4c767c2c662e240e8b9749d01',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(13,NULL,'3c8c0e26528d4f38defcfdcf4bb767327eb4f295',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(14,NULL,'d41b564762eda4fc3405cd45dd6f1e74727b27dc',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(15,NULL,'b060268f9575814bffbeeaec3af4eac32cbc32b3',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(16,NULL,'3639e7d0691aad743b283c340c183e6bbd46ec68',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(17,NULL,'e3e837ac410cd07ede3ca7a7e6072488c4313b96',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(18,NULL,'14b349a86c04ec27ec0035c1cbeb92f700342236',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(20,NULL,'9030ad4b0f91c832ddb4a7c9d9b090c1f035b1d3',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(21,NULL,'6a693e36a2afae1cbc571cce52c1676065c1ed24',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),
(24,NULL,'d78e2b50954d74d6946d2c113cb7393b3b6146f3',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e');
/*!40000 ALTER TABLE `entity_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enum_data`
--

DROP TABLE IF EXISTS `enum_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enum_data` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `value` varbinary(255) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') DEFAULT NULL,
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  KEY `enum_ov_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `enum_ov_forkey_ent` (`entity_id`),
  KEY `enum_ov_forkey_pro` (`property_id`),
  CONSTRAINT `enum_ov_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `enum_ov_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `enum_ov_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enum_data`
--

LOCK TABLES `enum_data` WRITE;
/*!40000 ALTER TABLE `enum_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `enum_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feature_config`
--

DROP TABLE IF EXISTS `feature_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feature_config` (
  `_key` varchar(255) NOT NULL,
  `_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feature_config`
--

LOCK TABLES `feature_config` WRITE;
/*!40000 ALTER TABLE `feature_config` DISABLE KEYS */;
INSERT INTO `feature_config` VALUES
('ENTITY_VERSIONING','ENABLED');
/*!40000 ALTER TABLE `feature_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `file_id` int(10) unsigned NOT NULL COMMENT 'The file''s ID.',
  `path` varchar(255) NOT NULL COMMENT 'Directory of the file.',
  `size` bigint(20) unsigned NOT NULL COMMENT 'Size in kB (oktet bytes).',
  `hash` binary(64) DEFAULT NULL,
  `checked_timestamp` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`file_id`),
  CONSTRAINT `fil_file_id_entity` FOREIGN KEY (`file_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `integer_data`
--

DROP TABLE IF EXISTS `integer_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integer_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `value` bigint(20) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  `unit_sig` bigint(20) DEFAULT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `int_entity_id_entity` (`entity_id`),
  KEY `int_property_id_entity` (`property_id`),
  CONSTRAINT `int_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `int_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `int_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `integer_data`
--

LOCK TABLES `integer_data` WRITE;
/*!40000 ALTER TABLE `integer_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `integer_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `isa_cache`
--

DROP TABLE IF EXISTS `isa_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `isa_cache` (
  `child` int(10) unsigned NOT NULL,
  `parent` int(10) unsigned NOT NULL,
  `rpath` varchar(255) NOT NULL,
  PRIMARY KEY (`child`,`parent`,`rpath`),
  KEY `isa_cache_parent_entity` (`parent`),
  CONSTRAINT `isa_cache_child_entity` FOREIGN KEY (`child`) REFERENCES `entities` (`id`),
  CONSTRAINT `isa_cache_parent_entity` FOREIGN KEY (`parent`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `isa_cache`
--

LOCK TABLES `isa_cache` WRITE;
/*!40000 ALTER TABLE `isa_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `isa_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `name_data`
--

DROP TABLE IF EXISTS `name_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `name_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` varchar(255) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') NOT NULL,
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  UNIQUE KEY `domain_id_2` (`domain_id`,`entity_id`,`property_id`),
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  KEY `value` (`value`),
  CONSTRAINT `name_data_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `name_data_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `name_data_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `name_data`
--

LOCK TABLES `name_data` WRITE;
/*!40000 ALTER TABLE `name_data` DISABLE KEYS */;
INSERT INTO `name_data` VALUES
(0,0,20,'DOMAIN','FIX',0),
(0,1,20,'RECORDTYPE','FIX',0),
(0,2,20,'RECORD','FIX',0),
(0,3,20,'FILE','FIX',0),
(0,4,20,'PROPERTY','FIX',0),
(0,7,20,'DATATYPE','FIX',0),
(0,8,20,'QUERYTEMPLATE','FIX',0),
(0,11,20,'REFERENCE','FIX',0),
(0,12,20,'INTEGER','FIX',0),
(0,13,20,'DOUBLE','FIX',0),
(0,14,20,'TEXT','FIX',0),
(0,15,20,'DATETIME','FIX',0),
(0,16,20,'TIMESPAN','FIX',0),
(0,17,20,'FILE','FIX',0),
(0,18,20,'BOOLEAN','FIX',0),
(0,20,20,'name','FIX',0),
(0,21,20,'unit','FIX',0),
(0,24,20,'description','FIX',0);
/*!40000 ALTER TABLE `name_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `name_overrides`
--

DROP TABLE IF EXISTS `name_overrides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `name_overrides` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  UNIQUE KEY `name_ov_ukey` (`domain_id`,`entity_id`,`property_id`),
  KEY `name_ov_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `name_ov_forkey_ent` (`entity_id`),
  KEY `name_ov_forkey_pro` (`property_id`),
  CONSTRAINT `name_ov_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `name_ov_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `name_ov_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `name_overrides`
--

LOCK TABLES `name_overrides` WRITE;
/*!40000 ALTER TABLE `name_overrides` DISABLE KEYS */;
/*!40000 ALTER TABLE `name_overrides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `null_data`
--

DROP TABLE IF EXISTS `null_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `null_data` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') DEFAULT NULL,
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  KEY `null_data_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `null_forkey_ent` (`entity_id`),
  KEY `null_forkey_pro` (`property_id`),
  CONSTRAINT `null_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `null_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `null_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `null_data`
--

LOCK TABLES `null_data` WRITE;
/*!40000 ALTER TABLE `null_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `null_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passwd`
--

DROP TABLE IF EXISTS `passwd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passwd` (
  `principal` varbinary(255) NOT NULL,
  `hash` varbinary(255) NOT NULL,
  `alg` varchar(255) DEFAULT 'SHA-512',
  `it` int(10) unsigned DEFAULT 5000,
  `salt` varbinary(255) NOT NULL,
  PRIMARY KEY (`principal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passwd`
--

LOCK TABLES `passwd` WRITE;
/*!40000 ALTER TABLE `passwd` DISABLE KEYS */;
/*!40000 ALTER TABLE `passwd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `role` varbinary(255) NOT NULL,
  `permissions` mediumtext NOT NULL,
  PRIMARY KEY (`role`),
  CONSTRAINT `perm_name_roles` FOREIGN KEY (`role`) REFERENCES `roles` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES
('administration','[{\"grant\":\"true\",\"priority\":\"true\",\"permission\":\"*\"}]');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `query_template_def`
--

DROP TABLE IF EXISTS `query_template_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `query_template_def` (
  `id` int(10) unsigned NOT NULL,
  `definition` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `query_template_def_ibfk_1` FOREIGN KEY (`id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `query_template_def`
--

LOCK TABLES `query_template_def` WRITE;
/*!40000 ALTER TABLE `query_template_def` DISABLE KEYS */;
/*!40000 ALTER TABLE `query_template_def` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reference_data`
--

DROP TABLE IF EXISTS `reference_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reference_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `value` int(10) unsigned NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  `value_iversion` int(10) unsigned DEFAULT NULL,
  KEY `entity_id` (`entity_id`,`property_id`),
  KEY `ref_domain_id_entity` (`domain_id`),
  KEY `ref_property_id_entity` (`property_id`),
  KEY `ref_value_entity` (`value`),
  KEY `value` (`value`,`value_iversion`),
  CONSTRAINT `ref_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `ref_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `ref_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `ref_value_entity` FOREIGN KEY (`value`) REFERENCES `entities` (`id`),
  CONSTRAINT `reference_data_ibfk_1` FOREIGN KEY (`value`, `value_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reference_data`
--

LOCK TABLES `reference_data` WRITE;
/*!40000 ALTER TABLE `reference_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `reference_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `name` varbinary(255) NOT NULL,
  `description` mediumtext DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES
('administration','Users with this role have unrestricted permissions.'),
('anonymous','Users who did not authenticate themselves.');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats`
--

DROP TABLE IF EXISTS `stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats` (
  `name` varchar(255) NOT NULL,
  `value` blob DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats`
--

LOCK TABLES `stats` WRITE;
/*!40000 ALTER TABLE `stats` DISABLE KEYS */;
INSERT INTO `stats` VALUES
('RootBenchmark','��\0sr\0-org.caosdb.server.database.misc.RootBenchmark����Qk]\0\0xr\04org.caosdb.server.database.misc.TransactionBenchmark����Qk]\0J\0sinceL\0measurementst\0Ljava/util/Map;[\0stackTraceElementst\0[Ljava/lang/StackTraceElement;L\0\rsubBenchmarksq\0~\0xp\0\0�L�	�sr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xur\0[Ljava.lang.StackTraceElement;F*<<�\"9\0\0xp\0\0\0sr\0java.lang.StackTraceElementa	Ś&6݅\0B\0formatI\0\nlineNumberL\0classLoaderNamet\0Ljava/lang/String;L\0declaringClassq\0~\0\nL\0fileNameq\0~\0\nL\0\nmethodNameq\0~\0\nL\0\nmoduleNameq\0~\0\nL\0\rmoduleVersionq\0~\0\nxp\0\0Spt\0java.lang.Threadt\0Thread.javat\0\rgetStackTracet\0	java.baset\017.0.12sq\0~\0	\0\0!t\0appt\04org.caosdb.server.database.misc.TransactionBenchmarkt\0TransactionBenchmark.javat\0<init>ppsq\0~\0	\0\0\0�q\0~\0t\0-org.caosdb.server.database.misc.RootBenchmarkq\0~\0q\0~\0ppsq\0~\0	\0\0q\0~\0q\0~\0q\0~\0t\0<clinit>ppsq\0~\0	\0\0fq\0~\0t\0org.caosdb.server.CaosDBServert\0CaosDBServer.javat\0initBackendppsq\0~\0	\0\0\0�q\0~\0q\0~\0q\0~\0t\0mainppsq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0t\0Infosr\0,org.caosdb.server.database.misc.SubBenchmark����Qk]\0L\0nameq\0~\0\nxq\0~\0\0\0�L�	�sq\0~\0?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xuq\0~\0\0\0\0\nsq\0~\0	\0\0Spq\0~\0q\0~\0\rq\0~\0q\0~\0q\0~\0sq\0~\0	\0\0!q\0~\0q\0~\0q\0~\0q\0~\0ppsq\0~\0	\0\0\0�q\0~\0t\0,org.caosdb.server.database.misc.SubBenchmarkq\0~\0q\0~\0ppsq\0~\0	\0\0�q\0~\0q\0~\0q\0~\0t\0getBenchmarkppsq\0~\0	\0\0�q\0~\0q\0~\0q\0~\0q\0~\0+ppsq\0~\0	\0\0\0$q\0~\0t\02org.caosdb.server.transaction.TransactionInterfacet\0TransactionInterface.javat\0getTransactionBenchmarkppsq\0~\0	\0\0\00q\0~\0q\0~\0.q\0~\0/t\0executeppsq\0~\0	\0\0\0�q\0~\0t\0org.caosdb.server.utils.Infot\0	Info.javat\0syncDatabaseppsq\0~\0	\0\0\0�q\0~\0t\0/org.caosdb.server.database.misc.RootBenchmark$1q\0~\0t\0runppsq\0~\0	\0\0Hpq\0~\0q\0~\0\rq\0~\09q\0~\0q\0~\0sq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0t\0	SyncStatssq\0~\0\"\0\0�L�	�sq\0~\0?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xuq\0~\0\0\0\0	sq\0~\0	\0\0Spq\0~\0q\0~\0\rq\0~\0q\0~\0q\0~\0sq\0~\0	\0\0!q\0~\0q\0~\0q\0~\0q\0~\0ppsq\0~\0	\0\0\0�q\0~\0q\0~\0)q\0~\0q\0~\0ppsq\0~\0	\0\0�q\0~\0q\0~\0q\0~\0q\0~\0+ppsq\0~\0	\0\0�q\0~\0q\0~\0q\0~\0q\0~\0+ppsq\0~\0	\0\0\00q\0~\0q\0~\0.q\0~\0/q\0~\02ppsq\0~\0	\0\0\0�q\0~\0q\0~\04q\0~\05q\0~\06ppsq\0~\0	\0\0\0�q\0~\0q\0~\08q\0~\0q\0~\09ppsq\0~\0	\0\0Hpq\0~\0q\0~\0\rq\0~\09q\0~\0q\0~\0sq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0t\0MySQLSyncStatssq\0~\0\"\0\0�L�	�sq\0~\0?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xuq\0~\0\0\0\0sq\0~\0	\0\0Spq\0~\0q\0~\0\rq\0~\0q\0~\0q\0~\0sq\0~\0	\0\0!q\0~\0q\0~\0q\0~\0q\0~\0ppsq\0~\0	\0\0\0�q\0~\0q\0~\0)q\0~\0q\0~\0ppsq\0~\0	\0\0�q\0~\0q\0~\0q\0~\0q\0~\0+ppsq\0~\0	\0\0�q\0~\0q\0~\0q\0~\0q\0~\0+ppsq\0~\0	\0\0q\0~\0t\0-org.caosdb.server.database.BackendTransactiont\0BackendTransaction.javat\0getImplementationppsq\0~\0	\0\0\0+q\0~\0t\08org.caosdb.server.database.backend.transaction.SyncStatst\0SyncStats.javaq\0~\02ppsq\0~\0	\0\0\0�q\0~\0q\0~\0Tq\0~\0Ut\0executeTransactionppsq\0~\0	\0\0\01q\0~\0q\0~\0.q\0~\0/q\0~\02ppsq\0~\0	\0\0\0�q\0~\0q\0~\04q\0~\05q\0~\06ppsq\0~\0	\0\0\0�q\0~\0q\0~\08q\0~\0q\0~\09ppsq\0~\0	\0\0Hpq\0~\0q\0~\0\rq\0~\09q\0~\0q\0~\0sq\0~\0?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xq\0~\0Jxq\0~\0<xq\0~\0!x'),
('TransactionBenchmark','��\0sr\00caosdb.server.database.misc.TransactionBenchmark�Cl=���E\0J\0sinceL\0acct\0Ljava/util/HashMap;L\0countsq\0~\0xp\0\0l���Wsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0	SyncStatssr\0java.lang.Long;��̏#�\0J\0valuexr\0java.lang.Number������\0\0xp\0\0\0\0\0\0\0t\0GetInfosq\0~\0\0\0\0\0\0\0 xsq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0q\0~\0sr\0java.lang.Integer⠤���8\0I\0valuexq\0~\0\0\0\0q\0~\0	sq\0~\0\0\0\0x');
/*!40000 ALTER TABLE `stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_data`
--

DROP TABLE IF EXISTS `text_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `value` text NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `str_entity_id_entity` (`entity_id`),
  KEY `str_property_id_entity` (`property_id`),
  CONSTRAINT `str_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `str_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `str_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_data`
--

LOCK TABLES `text_data` WRITE;
/*!40000 ALTER TABLE `text_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `text_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_log`
--

DROP TABLE IF EXISTS `transaction_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_log` (
  `transaction` varchar(255) NOT NULL COMMENT 'Transaction.',
  `entity_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `username` varbinary(255) NOT NULL,
  `seconds` bigint(20) unsigned NOT NULL DEFAULT 0,
  `nanos` int(10) unsigned NOT NULL DEFAULT 0,
  `realm` varbinary(255) NOT NULL,
  KEY `entity_id` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_log`
--

LOCK TABLES `transaction_log` WRITE;
/*!40000 ALTER TABLE `transaction_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `srid` varbinary(255) NOT NULL,
  `username` varbinary(255) NOT NULL,
  `realm` varbinary(255) NOT NULL,
  `seconds` bigint(20) unsigned NOT NULL,
  `nanos` int(10) unsigned NOT NULL,
  PRIMARY KEY (`srid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES
('cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e','administration','CaosDB',0,0);
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units_lin_con`
--

DROP TABLE IF EXISTS `units_lin_con`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units_lin_con` (
  `signature_from` bigint(20) NOT NULL,
  `signature_to` bigint(20) NOT NULL,
  `a` decimal(65,30) NOT NULL,
  `b_dividend` int(11) NOT NULL,
  `b_divisor` int(11) NOT NULL,
  `c` decimal(65,30) NOT NULL,
  PRIMARY KEY (`signature_from`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units_lin_con`
--

LOCK TABLES `units_lin_con` WRITE;
/*!40000 ALTER TABLE `units_lin_con` DISABLE KEYS */;
/*!40000 ALTER TABLE `units_lin_con` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info` (
  `realm` varbinary(255) NOT NULL,
  `name` varbinary(255) NOT NULL,
  `email` varbinary(255) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'INACTIVE',
  `entity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`realm`,`name`),
  KEY `subject_entity` (`entity`),
  CONSTRAINT `subjects_ibfk_2` FOREIGN KEY (`entity`) REFERENCES `entity_ids` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_info`
--

LOCK TABLES `user_info` WRITE;
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `realm` varbinary(255) NOT NULL,
  `user` varbinary(255) NOT NULL,
  `role` varbinary(255) NOT NULL,
  PRIMARY KEY (`realm`,`user`,`role`),
  KEY `user_roles_ibfk_1` (`role`),
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`role`) REFERENCES `roles` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'caosdb'
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `CaosDBVersion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `CaosDBVersion`() RETURNS varchar(255) CHARSET utf8 COLLATE utf8_unicode_ci
    DETERMINISTIC
RETURN 'v7.0.2' ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `constructDateTimeWhereClauseForColumn` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `constructDateTimeWhereClauseForColumn`(seconds_col VARCHAR(255), nanos_col VARCHAR(255), vDateTimeSecLow VARCHAR(255), vDateTimeNSLow VARCHAR(255), vDateTimeSecUpp VARCHAR(255), vDateTimeNSUpp VARCHAR(255), operator CHAR(4)) RETURNS varchar(20000) CHARSET utf8 COLLATE utf8_unicode_ci
    DETERMINISTIC
BEGIN

    DECLARE isInterval BOOLEAN DEFAULT vDateTimeSecUpp IS NOT NULL or vDateTimeNSUpp IS NOT NULL;
    DECLARE operator_prefix CHAR(1) DEFAULT LEFT(operator,1);

    IF isInterval THEN
        IF operator = '=' THEN
            RETURN " 0=1";
        ELSEIF operator = '!=' THEN
            RETURN " 0=1";
        ELSEIF operator = '>' or operator = '<=' THEN
            RETURN CONCAT(" ", seconds_col, operator_prefix, vDateTimeSecUpp);
        ELSEIF operator = '<' or operator = '>=' THEN
            RETURN CONCAT(" ", seconds_col, operator_prefix, vDateTimeSecLow);
        ELSEIF operator = "(" THEN
            RETURN CONCAT(" ", seconds_col, ">=", vDateTimeSecLow, " AND ",seconds_col, "<", vDateTimeSecUpp);
        ELSEIF operator = "!(" THEN
            RETURN CONCAT(" ", seconds_col, "<", vDateTimeSecLow, " OR ", seconds_col, ">=", vDateTimeSecUpp);
        END IF;
    ELSE
        IF operator = '=' THEN
            RETURN CONCAT(" ",
                seconds_col,
                "=", vDateTimeSecLow, IF(vDateTimeNSLow IS NULL, CONCAT(' AND ', nanos_col, ' IS NULL'), CONCAT(' AND ',
                    nanos_col,
                '=', vDateTimeNSLow)));
        ELSEIF operator = '!=' THEN
            RETURN CONCAT(" ",
                seconds_col,
                "!=", vDateTimeSecLow, IF(vDateTimeNSLow IS NULL, '', CONCAT(' OR ',
                        nanos_col,
                        '!=', vDateTimeNSLow)));
        ELSEIF operator = '>' or operator = '<' THEN
            RETURN CONCAT(" ",
                seconds_col, operator, vDateTimeSecLow, IF(vDateTimeNSLow IS NULL, '', CONCAT(' OR (',seconds_col,'=', vDateTimeSecLow, ' AND ',nanos_col, operator, vDateTimeNSLow, ')')));
        ELSEIF operator = '>=' or operator = '<=' THEN
            RETURN CONCAT(
                " ", seconds_col, operator, vDateTimeSecLow,
                IF(vDateTimeNSLow IS NULL,
                    '',
                    CONCAT(
                        ' AND (', seconds_col, operator_prefix, vDateTimeSecLow,
                        ' OR ', nanos_col, operator, vDateTimeNSLow,
                        ' OR ', nanos_col, ' IS NULL)')));
        ELSEIF operator = "(" THEN
            RETURN IF(vDateTimeNSLow IS NULL,CONCAT(" ",seconds_col,"=", vDateTimeSecLow),CONCAT(" ",seconds_col,"=",vDateTimeSecLow," AND ",nanos_col,"=",vDateTimeNSLow));
        ELSEIF operator = "!(" THEN
            RETURN IF(vDateTimeNSLow IS NULL,CONCAT(" ",seconds_col,"!=",vDateTimeSecLow, ""),CONCAT(" ",seconds_col,"!=",vDateTimeSecLow," OR ",nanos_col, " IS NULL OR ", nanos_col, "!=",vDateTimeNSLow));
        END IF;
    END IF;
    return ' 0=1';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `convert_unit` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `convert_unit`(unit_sig BIGINT, value DECIMAL(65,30)) RETURNS decimal(65,30)
    DETERMINISTIC
BEGIN
    DECLARE ret DECIMAL(65,30) DEFAULT value;

    SELECT (((value+a)*b_dividend)/b_divisor+c) INTO ret FROM units_lin_con WHERE signature_from=unit_sig;
    RETURN ret;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `getAggValueWhereClause` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getAggValueWhereClause`(entities VARCHAR(255), properties VARCHAR(255)) RETURNS varchar(20000) CHARSET utf8 COLLATE utf8_unicode_ci
    DETERMINISTIC
BEGIN
        RETURN CONCAT(" EXISTS (SELECT 1 FROM `", entities, "` AS ent WHERE ent.id = subdata.entity_id LIMIT 1)", IF(properties IS NOT NULL AND properties != '', CONCAT(" AND EXISTS (SELECT 1 FROM `", properties, "` as props WHERE props.id = subdata.property_id LIMIT 1)"),''));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `getDateTimeWhereClause` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getDateTimeWhereClause`(vDateTime VARCHAR(255), operator CHAR(4)) RETURNS varchar(20000) CHARSET utf8 COLLATE utf8_unicode_ci
    DETERMINISTIC
BEGIN
    DECLARE sep_loc INTEGER DEFAULT LOCATE('--',vDateTime);
    DECLARE vDateTimeLow VARCHAR(255) DEFAULT IF(sep_loc != 0, SUBSTRING_INDEX(vDateTime, '--',1), vDateTime);
    DECLARE vDateTimeUpp VARCHAR(255) DEFAULT IF(sep_loc != 0, SUBSTRING_INDEX(vDateTime, '--',-1), NULL);

    DECLARE vDateTimeSecLow VARCHAR(255) DEFAULT SUBSTRING_INDEX(vDateTimeLow, 'UTC', 1);
    DECLARE vDateTimeNSLow VARCHAR(255) DEFAULT IF(SUBSTRING_INDEX(vDateTimeLow, 'UTC', -1)='',NULL,SUBSTRING_INDEX(vDateTimeLow, 'UTC', -1));

    DECLARE vDateTimeSecUpp VARCHAR(255) DEFAULT IF(sep_loc != 0, SUBSTRING_INDEX(vDateTimeUpp, 'UTC', 1), NULL);
    DECLARE vDateTimeNSUpp VARCHAR(255) DEFAULT IF(sep_loc != 0 AND SUBSTRING_INDEX(vDateTimeUpp, 'UTC', -1)!='',SUBSTRING_INDEX(vDateTimeUpp, 'UTC', -1),NULL);


    RETURN constructDateTimeWhereClauseForColumn("subdata.value", "subdata.value_ns", vDateTimeSecLow, vDateTimeNSLow, vDateTimeSecUpp, vDateTimeNSUpp, operator);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `getDateWhereClause` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getDateWhereClause`(vDateTimeDotNotation VARCHAR(255), operator CHAR(4)) RETURNS varchar(20000) CHARSET utf8 COLLATE utf8_unicode_ci
    DETERMINISTIC
BEGIN
    DECLARE isInterval INTEGER DEFAULT LOCATE('--',vDateTimeDotNotation);
    
    DECLARE vILB VARCHAR(255) DEFAULT IF(isInterval != 0, SUBSTRING_INDEX(vDateTimeDotNotation, '--', 1), vDateTimeDotNotation);
    
    DECLARE vEUB VARCHAR(255) DEFAULT IF(isInterval != 0, SUBSTRING_INDEX(vDateTimeDotNotation, '--', -1), NULL);
    DECLARE vILB_Date INTEGER DEFAULT SUBSTRING_INDEX(vILB, '.', 1);
    DECLARE vEUB_Date INTEGER DEFAULT SUBSTRING_INDEX(vEUB, '.', 1);
    
    DECLARE hasTime INTEGER DEFAULT LOCATE('.NULL.NULL',vILB);
    
    DECLARE dom INTEGER DEFAULT vILB_Date % 100;
    
    DECLARE mon INTEGER DEFAULT ((vILB_Date % 10000) - dom) / 100;
    
    DECLARE yea INTEGER DEFAULT (vILB_Date - (vILB_Date % 10000)) / 10000;

    IF operator = '=' and hasTime != 0 THEN
        RETURN CONCAT(" subdata.value=", vILB_Date);
    ELSEIF operator = "!=" and hasTime != 0 THEN
        IF mon != 0  and dom != 0 THEN
            RETURN CONCAT(" subdata.value!=", vILB_Date, " and subdata.value%100!=0");
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" subdata.value!=", vILB_Date, " and subdata.value%100=0 and subdata.value%10000!=0");
        ELSE
            RETURN CONCAT(" subdata.value!=", vILB_Date, " and subdata.value%10000=0");
        END IF;
    ELSEIF operator = "(" and hasTime != 0 THEN
        IF mon != 0 and dom != 0 THEN
            RETURN CONCAT(" subdata.value=", vILB_Date);
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" subdata.value=",vILB_Date," OR (subdata.value>", vILB_Date, " and subdata.value<", vEUB_Date, " and subdata.value%10000!=0)");
        ELSE
            RETURN CONCAT(" subdata.value=",vILB_Date," OR (subdata.value>", vILB_Date, " and subdata.value<", vEUB_Date,")");
        END IF;
    ELSEIF operator = "!(" THEN
        IF hasTime = 0 THEN
            RETURN " 0=0";
        END IF;
        IF mon != 0 and dom != 0 THEN
            RETURN CONCAT(" subdata.value!=",vILB_Date);
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" (subdata.value!=",vILB_Date, " AND subdata.value%100=0) OR ((subdata.value<", vILB_Date, " or subdata.value>", vEUB_Date, ") and subdata.value%100!=0)");
        ELSE
            RETURN CONCAT(" (subdata.value!=",vILB_Date, " AND subdata.value%10000=0) OR ((subdata.value<", vILB_Date, " or subdata.value>=", vEUB_Date, ") and subdata.value%10000!=0)");
        END IF;
    ELSEIF operator = "<" THEN
        IF mon != 0 and dom != 0 THEN
            RETURN CONCAT(" subdata.value<", vILB_Date, " and (subdata.value%100!=0 or (subdata.value<", yea*10000+mon*100, " and subdata.value%10000!=0) or (subdata.value<", yea*10000, " and subdata.value%10000=0))");
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" subdata.value<", vILB_Date, " and (subdata.value%10000!=0 or (subdata.value<", yea*10000, "))");
        ELSE
            RETURN CONCAT(" subdata.value<", vILB_Date);
        END IF;
    ELSEIF operator = ">" THEN
        IF mon != 0 and dom != 0 THEN
            RETURN CONCAT(" subdata.value>", vILB_Date);
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" subdata.value>=",vEUB_Date);
        ELSE
            RETURN CONCAT(" subdata.value>=",vEUB_Date);
        END IF;
    ELSEIF operator = "<=" THEN
        IF mon != 0 and dom != 0 THEN
            
            RETURN CONCAT(" subdata.value<=", vILB_Date,
                          " or (subdata.value<=", yea*10000 + mon*100, " and subdata.value%100=0)");
        ELSEIF mon != 0 THEN
            
            RETURN CONCAT(" subdata.value<", vEUB_Date);
        ELSE
            
            RETURN CONCAT(" subdata.value<", vEUB_Date);
        END IF;
    ELSEIF operator = ">=" THEN
        IF mon != 0 and dom != 0 THEN
            
            RETURN CONCAT(" subdata.value>=", vILB_Date,
                          " or (subdata.value>=", yea*10000 + mon*100, " and subdata.value%100=0)",
                          " or (subdata.value>=", yea*10000, " and subdata.value%10000=0)");
        ELSEIF mon != 0 THEN
            
            RETURN CONCAT(" subdata.value>=", yea*10000 + mon*100,
                          " or (subdata.value>=", yea*10000, " and subdata.value%10000=0)");
        ELSE
            
            RETURN CONCAT(" subdata.value>=", yea*10000);
        END IF;
    END IF;

    return ' 0=1';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `getDoubleWhereClause` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getDoubleWhereClause`(value DOUBLE, unit_sig BIGINT, valueStdUnit DECIMAL(65,30), stdUnit_sig BIGINT, o CHAR(4)) RETURNS varchar(20000) CHARSET utf8 COLLATE utf8_unicode_ci
    DETERMINISTIC
BEGIN
    RETURN IF(unit_sig IS NULL AND value IS NOT NULL, 
        CONCAT('subdata.value ', o, ' \'', value, '\''), 
        CONCAT(
            IF(value IS NULL, '', 
                CONCAT('(subdata.unit_sig=', unit_sig, ' AND subdata.value ', o, ' \'', value, '\') OR ')), 
        	IF(unit_sig = stdUnit_sig,'',CONCAT('(subdata.unit_sig=', stdUnit_sig,' AND subdata.value ', o, ' \'', valueStdUnit, '\') OR ')),'(standard_unit(subdata.unit_sig)=', stdUnit_sig,' AND convert_unit(subdata.unit_sig,subdata.value) ', o, ' ', valueStdUnit, ')')); 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_head_relative` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `get_head_relative`(EntityID VARCHAR(255),
    HeadOffset INT UNSIGNED) RETURNS varbinary(255)
    READS SQL DATA
BEGIN
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalEntityID FROM entity_ids WHERE id = EntityID;

    
    
    
    
    RETURN (
        SELECT e.version
            FROM entity_version AS e
            WHERE e.entity_id = InternalEntityID
            ORDER BY e._iversion DESC
            LIMIT 1 OFFSET HeadOffset
        );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_head_version` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `get_head_version`(EntityID VARCHAR(255)) RETURNS varbinary(255)
    READS SQL DATA
BEGIN
    RETURN get_head_relative(EntityID, 0);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_iversion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `get_iversion`(InternalEntityID INT UNSIGNED,
    Version VARBINARY(255)) RETURNS int(10) unsigned
    READS SQL DATA
BEGIN
    RETURN (
        SELECT e._iversion
            FROM entity_version AS e
            WHERE e.entity_id = InternalEntityID
                AND e.version = Version
        );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_primary_parent_version` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `get_primary_parent_version`(EntityID VARCHAR(255),
    Version VARBINARY(255)) RETURNS varbinary(255)
    READS SQL DATA
BEGIN
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalEntityID FROM entity_ids WHERE id = EntityID;

    RETURN (
        SELECT p.version
            FROM entity_version AS e INNER JOIN entity_version AS p
                ON (e._ipparent = p._iversion
                    AND e.entity_id = p.entity_id)
            WHERE e.entity_id = InternalEntityID
                AND e.version = Version
        );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_version_timestamp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `get_version_timestamp`(EntityID VARCHAR(255),
    Version VARBINARY(255)) RETURNS varchar(255) CHARSET utf8 COLLATE utf8_unicode_ci
    READS SQL DATA
BEGIN
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalEntityID FROM entity_ids WHERE id = EntityID;

    RETURN (
        SELECT concat(t.seconds, '.', t.nanos)
            FROM entity_version AS e INNER JOIN transactions AS t
                ON ( e.srid = t.srid )
            WHERE e.entity_id = InternalEntityID
            AND e.version = Version
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `is_feature_config` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `is_feature_config`(_Key VARCHAR(255),
    Expected VARCHAR(255)) RETURNS tinyint(1)
    READS SQL DATA
BEGIN
    RETURN (
        SELECT f._value = Expected FROM feature_config as f WHERE f._key = _Key
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `makeStmt` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `makeStmt`(sourceSet VARCHAR(255), targetSet VARCHAR(255), data VARCHAR(20000),
                                properties VARCHAR(20000), versioned BOOLEAN) RETURNS varchar(20000) CHARSET utf8 COLLATE utf8_unicode_ci
    NO SQL
BEGIN
        IF sourceSet = "entities" AND versioned THEN
            RETURN CONCAT('INSERT IGNORE INTO `',
                targetSet,
                '` (id, _iversion) SELECT entity_id, _iversion FROM ',
                data,
                IF(properties IS NULL, '',
                    CONCAT(' AS data JOIN `', properties, '` AS prop ON (data.property_id = prop.id) WHERE ',
                           'data.entity_id = prop.id2 OR prop.id2 = 0')));
        END IF;
        RETURN CONCAT(
            IF(targetSet IS NULL,
                CONCAT('DELETE FROM `',sourceSet,'` WHERE NOT EXISTS (SELECT 1 FROM '), 
                CONCAT('INSERT IGNORE INTO `',targetSet,'` (id) SELECT id FROM `',sourceSet,'` ',
                       'WHERE EXISTS (SELECT 1 FROM ')),
            IF(properties IS NULL,
                CONCAT(data,' as data WHERE '),
                CONCAT('`',properties,'` as prop JOIN ',data,' as data ON (data.property_id=prop.id) WHERE ',
                       '(data.entity_id=prop.id2 OR prop.id2=0) AND ')),
            'data.entity_id=`', sourceSet, '`.`id` LIMIT 1)'
        );

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `standard_unit` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `standard_unit`(unit_sig BIGINT) RETURNS bigint(20)
    DETERMINISTIC
BEGIN
    DECLARE ret BIGINT DEFAULT unit_sig;

    SELECT signature_to INTO ret FROM units_lin_con WHERE signature_from=unit_sig;
    RETURN ret;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `_get_head_iversion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `_get_head_iversion`(InternalEntityID INT UNSIGNED) RETURNS int(10) unsigned
    READS SQL DATA
BEGIN
    
    
    
    
    RETURN (
        SELECT e._iversion
            FROM entity_version AS e
            WHERE e.entity_id = InternalEntityID
            ORDER BY e._iversion DESC
            LIMIT 1
        );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `_get_version` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `_get_version`(InternalEntityID INT UNSIGNED,
    IVersion INT UNSIGNED) RETURNS varbinary(255)
    READS SQL DATA
BEGIN
    RETURN (
        SELECT version FROM entity_version
            WHERE entity_id = InternalEntityID
            AND _iversion = IVersion
        );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `applyBackReference` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `applyBackReference`(in sourceSet VARCHAR(255), targetSet VARCHAR(255),
    in propertiesTable VARCHAR(255), in entitiesTable VARCHAR(255), in subQuery BOOLEAN,
    in versioned BOOLEAN)
BEGIN
    DECLARE newTableName VARCHAR(255) DEFAULT NULL;


    IF subQuery IS TRUE THEN
        call registerTempTableName(newTableName);

        SET @createBackRefSubQueryTableStr = CONCAT('CREATE TEMPORARY TABLE `',newTableName,'` ( entity_id INT UNSIGNED NOT NULL, id INT UNSIGNED NOT NULL, CONSTRAINT `',newTableName,'PK` PRIMARY KEY (id, entity_id))');

        PREPARE createBackRefSubQueryTable FROM @createBackRefSubQueryTableStr;
        EXECUTE createBackRefSubQueryTable;
        DEALLOCATE PREPARE createBackRefSubQueryTable;

        SET @backRefSubResultSetStmtStr = CONCAT('INSERT IGNORE INTO `',
            newTableName,
            '` (id,entity_id) SELECT entity_id AS id, value AS entity_id FROM `reference_data` AS data ',
            'WHERE EXISTS (SELECT 1 FROM `',
                sourceSet,
                '` AS source WHERE source.id=data.value LIMIT 1)',
            IF(propertiesTable IS NULL,
                '',
                CONCAT(' AND EXISTS (SELECT 1 FROM `',
                    propertiesTable,
                    '` AS p WHERE p.id=data.property_id LIMIT 1)')),
            IF(entitiesTable IS NULL,
                '',
                CONCAT(' AND EXISTS (SELECT 1 FROM `',
                    entitiesTable,
                    '` AS e WHERE e.id=data.entity_id LIMIT 1)'))
        );

        PREPARE backRefSubResultSetStmt FROM @backRefSubResultSetStmtStr;
        EXECUTE backRefSubResultSetStmt;
        DEALLOCATE PREPARE backRefSubResultSetStmt;

        SELECT newTableName as list;
    ELSE
        IF versioned THEN
            IF sourceSet = "entities" THEN
                
                SET @stmtBackRefStr = CONCAT('INSERT IGNORE INTO `',
                    targetSet,
                    '` (id, _iversion) SELECT source.id, _get_head_iversion(source.id)',
                    
                    ' FROM entities AS source WHERE EXISTS (',
                        'SELECT 1 FROM `reference_data` AS data WHERE data.value=source.id AND (',
                            'data.value_iversion IS NULL OR data.value_iversion=_get_head_iversion(source.id))',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),
                    ') UNION ALL ',
                    
                    'SELECT source.id, source._iversion FROM archive_entities AS source WHERE EXISTS (',
                        'SELECT 1 FROM `reference_data` AS data WHERE data.value=source.id AND ',
                          '(data.value_iversion IS NULL OR data.value_iversion=source._iversion)',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),

                    ')');
            ELSEIF targetSet IS NULL OR sourceSet = targetSet THEN
                SET @stmtBackRefStr = CONCAT('DELETE FROM `',
                    sourceSet,
                    '` WHERE NOT EXISTS (SELECT 1 FROM `reference_data` AS data WHERE data.value=`',
                    sourceSet,
                    '`.`id` AND ( data.value_iversion IS NULL OR data.value_iversion=`',
                    sourceSet,
                    '`._iversion)',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),
                    ')');
            ELSE
                SET @stmtBackRefStr = CONCAT('INSERT IGNORE INTO `',
                    targetSet,
                    '` (id, _iversion) SELECT source.id, source._iversion FROM `',
                    sourceSet,
                    '` AS source WHERE EXISTS (',
                    'SELECT 1 FROM `reference_data` AS data WHERE data.value=source.id AND',
                    ' (data.value_iversion IS NULL OR data.value_iversion=source._iversion)',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),

                    ')');
            END IF;
        ELSE
            
            IF targetSet IS NULL OR sourceSet = targetSet THEN
                
                SET @stmtBackRefStr = CONCAT('DELETE FROM `',
                    sourceSet,
                    '` WHERE NOT EXISTS (SELECT 1 FROM `reference_data` AS data WHERE data.value=`',
                    sourceSet,
                    '`.`id`',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT('
                            AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT('
                            AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),
                    ')');
            ELSE
                
                SET @stmtBackRefStr = CONCAT('INSERT IGNORE INTO `',
                    targetSet,
                    '` (id) SELECT id FROM `',
                    sourceSet,
                    '` AS source WHERE EXISTS (SELECT 1 FROM `reference_data` AS data WHERE data.value=source.id',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),
                    ')');
            END IF;
        END IF;

        PREPARE stmtBackRef FROM @stmtBackRefStr;
        EXECUTE stmtBackRef;
        DEALLOCATE PREPARE stmtBackRef;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `applyIDFilter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `applyIDFilter`(in sourceSet VARCHAR(255), in targetSet VARCHAR(255),
    in o CHAR(2), in EntityID VARCHAR(255), in agg CHAR(3), in versioned BOOLEAN)
IDFILTER_LABEL: BEGIN
DECLARE data VARCHAR(20000) DEFAULT NULL;
DECLARE aggVal VARCHAR(255) DEFAULT NULL;
DECLARE direction CHAR(4) DEFAULT NULL;
DECLARE entity_id_type VARCHAR(255) DEFAULT "eids.id ";


IF agg IS NOT NULL THEN
    IF versioned THEN
        
        SELECT 1 FROM id_agg_with_versioning_not_implemented;
    ELSEIF agg = "max" THEN
        SET direction = "DESC";
    ELSEIF agg = "min" THEN
        SET direction = "ASC ";
    ELSE
        SELECT 1 FROM unknown_agg_parameter;
    END IF;

    SET @stmtIDAggValStr = CONCAT(
        "SELECT e.internal_id INTO @sAggVal FROM `",
        sourceSet,
        "` AS s LEFT JOIN entity_ids AS e ON (s.id=e.internal_id) WHERE s.id>99 ORDER BY CAST(e.id AS UNSIGNED INT) ",
        direction,
        " LIMIT 1");

    PREPARE stmtIDAggVal FROM @stmtIDAggValStr;
    EXECUTE stmtIDAggVal;
    DEALLOCATE PREPARE stmtIDAggVal;
    SET aggVal = @sAggVal;
END IF;

IF o = ">" OR o = ">=" OR o = "<" or o = "<=" THEN
    SET entity_id_type = "CAST(eids.id AS UNSIGNED INT) ";
END IF;


IF targetSet IS NULL OR targetSet = sourceSet THEN
    SET data = CONCAT(
        "DELETE FROM `",
        sourceSet,
        "` WHERE ",
        IF(o IS NULL OR EntityID IS NULL,
            "1=1",
            CONCAT("NOT EXISTS (SELECT 1 FROM entity_ids AS eids WHERE ",
                entity_id_type,
                o,
                ' "',
                EntityID,
                '" ',
                " AND eids.internal_id = `",
                sourceSet,
                "`.id)"
            )),
        IF(aggVal IS NULL,
            "",
            CONCAT(" AND `", sourceSet, "`.id!=",
                aggVal)));
ELSEIF versioned AND sourceSet = "entities" THEN
    
    SET data = CONCAT(
        "INSERT IGNORE INTO `",
        targetSet,
        '` (id, _iversion) SELECT e.id, _get_head_iversion(e.id) FROM `entities` AS e JOIN entity_ids AS eids ON (e.id = eids.internal_id) WHERE ',
        IF(o IS NULL OR EntityID IS NULL,
            "1=1",
            CONCAT(entity_id_type,
                o,
                ' "',
                EntityID,
                '"'
            )),
        IF(aggVal IS NULL,
            "",
            CONCAT(" AND e.id=",
                aggVal)),
        ' UNION SELECT e.id, _iversion FROM `archive_entities` AS e JOIN entity_ids AS eids ON (e.id = eids.internal_id) WHERE ',
        IF(o IS NULL OR EntityID IS NULL,
            "1=1",
            CONCAT(entity_id_type,
                o,
                ' "',
                EntityID,
                '"'
            )),
        IF(aggVal IS NULL,
            "",
            CONCAT(" AND e.id=",
                aggVal)));
    

ELSE
    SET data = CONCAT(
        "INSERT IGNORE INTO `",
        targetSet,
        IF(versioned,
            '` (id, _iversion) SELECT data.id, data._iversion FROM `',
            '` (id) SELECT data.id FROM `'),
        sourceSet,
        "` AS data JOIN entity_ids AS eids ON (eids.internal_id = data.id) WHERE ",
        IF(o IS NULL OR EntityID IS NULL,
            "1=1",
            CONCAT(entity_id_type,
                o,
                ' "',
                EntityID,
                '"'
            )),
        IF(aggVal IS NULL,
            "",
            CONCAT(" AND data.id=",
                aggVal)));
END IF;

Set @stmtIDFilterStr = data;
PREPARE stmtIDFilter FROM @stmtIDFilterStr;
EXECUTE stmtIDFilter;
DEALLOCATE PREPARE stmtIDFilter;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `applyPOV` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `applyPOV`(in sourceSet VARCHAR(255), 
                                 in targetSet VARCHAR(255), 
                                 in propertiesTable VARCHAR(255),
                                 in refIdsTable VARCHAR(255), 
                                 in o CHAR(4), 
                                 in vText VARCHAR(255),  
                                 in vInt INT,  
                                 in vDouble DOUBLE,  
                                 in unit_sig BIGINT, 
                                 in vDoubleStdUnit DOUBLE, 
                                 in stdUnit_sig BIGINT, 
                                 in vDateTime VARCHAR(255),
                                 in vDateTimeDotNotation VARCHAR(255),
                                 in agg CHAR(3), 
                                 in pname VARCHAR(255), 
                                 in versioned BOOLEAN)
POV_LABEL: BEGIN
    DECLARE data TEXT DEFAULT NULL; 
    DECLARE sTextData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sNameData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sEnumData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sIntData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sDoubleData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sDatetimeData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sNullData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sDateData VARCHAR(20000) DEFAULT NULL; 
    DECLARE sRefData VARCHAR(20000) DEFAULT NULL; 
    DECLARE aggValue VARCHAR(255) DEFAULT NULL;
    DECLARE aggValueWhereClause VARCHAR(20000) DEFAULT NULL;
    DECLARE distinctUnits INT DEFAULT 0;
    DECLARE usedStdUnit BIGINT DEFAULT NULL;
    DECLARE keepTabl VARCHAR(255) DEFAULT NULL;
    DECLARE existence_op VARCHAR(255) DEFAULT "EXISTS";

        
        
        

    IF o = '->' THEN
        
        call applyRefPOV(sourceSet,targetSet, propertiesTable, refIdsTable, versioned);
        LEAVE POV_LABEL;
    ELSEIF o = '0' THEN
        
        
        SET vText = NULL;
        SET sTextData = 'SELECT domain_id, entity_id, property_id FROM `null_data` AS subdata';

        

    ELSEIF o = '!0' THEN
        
        
        SET vText = NULL;
        
        SET sTextData = CONCAT(
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `text_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL UNION ALL ',
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `name_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL UNION ALL ',
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `enum_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL UNION ALL ',
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `integer_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL UNION ALL ',
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `double_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL UNION ALL ',
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `date_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL UNION ALL ',
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `datetime_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL UNION ALL ',
        'SELECT DISTINCT domain_id, entity_id, property_id FROM `reference_data` AS subdata ',
            'WHERE subdata.value IS NOT NULL');

    ELSEIF o = "(" or o = "!(" THEN  
        IF versioned THEN
            SET sTextData = IF(vText IS NULL,
            CONCAT(
                ' SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) ',
                'AS _iversion, property_id FROM `date_data` UNION ALL ',
                'SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_date_data`'),
            IF(vDateTimeDotNotation IS NULL, NULL,  
                CONCAT(' SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) ',
                       'AS _iversion, property_id FROM `date_data` AS subdata WHERE ',
                       getDateWhereClause(vDateTimeDotNotation, o), ' UNION ALL ',
                       'SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_date_data` ',
                       'AS subdata WHERE ', getDateWhereClause(vDateTimeDotNotation, o))));
            SET sDatetimeData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `datetime_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_datetime_data`',
                                                  IF(vDateTime IS NULL, NULL,
                                                                        CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `datetime_data` AS subdata WHERE ', getDateTimeWhereClause(vDateTime, o), ' UNION ALL SELECT DISTINCT domain_id, entity_id,_iversion, property_id FROM `archive_datetime_data` AS subdata WHERE ', getDateTimeWhereClause(vDateTime, o))));
        ELSE  
            SET sTextData = IF(vText IS NULL,
                ' SELECT DISTINCT domain_id, entity_id, property_id FROM `date_data`',
                IF(vDateTimeDotNotation IS NULL, NULL,
                    CONCAT(' SELECT DISTINCT domain_id, entity_id, property_id FROM `date_data` AS subdata WHERE ',
                             getDateWhereClause(vDateTimeDotNotation, o))));
            SET sDatetimeData = IF(vText IS NULL,
                ' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `datetime_data`',
                IF(vDateTime IS NULL, NULL,
                   CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `datetime_data` ',
                           'AS subdata WHERE ', getDateTimeWhereClause(vDateTime, o))));
        END IF;
        SET vText = NULL;
    ELSEIF agg IS NOT NULL THEN
        IF versioned THEN
            SELECT 1 FROM versioned_agg_pov_filter_not_implemented;
        END IF;
        

        
        SET aggValueWhereClause = CONCAT(getDoubleWhereClause(vDouble, unit_sig, vDoubleStdUnit, stdUnit_sig, o), ' AND ');
        SET aggValueWhereClause = CONCAT(IF(aggValueWhereClause IS NULL, '', aggValueWhereClause), getAggValueWhereClause(sourceSet, propertiesTable));

        
        SET @aggValueStmtStr = CONCAT('SELECT ',agg,'(subdata.value), ', agg, '(convert_unit(subdata.unit_sig,subdata.value)), COUNT(DISTINCT standard_unit(subdata.unit_sig)), max(standard_unit(subdata.unit_sig)) INTO @sAggValue, @sAggValueConvert, @distinctUnits, @StdUnitSig FROM (SELECT entity_id, property_id, value, unit_sig FROM `integer_data` UNION ALL SELECT entity_id, property_id, value, unit_sig FROM `double_data`) AS subdata WHERE ', aggValueWhereClause);

        
        PREPARE stmtAggValueStmt FROM @aggValueStmtStr;
        EXECUTE stmtAggValueStmt;
        DEALLOCATE PREPARE stmtAggValueStmt;

        SET distinctUnits = @distinctUnits;
        SET aggValue = @sAggValue;

        
        IF distinctUnits = 1 THEN
            SET aggValue = @sAggValueConvert;
            SET usedStdUnit = @StdUnitSig;
        ELSE
            call raiseWarning(CONCAT("The filter POV(",IF(pname IS NULL, 'NULL', pname),",",IF(o IS NULL, 'NULL', o),",",IF(vText IS NULL, 'NULL', vText),") with the aggregate function '", agg, "' could not match the values against each other with their units. The values had different base units. Only their numric value had been taken into account." ));
        END IF;

        IF aggValue IS NULL THEN
            SET sTextData = 'SELECT NULL as domain_id, NULL as entity_id, NULL as property_id';
        ELSE
            SET sTextData = '';
            SET sIntData = CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `integer_data` as subdata WHERE ', getDoubleWhereClause(aggValue, usedStdUnit, aggValue, usedStdUnit, '='));
            SET sDoubleData = CONCAT(' SELECT DISTINCT domain_id, entity_id, property_id FROM `double_data` as subdata WHERE ', getDoubleWhereClause(aggValue, usedStdUnit, aggValue, usedStdUnit, '='));
        END IF;

        SET vText = NULL;
    ELSE
        
        IF versioned THEN
            SET sTextData = IF(vText IS NULL,
            'SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `text_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_text_data` ',
            CONCAT(
            'SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id ',
              'FROM `text_data` AS subdata WHERE subdata.value ', o,' ? ',
            'UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id ',
              'FROM `archive_text_data` AS subdata WHERE subdata.value ', o, '?'
            ));
            SET sNameData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `name_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_name_data` ', CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `name_data` AS subdata WHERE subdata.value ', o, ' ? UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_name_data` AS subdata WHERE subdata.value ', o, '?'));
            SET sEnumData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `enum_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_enum_data` ', CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `enum_data` AS subdata WHERE subdata.value ', o, ' ? UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_enum_data` AS subdata WHERE subdata.value ', o, '?'));
            IF o = "!=" AND refIdsTable IS NOT NULL THEN
                SET existence_op = "NOT EXISTS";
            END IF;
            SET sRefData = IF(vText IS NULL,
                ' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `reference_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_reference_data`',
                IF(refIdsTable IS NULL,
                    NULL,
                    CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `reference_data` AS subdata WHERE ', existence_op, ' (SELECT 1 FROM `', refIdsTable, '` AS refIdsTable WHERE subdata.value=refIdsTable.id LIMIT 1) AND subdata.status != "REPLACEMENT" UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_reference_data` AS subdata WHERE ', existence_op, ' (SELECT 1 FROM `', refIdsTable, '` AS refIdsTable WHERE subdata.value=refIdsTable.id LIMIT 1) AND subdata.status != "REPLACEMENT"')));
            SET sDoubleData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT subdata.domain_id, subdata.entity_id, _get_head_iversion(subdata.entity_id) AS _iversion, subdata.property_id FROM `double_data` AS subdata UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_double_data` ', IF(vDouble IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id), property_id FROM `double_data` AS subdata WHERE ', getDoubleWhereClause(vDouble,unit_sig,vDoubleStdUnit,stdUnit_sig,o), ' UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_double_data` AS subdata WHERE ', getDoubleWhereClause(vDouble, unit_sig, vDoubleStdUnit, stdUnit_sig, o))));
            SET sIntData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT subdata.domain_id, subdata.entity_id, _get_head_iversion(subdata.entity_id) AS _iversion, subdata.property_id FROM `integer_data` AS subdata UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_integer_data`', IF(vInt IS NULL AND vDoubleStdUnit IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `integer_data` AS subdata WHERE ', getDoubleWhereClause(vInt, unit_sig, vDoubleStdUnit, stdUnit_sig, o), ' UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_integer_data` AS subdata WHERE ', getDoubleWhereClause(vInt, unit_sig, vDoubleStdUnit, stdUnit_sig, o))));
            SET sDatetimeData = IF(vText IS NULL,' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `datetime_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_datetime_data`', IF(vDateTime IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `datetime_data` AS subdata WHERE ',getDateTimeWhereClause(vDateTime,o), ' UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_datetime_data` AS subdata WHERE ',getDateTimeWhereClause(vDateTime,o))));
            SET sDateData = IF(vText IS NULL,' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `date_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_date_data`', IF(vDateTimeDotNotation IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `date_data` AS subdata WHERE ', getDateWhereClause(vDateTimeDotNotation,o), ' UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_date_data` AS subdata WHERE ', getDateWhereClause(vDateTimeDotNotation,o))));
            SET sNullData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id FROM `null_data` UNION ALL SELECT DISTINCT domain_id, entity_id, _iversion, property_id FROM `archive_null_data`', NULL);
        
        ELSE
            SET sTextData = IF(vText IS NULL, 'SELECT DISTINCT domain_id, entity_id, property_id FROM `text_data`', CONCAT('SELECT DISTINCT domain_id, entity_id, property_id FROM `text_data` AS subdata WHERE subdata.value ',o,' ?'));
            SET sNameData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `name_data`', CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `name_data` AS subdata WHERE subdata.value ', o, ' ?'));
            SET sEnumData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `enum_data`', CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `enum_data` AS subdata WHERE subdata.value ', o, ' ?'));
            IF o = "!=" AND refIdsTable IS NOT NULL THEN
                SET existence_op = "NOT EXISTS";
            END IF;
			
            SET sRefData = IF(vText IS NULL,
                ' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `reference_data`',
                IF(refIdsTable IS NULL,
                    NULL,
                    CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `reference_data` AS subdata WHERE ',existence_op ,' (SELECT 1 FROM `', refIdsTable, '` AS refIdsTable WHERE subdata.value=refIdsTable.id LIMIT 1) AND subdata.status != "REPLACEMENT"')));
            SET sDoubleData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT subdata.domain_id, subdata.entity_id, subdata.property_id FROM `double_data` AS subdata', IF(vDouble IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `double_data` AS subdata WHERE ', getDoubleWhereClause(vDouble,unit_sig,vDoubleStdUnit,stdUnit_sig,o))));
            SET sIntData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT subdata.domain_id, subdata.entity_id, subdata.property_id FROM `integer_data` AS subdata', IF(vInt IS NULL AND vDoubleStdUnit IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `integer_data` AS subdata WHERE ', getDoubleWhereClause(vInt, unit_sig, vDoubleStdUnit, stdUnit_sig, o))));
            SET sDatetimeData = IF(vText IS NULL,' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `datetime_data`', IF(vDateTime IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `datetime_data` AS subdata WHERE ',getDateTimeWhereClause(vDateTime,o))));
            SET sDateData = IF(vText IS NULL,' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `date_data`', IF(vDateTimeDotNotation IS NULL, NULL, CONCAT(' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `date_data` AS subdata WHERE ',getDateWhereClause(vDateTimeDotNotation,o))));
            SET sNullData = IF(vText IS NULL, ' UNION ALL SELECT DISTINCT domain_id, entity_id, property_id FROM `null_data`', NULL);
        END IF;

    END IF;

    
    SET data = CONCAT('(',sTextData,
                IF(sNameData IS NULL, '', sNameData),
                IF(sEnumData IS NULL, '', sEnumData),
                IF(sDoubleData IS NULL, '', sDoubleData),
                IF(sIntData IS NULL, '', sIntData),
                IF(sDatetimeData IS NULL, '', sDatetimeData),
                IF(sDateData IS NULL, '', sDateData),
                IF(sRefData IS NULL, '', sRefData),
                IF(sNullData IS NULL, '', sNullData),
                ')'
            );


    call createTmpTable(keepTabl, versioned);
    IF versioned THEN
        
        
        SET @stmtPOVkeepTblStr = CONCAT(
            'INSERT IGNORE INTO `', keepTabl, '` (id, _iversion) SELECT entity_id AS id, _iversion FROM ', data,
            ' as data', IF(propertiesTable IS NULL, '', CONCAT(
                ' WHERE EXISTS (Select 1 from `', propertiesTable, '` AS prop ',
                  'WHERE prop.id = data.property_id AND (prop.id2=data.entity_id OR prop.id2=0))')));

        IF targetSet IS NOT NULL THEN
            SET @stmtPOVStr = CONCAT('INSERT IGNORE INTO `',
                    targetSet,
                    '` (id, _iversion) SELECT source.id, source._iversion FROM `',
                    keepTabl,
                    '` AS source');
        ELSE
        
            SET @stmtPOVStr = CONCAT('DELETE FROM `',
                    sourceSet,
                    '` WHERE NOT EXISTS (SELECT 1 FROM `',
                    keepTabl,
                    '` AS data WHERE data.id=`',
                    sourceSet,
                    '`.`id` AND data._iversion=`',
                    sourceSet,
                    '`._iversion LIMIT 1)');

        END IF;

        
        PREPARE stmt3 FROM @stmtPOVStr;
        PREPARE stmtPOVkeepTbl FROM @stmtPOVkeepTblStr;
        IF vText IS NULL THEN
            EXECUTE stmtPOVkeepTbl;
        ELSE
            SET @vText = vText;
            EXECUTE stmtPOVkeepTbl USING @vText, @vText, @vText, @vText, @vText, @vText;
        END IF;
        EXECUTE stmt3;
        DEALLOCATE PREPARE stmt3;
        DEALLOCATE PREPARE stmtPOVkeepTbl;
    ELSE
        
        SET @stmtPOVkeepTblStr = CONCAT(
            'INSERT IGNORE INTO `', keepTabl,
            '` (id) SELECT DISTINCT entity_id AS id FROM ', data, ' as data',
            IF(propertiesTable IS NULL, '',
                CONCAT(' WHERE EXISTS (Select 1 from `', propertiesTable,
                        '` AS prop WHERE prop.id = data.property_id AND
                        (prop.id2=data.entity_id OR prop.id2=0))')));
        

        SET @stmtPOVStr = CONCAT(
                IF(targetSet IS NULL,
                    CONCAT('DELETE FROM `',
                        sourceSet,
                        '` WHERE NOT EXISTS (SELECT 1 FROM `'),
                    CONCAT('INSERT IGNORE INTO `',
                        targetSet,
                        '` (id) SELECT id FROM `',
                        sourceSet,
                        '` WHERE EXISTS (SELECT 1 FROM `')),
                keepTabl,
                '` AS data WHERE data.id=`',
                sourceSet,
                '`.`id` LIMIT 1)'
            );

        
        PREPARE stmt3 FROM @stmtPOVStr;
        PREPARE stmtPOVkeepTbl FROM @stmtPOVkeepTblStr;
        IF vText IS NULL THEN
            EXECUTE stmtPOVkeepTbl;
        ELSE
            SET @vText = vText;
            EXECUTE stmtPOVkeepTbl USING @vText, @vText, @vText;
        END IF;
        EXECUTE stmt3;
        DEALLOCATE PREPARE stmt3;
        DEALLOCATE PREPARE stmtPOVkeepTbl;
    END IF;

    SELECT @stmtPOVkeepTblStr as applyPOVStmt1, @stmtPOVStr as applyPOVStmt2, keepTabl as applyPOVIntermediateResultSet;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `applyRefPOV` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `applyRefPOV`(in sourceSet VARCHAR(255), in targetSet VARCHAR(255),
                                    in properties VARCHAR(255), in refs VARCHAR(255),
                                    in versioned BOOLEAN)
BEGIN
    DECLARE data VARCHAR(20000) DEFAULT CONCAT(
        '(SELECT domain_id, entity_id, property_id FROM `reference_data` AS subdata ',
        'WHERE EXISTS (SELECT 1 FROM `', refs, '` AS refs WHERE subdata.value=refs.id LIMIT 1))');

    IF versioned THEN
        SET data = CONCAT(
            '(SELECT domain_id, entity_id, _get_head_iversion(entity_id) AS _iversion, property_id ',
                'FROM `reference_data` AS subdata WHERE EXISTS (',
                    'SELECT 1 FROM `', refs, '` AS refs WHERE subdata.value=refs.id LIMIT 1) ',
            'UNION ALL SELECT domain_id, entity_id, _iversion, property_id ',
                'FROM `archive_reference_data` AS subdata WHERE EXISTS (',
                    'SELECT 1 FROM `', refs, '` AS refs WHERE subdata.value=refs.id LIMIT 1))');
    END IF;
    SET @stmtRefPOVStr = makeStmt(sourceSet,targetSet,data,properties, versioned);

    PREPARE stmt4 FROM @stmtRefPOVStr;
    EXECUTE stmt4;

    SELECT @stmtRefPOVstr as applyRefPOVStmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `applySAT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `applySAT`(in sourceSet VARCHAR(255), in targetSet VARCHAR(255), in loc MEDIUMTEXT, in op CHAR(5))
BEGIN

    IF targetSet IS NULL OR sourceSet = targetSet THEN
        SET @stmtSATString = CONCAT('DELETE FROM `', sourceSet, '` WHERE id NOT IN (SELECT file_id FROM files WHERE path ', op, ' ?)');  
    ELSE
        SET @stmtSATString = CONCAT('INSERT INTO `', targetSet, '` (id) SELECT data.id FROM `',sourceSet,'` as data WHERE EXISTS (SELECT 1 FROM `files` as f WHERE f.file_id=data.id AND f.path ', op, ' ?)');
    END IF;
    PREPARE stmtSAT FROM @stmtSATString;
	SET @loc = loc;
    EXECUTE stmtSAT USING @loc;
    DEALLOCATE PREPARE stmtSAT;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `applyTransactionFilter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `applyTransactionFilter`(in sourceSet VARCHAR(255), targetSet VARCHAR(255), in transaction VARCHAR(255), in operator_u CHAR(2), in realm VARCHAR(255), in userName VARCHAR(255), in ilb BIGINT, in ilb_nanos INT UNSIGNED, in eub BIGINT, in eub_nanos INT UNSIGNED, in operator_t CHAR(2))
BEGIN
    DECLARE data TEXT default CONCAT("(SELECT internal_id AS entity_id FROM transaction_log AS t JOIN entity_ids AS eids ON ( t.entity_id = eids.id ) WHERE t.transaction='",
        transaction,
        "'",
        IF(userName IS NOT NULL,
            CONCAT(' AND t.realm', operator_u, '? AND t.username', operator_u, '?'),
            ''
        ),
        IF(ilb IS NOT NULL,
            CONCAT(" AND", constructDateTimeWhereClauseForColumn("t.seconds", "t.nanos", ilb, ilb_nanos, eub, eub_nanos, operator_t)),
            ""
        ),
        ')'
    );

    SET @stmtTransactionStr = makeStmt(sourceSet, targetSet, data, NULL, FALSE);
    PREPARE stmtTransactionFilter from @stmtTransactionStr;
    IF userName IS NOT NULL THEN
        SET @userName = userName;
        SET @realm = realm;
        EXECUTE stmtTransactionFilter USING @realm, @userName;
    ELSE
        EXECUTE stmtTransactionFilter;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calcComplementUnion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `calcComplementUnion`(in targetSet VARCHAR(255), in subResultSet VARCHAR(255), in universe VARCHAR(255), in versioned BOOLEAN)
BEGIN
    IF versioned AND universe = "entities" THEN
        SET @stmtComplementUnionStr = CONCAT(
            'INSERT IGNORE INTO `', targetSet,
            '` SELECT e.id, _get_head_iversion(e.id) FROM entities as e WHERE NOT EXISTS ( SELECT 1 FROM `',
            subResultSet,
            '` AS diff WHERE diff.id=e.id AND diff._iversion = _get_head_iversion(e.id)) UNION ALL SELECT e.id, e._iversion FROM archive_entities AS e WHERE NOT EXISTS ( SELECT 1 FROM `',
            subResultSet,
            '` as diff WHERE e.id = diff.id AND e._iversion = diff._iversion)');
    ELSEIF versioned THEN
        SET @stmtComplementUnionStr = CONCAT(
            'INSERT IGNORE INTO `', targetSet,
            '` SELECT id FROM `',universe,
            '` AS universe WHERE NOT EXISTS ( SELECT 1 FROM `',
                subResultSet,'`
                AS diff WHERE diff.id=universe.id AND diff._iversion = universe.id_version)');
    ELSE
        SET @stmtComplementUnionStr = CONCAT('INSERT IGNORE INTO `', targetSet, '` SELECT id FROM `',universe, '` AS universe WHERE NOT EXISTS ( SELECT 1 FROM `', subResultSet,'` AS diff WHERE diff.id=universe.id)');
    END IF;
    PREPARE stmtComplementUnion FROM @stmtComplementUnionStr;
    EXECUTE stmtComplementUnion;
    DEALLOCATE PREPARE stmtComplementUnion;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calcDifference` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `calcDifference`(in resultSetTable VARCHAR(255), in diff VARCHAR(255), in versioned BOOLEAN)
BEGIN
    IF versioned THEN
        SET @diffStmtStr = CONCAT('DELETE FROM `', resultSetTable, '` WHERE EXISTS ( SELECT 1 FROM `', diff,'` AS diff WHERE diff.id=`',resultSetTable,'`.`id` AND diff._iversion=`', resultSetTable, '`.`_iversion`)');
    ELSE
        SET @diffStmtStr = CONCAT('DELETE FROM `', resultSetTable, '` WHERE EXISTS ( SELECT 1 FROM `', diff,'` AS diff WHERE diff.id=`',resultSetTable,'`.`id`)');
    END IF;
    PREPARE diffStmt FROM @diffStmtStr;
    EXECUTE diffStmt;
    DEALLOCATE PREPARE diffStmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calcIntersection` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `calcIntersection`(in resultSetTable VARCHAR(255), in intersectWith VARCHAR(255), in versioned BOOLEAN)
BEGIN
    IF versioned THEN
        SET @diffStmtStr = CONCAT('DELETE FROM `',
            resultSetTable,
            '` WHERE NOT EXISTS ( SELECT 1 FROM `',
            intersectWith,
            '` AS diff WHERE diff.id=`',
            resultSetTable,
            '`.`id` AND diff._iversion=`',
            resultSetTable,
            '`.`_iversion`)');
    ELSE
        SET @diffStmtStr = CONCAT('DELETE FROM `', resultSetTable, '` WHERE NOT EXISTS ( SELECT 1 FROM `', intersectWith,'` AS diff WHERE diff.id=`',resultSetTable,'`.`id`)');
    END IF;
    PREPARE diffStmt FROM @diffStmtStr;
    EXECUTE diffStmt;

    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calcUnion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `calcUnion`(in targetSet VARCHAR(255), in sourceSet VARCHAR(255))
BEGIN
    SET @diffStmtStr = CONCAT('INSERT IGNORE INTO `', targetSet, '` SELECT * FROM `',sourceSet,'`');
    PREPARE diffStmt FROM @diffStmtStr;
    EXECUTE diffStmt;
    DEALLOCATE PREPARE diffStmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cleanUpLinCon` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `cleanUpLinCon`()
BEGIN

    DELETE FROM units_lin_con WHERE NOT EXISTS (SELECT '1' FROM double_data WHERE unit_sig=signature_from) AND NOT EXISTS (SELECT '1' FROM integer_data WHERE unit_sig=signature_from);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cleanUpQuery` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `cleanUpQuery`()
BEGIN
    CREATE TEMPORARY TABLE IF NOT EXISTS warnings (warning TEXT NOT NULL);
    SELECT * from warnings;

    SET @pstmtstr = CONCAT('DROP TEMPORARY TABLE IF EXISTS `warnings`',
        IF(@tempTableList IS NULL, '', CONCAT(',',@tempTableList)));
    PREPARE pstmt FROM @pstmtstr;
    EXECUTE pstmt;

    SET @tempTableList = NULL;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `copyTable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `copyTable`(in fromTable VARCHAR(255), in toTable VARCHAR(255))
BEGIN
    SET @copyTableStmtStr = CONCAT('INSERT IGNORE INTO `', toTable, '` (id) SELECT id FROM `', fromTable, '`');
    PREPARE copyTableStmt FROM @copyTableStmtStr;
    EXECUTE copyTableStmt;
    DEALLOCATE PREPARE copyTableStmt;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `createTmpTable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `createTmpTable`(out newTableName VARCHAR(255), in versioned BOOLEAN)
BEGIN
    call registerTempTableName(newTableName);

    IF versioned THEN
        SET @createTableStmtStr = CONCAT('CREATE TEMPORARY TABLE `', newTableName,
            '` ( id INT UNSIGNED, _iversion INT UNSIGNED, PRIMARY KEY (id, _iversion))' );
    ELSE
        SET @createTableStmtStr = CONCAT('CREATE TEMPORARY TABLE `', newTableName,'` ( id INT UNSIGNED PRIMARY KEY)' );
    END IF;

    PREPARE createTableStmt FROM @createTableStmtStr; 
    EXECUTE createTableStmt;
    DEALLOCATE PREPARE createTableStmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `createTmpTable2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `createTmpTable2`(out newTableName VARCHAR(255))
BEGIN
    call registerTempTableName(newTableName);
    SET @createTableStmtStr = CONCAT('CREATE TEMPORARY TABLE `', newTableName,
        '` ( id INT UNSIGNED, id2 INT UNSIGNED, domain INT UNSIGNED, CONSTRAINT `',
        newTableName,'PK` PRIMARY KEY (id,id2,domain) )' );

    PREPARE createTableStmt FROM @createTableStmtStr; 
    EXECUTE createTableStmt;
    DEALLOCATE PREPARE createTableStmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteEntity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `deleteEntity`(in EntityID VARCHAR(255))
BEGIN
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalEntityID from entity_ids WHERE id = EntityID;

    
    DELETE FROM files where file_id=InternalEntityID;

    
    DELETE FROM data_type
        WHERE ( domain_id = 0
            AND entity_id = 0
            AND property_id = InternalEntityID )
        OR datatype = InternalEntityID;
    DELETE FROM collection_type
        WHERE domain_id = 0
        AND entity_id = 0
        AND property_id = InternalEntityID;

    
    DELETE FROM name_data
        WHERE domain_id = 0
        AND entity_id = InternalEntityID
        AND property_id = 20;

    DELETE FROM entity_ids
        WHERE internal_id = InternalEntityID;

    DELETE FROM entities where id=InternalEntityID;

    
    DELETE FROM entity_acl
        WHERE NOT EXISTS (
            SELECT 1 FROM entities
            WHERE entities.acl = entity_acl.id LIMIT 1)
        AND NOT EXISTS (
            SELECT 1 FROM archive_entities
            WHERE archive_entities.acl = entity_acl.id LIMIT 1);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteEntityProperties` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `deleteEntityProperties`(in EntityID VARCHAR(255))
BEGIN
    DECLARE IVersion INT UNSIGNED DEFAULT NULL;
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalEntityID from entity_ids WHERE id = EntityID;

    CALL deleteIsa(InternalEntityID);

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        SELECT max(e._iversion) INTO IVersion 
            FROM entity_version AS e
            WHERE e.entity_id = InternalEntityID;

        
        INSERT INTO archive_reference_data (domain_id, entity_id,
                property_id, value, value_iversion, status, pidx, _iversion)
            SELECT domain_id, entity_id, property_id, value, value_iversion,
                status, pidx, IVersion AS _iversion
            FROM reference_data
            WHERE (domain_id = 0 AND entity_id = InternalEntityID)
            OR domain_id = InternalEntityID;

        INSERT INTO archive_null_data (domain_id, entity_id,
                property_id, status, pidx, _iversion)
            SELECT domain_id, entity_id, property_id, status,
                pidx, IVersion AS _iversion
            FROM null_data
            WHERE (domain_id = 0 AND entity_id = InternalEntityID)
            OR domain_id = InternalEntityID;

        INSERT INTO archive_text_data (domain_id, entity_id,
                property_id, value, status, pidx, _iversion)
            SELECT domain_id, entity_id, property_id, value, status,
                pidx, IVersion AS _iversion
            FROM text_data
            WHERE (domain_id = 0 AND entity_id = InternalEntityID)
            OR domain_id = InternalEntityID;

        INSERT INTO archive_name_data (domain_id, entity_id,
                property_id, value, status, pidx, _iversion)
            SELECT domain_id, entity_id, property_id, value, status,
                pidx, IVersion AS _iversion
            FROM name_data
            WHERE (domain_id = 0 AND entity_id = InternalEntityID)
            OR domain_id = InternalEntityID;

        INSERT INTO archive_enum_data (domain_id, entity_id,
                property_id, value, status, pidx, _iversion)
            SELECT domain_id, entity_id, property_id, value, status,
                pidx, IVersion AS _iversion
            FROM enum_data
            WHERE (domain_id = 0 AND entity_id = InternalEntityID)
            OR domain_id = InternalEntityID;

        INSERT INTO archive_integer_data (domain_id, entity_id,
                property_id, value, status, pidx, _iversion, unit_sig)
            SELECT domain_id, entity_id, property_id, value, status,
                pidx, IVersion AS _iversion, unit_sig
            FROM integer_data
            WHERE (domain_id = 0 AND entity_id = InternalEntityID)
            OR domain_id = InternalEntityID;

        INSERT INTO archive_double_data (domain_id, entity_id,
                property_id, value, status, pidx, _iversion, unit_sig)
            SELECT domain_id, entity_id, property_id, value, status,
                pidx, IVersion AS _iversion, unit_sig
            FROM double_data
            WHERE (domain_id = 0 AND entity_id = InternalEntityID)
            OR domain_id = InternalEntityID;

        INSERT INTO archive_datetime_data (domain_id, entity_id,
                property_id, value, value_ns, status, pidx, _iversion)
            SELECT domain_id, entity_id, property_id, value, value_ns,
                status, pidx, IVersion AS _iversion
            FROM datetime_data
            WHERE (domain_id = 0 AND entity_id = InternalEntityID)
            OR domain_id = InternalEntityID;

        INSERT INTO archive_date_data (domain_id, entity_id,
                property_id, value, status, pidx, _iversion)
            SELECT domain_id, entity_id, property_id, value, status,
                pidx, IVersion AS _iversion
            FROM date_data
            WHERE (domain_id = 0 AND entity_id = InternalEntityID)
            OR domain_id = InternalEntityID;

        INSERT INTO archive_name_overrides (domain_id, entity_id,
                property_id, name, _iversion)
            SELECT domain_id, entity_id, property_id, name,
                IVersion AS _iversion
            FROM name_overrides
            WHERE (domain_id = 0 AND entity_id = InternalEntityID)
            OR domain_id = InternalEntityID;

        INSERT INTO archive_desc_overrides (domain_id, entity_id,
                property_id, description, _iversion)
            SELECT domain_id, entity_id, property_id, description,
                IVersion AS _iversion
            FROM desc_overrides
            WHERE (domain_id = 0 AND entity_id = InternalEntityID)
            OR domain_id = InternalEntityID;

        INSERT INTO archive_data_type (domain_id, entity_id,
                property_id, datatype, _iversion)
            SELECT domain_id, entity_id, property_id, datatype,
                IVersion AS _iversion
            FROM data_type
            WHERE (domain_id = 0 AND entity_id = InternalEntityID)
            OR domain_id = InternalEntityID;

        INSERT INTO archive_collection_type (domain_id, entity_id,
                property_id, collection, _iversion)
            SELECT domain_id, entity_id, property_id, collection,
                IVersion AS _iversion
            FROM collection_type
            WHERE (domain_id = 0 AND entity_id = InternalEntityID)
            OR domain_id = InternalEntityID;

        INSERT INTO archive_query_template_def (id, definition, _iversion)
            SELECT id, definition, IVersion AS _iversion
            FROM query_template_def
            WHERE id = InternalEntityID;

    END IF;

    DELETE FROM reference_data
    where (domain_id=0 AND entity_id=InternalEntityID) OR domain_id=InternalEntityID;
    DELETE FROM null_data
    where (domain_id=0 AND entity_id=InternalEntityID) OR domain_id=InternalEntityID;
    DELETE FROM text_data
    where (domain_id=0 AND entity_id=InternalEntityID) OR domain_id=InternalEntityID;
    DELETE FROM name_data
    where (domain_id=0 AND entity_id=InternalEntityID) OR domain_id=InternalEntityID;
    DELETE FROM enum_data
    where (domain_id=0 AND entity_id=InternalEntityID) OR domain_id=InternalEntityID;
    DELETE FROM integer_data
    where (domain_id=0 AND entity_id=InternalEntityID) OR domain_id=InternalEntityID;
    DELETE FROM double_data
    where (domain_id=0 AND entity_id=InternalEntityID) OR domain_id=InternalEntityID;
    DELETE FROM datetime_data
    where (domain_id=0 AND entity_id=InternalEntityID) OR domain_id=InternalEntityID;
    DELETE FROM date_data
    where (domain_id=0 AND entity_id=InternalEntityID) OR domain_id=InternalEntityID;

    DELETE FROM name_overrides
    WHERE (domain_id=0 AND entity_id=InternalEntityID) OR domain_id=InternalEntityID;
    DELETE FROM desc_overrides
    WHERE (domain_id=0 AND entity_id=InternalEntityID) OR domain_id=InternalEntityID;

    DELETE FROM data_type
    WHERE (domain_id=0 AND entity_id=InternalEntityID) OR domain_id=InternalEntityID;
    DELETE FROM collection_type
    WHERE (domain_id=0 AND entity_id=InternalEntityID) OR domain_id=InternalEntityID;

    DELETE FROM query_template_def WHERE id=InternalEntityID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteIsa` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `deleteIsa`(IN InternalEntityID INT UNSIGNED)
BEGIN
    DECLARE IVersion INT UNSIGNED DEFAULT NULL;

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        SELECT max(_iversion) INTO IVersion
            FROM entity_version
            WHERE entity_id = InternalEntityID;

        
        INSERT IGNORE INTO archive_isa (child, child_iversion, parent, direct)
            SELECT e.child, IVersion AS child_iversion, e.parent, rpath = InternalEntityID
            FROM isa_cache AS e
            WHERE e.child = InternalEntityID;
    END IF;

    DELETE FROM isa_cache
        WHERE child = InternalEntityID
        OR rpath = InternalEntityID
        OR rpath LIKE concat('%>', InternalEntityID)
        OR rpath LIKE concat('%>', InternalEntityID, '>%');

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteLinCon` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `deleteLinCon`(in sig BIGINT)
BEGIN

    DELETE FROM units_lin_con WHERE signature_from=sig;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `entityACL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `entityACL`(out ACLID INT UNSIGNED, in ACLSTR VARBINARY(65525))
BEGIN
   SELECT id INTO ACLID FROM entity_acl as t WHERE t.acl=ACLSTR LIMIT 1;
   IF ACLID IS NULL THEN
        INSERT INTO entity_acl (acl) VALUES (ACLSTR);
        SET ACLID = LAST_INSERT_ID();
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `finishNegationFilter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `finishNegationFilter`(in resultSetTable VARCHAR(255), in diff VARCHAR(255))
BEGIN
    call calcDifference(resultSetTable, diff);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `finishSubProperty` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `finishSubProperty`(in sourceSet VARCHAR(255),in targetSet VARCHAR(255),
                                          in list VARCHAR(255), in versioned BOOLEAN)
BEGIN
    DECLARE data VARCHAR(20000) DEFAULT CONCAT('`',list,'`');
    SET @finishSubPropertyStmtStr = makeStmt(sourceSet, targetSet, data, NULL, versioned);

    PREPARE finishSubPropertyStmt FROM @finishSubPropertyStmtStr;
    EXECUTE finishSubPropertyStmt;
    DEALLOCATE PREPARE finishSubPropertyStmt;

    SELECT @finishSubPropertyStmtStr AS finishSubPropertyStmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getChildren` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `getChildren`(in tableName varchar(255), in versioned BOOLEAN)
BEGIN
    DECLARE found_children INT UNSIGNED DEFAULT 0;

    DROP TEMPORARY TABLE IF EXISTS dependTemp;
    CREATE TEMPORARY TABLE dependTemp (id INT UNSIGNED, _iversion INT UNSIGNED, PRIMARY KEY(id, _iversion));


    SET @initDepend = CONCAT(
        'INSERT IGNORE INTO dependTemp (id, _iversion) SELECT i.child, ',
        IF(versioned,
            '_get_head_iversion(i.child)',
            '0'),
        ' FROM isa_cache AS i INNER JOIN `',
        tableName,
        '` AS t ON (i.parent=t.id);');
    PREPARE initDependStmt FROM @initDepend;

    EXECUTE initDependStmt;
    SET found_children = found_children + ROW_COUNT();

    

    IF versioned IS TRUE THEN
        SET @initDepend = CONCAT(
            'INSERT IGNORE INTO dependTemp (id, _iversion) ',
            'SELECT i.child, i.child_iversion FROM archive_isa AS i INNER JOIN `',
            tableName,
            '` AS t ON (i.parent=t.id);');
        PREPARE initDependStmt FROM @initDepend;

        EXECUTE initDependStmt;
        SET found_children = found_children + ROW_COUNT();
    END IF;

    


    IF found_children != 0 THEN
        SET @transfer = CONCAT(
            'INSERT IGNORE INTO `',
            tableName,
            IF(versioned,
                '` (id, _iversion) SELECT id, _iversion FROM dependTemp',
                '` (id) SELECT id FROM dependTemp'));
        PREPARE transferstmt FROM @transfer;
        EXECUTE transferstmt;
        DEALLOCATE PREPARE transferstmt;
    END IF;


    DEALLOCATE PREPARE initDependStmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getDependentEntities` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `getDependentEntities`(in EntityID VARCHAR(255))
BEGIN

    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    DROP TEMPORARY TABLE IF EXISTS referring;
    CREATE TEMPORARY TABLE referring (
        id INT UNSIGNED UNIQUE
    );

    SELECT internal_id INTO InternalEntityID from entity_ids WHERE id = EntityID;

    INSERT IGNORE INTO referring (id) SELECT entity_id FROM reference_data WHERE (value=InternalEntityID OR property_id=InternalEntityID) AND domain_id=0 AND entity_id!=InternalEntityID;
    INSERT IGNORE INTO referring (id) SELECT domain_id FROM reference_data WHERE (value=InternalEntityID OR property_id=InternalEntityID) AND domain_id!=InternalEntityID AND entity_id!=InternalEntityID AND domain_id!=0;

    INSERT IGNORE INTO referring (id) SELECT entity_id FROM text_data WHERE property_id=InternalEntityID AND domain_id=0 AND entity_id!=InternalEntityID;
    INSERT IGNORE INTO referring (id) SELECT domain_id FROM text_data WHERE property_id=InternalEntityID AND domain_id!=InternalEntityID AND entity_id!=InternalEntityID AND domain_id!=0;

    INSERT IGNORE INTO referring (id) SELECT entity_id FROM enum_data WHERE property_id=InternalEntityID AND domain_id=0 AND entity_id!=InternalEntityID;
    INSERT IGNORE INTO referring (id) SELECT domain_id FROM enum_data WHERE property_id=InternalEntityID AND domain_id!=InternalEntityID AND entity_id!=InternalEntityID AND domain_id!=0;

    INSERT IGNORE INTO referring (id) SELECT entity_id FROM name_data WHERE property_id=InternalEntityID AND domain_id=0 AND entity_id!=InternalEntityID;
    INSERT IGNORE INTO referring (id) SELECT domain_id FROM name_data WHERE property_id=InternalEntityID AND domain_id!=InternalEntityID AND entity_id!=InternalEntityID AND domain_id!=0;

    INSERT IGNORE INTO referring (id) SELECT entity_id FROM integer_data WHERE property_id=InternalEntityID AND domain_id=0 AND entity_id!=InternalEntityID;
    INSERT IGNORE INTO referring (id) SELECT domain_id FROM integer_data WHERE property_id=InternalEntityID AND domain_id!=InternalEntityID AND entity_id!=InternalEntityID AND domain_id!=0;

    INSERT IGNORE INTO referring (id) SELECT entity_id FROM double_data WHERE property_id=InternalEntityID AND domain_id=0 AND entity_id!=InternalEntityID;
    INSERT IGNORE INTO referring (id) SELECT domain_id FROM double_data WHERE property_id=InternalEntityID AND domain_id!=InternalEntityID AND entity_id!=InternalEntityID AND domain_id!=0;

    INSERT IGNORE INTO referring (id) SELECT entity_id FROM datetime_data WHERE property_id=InternalEntityID AND domain_id=0 AND entity_id!=InternalEntityID;
    INSERT IGNORE INTO referring (id) SELECT domain_id FROM datetime_data WHERE property_id=InternalEntityID AND domain_id!=InternalEntityID AND entity_id!=InternalEntityID AND domain_id!=0;

    INSERT IGNORE INTO referring (id) SELECT entity_id FROM date_data WHERE property_id=InternalEntityID AND domain_id=0 AND entity_id!=InternalEntityID;
    INSERT IGNORE INTO referring (id) SELECT domain_id FROM date_data WHERE property_id=InternalEntityID AND domain_id!=InternalEntityID AND entity_id!=InternalEntityID AND domain_id!=0;

    INSERT IGNORE INTO referring (id) SELECT entity_id FROM null_data WHERE property_id=InternalEntityID AND domain_id=0 AND entity_id!=InternalEntityID;
    INSERT IGNORE INTO referring (id) SELECT domain_id FROM null_data WHERE property_id=InternalEntityID AND domain_id!=InternalEntityID AND entity_id!=InternalEntityID AND domain_id!=0;

    INSERT IGNORE INTO referring (id) SELECT entity_id from data_type WHERE datatype=InternalEntityID AND domain_id=0 AND entity_id!=InternalEntityID;
    INSERT IGNORE INTO referring (id) SELECT domain_id from data_type WHERE datatype=InternalEntityID;

    INSERT IGNORE INTO referring (id) SELECT child FROM isa_cache WHERE parent = InternalEntityID AND rpath = child;

    SELECT e.id FROM referring AS r LEFT JOIN entity_ids AS e ON r.id = e.internal_id WHERE r.id!=0 AND e.internal_id!=InternalEntityID;

    DROP TEMPORARY TABLE referring;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getFile` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `getFile`(in FileID INT)
BEGIN 

Select name, description, role into @name, @description, @role from entities where id=FileID LIMIT 1;

IF @role = 'file' Then
		Select path, hash, size into @FilePath, @FileHash, @FileSize from files where file_id=FileID LIMIT 1;
		Select timestamp, user_id, user_agent into @FileCreated, @FileCreator, @FileGenerator from history where entity_id=FileID AND event='insertion' LIMIT 1;

Select 
FileID as FileID,
@FilePath as FilePath,
@FileSize as FileSize,
@FileHash as FileHash,
@FileDescription as FileDescription,
@FileCreated as FileCreated,
@FileCreator as FileCreator,
@FileGenerator as FileGenerator,
NULL	as FileOwner,
NULL as FilePermission,
NULL as FileChecksum;

END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getFileIdByPath` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `getFileIdByPath`(in FilePath TEXT)
BEGIN

    SELECT e.id AS FileID FROM files AS f LEFT JOIN entity_ids ON e.internal_in = f.file_id WHERE f.path=FilePath LIMIT 1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getIdByName` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `getIdByName`(in Name VARCHAR(255), in Role VARCHAR(255), in Lmt INT UNSIGNED)
BEGIN

    SET @stmtStr = "SELECT e.id AS id FROM name_data AS n JOIN entity_ids AS e ON (n.domain_id=0 AND n.property_id=20 AND e.internal_id = n.entity_id) JOIN entities AS i ON (i.id = e.internal_id) WHERE n.value = ?";

    IF Role IS NULL THEN
        SET @stmtStr = CONCAT(@stmtStr, " AND i.role!='ROLE'");
    ELSE
        SET @stmtStr = CONCAT(@stmtStr, " AND i.role='", Role, "'");
    END IF;

    IF Lmt IS NOT NULL THEN
        SET @stmtStr = CONCAT(@stmtStr, " LIMIT ", Lmt);
    END IF;

    SET @vName = Name;
    PREPARE stmt FROM @stmtStr;
    EXECUTE stmt USING @vName;
    DEALLOCATE PREPARE stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getRules` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`caosdb`@`%` PROCEDURE `getRules`(in DomainID INT UNSIGNED, in EntityID INT UNSIGNED, in TransType VARCHAR(255))
BEGIN

		
		
		
SELECT rules.transaction, rules.criterion, rules.modus from rules where if(DomainID is null, rules.domain_id=0,rules.domain_id=DomainID) AND if(EntityID is null, rules.entity_id=0,rules.entity_id=EntityID) AND if(TransType is null,true=true,rules.transaction=TransType);




END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_version_history` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_version_history`(
    in EntityID VARCHAR(255))
BEGIN
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalEntityID FROM entity_ids WHERE id = EntityID;

    
    SELECT c.version AS child,
            NULL as parent,
            t.seconds AS child_seconds,
            t.nanos AS child_nanos,
            t.username AS child_username,
            t.realm AS child_realm
        FROM entity_version AS c INNER JOIN transactions as t
        ON ( c.srid = t.srid )
        WHERE c.entity_id = InternalEntityID
        AND c._ipparent is Null

    
    
    

    
    UNION SELECT c.version AS child,
            p.version AS parent,
            t.seconds AS child_seconds,
            t.nanos AS child_nanos,
            t.username AS child_username,
            t.realm AS child_realm
        FROM entity_version AS p
            INNER JOIN entity_version as c
            INNER JOIN transactions AS t
            ON (c._ipparent = p._iversion
                AND c.entity_id = p.entity_id
                AND t.srid = c.srid)
        WHERE p.entity_id = InternalEntityID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initBackReference` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `initBackReference`(in PropertyID VARCHAR(255), in PropertyName VARCHAR(255), in EntityID VARCHAR(255), in EntityName VARCHAR(255))
BEGIN
    DECLARE propertiesTable VARCHAR(255) DEFAULT NULL;
    DECLARE entitiesTable VARCHAR(255) DEFAULT NULL;

    IF PropertyName IS NOT NULL THEN
        
        call createTmpTable(propertiesTable, FALSE);
        call initSubEntity(PropertyID, PropertyName, propertiesTable);
    END IF;

    IF EntityName IS NOT NULL THEN
        
        call createTmpTable(entitiesTable, FALSE);
        call initSubEntity(EntityID, EntityName, entitiesTable);
    END IF;

    SELECT propertiesTable, entitiesTable;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initDisjunctionFilter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `initDisjunctionFilter`(in versioned BOOLEAN)
BEGIN
    call initEmptyTargetSet(NULL, versioned);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initEmptyTargetSet` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `initEmptyTargetSet`(in targetSet VARCHAR(255), in versioned BOOLEAN)
BEGIN
    DECLARE newTableName VARCHAR(255) DEFAULT targetSet;
    IF targetSet IS NOT NULL THEN
        SET @isNotEmptyVar = NULL; 
        SET @isEmptyStmtStr = CONCAT("SELECT 1 INTO @isNotEmptyVar FROM `",targetSet,"` LIMIT 1");
        PREPARE stmtIsNotEmpty FROM @isEmptyStmtStr;
        EXECUTE stmtIsNotEmpty;
        DEALLOCATE PREPARE stmtIsNotEmpty;
        IF @isNotEmptyVar IS NOT NULL THEN 
            call createTmpTable(newTableName, versioned);
        END IF;
    ELSE
        call createTmpTable(newTableName, versioned);
    END IF;
    SELECT newTableName AS newTableName;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initEntity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `initEntity`(in eid VARCHAR(255), in ename VARCHAR(255),
                                   in enameLike VARCHAR(255), in enameRegexp VARCHAR(255),
                                   in resultset VARCHAR(255), in versioned BOOLEAN)
initEntityLabel: BEGIN
    DECLARE select_columns VARCHAR(255) DEFAULT '` (id) SELECT entity_id FROM name_data ';
    SET @initEntityStmtStr = NULL;

    
    
    IF versioned IS TRUE THEN
        SET select_columns = '` (id, _iversion) SELECT entity_id, _get_head_iversion(entity_id) FROM name_data ';
    END IF;
    IF ename IS NOT NULL THEN
        SET @initEntityStmtStr = CONCAT(
            'INSERT IGNORE INTO `',
            resultset,
            select_columns,
            'WHERE value=?; ');
        SET @query_param = ename;
    ELSEIF enameLike IS NOT NULL THEN
        SET @initEntityStmtStr = CONCAT(
            'INSERT IGNORE INTO `',
            resultset,
            select_columns,
            'WHERE value LIKE ?;');
        SET @query_param = enameLike;
    ELSEIF enameRegexp IS NOT NULL THEN 
        SET @initEntityStmtStr = CONCAT(
            'INSERT IGNORE INTO `',
            resultset,
            select_columns,
            'WHERE value REGEXP ?;');
        SET @query_param = enameRegexp;
    END IF;

    
    IF @initEntityStmtStr IS NOT NULL THEN
        PREPARE initEntityStmt FROM @initEntityStmtStr;
        EXECUTE initEntityStmt USING @query_param;
        DEALLOCATE PREPARE initEntityStmt;
    END IF;

    IF eid IS NOT NULL THEN
        
        SET @initEntityStmtStr = CONCAT(
            'INSERT IGNORE INTO `',
            resultset,
            IF(versioned,
                '` (id, _iversion) SELECT eids.internal_id, _get_head_iversion(eids.internal_id) ',
                '` (id) SELECT eids.internal_id '),
            'FROM entity_ids AS eids WHERE eids.id=',eid,';');
        PREPARE initEntityStmt FROM @initEntityStmtStr;
        EXECUTE initEntityStmt;
        DEALLOCATE PREPARE initEntityStmt;
    END IF;


    
    
    IF versioned IS TRUE THEN
        SET select_columns = '` (id, _iversion) SELECT entity_id, _iversion FROM archive_name_data ';
        IF ename IS NOT NULL THEN
            SET @initEntityStmtStr = CONCAT(
                'INSERT IGNORE INTO `',
                resultset,
                select_columns,
                'WHERE value=?; ');
            SET @query_param = ename;
        ELSEIF enameLike IS NOT NULL THEN
            SET @initEntityStmtStr = CONCAT(
                'INSERT IGNORE INTO `',
                resultset,
                select_columns,
                'WHERE value LIKE ?;');
            SET @query_param = enameLike;
        ELSEIF enameRegexp IS NOT NULL THEN
            SET @initEntityStmtStr = CONCAT(
                'INSERT IGNORE INTO `',
                resultset,
                'WHERE value REGEXP ?;');
            SET @query_param = enameRegexp;
        END IF;

        
        IF @initEntityStmtStr IS NOT NULL THEN
            PREPARE initEntityStmt FROM @initEntityStmtStr;
            EXECUTE initEntityStmt USING @query_param;
            DEALLOCATE PREPARE initEntityStmt;
        END IF;
    END IF;
    


    IF @initEntityStmtStr IS NOT NULL THEN
        call getChildren(resultset, versioned);
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initPOVPropertiesTable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `initPOVPropertiesTable`(in PropertyID VARCHAR(255), in PropertyName VARCHAR(255), in sourceSet VARCHAR(255))
BEGIN
    DECLARE propertiesTable VARCHAR(255) DEFAULT NULL; 
    DECLARE replTbl VARCHAR(255) DEFAULT NULL;
    DECLARE ecount INT DEFAULT 0;
    DECLARE t1 BIGINT DEFAULT 0;
    DECLARE t2 BIGINT DEFAULT 0;
    DECLARE t3 BIGINT DEFAULT 0;
    DECLARE t4 BIGINT DEFAULT 0;
    DECLARE t5 BIGINT DEFAULT 0;
    DECLARE t6 BIGINT DEFAULT 0;


    IF PropertyName is NOT NULL THEN 
        SELECT conv( concat( substring(uid,16,3), substring(uid,10,4), substring(uid,1,8)),16,10) div 10000 - (141427 * 24 * 60 * 60 * 1000) as current_mills INTO t1 from (select uuid() uid) as alias;
        call createTmpTable2(propertiesTable);

        
        
        
        
        
        
        
        
        SET @initPOVPropertiesTableStmt1 = CONCAT('INSERT IGNORE INTO `', propertiesTable, '` (id, id2, domain) SELECT property_id, entity_id, domain_id from name_overrides WHERE name = ? UNION ALL SELECT entity_id, domain_id, 0 FROM name_data WHERE value = ?;');
        PREPARE stmt FROM @initPOVPropertiesTableStmt1;
        SET @PropertyName = PropertyName;
        EXECUTE stmt USING @PropertyName, @PropertyName;
        SET ecount = ROW_COUNT();

        
        SELECT conv( concat( substring(uid,16,3), substring(uid,10,4), substring(uid,1,8)),16,10) div 10000 - (141427 * 24 * 60 * 60 * 1000) as current_mills INTO t2 from (select uuid() uid) as alias;
        IF PropertyID IS NOT NULL THEN
            SET @initPOVPropertiesTableStmt2 = CONCAT('INSERT IGNORE INTO `', propertiesTable, '` (id, id2, domain) VALUES (?, 0, 0)');
            PREPARE stmt FROM @initPOVPropertiesTableStmt2;
            SET @PropertyID = PropertyID;
            EXECUTE stmt USING @PropertyID;
            SET ecount = ecount + ROW_COUNT();
        END IF;

        
        SELECT conv( concat( substring(uid,16,3), substring(uid,10,4), substring(uid,1,8)),16,10) div 10000 - (141427 * 24 * 60 * 60 * 1000) as current_mills INTO t3 from (select uuid() uid) as alias;
        IF ecount > 0 THEN
            
            call getChildren(propertiesTable, False);
        END IF;

        
        SELECT conv( concat( substring(uid,16,3), substring(uid,10,4), substring(uid,1,8)),16,10) div 10000 - (141427 * 24 * 60 * 60 * 1000) as current_mills INTO t4 from (select uuid() uid) as alias;
        IF ecount > 0 THEN
            call createTmpTable2(replTbl);
            SET @replTblStmt1 := CONCAT('INSERT IGNORE INTO `',replTbl, '` (id, id2, domain) SELECT r.value as id, r.entity_id as id2, 0 as domain_id FROM reference_data AS r WHERE status="REPLACEMENT" AND domain_id=0 AND EXISTS (SELECT * FROM `', sourceSet, '` AS s WHERE s.id=r.entity_id) AND EXISTS (SELECT * FROM `', propertiesTable, '` AS p WHERE p.domain = 0 AND p.id2=0 AND p.id=r.property_id);');
            PREPARE replStmt1 FROM @replTblStmt1;
            EXECUTE replStmt1;
            DEALLOCATE PREPARE replStmt1;
            SELECT conv( concat( substring(uid,16,3), substring(uid,10,4), substring(uid,1,8)),16,10) div 10000 - (141427 * 24 * 60 * 60 * 1000) as current_mills INTO t5 from (select uuid() uid) as alias;

            SET @replTblStmt2 := CONCAT('INSERT IGNORE INTO `', propertiesTable, '` SELECT id, id2, domain FROM `', replTbl, '`;');
            PREPARE replStmt2 FROM @replTblStmt2;
            EXECUTE replStmt2;
            DEALLOCATE PREPARE replStmt2;
            SELECT conv( concat( substring(uid,16,3), substring(uid,10,4), substring(uid,1,8)),16,10) div 10000 - (141427 * 24 * 60 * 60 * 1000) as current_mills INTO t6 from (select uuid() uid) as alias;
        END IF;
    END IF;
    SELECT propertiesTable, t1, t2, t3, t4, t5, t6, @initPOVPropertiesTableStmt1 as initPOVPropertiesTableStmt1, @initPOVPropertiesTableStmt2 as initPOVPropertiesTableStmt2, @replTblStmt1 as replTblStmt1, @replTblStmt2 as replTblStmt2;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initPOVRefidsTable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `initPOVRefidsTable`(in PropertyID VARCHAR(255), in PropertyName VARCHAR(255))
BEGIN
    DECLARE refIdsTable VARCHAR(255) DEFAULT NULL; 

    
    IF PropertyName IS NOT NULL THEN
        
        call createTmpTable(refIdsTable, FALSE);
        call initSubEntity(PropertyID, PropertyName, refIdsTable);
        
    END IF;
    SELECT refIdsTable;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initQuery` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `initQuery`(in versioned BOOLEAN)
BEGIN
    CREATE TEMPORARY TABLE IF NOT EXISTS warnings (warning TEXT NOT NULL);

    call createTmpTable(@resultSet, versioned);
    SELECT @resultSet as tablename;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initSubEntity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `initSubEntity`(in EntityID VARCHAR(255), in ename VARCHAR(255), in tableName VARCHAR(255))
BEGIN
    DECLARE ecount INT DEFAULT 0;
    DECLARE op VARCHAR(255) DEFAULT '=';


    IF LOCATE("%", ename) > 0 THEN
        SET op = "LIKE";
    END IF;

    SET @stmtStr = CONCAT('INSERT IGNORE INTO `',
        tableName,
        '` (id) SELECT entity_id FROM name_data WHERE value ',
        op,
        ' ? AND domain_id=0;');

    PREPARE stmt FROM @stmtStr;
    SET @ename = ename;
    EXECUTE stmt USING @ename;
    SET ecount = ROW_COUNT();
    DEALLOCATE PREPARE stmt;

    IF EntityID IS NOT NULL THEN
        SET @stmtStr = CONCAT('INSERT IGNORE INTO `', tableName, '` (id) SELECT internal_id FROM entity_ids WHERE id = ?');
        PREPARE stmt FROM @stmtStr;
        SET @eid = EntityID;
        EXECUTE stmt USING @eid;
        SET ecount = ecount + ROW_COUNT();
        DEALLOCATE PREPARE stmt;
    END IF;

    IF ecount > 0 THEN
        
        call getChildren(tableName, False);
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `initSubProperty` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `initSubProperty`(in sourceSet VARCHAR(255), in propertiesTable VARCHAR(255), in refIdsTable VARCHAR(255))
BEGIN
DECLARE newTableName VARCHAR(255) DEFAULT NULL;
    call registerTempTableName(newTableName);	
    
    SET @createSubPropertyListTableStr = CONCAT('CREATE TEMPORARY TABLE `', newTableName,'` ( entity_id INT UNSIGNED NOT NULL, id INT UNSIGNED NOT NULL, domain INT UNSIGNED NOT NULL, CONSTRAINT `',newTableName,'PK` PRIMARY KEY (entity_id, id, domain)) ' );
    
    PREPARE createSubPropertyListTable FROM @createSubPropertyListTableStr; 
    EXECUTE createSubPropertyListTable;
    DEALLOCATE PREPARE createSubPropertyListTable;

	SET @subResultSetStmtStr = CONCAT('INSERT IGNORE INTO `', newTableName, '` (domain, entity_id, id) 
            SELECT data1.domain_id as domain, data1.entity_id as entity_id, data1.value as id 
                FROM reference_data as data1 JOIN reference_data as data2 
                    ON (data1.domain_id=0 
                        AND data1.domain_id=data2.domain_id 
                        AND data2.entity_id=data1.entity_id 
                        AND (
                            (data1.property_id=data2.value AND data2.status="REPLACEMENT")
                            OR
                            (data1.property_id!=data2.value AND data2.status!="REPLACEMENT" AND data1.status!="REPLACEMENT" AND data1.property_id=data2.property_id)
                        )
                        AND EXISTS (SELECT 1 FROM `', sourceSet, '` as source WHERE source.id=data1.entity_id LIMIT 1)',
                        IF(propertiesTable IS NULL, '', CONCAT(' AND EXISTS (SELECT 1 FROM `', propertiesTable, '` as props WHERE props.id=data2.property_id LIMIT 1)')),
                        IF(refIdsTable IS NULL, '', CONCAT(' AND EXISTS (SELECT 1 FROM `', refIdsTable, '` as refs WHERE refs.id=data1.value LIMIT 1)')),	
		')'
        );


	PREPARE subResultSetStmt FROM @subResultSetStmtStr;
	EXECUTE subResultSetStmt;
    DEALLOCATE PREPARE subResultSetStmt;

	SELECT newTableName as list;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertEntity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `insertEntity`(in EntityID VARCHAR(255), in EntityName VARCHAR(255), in EntityDesc TEXT, in EntityRole VARCHAR(255), in ACL VARBINARY(65525))
BEGIN
    DECLARE NewACLID INT UNSIGNED DEFAULT NULL;
    DECLARE Hash VARBINARY(255) DEFAULT NULL;
    DECLARE Version VARBINARY(255) DEFAULT NULL;
    DECLARE Transaction VARBINARY(255) DEFAULT NULL;
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    
    
    call entityACL(NewACLID, ACL);

    
    INSERT INTO entities (description, role, acl)
        VALUES (EntityDesc, EntityRole, NewACLID);

    
    SET InternalEntityID = LAST_INSERT_ID();

    INSERT INTO entity_ids (internal_id, id) VALUES (InternalEntityID, EntityID);

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        
        SET Transaction = @SRID;
        SET Version = SHA1(UUID());
        CALL insert_single_child_version(InternalEntityID, Hash, Version, Null, Transaction);
    END IF;

    
    
    IF EntityName IS NOT NULL THEN
        INSERT INTO name_data
            (domain_id, entity_id, property_id, value, status, pidx)
            VALUES (0, InternalEntityID, 20, EntityName, "FIX", 0);
    END IF;

    SELECT Version as Version;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertEntityCollection` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `insertEntityCollection`(in PropertyID VARCHAR(255), in Collection VARCHAR(255))
BEGIN
    DECLARE InternalPropertyID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalPropertyID FROM entity_ids WHERE id=PropertyID;

    INSERT INTO collection_type (domain_id, entity_id, property_id, collection) SELECT 0, 0, InternalPropertyID, Collection;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertEntityDataType` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `insertEntityDataType`(in PropertyID VARCHAR(255), in DataTypeID VARCHAR(255))
BEGIN
    DECLARE InternalPropertyID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalPropertyID FROM entity_ids WHERE id=PropertyID;

    INSERT INTO data_type (domain_id, entity_id, property_id, datatype) SELECT 0, 0, InternalPropertyID, ( SELECT internal_id FROM entity_ids WHERE id = DataTypeID);


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertEntityProperty` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `insertEntityProperty`(
    in DomainID VARCHAR(255),
    in EntityID VARCHAR(255),
    in PropertyID VARCHAR(255),
    in Datatable VARCHAR(255),
    in PropertyValue TEXT,
    in PropertyUnitSig BIGINT,
    in PropertyStatus VARCHAR(255),
    in NameOverride VARCHAR(255),
    in DescOverride TEXT,
    in DatatypeOverride VARCHAR(255),
    in Collection VARCHAR(255),
    in PropertyIndex INT UNSIGNED)
BEGIN
    DECLARE ReferenceValueIVersion INT UNSIGNED DEFAULT NULL;
    DECLARE ReferenceValue INT UNSIGNED DEFAULT NULL;
    DECLARE AT_PRESENT INTEGER DEFAULT NULL;
    DECLARE InternalDataTypeID INT UNSIGNED DEFAULT NULL;
    DECLARE InternalPropertyID INT UNSIGNED DEFAULT NULL;
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;
    DECLARE InternalDomainID INT UNSIGNED DEFAULT 0;

    SELECT internal_id INTO InternalDomainID FROM entity_ids WHERE id = DomainID;
    
    
    
    IF LOCATE("$", EntityID) = 1 THEN
        SET InternalEntityID=SUBSTRING(EntityID, 2);
    ELSE
        SELECT internal_id INTO InternalEntityID FROM entity_ids WHERE id = EntityID;
    END IF;
    IF LOCATE("$", PropertyID) = 1 THEN
        SET InternalPropertyID=SUBSTRING(PropertyID, 2);
    ELSE
        SELECT internal_id INTO InternalPropertyID FROM entity_ids WHERE id = PropertyID;
    END IF;

    CASE Datatable
    WHEN 'double_data' THEN
        INSERT INTO double_data
        (domain_id, entity_id, property_id, value, unit_sig, status, pidx)
        VALUES
        (InternalDomainID, InternalEntityID, InternalPropertyID, PropertyValue, PropertyUnitSig, PropertyStatus, PropertyIndex);
    WHEN 'integer_data' THEN
        INSERT INTO integer_data
        (domain_id, entity_id, property_id, value, unit_sig, status, pidx)
        VALUES
        (InternalDomainID, InternalEntityID, InternalPropertyID, PropertyValue, PropertyUnitSig, PropertyStatus, PropertyIndex);
    WHEN 'datetime_data' THEN
        INSERT INTO datetime_data
        (domain_id, entity_id, property_id, value, value_ns, status, pidx)
        VALUES
        (InternalDomainID, InternalEntityID, InternalPropertyID, SUBSTRING_INDEX(PropertyValue, 'UTC', 1), IF(SUBSTRING_INDEX(PropertyValue, 'UTC', -1)='',NULL,SUBSTRING_INDEX(PropertyValue, 'UTC', -1)), PropertyStatus, PropertyIndex);
    WHEN 'reference_data' THEN

        
        SET AT_PRESENT=LOCATE("@", PropertyValue);
        IF is_feature_config("ENTITY_VERSIONING", "ENABLED") AND AT_PRESENT > 0 THEN
            SELECT internal_id INTO ReferenceValue FROM entity_ids WHERE id = SUBSTRING_INDEX(PropertyValue, '@', 1);
            SET ReferenceValueIVersion = get_iversion(ReferenceValue,
                SUBSTRING_INDEX(PropertyValue, '@', -1));
            IF ReferenceValueIVersion IS NULL THEN
                
                SELECT 0 from `ReferenceValueIVersion_WAS_NULL`;
            END IF;

        ELSEIF LOCATE("$", PropertyValue) = 1 THEN
            SET ReferenceValue = SUBSTRING(PropertyValue, 2);
        ELSE
            SELECT internal_id INTO ReferenceValue FROM entity_ids WHERE id = PropertyValue;
        END IF;


        INSERT INTO reference_data
            (domain_id, entity_id, property_id, value, value_iversion, status,
                pidx)
        VALUES
            (InternalDomainID, InternalEntityID, InternalPropertyID, ReferenceValue,
                ReferenceValueIVersion, PropertyStatus, PropertyIndex);
    WHEN 'enum_data' THEN
        INSERT INTO enum_data
        (domain_id, entity_id, property_id, value, status, pidx)
        VALUES
        (InternalDomainID, InternalEntityID, InternalPropertyID, PropertyValue, PropertyStatus, PropertyIndex);
    WHEN 'date_data' THEN
        INSERT INTO date_data
        (domain_id, entity_id, property_id, value, status, pidx)
        VALUES
        (InternalDomainID, InternalEntityID, InternalPropertyID, SUBSTRING_INDEX(PropertyValue, '.', 1), PropertyStatus, PropertyIndex);
    WHEN 'text_data' THEN
        INSERT INTO text_data
        (domain_id, entity_id, property_id, value, status, pidx)
        VALUES
        (InternalDomainID, InternalEntityID, InternalPropertyID, PropertyValue, PropertyStatus, PropertyIndex);
    WHEN 'null_data' THEN
        INSERT INTO null_data
        (domain_id, entity_id, property_id, status, pidx)
        VALUES
        (InternalDomainID, InternalEntityID, InternalPropertyID, PropertyStatus, PropertyIndex);
    WHEN 'name_data' THEN
        INSERT INTO name_data
        (domain_id, entity_id, property_id, value, status, pidx)
        VALUES
        (InternalDomainID, InternalEntityID, InternalPropertyID, PropertyValue, PropertyStatus, PropertyIndex);

    ELSE
        
        SELECT * FROM table_does_not_exist;
    END CASE;

    IF DatatypeOverride IS NOT NULL THEN
        SELECT internal_id INTO InternalDataTypeID from entity_ids WHERE id = DatatypeOverride;
        call overrideType(InternalDomainID, InternalEntityID, InternalPropertyID, InternalDataTypeID);
        IF Collection IS NOT NULL THEN
            INSERT INTO collection_type (domain_id, entity_id, property_id, collection) VALUES (InternalDomainID, InternalEntityID, InternalPropertyID, Collection);
        END IF;
    END IF;

    IF NameOverride IS NOT NULL THEN
        call overrideName(InternalDomainID, InternalEntityID, InternalPropertyID, NameOverride);
    END IF;

    IF DescOverride IS NOT NULL THEN
        call overrideDesc(InternalDomainID, InternalEntityID, InternalPropertyID, DescOverride);
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertIsa` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `insertIsa`(IN ChildID VARCHAR(255), IN ParentID VARCHAR(255))
insert_is_a_proc: BEGIN

    DECLARE c INT UNSIGNED DEFAULT NULL;
    DECLARE p INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO c FROM entity_ids WHERE id = ChildID;
    SELECT internal_id INTO p FROM entity_ids WHERE id = ParentID;

    INSERT INTO isa_cache (child, parent, rpath) VALUES (c, p, c);

    IF p = c THEN
        
        LEAVE insert_is_a_proc;
    END IF;
    

    
    
    
    INSERT IGNORE INTO isa_cache SELECT
        c
            AS child,   
        i.parent
            AS parent,  
        IF(p=i.rpath or i.rpath=parent,  
           p,                            
           concat(p, ">", i.rpath))      
            AS rpath
        FROM isa_cache AS i WHERE i.child = p AND i.child != i.parent;  

    
    
    INSERT IGNORE INTO isa_cache SELECT
        l.child,    
        r.parent,   
        IF(l.rpath=l.child AND r.rpath=c,  
           c,                              
           concat(IF(l.rpath=l.child,        
                     c,                         
                     concat(l.rpath, '>', c)),  
                  IF(r.rpath=c,              
                     '',                        
                     concat('>', r.rpath))))    
            AS rpath
        FROM
            isa_cache AS l INNER JOIN isa_cache AS r
            ON (l.parent = c AND c = r.child AND l.child != l.parent); 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertLinCon` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `insertLinCon`(in signature_from BIGINT, in signature_to BIGINT, in a DECIMAL(65,30), in b_dividend BIGINT, in b_divisor BIGINT, in c DECIMAL(65,30))
BEGIN

    INSERT IGNORE INTO units_lin_con (signature_from, signature_to, a, b_dividend, b_divisor, c) VALUES (signature_from, signature_to, a, b_dividend, b_divisor, c);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_single_child_version` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `insert_single_child_version`(
    in InternalEntityID INT UNSIGNED,
    in Hash VARBINARY(255),
    in Version VARBINARY(255),
    in Parent VARBINARY(255),
    in Transaction VARBINARY(255))
BEGIN
    DECLARE newiversion INT UNSIGNED DEFAULT NULL;
    DECLARE newipparent INT UNSIGNED DEFAULT NULL;

    
    IF Parent IS NOT NULL THEN
        SELECT e._iversion INTO newipparent
            FROM entity_version AS e
            WHERE e.entity_id = InternalEntityID
            AND e.version = Parent;
        IF newipparent IS NULL THEN
            
            SELECT concat("This parent does not exists: ", Parent)
            FROM parent_version_does_not_exist;
        END IF;
    END IF;


    
    SELECT max(e._iversion)+1 INTO newiversion
        FROM entity_version AS e
        WHERE e.entity_id=InternalEntityID;
    IF newiversion IS NULL THEN
        SET newiversion = 1;
    END IF;

    INSERT INTO entity_version
        (entity_id, hash, version, _iversion, _ipparent, srid)
        VALUES
        (InternalEntityID, Hash, Version, newiversion, newipparent, Transaction);



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `intersectTable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `intersectTable`(in resultSetTable VARCHAR(255), in diff VARCHAR(255))
BEGIN
    SET @diffStmtStr = CONCAT('DELETE FROM `', resultSetTable, '` WHERE id NOT IN ( SELECT id FROM `', diff,'`)');
    PREPARE diffStmt FROM @diffStmtStr;
    EXECUTE diffStmt;

    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `isSubtype` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `isSubtype`(in ChildID VARCHAR(255), in ParentID VARCHAR(255))
BEGIN
    DECLARE c INT UNSIGNED DEFAULT NULL;
    DECLARE p INT UNSIGNED DEFAULT NULL;
    DECLARE ret BOOLEAN DEFAULT FALSE;

    SELECT internal_id INTO c from entity_ids WHERE id = ChildID;
    SELECT internal_id INTO p from entity_ids WHERE id = ParentID;

    SELECT TRUE INTO ret FROM isa_cache AS i WHERE i.child=c AND i.parent=p LIMIT 1;
    SELECT ret as ISA;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `overrideDesc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `overrideDesc`(in InternalDomainID INT UNSIGNED, in InternalEntityID INT UNSIGNED, in InternalPropertyID INT UNSIGNED, in Description TEXT)
BEGIN
    INSERT INTO desc_overrides (domain_id, entity_id, property_id, description) VALUES (InternalDomainID, InternalEntityID, InternalPropertyID, Description);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `overrideName` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `overrideName`(in InternalDomainID INT UNSIGNED, in InternalEntityID INT UNSIGNED, in InternalPropertyID INT UNSIGNED, in Name VARCHAR(255))
BEGIN
    INSERT INTO name_overrides (domain_id, entity_id, property_id, name) VALUES (InternalDomainID, InternalEntityID, InternalPropertyID, Name);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `overrideType` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `overrideType`(in InternalDomainID INT UNSIGNED, in InternalEntityID INT UNSIGNED, in InternalPropertyID INT UNSIGNED, in InternalDataTypeID INT UNSIGNED)
BEGIN
    INSERT INTO data_type (domain_id, entity_id, property_id, datatype) VALUES (InternalDomainID, InternalEntityID, InternalPropertyID, InternalDataTypeID);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `raiseWarning` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `raiseWarning`(in str VARCHAR(20000))
BEGIN
    INSERT INTO warnings VALUES (str);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registerReplacementIds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `registerReplacementIds`(in amount INT UNSIGNED)
BEGIN
    DECLARE ED INTEGER DEFAULT NULL;

    SELECT COUNT(id) INTO ED FROM entities WHERE Role='_REPLACEMENT' AND id!=0;

    WHILE ED < amount DO
        INSERT INTO entities (description, role, acl) VALUES
            (NULL, '_REPLACEMENT', 0);

        SET ED = ED + 1;
    END WHILE;

    SELECT CONCAT("$", e.id) as ReplacementID FROM entities AS e WHERE e.Role='_REPLACEMENT' and e.id!=0;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registerTempTableName` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `registerTempTableName`(out newTableName VARCHAR(255))
BEGIN
    SET newTableName = md5(CONCAT(RAND(),CURRENT_TIMESTAMP()));
    SET @tempTableList = IF(@tempTableList IS NULL,
        CONCAT('`',newTableName,'`'),
        CONCAT(@tempTableList, ',`', newTableName, '`')
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `retrieveEntity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `retrieveEntity`(
    in EntityID VARCHAR(255),
    in Version VARBINARY(255))
retrieveEntityBody: BEGIN
    DECLARE FilePath VARCHAR(255) DEFAULT NULL;
    DECLARE FileSize VARCHAR(255) DEFAULT NULL;
    DECLARE FileHash VARCHAR(255) DEFAULT NULL;
    DECLARE DatatypeID VARCHAR(255) DEFAULT NULL;
    DECLARE InternalDatatypeID INT UNSIGNED DEFAULT NULL;
    DECLARE CollectionName VARCHAR(255) DEFAULT NULL;
    DECLARE IsHead BOOLEAN DEFAULT TRUE;
    DECLARE IVersion INT UNSIGNED DEFAULT NULL;
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalEntityID from entity_ids WHERE id = EntityID;

    IF InternalEntityID IS NULL THEN
        
        SELECT 0 FROM entities WHERE 0 = 1;
        LEAVE retrieveEntityBody;
    END IF;

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        
        IF Version IS NULL OR UPPER(Version) = "HEAD" THEN
            SET Version = get_head_version(EntityID);
        ELSEIF UPPER(LEFT(Version, 5)) = "HEAD~" THEN
            SET IsHead = FALSE;
            SET Version = get_head_relative(EntityID, SUBSTR(Version, 6));
        ELSE
            SELECT get_head_version(EntityID) = Version INTO IsHead;
        END IF;

        IF IsHead IS FALSE THEN
            SET IVersion=get_iversion(InternalEntityID, Version);

            IF IVersion IS NULL THEN
                
                SELECT 0 FROM entities WHERE 0 = 1;
                LEAVE retrieveEntityBody;
            END IF;

            SELECT path, size, HEX(hash)
                INTO FilePath, FileSize, FileHash
                FROM archive_files
                WHERE file_id = InternalEntityID
                AND _iversion = IVersion
                LIMIT 1;

            SELECT datatype
                INTO InternalDatatypeID
                FROM archive_data_type
                WHERE domain_id = 0
                AND entity_id = 0
                AND property_id = InternalEntityID
                AND _iversion = IVersion
                LIMIT 1;

            SELECT collection
                INTO CollectionName
                FROM archive_collection_type
                WHERE domain_id = 0
                AND entity_id = 0
                AND property_id = InternalEntityID
                AND _iversion = IVersion
                LIMIT 1;

            
            SELECT
                ( SELECT value FROM
                    ( SELECT value FROM name_data
                        WHERE domain_id = 0
                        AND entity_id = InternalDatatypeID
                        AND property_id = 20
                    ) AS tmp LIMIT 1 ) AS DatatypeName,
                ( SELECT id FROM entity_ids WHERE internal_id=InternalDatatypeID ) AS DatatypeID,
                CollectionName AS Collection,
                EntityID AS EntityID,
                ( SELECT value FROM archive_name_data
                    WHERE domain_id = 0
                    AND entity_ID = InternalEntityID
                    AND property_id = 20
                    AND _iversion = IVersion
                    ) AS EntityName,
                e.description AS EntityDesc,
                e.role AS EntityRole,
                FileSize AS FileSize,
                FilePath AS FilePath,
                FileHash AS FileHash,
                (SELECT acl FROM entity_acl AS a WHERE a.id = e.acl) AS ACL,
                Version AS Version
            FROM archive_entities AS e
            WHERE e.id = InternalEntityID
            AND e._iversion = IVersion
            LIMIT 1;

            
            LEAVE retrieveEntityBody;

        END IF;
    END IF;

    SELECT path, size, hex(hash)
        INTO FilePath, FileSize, FileHash
        FROM files
        WHERE file_id = InternalEntityID
        LIMIT 1;

    SELECT dt.datatype INTO InternalDatatypeID
        FROM data_type as dt
        WHERE dt.domain_id=0
        AND dt.entity_id=0
        AND dt.property_id=InternalEntityID
        LIMIT 1;

    SELECT collection INTO CollectionName
        FROM collection_type
        WHERE domain_id=0
        AND entity_id=0
        AND property_id=InternalEntityID
        LIMIT 1;

    SELECT
        ( SELECT value FROM name_data
            WHERE domain_id = 0
            AND entity_id = InternalDatatypeID
            AND property_id = 20 LIMIT 1 ) AS DatatypeName,
        ( SELECT id FROM entity_ids WHERE internal_id=InternalDatatypeID ) AS DatatypeID,
        CollectionName AS Collection,
        EntityID AS EntityID,
        ( SELECT value FROM name_data
            WHERE domain_id = 0
            AND entity_ID = InternalEntityID
            AND property_id = 20 LIMIT 1) AS EntityName,
        e.description AS EntityDesc,
        e.role AS EntityRole,
        FileSize AS FileSize,
        FilePath AS FilePath,
        FileHash AS FileHash,
        (SELECT acl FROM entity_acl AS a WHERE a.id = e.acl) AS ACL,
        Version AS Version
    FROM entities e WHERE id = InternalEntityID LIMIT 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `retrieveEntityParents` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `retrieveEntityParents`(
    in EntityID VARCHAR(255),
    in Version VARBINARY(255))
retrieveEntityParentsBody: BEGIN

    DECLARE IVersion INT UNSIGNED DEFAULT NULL;
    DECLARE IsHead BOOLEAN DEFAULT TRUE;
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalEntityID from entity_ids WHERE id = EntityID;

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        IF Version IS NOT NULL THEN
            SELECT get_head_version(EntityID) = Version INTO IsHead;
        END IF;

        IF IsHead IS FALSE THEN
            SELECT e._iversion INTO IVersion
                FROM entity_version as e
                WHERE e.entity_id = InternalEntityID
                AND e.version = Version;

            IF IVersion IS NULL THEN
                
                LEAVE retrieveEntityParentsBody;
            END IF;

            SELECT
                ( SELECT id FROM entity_ids WHERE internal_id = i.parent) AS ParentID,
                ( SELECT value FROM name_data
                    WHERE domain_id = 0
                    AND entity_id = i.parent
                    AND property_id = 20
                ) AS ParentName, 
                                 
                                 
                                 
                                 
                e.description AS ParentDescription,
                e.role AS ParentRole,
                (SELECT acl FROM entity_acl AS a WHERE a.id = e.acl) AS ACL
                FROM archive_isa AS i JOIN entities AS e
                    ON (i.parent = e.id)
                WHERE i.child = InternalEntityID
                AND i.child_iversion = IVersion
                AND i.direct IS TRUE
                ;

            LEAVE retrieveEntityParentsBody;
        END IF;
    END IF;

    SELECT
        ( SELECT id FROM entity_ids WHERE internal_id = i.parent) AS ParentID,
        ( SELECT value FROM name_data
            WHERE domain_id = 0
            AND entity_id = i.parent
            AND property_id = 20 ) AS ParentName,
        e.description AS ParentDescription,
        e.role AS ParentRole,
        (SELECT acl FROM entity_acl AS a WHERE a.id = e.acl) AS ACL
        FROM isa_cache AS i JOIN entities AS e
            ON (i.parent = e.id)
        WHERE i.child = InternalEntityID
        AND i.rpath = InternalEntityID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `retrieveEntityProperties` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `retrieveEntityProperties`(
    in DomainID VARCHAR(255),
    in EntityID VARCHAR(255),
    in Version VARBINARY(255))
retrieveEntityPropertiesBody: BEGIN

    DECLARE IVersion INT UNSIGNED DEFAULT NULL;
    DECLARE IsHead BOOLEAN DEFAULT TRUE;
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;
    DECLARE InternalDomainID INT UNSIGNED DEFAULT 0;

    
    
    
    IF LOCATE("$", EntityID) = 1 THEN
        SET InternalEntityID=SUBSTRING(EntityID, 2);
    ELSE
        SELECT internal_id INTO InternalEntityID FROM entity_ids WHERE id = EntityID;
    END IF;
    
    SELECT internal_id INTO InternalDomainID from entity_ids WHERE id = DomainID;

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        IF Version IS NOT NULL THEN
            IF InternalDomainID = 0 THEN
                SELECT get_head_version(EntityID) = Version INTO IsHead;
            ELSE
                SELECT get_head_version(DomainID) = Version INTO IsHead;
            END IF;
        END IF;

        IF IsHead IS FALSE THEN
            SELECT e._iversion INTO IVersion
                FROM entity_version as e
                WHERE ((e.entity_id = InternalEntityID AND InternalDomainID = 0)
                    OR (e.entity_id = InternalDomainID))
                AND e.version = Version;

            IF IVersion IS NULL THEN
                
                LEAVE retrieveEntityPropertiesBody;
            END IF;

            
            SELECT
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
                value AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_double_data
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
                value AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_integer_data
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
                CONCAT(value, '.NULL.NULL') AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_date_data
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
                CONCAT(value, 'UTC', IF(value_ns IS NULL, '', value_ns))
                    AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_datetime_data
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
                value AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_text_data
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
                value AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_enum_data
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
                IF(value_iversion IS NULL,
                    IF(status = "REPLACEMENT",
                        CONCAT("$", value),
                        ( SELECT id FROM entity_ids WHERE internal_id = value )),
                    
                    CONCAT(
                        ( SELECT id FROM entity_ids WHERE internal_id = value ),
                          "@", _get_version(value, value_iversion)))
                    AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_reference_data
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
                NULL AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_null_data
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
                value AS PropertyValue,
                status AS PropertyStatus,
                pidx AS PropertyIndex
            FROM archive_name_data
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND property_id != 20
            AND _iversion = IVersion;

            LEAVE retrieveEntityPropertiesBody;
        END IF;
    END IF;

    
    SELECT
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
        value AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM double_data
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID

    UNION ALL

    
    SELECT
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
        value AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM integer_data
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID

    UNION ALL

    
    SELECT
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
        CONCAT(value, '.NULL.NULL') AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM date_data
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID

    UNION ALL

    
    SELECT
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
        CONCAT(value, 'UTC', IF(value_ns IS NULL, '', value_ns))
            AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM datetime_data
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID

    UNION ALL

    
    SELECT
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
        value AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM text_data
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID

    UNION ALL

    
    SELECT
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
        value AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM enum_data
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID

    UNION ALL

    
    SELECT
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
        IF(value_iversion IS NULL,
            IF(status = "REPLACEMENT",
                CONCAT("$", value),
                ( SELECT id FROM entity_ids WHERE internal_id = value )),
            
            CONCAT(
                ( SELECT id FROM entity_ids WHERE internal_id = value ),
                  "@", _get_version(value, value_iversion)))
            AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM reference_data
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID

    UNION ALL

    
    SELECT
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
        NULL AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM null_data
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID

    UNION ALL

    
    SELECT
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS PropertyID,
        value AS PropertyValue,
        status AS PropertyStatus,
        pidx AS PropertyIndex
    FROM name_data
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID
    AND property_id != 20;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `retrieveOverrides` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `retrieveOverrides`(
    in DomainID VARCHAR(255),
    in EntityID VARCHAR(255),
    in Version VARBINARY(255))
retrieveOverridesBody: BEGIN

    DECLARE IVersion INT UNSIGNED DEFAULT NULL;
    DECLARE IsHead BOOLEAN DEFAULT TRUE;
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;
    DECLARE InternalDomainID INT UNSIGNED DEFAULT 0;

    
    
    
    IF LOCATE("$", EntityID) = 1 THEN
        SET InternalEntityID=SUBSTRING(EntityID, 2);
    ELSE
        SELECT internal_id INTO InternalEntityID FROM entity_ids WHERE id = EntityID;
    END IF;
    
    SELECT internal_id INTO InternalDomainID from entity_ids WHERE id = DomainID;

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        IF Version IS NOT NULL THEN
            IF InternalDomainID = 0 THEN
                SELECT get_head_version(EntityID) = Version INTO IsHead;
            ELSE
                SELECT get_head_version(DomainID) = Version INTO IsHead;
            END IF;
        END IF;

        IF IsHead IS FALSE THEN
            SELECT e._iversion INTO IVersion
                FROM entity_version as e
                WHERE ((e.entity_id = InternalEntityID AND InternalDomainID = 0)
                    OR (e.entity_id = InternalDomainID))
                AND e.version = Version;

            IF IVersion IS NULL THEN
                
                LEAVE retrieveOverridesBody;
            END IF;

            
            SELECT
                NULL AS collection_override,
                name AS name_override,
                NULL AS desc_override,
                NULL AS type_name_override,
                NULL AS type_id_override,
                EntityID AS entity_id,
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
            FROM archive_name_overrides
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                NULL AS collection_override,
                NULL AS name_override,
                description AS desc_override,
                NULL AS type_name_override,
                NULL AS type_id_override,
                EntityID AS entity_id,
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
            FROM archive_desc_overrides
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                NULL AS collection_override,
                NULL AS name_override,
                NULL AS desc_override,
                (SELECT value FROM name_data
                    WHERE domain_id = 0
                    AND entity_id = datatype
                    AND property_id = 20
                    LIMIT 1) AS type_name_override,
                (SELECT id FROM entity_ids WHERE internal_id = datatype) AS type_id_override,
                EntityID AS entity_id,
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
            FROM archive_data_type
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion

            UNION ALL

            
            SELECT
                collection AS collection_override,
                NULL AS name_override,
                NULL AS desc_override,
                NULL AS type_name_override,
                NULL AS type_id_override,
                EntityID AS entity_id,
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
            FROM archive_collection_type
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion;

            LEAVE retrieveOverridesBody;
        END IF;
    END IF;

    SELECT
        NULL AS collection_override,
        name AS name_override,
        NULL AS desc_override,
        NULL AS type_name_override,
        NULL AS type_id_override,
        EntityID AS entity_id,
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
    FROM name_overrides
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID

    UNION ALL

    SELECT
        NULL AS collection_override,
        NULL AS name_override,
        description AS desc_override,
        NULL AS type_name_override,
        NULL AS type_id_override,
        EntityID AS entity_id,
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
    FROM desc_overrides
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID

    UNION ALL

    SELECT
        NULL AS collection_override,
        NULL AS name_override,
        NULL AS desc_override,
        (SELECT value FROM name_data
            WHERE domain_id = 0
            AND entity_ID = datatype
            AND property_id = 20 LIMIT 1) AS type_name_override,
        (SELECT id FROM entity_ids WHERE internal_id = datatype) AS type_id_override,
        EntityID AS entity_id,
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
    FROM data_type
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID

    UNION ALL

    SELECT
        collection AS collection_override,
        NULL AS name_override,
        NULL AS desc_override,
        NULL AS type_name_override,
        NULL AS type_id_override,
        EntityID AS entity_id,
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
    FROM collection_type
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `retrieveQueryTemplateDef` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `retrieveQueryTemplateDef`(
    in EntityID VARCHAR(255),
    in Version VARBINARY(255))
retrieveQueryTemplateDefBody: BEGIN

    DECLARE IVersion INT UNSIGNED DEFAULT NULL;
    DECLARE IsHead BOOLEAN DEFAULT TRUE;
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalEntityID FROM entity_ids WHERE id = EntityID;


    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        
        IF Version IS NOT NULL THEN
            SELECT get_head_version(EntityID) = Version INTO IsHead;
        END IF;

        IF IsHead IS FALSE THEN
            SET IVersion = get_iversion(InternalEntityID, Version);

            IF IVersion IS NULL THEN
                
                LEAVE retrieveQueryTemplateDefBody;
            END IF;

            SELECT definition
            FROM archive_query_template_def
            WHERE id = InternalEntityID
            AND _iversion = IVersion;

            LEAVE retrieveQueryTemplateDefBody;
        END IF;
    END IF;

    SELECT definition
    FROM query_template_def
    WHERE id = InternalEntityID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `setFileProperties` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `setFileProperties`(
    in EntityID VARCHAR(255),
    in FilePath TEXT,
    in FileSize BIGINT UNSIGNED,
    in FileHash VARCHAR(255)
)
BEGIN
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;
    DECLARE IVersion INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalEntityID FROM entity_ids WHERE id = EntityID;

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        SELECT max(e._iversion) INTO IVersion
            FROM entity_version AS e
            WHERE e.entity_id = InternalEntityID;

        INSERT INTO archive_files (file_id, path, size, hash,
                _iversion)
            SELECT file_id, path, size, hash, IVersion AS _iversion
            FROM files
            WHERE file_id = InternalEntityID;
    END IF;

    DELETE FROM files WHERE file_id = InternalEntityID;

    IF FilePath IS NOT NULL THEN
        INSERT INTO files (file_id, path, size, hash)
            VALUES (InternalEntityID, FilePath, FileSize, unhex(FileHash));
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `set_transaction` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `set_transaction`(
    srid VARBINARY(255),
    username VARCHAR(255),
    realm VARCHAR(255),
    seconds BIGINT UNSIGNED,
    nanos INT(10) UNSIGNED)
BEGIN

    SET @SRID = srid;  
    INSERT INTO transactions (srid, username, realm, seconds, nanos)
        VALUES (srid, username, realm, seconds, nanos);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `showEntityAutoIncr` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `showEntityAutoIncr`()
BEGIN
SELECT `AUTO_INCREMENT`
FROM  INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'caosdb'
AND   TABLE_NAME   = 'entities';


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateEntity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `updateEntity`(
    in EntityID VARCHAR(255),
    in EntityName VARCHAR(255),
    in EntityDescription TEXT,
    in EntityRole VARCHAR(255),
    in DatatypeID VARCHAR(255),
    in Collection VARCHAR(255),
    in ACL VARBINARY(65525))
BEGIN
    DECLARE ACLID INT UNSIGNED DEFAULT NULL;
    DECLARE Hash VARBINARY(255) DEFAULT NULL;
    DECLARE Version VARBINARY(255) DEFAULT SHA1(UUID());
    DECLARE ParentVersion VARBINARY(255) DEFAULT NULL;
    DECLARE Transaction VARBINARY(255) DEFAULT NULL;
    DECLARE OldIVersion INT UNSIGNED DEFAULT NULL;
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalEntityID from entity_ids WHERE id = EntityID;

    call entityACL(ACLID, ACL);

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        SELECT max(_iversion) INTO OldIVersion
            FROM entity_version
            WHERE entity_id = InternalEntityID;

        
        INSERT INTO archive_entities (id, description, role,
                acl, _iversion)
            SELECT e.id, e.description, e.role, e.acl, OldIVersion
            FROM entities AS e
            WHERE e.id = InternalEntityID;

        INSERT INTO archive_data_type (domain_id, entity_id, property_id,
                datatype, _iversion)
            SELECT e.domain_id, e.entity_id, e.property_id, e.datatype,
                OldIVersion
            FROM data_type AS e
            WHERE e.domain_id = 0
            AND e.entity_id = 0
            AND e.property_id = InternalEntityID;

        INSERT INTO archive_collection_type (domain_id, entity_id, property_id,
                collection, _iversion)
            SELECT e.domain_id, e.entity_id, e.property_id, e.collection,
                OldIVersion
            FROM collection_type as e
            WHERE e.domain_id = 0
            AND e.entity_id = 0
            AND e.property_id = InternalEntityID;


        SET Transaction = @SRID;
        SELECT e.version INTO ParentVersion
            FROM entity_version as e
            WHERE e.entity_id = InternalEntityID
            AND e._iversion = OldIVersion;
        CALL insert_single_child_version(
            InternalEntityID, Hash, Version,
            ParentVersion, Transaction);
    END IF;

    UPDATE entities e
        SET e.description = EntityDescription,
            e.role=EntityRole,
            e.acl = ACLID
        WHERE e.id = InternalEntityID;

    
    
    DELETE FROM name_data
        WHERE domain_id = 0 AND entity_id = InternalEntityID AND property_id = 20;
    IF EntityName IS NOT NULL THEN
        INSERT INTO name_data
                (domain_id, entity_id, property_id, value, status, pidx)
            VALUES (0, InternalEntityID, 20, EntityName, "FIX", 0);
    END IF;

    DELETE FROM data_type
        WHERE domain_id=0 AND entity_id=0 AND property_id=InternalEntityID;

    DELETE FROM collection_type
        WHERE domain_id=0 AND entity_id=0 AND property_id=InternalEntityID;

    IF DatatypeID IS NOT NULL THEN
        INSERT INTO data_type (domain_id, entity_id, property_id, datatype)
            SELECT 0, 0, InternalEntityID,
                ( SELECT internal_id FROM entity_ids WHERE id = DatatypeID );

        IF Collection IS NOT NULL THEN
            INSERT INTO collection_type (domain_id, entity_id, property_id,
                    collection)
                SELECT 0, 0, InternalEntityID, Collection;
        END IF;
    END IF;

    Select Version as Version;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateLinCon` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `updateLinCon`(in sig_from BIGINT, in sig_to BIGINT, in new_a DECIMAL(65,30), in new_b_dividend BIGINT, in new_b_divisor BIGINT, in new_c DECIMAL(65,30))
BEGIN
    UPDATE units_lin_con SET signature_to=sig_to, a=new_a, b_dividend=new_b_dividend, b_divisor=new_b_divisor, c=new_c where signature_from=sig_from;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-10-02  9:54:57
