# syntax=docker/dockerfile:experimental

#######################################
###### Get the sources from git #######
#######################################
FROM debian:bookworm as git_base

# Check for availability of DNS
RUN if getent hosts indiscale.com > /dev/null; \
    then echo "Connected to the internet and DNS available"; \
    else echo "No internet connection or DNS not available"; \
    fi

# Proxy setup
# Let's just use the debian proxy cache for default network settings and not use any proxy if it's
# not available.

# Alternative approaches: Install netcat and iproute2 first, or use awk to get number from
# /proc/net/route.
RUN http_proxy=http://172.17.0.1:8123 apt -eany update \
  && (echo "Acquire::http::Proxy \"http://172.17.0.1:8123\";" > /etc/apt/apt.conf.d/30proxy.conf) \
  && (echo "Acquire::http::Proxy::ppa.launchpad.net DIRECT;" >> /etc/apt/apt.conf.d/30proxy.conf) \
  && echo "Squid proxy detected \`;,;´" \
  || echo "No squid proxy detected on docker host at default address."

RUN apt-get update && apt-get install -y \
    git
RUN mkdir -p /opt/caosdb/git
WORKDIR /opt/caosdb/git

##### caosdb-mysqlbackend #####
FROM git_base as git_mysql
ARG MYSQLBACKEND="dev"
ARG REM_MYSQLBACKEND="https://gitlab.indiscale.com/caosdb/src/caosdb-mysqlbackend.git"
ENV REF_MYSQLBACKEND=$MYSQLBACKEND
ENV PROJECT_NO=101
ENV REM_GITLAB_API="https://gitlab.indiscale.com/api/v4/projects/$PROJECT_NO/repository/commits/"
# Use the contents of .../repository/commits/dev as a condition whether to use the cache or not.
ADD ${REM_GITLAB_API}${MYSQLBACKEND} mysql_version.json
RUN git clone "$REM_MYSQLBACKEND" \
    && cd caosdb-mysqlbackend \
    && git checkout "$REF_MYSQLBACKEND"

# Store hash of HEAD commit:
RUN git -C caosdb-mysqlbackend rev-parse HEAD >> caosdb_mysqlbackend_commit

# Remove git objects
RUN rm -r caosdb-mysqlbackend/.git

# Remove proxy config
RUN mv /etc/apt/apt.conf.d/30proxy.conf /etc/apt/apt.conf.d/30proxy.conf.disabled || true

##### caosdb-webui #####
FROM git_base as git_webui
ARG WEBUI="dev"
ARG REM_WEBUI="https://gitlab.indiscale.com/caosdb/src/caosdb-webui.git"
ENV REF_WEBUI=$WEBUI
ENV PROJECT_NO=98
ENV REM_GITLAB_API="https://gitlab.indiscale.com/api/v4/projects/$PROJECT_NO/repository/commits/"
ADD ${REM_GITLAB_API}${WEBUI} webui_version.json

RUN mkdir caosdb-server && cd caosdb-server \
    && git clone "$REM_WEBUI" \
    && cd caosdb-webui && git checkout "$REF_WEBUI"

# Store hash of HEAD commit:
RUN git -C caosdb-server/caosdb-webui rev-parse HEAD >> caosdb_webui_commit

# Remove git objects
RUN rm -r caosdb-server/caosdb-webui/.git

# add acm module
COPY transfer/build_docker/acm.tar /opt/caosdb/git/caosdb-server/caosdb-webui/src/ext
RUN cd caosdb-server/caosdb-webui/src/ext && tar -xf acm.tar

# Remove proxy config
RUN mv /etc/apt/apt.conf.d/30proxy.conf /etc/apt/apt.conf.d/30proxy.conf.disabled || true

# make globally accessible
RUN chmod -R a+rwX caosdb-server/caosdb-webui

##### caosdb-pylib #####
FROM git_base as git_python
ARG PYLIB="dev"
ARG REM_PYLIB="https://gitlab.indiscale.com/caosdb/src/caosdb-pylib.git"
ENV REF_PYLIB=$PYLIB
ENV PROJECT_NO=97
ENV REM_GITLAB_API="https://gitlab.indiscale.com/api/v4/projects/$PROJECT_NO/repository/commits/"
ADD ${REM_GITLAB_API}${PYLIB} pylib_version.json
# clone the required repos
RUN git clone "$REM_PYLIB" \
    && cd caosdb-pylib \
    && git checkout "$REF_PYLIB"

# Store hash of HEAD commit:
RUN git -C caosdb-pylib rev-parse HEAD >> caosdb_pylib_commit && cp caosdb_pylib_commit caosdb-pylib/

# Remove git objects
RUN rm -r caosdb-pylib/.git

# Remove proxy config
RUN mv /etc/apt/apt.conf.d/30proxy.conf /etc/apt/apt.conf.d/30proxy.conf.disabled || true

##### caosdb-advanced-user-tools #####
FROM git_base as git_advancedtools
ARG ADVANCEDUSERTOOLS="dev"
ARG REM_ADVANCEDUSERTOOLS="https://gitlab.indiscale.com/caosdb/src/caosdb-advanced-user-tools.git"
ENV REF_ADVANCEDUSERTOOLS=$ADVANCEDUSERTOOLS
ENV PROJECT_NO=104
ENV REM_GITLAB_API="https://gitlab.indiscale.com/api/v4/projects/$PROJECT_NO/repository/commits/"
ADD ${REM_GITLAB_API}${ADVANCEDUSERTOOLS} advancedtools_version.json
# clone the required repos
RUN git clone "$REM_ADVANCEDUSERTOOLS" \
    && cd caosdb-advanced-user-tools \
    && git checkout "$REF_ADVANCEDUSERTOOLS"

# Store hash of HEAD commit:
RUN git -C caosdb-advanced-user-tools rev-parse HEAD >> caosdb_advancedusertools_commit

# Remove git objects
RUN rm -r caosdb-advanced-user-tools/.git

# Remove proxy config
RUN mv /etc/apt/apt.conf.d/30proxy.conf /etc/apt/apt.conf.d/30proxy.conf.disabled || true

###############################
###### Java server build ######
###############################
FROM git_base as jar

RUN apt-get install -y \
    make \
    maven \
    openjdk-17-jdk-headless

ARG SERVER="dev"
# Set to an empty string if the WebUI should be at the version set in the parent
ARG REM_SERVER="https://gitlab.indiscale.com/caosdb/src/caosdb-server.git"
ENV REF_SERVER=$SERVER
ENV PROJECT_NO=100
ENV REM_GITLAB_API="https://gitlab.indiscale.com/api/v4/projects/$PROJECT_NO/repository/commits/"
ADD ${REM_GITLAB_API}${SERVER} server_version.json

# Checkout set branch.
RUN git clone "$REM_SERVER" \
    && cd caosdb-server \
    && git checkout "$REF_SERVER" \
    # write commit hash to server.conf (which is readable by integration tests)
    && echo "SERVER_COMMIT = $(git rev-parse HEAD)" >> conf/core/server.conf \
    && mkdir authtoken \
    && mkdir -p scripting/home/

RUN git -C caosdb-server submodule update --init caosdb-proto
RUN git -C caosdb-server/caosdb-proto rev-parse HEAD >> caosdb_proto_commit
# Remove git objects
RUN rm -r caosdb-server/caosdb-proto/.git

RUN --mount=type=cache,target=/root/.m2,sharing=shared \
    cd /opt/caosdb/git/caosdb-server && \
    echo "Making CaosDB server jar" && \
    make jar && \
    # Copying the mvn repository cache for later use
    cp -a /root/.m2 /opt/caosdb/m2

# Store hash of HEAD commit:
RUN git -C caosdb-server rev-parse HEAD >> caosdb_server_commit

# Remove git objects
RUN rm -r caosdb-server/.git

# make globally accessible
RUN chmod -R a+rwX caosdb-server

# Remove proxy config
RUN mv /etc/apt/apt.conf.d/30proxy.conf /etc/apt/apt.conf.d/30proxy.conf.disabled || true

###############################
###### Main Image Build #######
###############################
# Use Debian as parent image
FROM debian:bookworm

# http://bugs.python.org/issue19846
ENV LANG C.UTF-8

# Working directory is /opt/caosdb
WORKDIR /opt/caosdb

# Set up the basic OS

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y \
    libpam-ldapd \
    git \
    sssd \
    pkgconf \
    python3-pip

RUN pip3 install --break-system-packages \
    jsonschema \
    py3-validate-email

RUN rm -r /var/cache/apt /var/lib/apt

RUN mkdir -p /opt/caosdb/build_docker/
COPY transfer/build_docker/setup_os.sh /opt/caosdb/build_docker/
RUN --mount=type=cache,target=/var/cache/apt \
    --mount=type=cache,target=/var/lib/apt build_docker/setup_os.sh

# Copy everything build-related there
COPY transfer/build_docker/* /opt/caosdb/build_docker/

RUN mkdir -p /opt/caosdb/mnt/caosroot && chmod a+w /opt/caosdb/mnt/caosroot && \
    mkdir -p /opt/caosdb/mnt/dropoffbox && \
    chmod a+w /opt/caosdb/mnt/dropoffbox  && \
    mkdir -p /opt/caosdb/mnt/other && mkdir -p /opt/caosdb/mnt/extroot && \
    mkdir -p /opt/caosdb/mnt/caosdb-server && mkdir -p /opt/caosdb/mnt/tmpfiles

COPY --from=git_python /opt/caosdb/git /opt/caosdb/git
COPY --from=git_advancedtools /opt/caosdb/git /opt/caosdb/git

RUN cd git/caosdb-pylib \
  && echo "Installing pylib" \
  && pip3 install --break-system-packages .

# `bottleneck` is required for https://github.com/pandas-dev/pandas/issues/45753
RUN cd git/caosdb-advanced-user-tools \
  && echo "Installing advanced user tools" \
  && pip3 install --break-system-packages -U numexpr \
  && pip3 install --break-system-packages -U bottleneck \
  && pip3 install --break-system-packages .

COPY --from=jar /opt/caosdb/git /opt/caosdb/git
COPY --from=git_mysql /opt/caosdb/git /opt/caosdb/git
COPY --from=git_webui /opt/caosdb/git /opt/caosdb/git

RUN chmod  a+wx /opt/caosdb/git/caosdb-server/caosdb-webui
RUN chmod  a+wx /opt/caosdb/git/caosdb-server

RUN build_docker/setup.sh prep_sql
RUN build_docker/setup.sh config
RUN build_docker/setup.sh make
COPY --from=jar /opt/caosdb/git/caosdb-server/target/caosdb-server.jar \
/opt/caosdb/git/caosdb-server/target/caosdb-server.jar
COPY transfer/settings/maven_m2_settings.xml /etc/maven/settings.xml

## Creates the TLS certificate.  Note: This will then be the same certificate
## for all containers created from this image.
RUN build_docker/setup.sh cert
RUN build_docker/setup.sh user
RUN build_docker/setup.sh privilege

# Finally: copy the runner environment
RUN mkdir /opt/caosdb/run_docker/
COPY transfer/run_docker /opt/caosdb/run_docker/
COPY transfer/sss_hostproxy /etc/pam.d/sss_hostproxy
RUN build_docker/setup.sh make_run
RUN env > /.creation_environment

RUN cp /opt/caosdb/run_docker/bin/* /usr/local/bin/
RUN cp /opt/caosdb/run_docker/etc/* /etc/ # copy stuff like the /etc/nsswitch.conf

# Remove proxy config
RUN mv /etc/apt/apt.conf.d/30proxy.conf /etc/apt/apt.conf.d/30proxy.conf.disabled || true

CMD ["./run_docker/run"]
HEALTHCHECK --interval=30s --timeout=5s --start-period=1m \
    CMD curl --cacert /opt/caosdb/cert/caosdb.cert.pem \
    -I https://localhost:10443/Entities || exit 1
