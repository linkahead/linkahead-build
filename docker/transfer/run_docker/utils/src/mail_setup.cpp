// ** header v3.0
// This file is a part of the CaosDB Project.
//
// Copyright (C) 2019 Daniel Hornung
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//
// ** end header

// Compile it with (the order of the cpp and the libs matters:
// g++ -std=c++17 -o mail_setup src/mail_setup.cpp -lboost_system \
// -lboost_filesystem -ldl -lstdc++fs

#include <experimental/filesystem>
#include <fstream>
#include <iostream>
#include <unistd.h>
#include <boost/dll.hpp>

namespace fs = std::experimental::filesystem;

int main(int argc, char* argv[]) {
  // Get the main.cf file
  if (argc != 2) {
    std::cerr << "`main.cf` file required as the argument!" << std::endl;
    std::cerr << "You gave " << argc - 1 << " arguments instead." << std::endl;
    return 1;
  }
  char* main_cf = argv[1];

  fs::copy(main_cf, "/etc/postfix/", fs::copy_options::overwrite_existing);
  // Change the real user ID
  setuid(0);
  int result = std::system("/usr/sbin/service postfix start");

  // We trust the first run of this program since we call it ourselves here.
  // Simply delete it after running. ;-)
  std::string delete_me = "/bin/rm " + boost::dll::program_location().string();
  std::cout << delete_me << std::endl;
  std::system(delete_me.c_str());
  return 0;
}
