#!/usr/bin/env python3

# This file is a part of the CaosDB Project.
#
# Copyright (C) 2022 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2019-2022 Daniel Hornung <d.hornung@indiscale.com>
# Copyright (C) 2022 Timm Fitschen <t.fitschen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
This script does the following:

1) Wait for the SQL server to become available.
2) Configure the SQL database, if necessary.
3) Start the CaosDB server.

"""

# import configparser
import datetime
import glob
import os
import re
import subprocess
import sys
import time

verbosity = 1


def _log(msg, min_level=0, newline=True):
    if min_level <= verbosity:
        end = "\n" if newline else ""
        print(msg, end=end, flush=True)


def sql_wait(sql_path, timeout=300, sleep=3):
    """Waits for the SQL server to become ready.

    Arguments
    ---------
    sql_path: str
        The path to the caosdb-mysql repository, where the makefile is located.

    timeout: int, optional
        How long (in seconds) to wait for the SQL server to become available
        before giving up.  Default is 300.

    sleep: int, optional
        How long (in seconds) to wait before retrying to connect to the SQL
        server.  Default is 3.
    """
    # with open(sql_conf_file, 'r') as f:
    #     conf_str = '[config]\n' + f.read()
    #     config = configparser.ConfigParser()
    #     config.read(conf_str)
    # print()
    # config["config"]["mysql_host"]
    start = datetime.datetime.now()

    _log("Trying to reach the SQL server ", 1)

    while (datetime.datetime.now() - start) <= datetime.timedelta(0, timeout+1):
        _log("Trying...", 1)

        if verbosity >= 2:
            out_err = None
        else:
            out_err = subprocess.PIPE
        result = subprocess.run(["make", "test-connection"], cwd=sql_path,
                                stdout=out_err, stderr=out_err)

        if result.returncode == 0:
            _log("", 1)
            _log("SQL server successfully reached.", 1)

            return True
        time.sleep(sleep)

    _log("", 1)
    _log("Timeout when trying to connect to the SQL server.", 1)

    return False


def sql_configure(sql_path, custom_other_path):
    """Configure the database for proper use by CaosDB.

    This function uses the same calls as the sql-backend make file to configure
    the database as needed.  This includes inserting data if commanded and `make
    install`.

    Inserting data will take place if there's an environment variable named
    `SQL_RESTORE` and its value is `1`.

    """
    sql_file_legacy = os.path.join(custom_other_path, "restore.sql")

    if os.path.exists(sql_file_legacy):
        raise RuntimeError("You have a `restore.sql` at the old location. "
                           "Please put it in a restore folder in custom and "
                           "have it end on 'dump.sql'.")

    if os.getenv("SQL_RESTORE") == "1":
        restore_dir = os.path.join(custom_other_path, "restore")

        if not os.path.exists(restore_dir):
            raise RuntimeError(
                "Trying to restore, but restore directory `{}` does not "
                "exist".format(restore_dir))

        sql_file = glob.glob(os.path.join(restore_dir, "*dump.sql"))

        if len(sql_file) != 1:
            raise RuntimeError(
                "Could not unambiguously identify restore file.")
        sql_file = os.path.join(restore_dir, sql_file[0])

        # Can we remove this if statement?
        if not os.path.exists(sql_file):
            _log("Trying to restore SQL from {path}, but file does not "
                 "exist.".format(path=sql_file), -1)
            raise FileNotFoundError(
                "This really should not happen! `{}`".format(sql_file))

        _log("Importing old data into database.", 1)
        # `make _install` for minimal database content
        result_install = subprocess.run(["make", "_install"], cwd=sql_path)
        if not result_install.returncode == 0:
            _log("Error while configuring SQL server.", -1)
            return False
        result_restore = subprocess.run(["./make_db", "restore_db", sql_file],
                                        cwd=os.path.join(sql_path, "utils"))

        if result_restore.returncode == 0:
            _log("SQL server successfully configured.", 1)
            _log("Data successfully inserted into SQL server.", 1)
        else:
            _log("Error while restoring data.", -1)
            _log("""
    Note
    ----

    If this happened while restoring from an SQL dump that was made with an outdated SQL server
    version, try using the update scripts in the `dump_updates/` directory of LinkAhead's SQL
    backend:

    https://gitlab.com/linkahead/linkahead-mariadbbackend/-/tree/dev/dump_updates
            """)

            return False

    result = subprocess.run(["make", "install"], cwd=sql_path)

    if result.returncode != 0:
        return False

    if os.getenv("ANON_ADMIN") == "1":
        anonymous_result = subprocess.run(
            ["./make_db", "grant-permission", "anonymous",
             '[{"grant":"true","priority":"true","permission":"*"}]'],
            cwd=os.path.join(sql_path, "utils"))

        if anonymous_result.returncode == 0:
            _log("Anonymous user successfully granted full "
                 "administrative rights.", 1)
        else:
            _log("Error while granting administrative rights to anonymous", -1)

            return False

    _log("SQL server successfully configured.", 1)

    return True


def initial_setup(custom_other_path, caosdb_path):
    """Performs initial setup.

    This includes:
    - time zone
    - NIS
    - email
    - network config
    - permissions of directories
    """

    if os.getenv("DOCKER_TZ"):
        _log("Setting time zone", 0)
        os.environ["CAOSDB_CONFIG_TIMEZONE"] = os.getenv("DOCKER_TZ")
        subprocess.run(["./run_docker/utils/tzsetup", os.getenv("DOCKER_TZ")],
                       check=True)

    if os.getenv("LOCAL_USERS") == "1" and not local_users(caosdb_path=caosdb_path):
        return False

    if os.getenv("NIS_ENABLE") == "1" and not nis_enable():
        return False

    if os.getenv("LDAP_ENABLE") == "1" and not ldap_enable():
        _log("Unsuccessfully tried to start LDAP client.", 0)
        return False

    if not init_mail(custom_other_path=custom_other_path):
        _log("Unsuccessfully tried to set up the email.", 0)

        return False

    if os.getenv("PORT_SSL") and not _create_port_config(caosdb_path):
        _log("Unsuccessfully tried to set up the networking settings.", 0)

        return False

    subprocess.run(["./run_docker/utils/permission_setup", str(os.getuid()),
                    str(os.getgid())], check=True)

    return True


def init_mail(custom_other_path):
    if os.getenv("DOCKER_MAIL") == "1":
        _log("Email sending: Enabled", 0)
        main_cf = os.path.join(custom_other_path, "main.cf")

        if os.path.exists(main_cf):
            result = subprocess.run(
                ["./run_docker/utils/mail_setup", main_cf],
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            print(result.stdout.decode())
            print(result.stderr.decode())

            if not result.returncode == 0:
                return False
        else:
            _log("No configuration file for Email given", 0)

    else:
        _log("Email sending: Disabled", 0)

    return True


def local_users(caosdb_path):
    """Enable local users.

More specifically, this modifies the default usersources.ini to include the users found in
/etc/passwd.

These settings can be overridden by users if they provide custom `usersources.ini` and
`global_entity_permissions.xml` config files.

Parameters
----------
caosdb_path : str
CaosDB's base directory.
"""
    local_ini_template = """
realms = PAM
defaultRealm = PAM
[PAM]
class = org.caosdb.server.accessControl.Pam
default_status = ACTIVE
# user.admin.roles = administration       # No user roles for default PAM users
# user.testuser.roles = administration
"""
    # Get minimum user ID
    uid_min = 1000
    try:
        pat = re.compile(
            "\\s*UID_MIN\\s+(?P<uid_min>\\d+)\\s*", flags=re.ASCII)
        with open("/etc/login.defs", "r") as login_defs:
            for line in login_defs.readlines():
                match = pat.match(line)
                if match:
                    uid_min = int(match.group("uid_min"))
    except (OSError, ValueError) as err:
        print(err)
    # read user names
    users = []
    with open("/etc/passwd", "r") as passwd:
        pat = re.compile(
            "^(?P<username>[^:]+):x:(?P<uid>\\d+):.*", flags=re.ASCII)
        for line in passwd.readlines():
            match = pat.match(line)
            if not match:
                continue
            name, uid = match.groups()
            uid = int(uid)
            if uid >= uid_min:
                users.append(name)
    # Append users to ini
    local_ini = local_ini_template + \
        "include.user = {}".format(", ".join(users))
    confdir = os.path.join(caosdb_path, "conf", "ext")
    os.makedirs(confdir, exist_ok=True)
    with open(os.path.join(confdir, "usersources.ini"), "w") as usersources:
        usersources.write(local_ini)
    return True


def ldap_enable():
    """Enable LDAP client daemon."""
    print("Start nslcd (LDAP client)")
    try:
        subprocess.run(["./run_docker/utils/start_nslcd"], check=True)
    except subprocess.CalledProcessError:
        return False
    return True


def nis_enable():
    """Enable NIS name lookup for PAM."""

    # Set up files and start yp service
    nis_domain = os.getenv("NIS_DOMAIN")
    nis_server = os.getenv("NIS_SERVER")
    print("NIS domain: {domain}\nNIS server: {server}".format(
        domain=nis_domain, server=nis_server))
    try:
        subprocess.run(["./run_docker/utils/ypsetup", nis_domain, nis_server],
                       check=True)
    except subprocess.CalledProcessError:
        return False

    return True


def caosdb_run(caosdb_path, debug=False, test=False, no_tls=False):
    """Start the CaosDB server."""
    webui_path = _get_path("caosdb-webui", base=os.getcwd(),
                           exclude_str="/opt/caosdb/mnt")

    if not webui_path:
        _log("Could not find WebUI!", -1)
        sys.exit(90)
    res_webui = subprocess.run(["make"], cwd=webui_path)

    if not res_webui.returncode == 0:
        _log("WebUI update failed!", -1)
        sys.exit(91)

    if test:
        subprocess.run(["mvn", "test"], cwd=caosdb_path)
    # result = subprocess.run(['mvn', 'exec:java@run',], cwd=caosdb_path)

    env = dict(os.environ)
    if no_tls:
        env["CAOSDB_COMMAND_LINE_OPTIONS"] = " ".join(
            os.environ.get("CAOSDB_COMMAND_LINE_OPTIONS", "").split()
            + ["--no-tls"])
    if debug:
        result = subprocess.run(
            ['make', 'run-debug-single', ], cwd=caosdb_path, env=env)
    else:
        result = subprocess.run(['make', 'run-single', ], cwd=caosdb_path, env=env)

    if result.returncode == 0:
        _log("CaosDB terminated gracefully.", 2)

        return True

    return False


def _get_path(dirname, base, exclude_str=None):
    """Tries to find a directory, "close" to the current working directory.

    Returns the path of said directory, if found.

    Arguments
    ---------
    dirname: str
        The name of the directory to be found.

    base: str
        Where to start searching for the directory.

    exclude_str: str, optional
        If given, this string must not be part of the result.
    """

    # glob cannot exclude natively, so we need to remimplement it
    def search_in_dir(dirname, base, excludes=[], depth=0, max_depth=100,
                      visited=set()):

        if depth == max_depth:
            return None
        next_level = []

        for entry in os.scandir(base):
            if not entry.is_dir():
                continue

            if entry.inode() in visited:
                _log("Visited before: {}".format(os.path.abspath(entry.path)))

                continue
            abspath = os.path.abspath(entry.path)

            if any([excl in abspath for excl in excludes]):
                _log("Blacklisted: {}".format(abspath), 3)

                continue

            if dirname == entry.name:
                return abspath
            # Else, continue deeper into the directory tree
            visited.add(entry.inode())
            _log("Adding for next recursion: {}".format(abspath), 3)
            next_level.append(abspath)

        for next_base in next_level:
            result = search_in_dir(dirname, next_base, excludes=excludes,
                                   depth=depth+1, max_depth=max_depth,
                                   visited=visited)

            if result:
                return result

    while not base == os.path.sep:
        base = os.path.split(base)[0]
        path = search_in_dir(dirname, base, excludes=[exclude_str])

        if path:
            return path


def _create_port_config(caosdb_path):
    """E.g. adds a config to set the external SSL port.
    """
    port_ssl_ext = os.getenv("PORT_SSL")
    network_template = """
# Additional network settings

# Set the same ports as from the outside
REDIRECT_HTTP_TO_HTTPS_PORT={port}
"""
    confdir = os.path.join(caosdb_path, "conf", "ext", "server.conf.d")
    os.makedirs(confdir, exist_ok=True)
    conffile = os.path.join(confdir, "80.network_docker.conf")
    with open(conffile, mode="w") as fd_conf:
        fd_conf.write(network_template.format(port=port_ssl_ext))
    return True


def import_custom(custom_server_path, base, custom_other_path):
    """Imports folders and specialized settings etc. from the `custom` directory.
    """
    #################
    # Server config and extensions
    caosdb_source = os.path.join(custom_server_path, "")
    caosdb_target = os.path.join(base, "git", "caosdb-server")
    if os.path.exists(caosdb_source):
        rsync = ["rsync", "-r", caosdb_source, caosdb_target]
        _log("Rsyncing caosdb: {}".format(rsync), 2)
        result = subprocess.run(rsync)
        if not result.returncode == 0:
            _log("Rsync of caosdb failed.", 0)
            return False

    #################
    # TLS certificate
    cert_source = os.path.join(custom_other_path, "cert", "")
    cert_target = os.path.join(base, "cert")
    if os.path.exists(cert_source):
        rsync = ["rsync", "-r", cert_source, cert_target]
        _log("Rsyncing cert: {}".format(rsync), 2)
        result = subprocess.run(rsync)
        if not result.returncode == 0:
            _log("Rsync of cert failed.", 0)
            return False

    return True


def main():
    # conf_file = os.path.join(_get_path("caosdb-mysqlbackend"), ".config")
    global verbosity
    verbosity = 1
    base = os.getcwd()
    test = (os.getenv("MAKE_TEST") == "1")
    debug = (os.getenv("DEBUG") == "1")
    no_tls = (os.getenv("NO_TLS") == "1")

    if debug:
        verbosity += 1
    _log("Getting paths, starting at {}...".format(base), 3)
    sql_path = _get_path("caosdb-mysqlbackend", base=base,
                         exclude_str="/opt/caosdb/mnt/"
                         )
    _log("sql_path: {}".format(sql_path), 3)
    caosdb_path = _get_path("caosdb-server", base=base,
                            exclude_str="/opt/caosdb/mnt/"
                            )
    _log("caosdb_path: {}".format(caosdb_path), 3)
    custom_other_path = os.path.join(base, "mnt", "other")
    if not sql_path:
        _log("SQL configuration path not found, exiting.", -1)
        sys.exit(1)

    if not caosdb_path:
        _log("CaosDB path not found, exiting.", -1)
        sys.exit(1)

    #########################
    # Initial setup procedures
    #########################
    # This also modifies configuration files according to environment variables etc.
    if not initial_setup(custom_other_path=custom_other_path,
                         caosdb_path=caosdb_path):
        _log("Problem during initial setup of system.", 0)
        sys.exit(10)

    #########################
    # Wait for SQL server
    #########################
    if not sql_wait(sql_path, timeout=800):
        _log("SQL server not reachable, giving up.", 0)
        sys.exit(20)

    #########################
    # Look for mounted overwritables
    #########################
    # This should happen after any local modification of the default files so that overwriting
    # manually has a higher priority.
    _log("Importing external material.", 1)
    custom_server_path = os.path.join(base, "mnt", "caosdb-server")
    if not import_custom(custom_server_path=custom_server_path, base=base,
                         custom_other_path=custom_other_path):
        _log("Problem while importing external material.", 0)
        sys.exit(30)
    #########################
    # Configure SQL server
    #########################
    if not sql_configure(sql_path, custom_other_path=custom_other_path):
        _log("Unsuccessfully tried to configure SQL server.", 0)
        sys.exit(40)

    #########################
    # Start CaosDB server
    #########################
    _log("Starting CaosDB server...", 1)
    if not caosdb_run(caosdb_path, test=test, debug=debug, no_tls=no_tls):
        _log("CaosDB exited unsuccessfully.", 0)
        sys.exit(50)


if __name__ == "__main__":
    main()
