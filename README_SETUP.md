# Getting Started #

See `INSTALL.md` or `INSTALL-GIT.md` for information on how to install LinkAhead.

## Start LinkAhead ##

Want to try it out?  Try these steps:

1. `./linkahead start --build`
   1. The `--build` option is only necessary to build the Docker image, it can be omitted for future
      runs.
   2. If you get `docker: command not found`, make sure that docker and
      docker-compose are installed (see below) and that you have
      permissions to run it. To obtain the permissions the current user
      typically has to be added to the `docker` group.
   3. It might be necessary on your system to start and / or enable the docker
      service manually. On systems with systemd this is usually done using `sudo
      systemctl start docker` and `sudo systemctl enable docker`.
2. In your preferred browser, open `https://localhost:10443/`, ignore TLS
   warnings. Login with `admin`, password `caosdb`.
   - Reload the browser page if no **Welcome to CaosDB!** page appears.
3. By default, `linkahead start` will continue showing logs. Stop that with
   `Ctrl-c`. This does not stop the server, which can be done by
   `linkahead stop`.

## Certificates ##

For SSL encrypted communication with the server certificates are needed. The
following describes how this can be done with Let's Encrypt.

Install `certbot`, e.g. using `apt-get`: `sudo apt-get install certbot` (on debian)

Setup Let's Encrypt in `/var/local/letsencrypt`:
This program is typically run as root and LinkAhead should not be running
(the SSL port needs to be available for certbot).
(`certbot certonly --standalone --config-dir /var/local/letsencrypt/conf --work-dir
/var/local/letsencrypt/work --logs-dir /var/local/letsencrypt/logs -d DOMAIN`)

The easiest way is to setup the `renewcert` service (see below) and run
it once manually `systemctl start linkahead_renewcert_${PROFILENAME}`.

## Using services ##

Services can be used to start, stop LinkAhead and/or to create new SSL certificates using
letsencrypt.

The Debian package installs a service for LinkAhead. Thus, you can start and stop LinkAhead using
`systemctl start/stop linkahead`. The service is enabled by default which means that LinkAhead
starts after reboot.

In order to use multiple services for multiple profiles, copy the service and adjust the commands to
use a specific profile.

For certificate renewal, templates for systemd service and timer files are written to
`/etc/systemd/system/`. The templates are named `linkahead_renewcert_default.service` and
`linkahead_renewcert_default.timer`, respectively.

Whether and how these systemd template files should be used depends on the specific setup.
Therefore you should only use them if you know what you are doing and if you checked the contents
and are confident that this is what you need.
1. Create a first certificate manually
2. Adjust the template files as it is described within them.
   Note that this service leads to server restart. The `keytool` executable needs to be available
   (This is included in `openjdk-11-jdk` on Debian.)
3. Change the times in `/etc/systemd/system/linkahead_renewcert_default.timer` to distribute the
   load on the Let's Encrypt servers.
4. Enable and start the timer `linkahead_renewcert_default.timer` to activate the regular renewal.

### Remote control ###

`linkahead` features several administration tools.  For a full list, invoke the
help with `linkahead -h`.  Some examples are:

- `linkahead log -f` :: Show the logging output, the `-f` (or `--follow`) option
  means that output is appended as new log lines are created.
- `linkahead backup --directory somewhere` :: Create SQL and filesystem dumps in
  directory `somewhere`.


## Configuration and profiles ##

Configuration of LinkAhead happens at several stages.  During startup, LinkAhead takes into account,
in the following order.  If specified, later settings override earlier ones:

1. The defaults in [`defaults.yml`](defaults.yml), this is also where the variables are documented
2. System-wide customization in `linkahead.conf` (typically in `/etc/linkahead/`)
3. Environment variables.
4. The `profile.yml` profile file.
5. Command line arguments to `linkahead`.

By default, the [example profile](profiles/default/profile.yml) is used. To use any other profile than this, execute linkahead
with the `-p` or `--profile` option:

```sh
./linkahead -p PROFILE start
```

Equally, provide the profile name when stopping a linkahead instance
that was started with th `-p` option:

```sh
./linkahead -p PROFILE stop
```

This is especially important when more than one linkahead instance are
running in order to stop the correct one.

The most important setting there for development and consistent deployment is
probably the `refs` section: There the user can specify which git commits to
use, for each of the several repositories.

Local files and data can be included to be used by the server. Thus the
directory containing the `profile.yml` typically has a `paths` and a `custom`
folder.  In a default setting a user does not need to do anything with the paths
folder.  Data files uploaded to and stored by the server will be in a Docker
volume, from where they can be retrieved via the `backup` action (see above).

The `custom` directory should have the following structure:
- caosdb-server :: This directory will be copied into the caosdb server file tree.
  Typically, this is used to modify the configuration files and the webui and
  scripting extensions.
- other :: "Special" configurations etc.
  - cert :: If this directory exists, its content will be used for SSL
    encryption.  Specifically, this means that with the default settings, it
    should contain `caosdb.jks`.
  - restore :: If this folder exists and the `conf.restore` seting is `true`,
    this sql and tar.gz files inside will be used to populate the database with
    data and files.
  - main.cf :: If this file exists, it will be copied into `/etc/postfix/` and
    be used as the configuration file for the postfix mail server.  Additionally
    the postfix server will be started and mails can be sent.  Typical lines in
    main.cf include the address of a relay smtp server. Additionally, you need
    to set "mail" to true in the `profile.yml`.

### The default profile

This installation comes with two different profiles. The default
profile the files of which are located in `profiles/default` contains
an example database filled with musical instruments and analyses
thereof. You might know the example already from our [demo
server](https://demo.indiscale.com). It is used by default, i.e., if
you start linkahead without specifying a profile. The default profile
is suited well to learn about the possibilities of linkahead and even
contains the tour when accessed via the web interface. However, if you
want to use linkahead with your own data you probably don't need the
example data. Instead, you might want to use ...

### The empty profile

This is the second profile that comes with this installation. As the
name suggests, it has no entries whatsoever in its database and thus
offers a clean basis for your own data. You can access it via

```sh
./linkahead -p profiles/empty/profile.yml start/stop
```

as explained above. Before the first start, you'll either have to
create an `extroot` directory or specify an existing one in the
`profile.yml`. Every file and directory contained (or linked, mounted,
... ) can be used within your instance of linkahead.

## Other Services

If other services should also be started and managed by the `linkahead` script
you can copy and change the `defaults.yml` and set the `base_compose_file`
option in your `profile.yml`. The actual docker-compose file is autogenerated
from those files.

If you need to specify the path of the `custom` directors in your customized
docker-compose file, you should use the `$CUSTOM_DIR` variable, because the
directory of the custom directory (during runtime) is not known. During the
generation of the docker-compose file which is used for starting the services,
the `$CUSTOM_DIR` variable is being replaced with the actual custom directory.

Configuration files for mariadb can be placed in the profile folder
`custom/mariadb.conf.d/`.

## Using the Debian package

This repository comes with a script to build a Debian (.deb) package. This package then allows for a
seamless installation on Debian based systems, e.g. virtual machines.

### Building the package

Building the package relies on the debian-tool `debmake` and therefore should be carried out on a
debian machine. Some other distros might include `debmake` as well (e.g. for Archlinux:
https://aur.archlinux.org/packages/debmake/), but this is currently untested. The recommended
workflow to build the package on other distros is to use a VM. E.g. the following commands can be
used to setup and run a QEMU-VM assuming that kernel virtualization (KVM) is enabled and that
`debian-10.0.0-amd64-netinst.iso` is a bootable debian netinstall iso:

```bash
# Create the base image `debian-large.img` with 100GiB HDD:
qemu-img create -f qcow2 debian.qcow2 50G

# Run the image using the iso file as CD-ROM:
qemu-system-x86_64 -m 4G -enable-kvm -drive file=debian.qcow2,format=qcow2,index=0,media=disk -cdrom debian-10.0.0-amd64-netinst.iso -boot d
# ... Installation follows ...

# Create an overlay:
qemu-img create -b debian.qcow2 -F qcow2 -f qcow2 debian-build-caosdb.qcow2

# Run the overlay image:
qemu-system-x86_64 -m 4G -enable-kvm -drive file=debian-build-caosdb.qcow2,format=qcow2,index=0,media=disk
```
On Ubuntu, debhelper, python-all is a dependency

Then proceed with the installation of requirements mentioned above. Note that on debian the
recommended package containing docker is `docker.io`.

### Running the build script

On debian navigate to `debian/` and run `prepare_deb.sh` and then
`./build_deb.sh`. The .deb-package will be placed in
`linkahead/`. Alternatively, in `debian/` run `make [demo|all]` to build
the default (or demo, or all three) package(s).

### Login for access to docker image

Newer versions of the package require access to a docker image registry for retrieving
the prebuilt image.

Use the following command line to login. Use your username <USER> with password <PASSWORD>:
`docker login -u <USER> -p <PASSWORD> gitlab.indiscale.com:5050`

It might be necessary to install `gnupg2` and `pass` before (see troubleshooting).

**Note:** The login command has to be run by the user that also starts the docker container. Very
likely you have to use `sudo docker login` instead of `docker login`.


### Configuration

Configuration files are placed in `/etc/linkahead`.

#### bind_ip

In the default configuration your CaosDB instance is only available from localhost.
Use the option `bind_ip` in the profile yaml file to allow access from other computers.


## Tests

A guide on how to test linkahead, including the integration tests, can be found
in [README_TEST](README_TEST.html).


## Troubleshooting

### Docker-Compose package on debian might depend on X11

If you experience this problem:

```bash
[+] Building 0.6s (3/3) FINISHED
 => [internal] load .dockerignore                                                                 0.0s
 => => transferring context: 2B                                                                   0.0s
 => [internal] load build definition from Dockerfile                                              0.0s
 => => transferring dockerfile: 5.21kB                                                            0.0s
 => ERROR resolve image config for docker.io/docker/dockerfile:experimental                       0.6s
------
 > resolve image config for docker.io/docker/dockerfile:experimental:
------
rpc error: code = Unknown desc = error getting credentials - err: exit status 1, out: `Cannot autolaunch D-Bus without X11 $DISPLAY`
```

It helps to remove `docker-compose`:
```bash
sudo apt-get remove docker-compose
```

and install docker-compose using pip (at least version 1.27 is needed):
```bash
pip3 install --user docker-compose
```

Apparently on debian docker-compose is bundled with a package depending on X11.
See this issue for details:
https://github.com/moby/moby/issues/38948


### No DNS from within docker

On some Debian systems and with some network configurations, there
might be no DNS from within the docker processes. Thus, the build
process fails when attempting to install, e.g., git, pip, or other
packages. If this happens, try installing `docker-ce` from the
official docker repositories by adding the source to your
`sources.list`, e.g., by creating
`/etc/apt/sources.list.d/50.docker.list` containing the line

```
deb [arch=amd64] https://download.docker.com/linux/debian buster stable
```

and execute

```sh
sudo apt-get update
sudo apt-get install docker-ce
```

You may have to add the public key of the docker repository in order
to do the update:

```sh
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

See also [the official
documentation](https://docs.docker.com/engine/install/) of docker
engine.

### Libsecret is missing or unavailable

In some versions of Debian 10 and libssecret, you may experience error messages like

```
The name org.freedesktop.secrets was not provided by any .service files
```

when building LinkAhead. In these cases, installing `gnome-keyring`
[may resolve](https://github.com/microsoft/vscode-docker/issues/1515)
this by adding the missing API functions to libsecret.

### Logging into docker-hub registry

If linkahead fails to build because it cannot connect to docker.io to pull the
basic images with an error similar to

```
=> ERROR resolve image config for docker.io/docker/dockerfile:experimental 0.5s
resolve image config for docker.io/docker/dockerfile:experimental:

failed to solve with frontend dockerfile.v0: failed to solve with frontend gateway.v0: rpc error: code = Unknown desc = error getting credentials - err: exit status 1, out: Could not connect: Permission denied
```

one workaround is to execute `docker login` before building and to log in with
your docker-hub credentials (you may have to create an account first). You may
have to install `pass` and `gnupg2` (see below) for the login to work.

### Problems logging in

The `docker login` command can lead to the following or a similar exception:

```
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
** Message: 13:14:32.552: Remote error from secret service: org.freedesktop.DBus.Error.ServiceUnknown: The name org.freedesktop.secrets was not provided by any .service files
Error saving credentials: error storing credentials - err: exit status 1, out: `The name org.freedesktop.secrets was not provided by any .service files`
```

You then have to install `pass` and `gnupg2`, e.g. via:
`sudo apt install gnupg2 pass`
You should generate a gpg key and initialize the pass keyring with
`pass init <gpg-id>`. Otherwise, your password will be stored unencrypted.

### Login as "admin" not possible

If you are expecting to be able to log in as "admin" with password "caosdb" in a recent build of an
old profile, it might fail with "permission denied".

Try to adapt the `profile.yml` as also mentioned above:
- Set `local_users` to false.
- Set anonymous admin to false.

The combination of these options will trigger the creation of the admin user during the next build
of the container.

### "read: connection refused"

If you receive messages like
```
Error response from daemon: Get https://gitlab.indiscale.com:5050/v2/: dial tcp: lookup gitlab.indiscale.com on [::1]:53: read udp [::1]:52377->[::1]:53: read: connection refused
```

this could have two likely causes:
- You credentials are not setup properly or not at all
- The internet connection is broken

Tests to debug the problem:
- You can check the users docker config file (found at `~/.docker/config.json`) for the presence of
  credentials (found under key "auths"). If it's not present, it is likely that you have forgotten
  to login (see above).
- Use `docker pull gitlab.indiscale.com:5050/caosdb/src/caosdb-deploy:<TAG>` (replace `<TAG>` with
  the tag that you want to pull, e.g. 0.3-rc3) to test if it's possible to download the image. You
  can find the currently used tag either in your `profile.yml` or in
  `/usr/share/linkahead/defaults.yml`.
- If it's not possible to download the image, although your internet connection is working, it might
  be necessary to generate and login with a new authentication token.


### Problems with pip packages

In some recent versions of the docker container, installations of pip packages could fail because of
a missing `pkg-config` binary.  The obvious fix is to install the corresponding package.

Unfortunately in some containers it fails with an error messasge looking like this:
```
/opt/caosdb# apt-get install pkgconf

Reading package lists... Done
Building dependency tree
Reading state information... Done
The following NEW packages will be installed:
  pkgconf
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 0 B/66.6 kB of archives.
After this operation, 307 kB of additional disk space will be used.
debconf: delaying package configuration, since apt-utils is not installed
dpkg: unrecoverable fatal error, aborting:
 unknown system group 'postdrop' in statoverride file; the system group got removed
before the override, which is most probably a packaging bug, to recover you
can remove the override manually with dpkg-statoverride
E: Sub-process /usr/bin/dpkg returned an error code (2)
```

This is due to a missing dependency in the docker container and will be fixed in new releases
of the docker container.

An image with tag "0.3-rc4" is currently confirmed to be working.

### LinkAhead does not restart ###

Under some circumstances, LinkAhead may not restart or fail to start after rebooting the host
system.  for example, your system log way complain about `Cannot create container for service sqldb:
Duplicate mount point: /var/lib/mysql`.  Please report such issues to LinkAhead's development team.

As a workaround, stopped Docker containers may need to be removed.  Either remove all stopped Docker
containers with
`docker container prune`
or, alternatively, only remove the Docker image with the SQL server (substitute the container's name
with the name as shown by `docker container ls -a`):
`docker container rm default_linkahead_sqldb`

### Debian package doesn't build due to missing linkahead-X.Y.Z.tar.gz ###

In case of an error message like `E: missing "linkahead-0.8.0.tar.gz"`, when
building a version like `0.8.1-dev`, the corresponding section in the debian
changelog is missing. Run `debian/update_changelog.sh` to auto-generate a dummy
section (which can be removed afterwards).

### Docker image doesn't build due to missing docker-buildx

If you see an error like 

```
ERROR: BuildKit is enabled but the buildx component is missing or broken.
       Install the buildx component to build images with BuildKit:
       https://docs.docker.com/go/buildx/
```

your ditribution doesn't ship the `docker-buildx` together with the remaining
docker components and you have to install it manually (e.g., `pacman -S
docker-buildx` on Arch Linux).


### Loadfiles
The Python tool loadFiles is often used to make files mounted on the LinkAhead server accessible.
However, loadFiles requires path that exist in the Container which are not obvious to the user.
A call could look like this:
```sh
python -m caosadvancedtools.loadFiles --exclude '/opt/caosdb/mnt/extroot/GS.*/dicom'   --include '/opt(/caosdb)?(/mnt)?(/extroot)?(/GS)?.*'  /opt/caosdb/mnt/extroot/GS
```
The path `/opt/caosdb/mnt/extroot/` is predefined when using LinkAhead. This example assumes, that a folder `GS` is mounted into the server. In the include argument, folder levels need to be optional because higher levels would otherwise not be matched. 

