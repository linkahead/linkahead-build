# Docker concepts #
- Image :: Package with code, libraries, config, etc.
- Container :: A runtime instance of an image, an image that is being
    executed. Can reference other containers?
    - Docker-Compose :: Composite containers, consisting of several other
      containers.
- Dockerfile :: Configuration like parent image, working directory,
    initialization scripts, port mapping.

# Simple commands #
## Images ##
- `docker search <query>` :: search for matching images
- `docker pull <imagename>` :: download an image
- `docker image ls` :: list downloaded images
- `docker run hello-world` :: run the image `hello world`

## Containers ##
- `docker ps` :: list running containers
- `docker inspect <container>` :: show configuration

## Customization ##
- `docker exec <container> <cmd> [arg ...]` :: Call a skript from the outside.

# Networking #
By default, docker creates a bridge `docker0` and net (e.g. 172.10.0.0/16).
Host is first IP address (172.10.0.1), subsequent addresses are given to running
containers.

- `-p <host port>:<container port>` :: Maps a port on the host system to a port
  inside of the container.
- nginx-Proxy :: Reverse proxy to route connection to several services.

# Docker-Compose #
Yaml file (emacs mode: docker-compose) to combine several Docker images.

- Installation :: with `wget` or pip3 install: `pip3 install --user
  docker-compose`
- `docker-compose up` :: Start the composition.

# Deploying #
On their own machine, the customer needs to:

- Install Docker
- (Optional) Customize config file(s)
- `docker stack deploy -c docker-compose.yml caosdb`

Ideally, all the steps after installing docker should be wrapped up in an
installation script or GUI program.
