
Getting Started
===============

.. toctree::
   :maxdepth: 1
   :glob:

   getting_started/*


This is the documentation for LinkAhead. It helps you to :doc:`get started<getting_started>`.

