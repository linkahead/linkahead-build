Welcome to LinkAhead's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   quick_guide
   installation
   Configuration <configuration>
   Administration <administration/index>
   Scripting <server_side_scripting>
   Tutorials <tutorials/index>
   Related Projects <related_projects/index>
   Back to Overview <https://docs.indiscale.com/>

Innovative and customer-centric companies in particular are increasingly confronted with enormous amounts of data, data of high complexity and changing data structures. LinkAhead can help you unearth this often still hidden treasure trove of data.

The dynamic toolbox LinkAhead enables professional data management where other approaches are too rigid and inflexible. When your requirements change, due to new circumstances, upgraded equipment or evolving needs, traditional databases are costly to adapt, if possible at all. With LinkAhead, however, these adaptations are already built in by design and easily possible whenever necessary. LinkAhead thus enables professional data management in areas which previously could not benefit from it.

LinkAhead was originally developed under the name CaosDB at the Max Planck Institute for Dynamics and Self-Organization in Göttingen for the management of laboratory data, because classical databases could not meet the requirements for flexible adaptability. In 2018, CaosDB was released as open source software and has since been commercially supported by IndiScale. A ready-to-use distribution of CaosDB is offered via a subscription by IndiScale under the brand LinkAhead. In order to provide maximum reliability to customers, IndiScale performs a thorough quality assurance, for example by employing extensive automated testing. The LinkAhead subscription provides these qualities and benefits like reduced maintenance efforts to customers.

LinkAhead uses Docker to assure a reproducable execution environment. The debian package will install every thing that is necessary to start LinkAhead in   a Docker container. The Docker image that contains most of the software dependencies is not included in the Debian package but will be downloaded
by Docker on first execution.

This documentation describes how to install, configure and use LinkAhead.

Report an issue
===============

Please report all bugs, feature request, or other issues with LinkAhead itself
or this documentation by opening an issue in the `official LinkAhead issues
<https://gitlab.com/linkahead/linkahead/-/issues>`_ repository.
