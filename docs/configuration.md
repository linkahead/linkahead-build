# Configuration

LinkAhead is configured via so-called profiles.  A profile is a directory that contains
at least a `profile.yml` file. Via this file the most important configuration
options can be set.

The profile that is used when LinkAhead starts is configured in the file `/etc/linkahead/linkahead.conf`.
After a fresh installation, this is the path to the default profile
(e.g. `/usr/share/linkahead/profiles/default/profile.yml`).

You should copy the default profile to get a starting point:
```bash
cp -r /usr/share/linkahead/profiles/default ~/.config/linkahead
```
These paths are examples but may depend on your system and your preferences.

Afterwards, set the path in the configuration file `/etc/linkahead/linkahead.conf`
to your new profile.

You might want to restart LinkAhead to check if you setup the profile correctly:

```bash
   $  systemctl restart linkahead
```

## Systemd service
We already used the systemd service a couple of times. However, there is another
useful tool to interact with the LinkAhead server: The ``linkahead`` script.
You can run

```bash
   $  linkahead -h
```

to learn more about the available options.

Note, that you need to provide the path to the profile of the instance with which
you want to interact. Using the above example creating a backup would look like this:

```bash
   $  linkahead -p ~/.config/linkahead/profile.yml backup -d ~/linkahead_backup
```


## Restore data
LinkAhead allows to restore data during start up. This can be enabled with the
`restore: true` option in the `profile.yml`. Corresponding files need to be
placed in `custom/other/restore/` in the profile directory. A typical use case is
to place the files created with `linkahead backup` here.

To disable restoring, set the option to `false` and move the `custom/other/restore/`
folder to some other location.

## SSL Encryption
In order to use HTTPS (which is strongly recommended) you need to provide a
certificate that is used by the server.

### Creating the Java Key Store
The LinkAhead server expects a Java Key Store named `caosdb.jks`. In the following
we will distinguish two cases:

1.  You have a valid certificate that you want to provide.
2. You want to create and use a self-signed certificate.

Note, that the first option is strongly recommended (ask you IT-department, if
you do not know how to get a valid certificate).

#### Option 1: Using an existing certificate

Suppose you have a certificate and private key file named  `fullchain.pem`  and
`privkey.pem`, respectively, you can create a key store from those files.

Note that the full certificate chain should be included into the `fullchain.pem`, not only the server certificate itself.

During the execution of the following two commands you will be asked to supply a password with at least six characters.
Use the **same password for both commands** and note that you will need it in a later step again.

```bash
openssl pkcs12 -export -inkey privkey.pem -in fullchain.pem -out all-certs.pkcs12
keytool -importkeystore -srckeystore all-certs.pkcs12 -srcstoretype PKCS12  -deststoretype pkcs12 -destkeystore caosdb.jks
```

`keytool` is provided by `openjdk-14-jre-headless`, for example.

#### Option 2: Creating a self-signed certificate.
In order to create a self-signed certificates (not recommended for production use) you can do (You can replace `localhost` by your host name):

```
mkdir certificates; cd certificates
keytool -genkey -keyalg RSA -alias selfsigned -keystore caosdb.jks -validity 375 -keysize 2048 -ext san=dns:localhost
keytool -importkeystore -srckeystore caosdb.jks -destkeystore caosdb.p12 -deststoretype PKCS12 -srcalias selfsigned
openssl pkcs12 -in caosdb.p12 -nokeys -out caosdb.cert.pem
```

During the execution of the following two commands you will be asked to supply a password with at least six characters.
Use the **same password for both commands** and note that you will need it in a later step again.

The last command exports only the public part of the certificate.
The resulting `cert.pem` can safely be given to users to allow SSL verification.

### Using the Java Key Store

Place the resulting file `caosdb.jks` in the profile directory under the path
`custom/other/cert/caosdb.jks`.
All certificate files should be readable/accessible only by the user that runs the LinkAhead
server.

The password to the certificate needs to be supplied to the LinkAhead server
via a configuration file. For example, create a file `custom/caosdb-server/conf/ext/server.conf.d/90-keystore.conf` with the content

```
CERTIFICATES_KEY_PASSWORD=<password>
CERTIFICATES_KEY_STORE_PASSWORD=<password>
```
where you replace `<password>` with the password that you used when creating the key store.

Now, you can restart LinkAhead: `sudo systemctl restart linkahead`. If you used
Option 1 above with a certificate that is signed by a known Certificate Authority,
you can now access LinkAhead with a browser (Unless you changed the port:
`https://domain:10443`) and SSL will work fine.

If you used a self-signed certificate, the browser will warn you that
`The certificate is not trusted because it is self-signed.`.
<!--TODO how to install cert for firefox -->

In order to allow the Python client to use SSL encryption with a self-signed certificate,
you need to supply the public part, that you created above: `caosdb.cert.pem`.
Provide the path to that file in the configuration file of the client `pycasodb.ini`:
`cacert=/path/to/caosdb.cert.pem`

### Troubleshooting

#### Check what certificate is delivered by the server
In order to exclude the possibility that the configuration did not go well
(wrong filename, directory, ...), you should check what certificate is provided
by LinkAhead. When you open the URL with a browser and get a Security Warning,
ask the browser to show the certificate (Firefox: Click on 'Advanced...' and
then 'View Certificate' below the Error Code). If it simply says 'localhost'
for Subject Name without any further Information like organization or state,
it is probably the default self-signed certificate that is served by LinkAhead
and the configuration is incorrect. Check paths and naming of files carefully.

#### Server cannot be reached
If for example a browser cannot establish a connection to the LinkAhead server,
check the logs (e.g. `journalctl -u linkahead -b`).
If you see the line `java.io.IOException: keystore password was incorrect`
then the password to access the Java Key Store supplied to LinkAhead is incorrect.
Check whether the file containing the password is in the server configuration folder and
whether the password is spelled correctly.

#### Clients cannot connect
Make sure that your clients use the fully qualified domain name (e.g. `linkahead.example.com`) in their configuration. The name must
equal the one of the certificate.

You may need to add the certificate of the CA (Certificate Authority) in the configuration of your client, if shipped with your operating system.

## Proxy

If your host accesses the internet via a proxy, the host's
`HTTP_PROXY`, `HTTPS_PROXY`, `FTP_PROXY`, and `NO_PROXY` environment
variables are transferred to the docker container (if running
LinkAhead as a systemd service, make sure that the service
[knows](https://www.freedesktop.org/software/systemd/man/latest/systemd.service.html#Command%20lines)
about these variables). Alternatively, you can specify the
corresponding `http_proxy`, `https_proxy`, `ftp_proxy`, and `no_proxy`
profile options which can also be used to overwrite the host's
environment variables if necessary. E.g., if the host has
`HTTP_PROXY=http://example.com:80`, setting `http_proxy: ""` in your
`profile.yml` will result in no http proxy within the docker container
(i.e., `HTTP_PROXY=""` within the container).

## Accessibility
By default, the server will only listen on the loopback device (`localhost`).
You can expand this by setting the `bind_ip` value in the `network` section of
`profile.yml`. Specify the IP of the desired interface or use an empty string ""
(equivalent to `0.0.0.0`) to listen on all interfaces.

## Envoy as reverse proxy

Envoy is needed in order to use components of the web interface that need to access the rather new
GRPC API. Additionally, there are some further benefits from using a reverse proxy:

- You can let Envoy do SSL termination.  Thus you do not need to convert certificate files to the
  Java Key Store format any more, that is needed by the Java server.
- All LinkAhead resources can be served under one port and Envoy distributes requests accordingly.

Envoy can be enabled by setting `envoy_enabled: true` in `profile.yml`.  This will start an envoy
image together with LinkAhead.

## Graphical Access Control Management

The WebUi comes with a graphical Access Control Management (ACM) module. This is currently
(2023-02-22) not enabled by default. To enable the ACM, set `envoy_enabled: true` in your
`profile.yml` and add `BUILD_MODULE_EXT_ENTITY_ACL=ENABLED` in a file in
`custom/caosdb-server/caosdb-webui/build.properties.d`.

## Switching profiles ##
LinkAhead stores data persistently using Docker volumes. If you switch the profile and the
new profile uses the same volumes, then changes are applied there and if the profile
is set to restore an old data set, this will **overwrite** the data in the volume.

You can create a backup before switching such that you do not care about the data in
the Docker volume. Or you can set the most top level key in the `profile.yml` of the profile that you want to switch to (by default it is
`default`) to something else. You should then stop your first instance and start the second.


## Standard profiles ##

You can try out a standard profile by stopping your current server (after creating a backup
if it contains something that should not get lost) and calling ``linkahead`` with the ``-p`` argument.

```bash
   $  linkahead -p /usr/share/linkahead/profiles/demo/profile.yml  start
```


The standard profiles in LinkAhead are:

### default ###

The `default` profile simply uses the defaults.  This means, above all: no anonymous user,
persistent data, and authentication to the IndiScale server must be provided to obtain the Docker
images.  This profile is a solid base to set up your own configuration in a production environment.

### debug ###

The `debug` profile is useful for writing scripts and tests, and for debugging.  Debugging in the
server is enabled, the anonymous user is disabled and an *empty* database is loaded on every
restart.

### demo ###

The `demo` profile is the one which is used on the demo server at [https://demo.indiscale.com](https://demo.indiscale.com).
It comes with customized content both for the database content and the web frontend, to provide user
interfaces and content for the demo tour.  Because the server is restarted periodically, the profile
is set to restore its content upon startup.  The anonymous user is enabled.
