# How to build the deb package #

## Requirements ##
- debmake (for `debmake`)
- devscripts (for `debuild`)

```
sudo apt-get install debmake devscripts
```

Also the dependencies of caosdb-deploy must be installed for building the Debian package.

One way to get an environment which fulfills all these requirements is using the `Dockerfile` from
`.docker/deb_make`.

## Build ##

```
cd debian
./build_deb.sh
```

# Test deb package #

- Start VirtualBox with Debian installed. (Mount directory with deb there)
```sh
dpkg -i /mnt/share/linkahead_0.1-1_all.deb
apt-get install -f  # to install the missing dependencies, around 350 MB
systemctl status linkahead
```

# Important reads #

- [Guide for Debian Maintainers](https://www.debian.org/doc/manuals/debmake-doc/index.en.html)
  - Its predecessor is the [Debian New Maintainer's Guide](https://www.debian.org/doc/manuals/maint-guide/)
- [Debian Policy Manual](https://www.debian.org/doc/debian-policy/)
- [Debian Developer's Reference](https://www.debian.org/doc/manuals/developers-reference/)

# General hints #

## Dependencies ##

- We should probably use libs from other packages, wherever possible.  [Embedded
  code copies](https://www.debian.org/doc/debian-policy/ch-source.html#embedded-code-copies) should be prevented.  (~P 3)

## Package building ##

- `quilt` for patch management
  (https://www.debian.org/doc/manuals/maint-guide/modify.en.html#quiltrc)
- `dch` to modify the package changelog.
- `fakeroot ./debian/rules install` to call `make install` and more?
- `debuild` builds and checks a package (with `lintian`)
- `debsign` to sign packages
- `pbuilder` provides a clean room (chroot) build environment

### Patches / Quilt ###

`quilt` is the patch manager used by Debian.

#### Configuration ####

There is an [online manual](https://www.debian.org/doc/manuals/debmake-doc/ch03.en.html#quilt-setup) with a customized configuration which makes working with quilt for
package creation a walk in the path.  In short, the configuration consists of this
`~/.quiltrc-dpkg`:
```sh
# -*- mode:sh; -*-

# Find directory with "debian" subdirectory
d=.
while [ ! -d $d/debian -a $(readlink -e $d) != / ]; do
    d=$d/..
done
if [ -d $d/debian ] && [ -z $QUILT_PATCHES ]; then
    # if in Debian packaging tree with unset $QUILT_PATCHES
    QUILT_PATCHES="debian/patches"
    QUILT_PATCH_OPTS="--reject-format=unified"
    QUILT_DIFF_ARGS="-p ab --no-timestamps --no-index --color=auto"
    QUILT_REFRESH_ARGS="-p ab --no-timestamps --no-index"
    QUILT_SERIES_ARGS="--color=auto"
    QUILT_COLORS="diff_hdr=1;32:diff_add=1;34:diff_rem=1;31:diff_hunk=1;33"
        QUILT_COLORS+=":diff_ctx=35:diff_cctx=33"
    if ! [ -d $d/debian/patches ]; then
        mkdir $d/debian/patches
    fi
fi
```

Then these lines should be called (or added to your startup scripts):
```sh
alias dquilt="quilt --quiltrc=${HOME}/.quiltrc-dpkg"
complete -F _quilt_completion -o filenames dquilt
_completion_loader quilt
```

#### The basics ####

Quilt comes with a generic [manual](file:///usr/share/doc/quilt/quilt.html), and the Debian maintainers guide also has a [section](https://www.debian.org/doc/manuals/debmake-doc/ch04.en.html#dquilt)
about the specific usage for package building. There is also a [wiki article](https://wiki.debian.org/UsingQuilt) about using
dquilt.

To create a new patch containing changes of file `somefile` do the following:
- `cd linkahead-X.X` Here is the `.pc` directory.
- `dquilt new 0XX-name.patch` Add new patch to stack
- `dquilt add somefile` Add file to be governed by patch
- change `somefile` the way you want
- `dquilt refresh` Reflect changes to `somefile` in patch
- `dquilt header -e --dep3` Modify header in editor

- `dquilt series` The series of patches.  Applied patches are green, the current one is brown,
  unapplied patches are in the default color.
- `dquilt push` Apply next patch in the series (push to stack), with `-a` to apply all.
- `dquilt pop` Unapply the topmost patch from the stack.

Make sure to update the files in `debian/debian_files/patches/` after you changed patches:
`rm debian/debian_files/patches/* && cp debian/linkahead/linkahead-0.3-rc/debian/patches/* debian/debian_files/patches/`

#### Managing later changes to original files ####

Patches may fail to apply if the original file has been changed.  In this case, the `dquilt push`
command fails and suggests to be run with `-f`.  If this is sufficient (the patch still can be
applied forcefully), the following commands should be sufficient:
- Run `./prepare_deb.sh`
- Go into the "versioned" LinkAhead folder `cd linkahead/linkahead-0.x.y`
- `dquilt push` This command is assumed to fail.
- `dquilt push -f` Try if this works (Output: `(forced; needs refresh)`).  Look at the rejected
  changes in `foo.rej` for the rejected changes and apply them manually to the resulting file `foo`
  if necessary.
- `dquilt refresh` This refreshes the patch to reflect the changes.
- If lines have changed (`offset X lines` in output of `dquilt push`), it may make sense to run
  `dquilt pop -a --refresh` to refresh all patch files.
- Copy new patch files back to `debian/debian_files/patches/`.

## Maintainer scripts ##

Scripts which are called before/after installation/removal.
- Must be idempotent, also sould continue at an appropriate stage if stopped
  during previous run.
- Should handle non-interactive environment, if at all possible.
- Are called with arguments describing the current (de)installation state.
  There are many such states!

## User management ##

- If needed, UID/GID will be dynamically from the range 100-999, via `adduser
  --system`.

## Scripts ##

- Service scripts must be backwards-compatible with SysV init.
- Check scripts with `checkbashisms`.

## Configuration ##

- Config files go to `/etc/caosdb/` (if more than one).
- See also `conffile`, config files which are specially supervised by Debian,
  for changes etc., xor use maintainer scripts.

# Error handling #

## Quilt / patching fails ##

Example error output:
```
patching file utils/install/linkahead.conf
Hunk #1 FAILED at 2.
1 out of 1 hunk FAILED
dpkg-source: info: the patch has fuzz which is not allowed, or is malformed
dpkg-source: info: if patch '000-conf.patch' is correctly applied by quilt, use 'quilt refresh' to update it
dpkg-source: error: LC_ALL=C patch -t -F 0 -N -p1 -u -V never -E -b -B .pc/000-conf.patch/ --reject-file=- < linkahead-0.1.orig.dZcUbe/debian/patches/000-conf.patch subprocess returned exit status 1
dpkg-buildpackage: error: dpkg-source -b . subprocess returned exit status 2
debuild: fatal error at line 1182:
dpkg-buildpackage -us -uc -ui failed
```

The set of patches may need to be updated if the original files change.  If manually changing the
patch file is sufficient, this is the way to go.
