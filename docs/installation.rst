Installation
============

You should have received access to the Debian package (for example ``linkahead-0.11.1.deb``) via
your subscription or from the download page.  For easier maintenance, LinkAhead uses Docker for
deployment. The Debian package does however not contain the Docker image (since the image is quite
large) and the image will be downloaded during the installation process from a docker registry. If
you have a subscription, the installer will ask you for the credentials to the docker registry.

On a Debian or Ubuntu system, install the package with: ``sudo apt install ./linkahead-0.11.1.deb``
(replace the file name with your exact version).

.. warning::

   ``apt`` may inform you about packages that can be removed now.  Note that there is `a bug in
   older Ubuntu versions <https://bugs.launchpad.net/ubuntu/+bug/1955047>`_ which may lead to
   inadvertently removing your graphics drivers when you follow that suggestion.

The installation procedure will ask if you have a subscription and may ask for your LinkAhead
subscription credentials (user name and token). It will automatically install, enable and start
LinkAhead as a ``systemd`` service, so that LinkAhead will be started automatically on reboot in the
future. You can also start the LinkAhead service manually using

.. code:: console

   $ systemctl start linkahead.service

To shut down LinkAhead, analogously type ``systemctl stop linkahead.service``.
   
.. note::

   Instead of a systemd service, LinkAhead can also be started completely
   manually. In that case, the path to the profile (see :doc:`Configuration
   <./configuration>`) and further options have to be given manually. The
   command to start LinkAhead then is ``linkahead -p path/to/profile.yml
   start``; it is stopped with ``linkahead -p path/to/profile.yml stop``.

Typically, the required docker image will be downloaded on first startup, if it does not yet exist
on the system. This may take a while. You can monitor the progress of the download and the
subsequent start with:

.. code:: console

   $ journalctl -u linkahead -f

Once the log output says ``Starting org.caosdb.server.CaosDBServer application`` the installation is
done and LinkAhead is running with the default configuration. You can check whether the LinkAhead
server can be accessed by sending a curl request on the same machine:

.. code:: console

   $ curl -k https://localhost:10443
   <?xml version="1.0" encoding="UTF-8"?>
   <?xml-stylesheet type="text/xsl" href="https://localhost:10443/webinterface/1614339482/webcaosdb.xsl" ?>
   <Response srid="11883904-301d-4bd9-8f74-86d95837239b" timestamp="1614339817505" baseuri="https://localhost:10443">
     <UserInfo>
       <Roles>
         <Role>anonymous</Role>
       </Roles>
     </UserInfo>
   </Response>

If the command shows a similar empty reply to the one above, the installation was successful and you
can continue with the configuration.

Requirements
------------

Requirements are installed automatically. You can list them via
``apt-cache depends linkahead``

Configuration
-------------

The path to the *profile* with which LinkAhead is started can be set in
``/etc/linkahead/linkahead.conf``. The default profile is located at
``/usr/share/linkahead/profiles/default/profile.yml``. See :doc:`Configuration <./configuration>`
for more details on how to configure your profile.

Troubleshooting
---------------

Unable to fetch some archives
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If apt finishes with this error, run ``apt update`` and try the installation again.

Old data still exists
~~~~~~~~~~~~~~~~~~~~~

Data is kept in persistent docker volumes. You can remove existing data
the following way:

.. warning::
   This cannot be undone. Make sure you have backups.

Have a look at the docker volumes: ``docker volume ls``. The name
depends on your profile, but typically you want to remove the following
two: ``docker volume rm default_caosdb-sqldata default_caosdb-caosroot``

Strange characters in my browser
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you see strange characters in your browser when trying to access LinkAhead, you should explicitly
specify https, like so: https://localhost:10443 If you try to access LinkAhead via plain http
(``http://localhost:10443``), your browser will receive encrypted content and display it like this:

.. image:: img/screenshot_plain_http.png
  :align: center
  :alt: Screenshot of what a browser may show if you try to access LinkAhead via plain http.



docker-compose is not recent enough
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can install an up-to-date version with (at least 1.27 is required):

.. code:: console

   $ pip install --user docker-compose

Extroot from NFS mounts fails
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If LinkAhead fails to start when trying to use an NFS mount as extroot, possibly with errors like
``Cannot create container for service caosdb-server: invalid mount config for type "bind": stat
/data/foo/bar: permission denied``, then restrictive access permissions may be the reason.  In this
example, the directory ``/data/foo/bar`` could not be accessed, because the mounted NFS resource
``/data/foo/`` was missing permissions.

As a workaround, you could mount ``/data/foo/bar`` directly instead of relying on the ``/data/foo``
ressource:

.. code-block:: cfg
   :caption: NFS line for ``/etc/fstab``

   server:/volume/foo/bar    /data/foo/bar    nfs   defaults    0 0

Or you could create a local bind mount which exposes ``/data/foo/bar``

.. code-block:: console
   :caption: Mount ``/data/foo/bar`` to ``/my_extroots/bar``, where ``/my_extroots/`` should be
             readable to root.

   # mount --bind /data/foo/bar /my_extroots/bar



The screen is black after the next reboot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is not a bug in LinkAhead, but may be because you followed ``apt``'s recommendation to
automatically remove all packages which are deemed unnecessary by the system.  This may remove your
graphics drivers.  It is a `known issue in Ubuntu 20.04
<https://launchpad.net/ubuntu/+bug/1955047>`_ and will hopefully be fixed soon.  In the meantime you
may want to `manually reinstall your graphics driver
<https://help.ubuntu.com/community/BinaryDriverHowto/>`_.

..  LocalWords:  extroot
