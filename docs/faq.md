- I cannot login in using the WebUI. What is wrong?
  Users can be authenticated using the SQL backend or using PAM.

  If you are using a user that should be authenticated via PAM and NIS is used,
  try the following steps (inside the docker container):
  1. Can you authenticate on the host (`su <username>`)
  2. Is NIS bound correctly (`ypbind`)
  3. Check whether you can retrieve passwords via `ypcat passwd`
  4. Check `getent passwd` which checks local settings and NIS

  If you are using PAM + LDAP instead of NIS try the following steps (inside the
  docker container):
  1. Check `getent passwd` which checks local settings and LDAP
  2. Can you authenticate on the host (`su <username>`)
  3. Check the debugging output of nslcd: `/etc/init.d/nslcd stop && nslcd -d`
     while trying to `getent passwd` again.
  4. Can you ldapwhoami? (`ldapwhoami -x -H "ldap[s]://<ldap-server>" -D <bind-dn> -W`)

  If you are using the `ldap_authentication.sh` script (in your
  `usersources.ini`) try the following steps (inside the docker container):
  1. Can you ldapwhoami? (`ldapwhoami -x -H "ldap[s]://<ldap-server>" -D <bind-dn> -W`)
  2. Try out the script manually
     `/opt/caosdb/git/caosdb-server/misc/pam_authentication/ldap_authentication.sh
     <username>`.

