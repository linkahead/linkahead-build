# User Authentication with Ldap

There are three ways to configure the user authentication with LDAP.

## Option 1: Using the host's SSSD configuration

As of 2024-08 and LinkAhead >= 0.15, this is probably the most
convenient option if you are using LinkAhead on a system that has SSSD
configured, although option 2 and option 3 are still
supported. However, using the host's sssd config (see, e.g.,
https://jhrozek.wordpress.com/2015/03/31/authenticating-a-docker-container-against-hosts-unix-accounts/
and
[linkahead-control#1](https://gitlab.com/linkahead/linkahead-control/-/issues/1)),
allows to authenticate against any database that is supported by the
host.

For this, you can simply set `sssd` profile option to true, and,
optionally, configure the location of the host's config via the
`sssd_config_path` variable if your host uses any other path than
`/var/lib/sss/pipes`.

Afterwards, the
[usersources.ini](https://docs.indiscale.com//caosdb-server/administration/configuration.html#further-configuration-files)
can be used to add users and, possibly, groups although the latter is
not tested at the moment.

## Option 2: PAM + LDAP

This is the more versatile setup. It support user authentication via name and
password and also can assign POSIX groups to the users. This is necessary if
you want to use the `include.group`, `exclude.group`, `group.<group
name>.roles` options in your `usersources.ini`.

### Requirements

This setup requires the LDAP server to provide objects the `objectClass:
posixAccount` and `objectClass: posixGroup` for the `LDAP -> POSIX` mapping and all
relevant LDAP attributes for those object classes. See
[ldapwiki.com](https://ldapwiki.com/wiki/POSIX)

Otherwise the LDAP client inside the LinkAhead container cannot identify the
LDAP objects as POSIX users or groups and subsequently PAM cannot include those
users or groups.

#### OpenLDAP

The OpenLDAP server supports the POSIX attributes out of the box and POSIX
users and groups can be managed easily with front-ends like
[phpLDAPadamin](http://phpldapadmin.sourceforge.net/wiki/index.php/Main_Page).

#### MS Active Directory

In MS Active Directory servers the POSIX attributes are handled by the
[Identity Management for UNIX](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/cc772571%28v=ws.11%29)
extension. Unfortunately, this extension is **deprecated** since Windows Server
2008 R2 and has been removed after Windows Server 2012 R2 (-- Maybe they want
you to buy more MS software instead...)

When the *Identity Management for UNIX* extension cannot be used you have to
revert to [Option 3: ldap_authentication.sh](#option-3-ldap_authenticationsh).

### Configuration

To activate the PAM + LDAP setup, set the `conf.ldap` option to `true` in your
`profile.yml`.

The configuration for the LDAP client is to be located in your profiles custom directory at
`custom/other/nslcd.conf`. This file must not be readable by anyone but the owner, so the permission
should be set with `chmod go-r nslcd.conf`. LinkAhead uses [nslcd](https://linux.die.net/man/8/nslcd) as LDAP client and the
configuration must contain at least:

	# The location at which the LDAP server(s) should be reachable.
	uri <ldap://ldap-service/>

	# The search base that will be used for all queries.
	base <dc=example,dc=org>

See [man nslcd.conf](https://linux.die.net/man/5/nslcd.conf) for more
information and have a look at the [Configure LinkAhead for
LDAP](#configure-linkahead-for-ldap).

### Example with OpenLDAP

#### OpenLDAP server

Start an OpenLDAP server, e.g. using this script:

```
#!/bin/bash -e
docker run --name ldap-service --hostname ldap-service --detach osixia/openldap:1.1.8
docker run --name phpldapadmin-service --hostname phpldapadmin-service --link ldap-service:ldap-host --env PHPLDAPADMIN_LDAP_HOSTS=ldap-host --detach osixia/phpldapadmin:0.9.0

PHPLDAP_IP=$(docker inspect -f "{{ .NetworkSettings.IPAddress }}" phpldapadmin-service)

echo "Go to: https://$PHPLDAP_IP"
echo "Login DN: cn=admin,dc=example,dc=org"
echo "Password: admin"
```

The `admin` user with password `admin` is available and can be used with tools
like [phpLDAPadamin](http://phpldapadmin.sourceforge.net/wiki/index.php/Main_Page)
or [ldap-utils](https://wiki.debian.org/LDAP/LDAPUtils) for administration.
Please use the Distinguished Name (DN) `cn=admin,dc=example,dc=org` with these
tools.

Insert some test data into LDAP server, e.g. a user `anton` with password
`anton` who is member of the group `group1` and a user `berta` with password
`berta` who is member of the group `group2`.

The ldif representation of the LDAP database should look like this:

```ldif
# cn=anton,dc=example
dn: cn=anton,dc=example,dc=org
cn: anton
gidnumber: 500
objectclass: inetOrgPerson
objectclass: posixAccount
objectclass: top
structuralobjectclass: inetOrgPerson
uid: anton
uidnumber: 1000
# userpassword: anton
userpassword: {MD5}eEdCpmo6DCcf7O1bFJ/42w==

# cn=berta,dc=example,dc=org
dn: cn=berta,dc=example,dc=org
cn: berta
gidnumber: 501
objectclass: inetOrgPerson
objectclass: posixAccount
objectclass: top
structuralobjectclass: inetOrgPerson
uid: berta
uidnumber: 1001
# userpassword: berta
userpassword: {MD5}Q9dQNflJ17rWY4O0Y3TS4g==

# cn=group1,dc=example,dc=org
dn: cn=group1,dc=example,dc=org
cn: group1
gidnumber: 500
memberuid: anton
objectclass: posixGroup
objectclass: top
structuralobjectclass: posixGroup

# cn=group2,dc=example,dc=org
dn: cn=group2,dc=example,dc=org
cn: group2
gidnumber: 501
memberuid: berta
objectclass: posixGroup
objectclass: top
structuralobjectclass: posixGroup
```

#### Configure LinkAhead for LDAP

1. Add or set `ldap: true` in your `profile.yml`
	```yaml
	# minimal profile.yml
	default:
	  conf:
		ldap: true
	```

2. Add the `nslcd.conf` to your profile's custom directory at
	`custom/other/nslcd.conf`:

	```pacmanconf
	uri ldap://ldap-service
	base dc=example,dc=org

	binddn cn=admin,dc=example,dc=org
	bindpw admin
	```

	Note: The `binddn` must be a user who has sufficient read permissions for the
	LDAP server in order to fetch the (hashed) passwords into the docker PAM.
	If the LDAP server allows anonymous lookups this can be omitted.

	Note: This setup doesn't use TLS. See [man
	nslcd.conf](https://linux.die.net/man/5/nslcd.conf) for more information
	about TLS.

2. Copy this `usersources.ini` to your profile's custom dir at
	`custom/caosdb-server/conf/ext/usersources.ini`:

	```ini
	# usersources.ini
	realms = PAM
	defaultRealm = PAM

	[PAM]
	class = org.caosdb.server.accessControl.Pam
	pam_script = ./misc/pam_authentication/pam_authentication.sh
	default_status = ACTIVE

	include.group = group1
	group.group1.roles = administration
	```

	This configures LinkAhead to include `anton` (because he's a member of
	`group1`) but exclude `berta` (because she is not). `anton` is being
	assigned the `administration` role.

#### Start LinkAhead

1. Start your LinkAhead via

	```console
	$ linkahead -p <path to your profile.yml> start
	```
2. Connect LinkAhead with the LDAP Server via

	```console
	$ docker network connect default_caosnet ldap-service
	```

3. Go to the webinterface or use another client and login successfully as
   `anton` with password `anton`. `berta` with password `berta` should fail,
   because she is not a member of `group1`.


## Option 3: ldap_authentication.sh

This option does not require any of the POSIX attributes. On the downside,
groups cannot be be identified by this method and thus none of the
`include.group`, `exclude.group`, `group.<group name>.roles` options in your
`usersources.ini` will work.

Additionally, the local users of the docker container (e.g. the `admin`
user) can not be used anymore.

This setup has been tested with an MS Active Directory Service without the
*Identity Management for UNIX* extension.

The authentication uses plain text passwords, SASL is not supported at this
moment, so it is highly recommended to use TLS.

### Configuration

The `conf.ldap` option in your `profile.yml` is not relevant here. Instead we
sneak the `ldap_authentication.sh` script into the PAM-Setup of the server:

Just replace the `PAM.pam_script` option of your `usersources.ini` like this:

```ini
[PAM]
pam_script = ./misc/pam_authentication/ldap_authentication.sh
```

And put the configuration to your profiles custom directory at
`custom/caosdb-server/misc/pam_authentication/ldap.env`. See the [Example with
ldap_authentication.sh and
OpenLDAP](#example-with-ldap_authenticationsh-and-openldap).

The full documentation of the `ldap.env` can be found in the [caosdb-server
repository](https://gitlab.com/caosdb/caosdb-server) at
`misc/pam_authentication/ldap.env`.

### Example with `ldap_authentication.sh` and OpenLDAP

Please setup the [OpenLDAP Server](#openldap-server) as described above.

#### Configure LinkAhead

1. Copy this `usersources.ini` to your profile's custom dir at
	`custom/caosdb-server/conf/ext/usersources.ini`:

	```ini
	# usersources.ini
	realms = PAM
	defaultRealm = PAM

	[PAM]
	class = org.caosdb.server.accessControl.Pam
	pam_script = ./misc/pam_authentication/ldap_authentication.sh
	default_status = ACTIVE

	include.user = anton
	user.anton.roles = administration
	```


2. Copy this `ldap.env` to your profile's custom dir at
	`custom/caosdb-server/misc/pam_authentication/ldap.env`:

	```bash
	# ldap.env
	export LDAPURI="ldaps://ldap-service"
	export USER_BASE="dc=example,dc=org"
	```

#### Start LinkAhead

1. Start your LinkAhead via

	```console
	$ linkahead -p <path to your profile.yml> start
	```
2. Connect LinkAhead with the LDAP Server via

	```console
	$ docker network connect default_caosnet ldap-service
	```

3. Go to the webinterface or use another client and login successfully as
   `anton` with password `anton`. `berta` with password `berta` should fail,
   because she is not included in the `usersources.ini`.
