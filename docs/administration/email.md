
# Email Notifications

The LinkAhead Docker image comes with a Postfix mail server. The typical use case is
that emails are sent to a relayhost (and the LinkAhead server's IP is white listed
on that host). In order to use it, you need to `mail: true` in the `profile.yml`
and provide a configuration file (see below).

The configuration file `main.cf` needs to be placed in the profile's custom folder:
`custom/other/main.cf`

The following shows an example configuration. Please replace at least the
values of `myhostname` and `relayhost`.

```unixconfig
# Debian specific:  Specifying a file name will cause the first
# line of that file to be used as the name.  The Debian default
# is /etc/mailname.
#myorigin = /etc/mailname

smtpd_banner = $myhostname ESMTP $mail_name (Ubuntu)
biff = no

# appending .domain is the MUA's job.
append_dot_mydomain = no

# Uncomment the next line to generate "delayed mail" warnings
#delay_warning_time = 4h

readme_directory = no

# TLS parameters
smtpd_tls_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem
smtpd_tls_key_file=/etc/ssl/private/ssl-cert-snakeoil.key
smtpd_use_tls=yes
smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache

# See /usr/share/doc/postfix/TLS_README.gz in the postfix-doc package for
# information on enabling SSL in the smtp client.

myhostname = full.domain.name.de
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
myorigin = $myhostname
mydestination = $myhostname, localhost
relayhost = [relay.host.de]:25
mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
mailbox_size_limit = 0
recipient_delimiter = +
inet_interfaces = loopback-only
```

You can test those settings by logging into the Docker container

```sh
docker exec -ti -u 0:0 linkahead bash
```

and send a mail. The content of the mail can be entered after submitting the following command. The
text is finished with providing a line that contains only a "." (dot).

```sh
sendmail  -f fromaddress@example.com toaddress@example.com
```

## Debugging

You can make postfix write logs to a file with (inside Docker container):

```sh
postfix stop
postconf maillog_file=/var/log/postfix.log
postfix start
```

## See also

There is extensive [online documentation](https://www.postfix.org/BASIC_CONFIGURATION_README.html) about Postfix configuration.
