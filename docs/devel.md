# Development and debugging rules, tips and tricks #

## Connecting to containers and services ##

- Start a shell inside the CaosDB container:  
  `docker exec -ti compose_caosdb-server_1 /bin/bash`
  - As root: `docker exec --user 0:0 -ti compose_caosdb-server_1 /bin/bash`
- Connect to the SQL database (from inside the caosdb-server container):  
  `mysql -h sqldb -u caosdb -prandom1234 caosdb`

## Setting up a Debian caching proxy for apt ##

A proxy which caches access to the apt repositories can speed up Docker building a lot, especially
on slow an/or unreliable internet connections.

LinkAhead by default looks for an HTTP proxy server running on the Docker host which listens on port
8123.

**Developer's note:** We chose port 8123 in order to keep the default port 8000 free for other
applications which use it by default.

### Squid ###

An easy way to set up a server is to use Squid, especially with the default settings as packaged on
Debian and Ubuntu in the `squid-deb-proxy` package:  
`apt install squid-deb-proxy`

After installation, edit `/etc/squid-deb-proxy/squid-deb-proxy.conf` and change the port to `8123`.

If you are paranoid, you may restrict the allowed networks to `172.17.0.0/24` in
`/etc/squid-deb-proxy/allowed-networks-src.acl`.
