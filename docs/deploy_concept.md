# Concept #
1. Docker images for
   - MariaDB (from upstream)
     - Has the root password set.
     - Does not yet create a caosdb user and database.
       - This might change in the future, but keeps compatibility with the
         current SQL setup process.
   - CaosDB server (initially always with web ui content)
   - Envoy (for routing)
2. Docker-compose file with basic configuration of the images.
   1. SQL server starts as plain container.
   2. CaosDB starts with wrapper script which does in a loop:
      1. Wait for the SQL server to be available
      2. Check if SQL server is configured correctly, configures it if not.
      3. Start CaosDB
3. Script to automatically fill the Docker compose file with configuration and
   to create the containers from it.
4. Startup service for systemd.
5. All of the above packaged into a .deb package.

# Envoy #
TODO: continue

Envoy allows serving GRPC and XML API under the same port.
It can be enabled by setting `envoy_enabled: true` in the profile in 
which case an envoy image is started together with
LinkAhead. The envoy config offers the basic prerequisites to be used together
with WebGRPC modules. It also serves a default configuration that allows to
serve a wiki under `/webinterface/wiki` and the ACM module under
`/webinterface/acm`. Note that these modules still have to be compiled manually
and copied to `profile/custom/caosdb-server/caosdb-webui/src/ext/include` for
the time being.

# Authentication and permissions #

- If `local_users` is set to `true` in the profile in Linux, the local user and password database
  will be included into the Docker container, so that users can authenticate themselves with their
  usual password.
  - In this case, also the anonymous user will have full administrative permissions, so starting to
    use LinkAhead is as simple as possible.
