<!-- 
** header v3.0
This file is a part of the CaosDB Project.

Copyright (C) 2019 IndiScale GmbH <info@indiscale.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

** end header

Copyright 2019 by Daniel Hornung
-->

# The Big Picture #
The CaosDB application is split into one image for the SQL server and one for
the CaosDB server itself.  Both communicate with each other in a network set up
by Docker Compose.

## Images ##
- SQL :: The SQL image is based on an upstream MariaDB image, additional setup
  of user and database schema are performed later in the build process.
- CaosDB :: The CaosDB image is based on Debian Stable.  The build process adds
  the necessary packages and downloads and builds the CaosDB server, including
  the WebUI.

## Composition ##
Docker Compose starts a container for each image and configures the runtime
images for matching settings.

### Customization ###
- Passwords :: At the moment there are default passwords for access to the SQL
  server, but it is planned that they be replaced by random-generated password
  for each `docker-compose` instantiation.  This should not be of utmost
  importance if it is possible to separate the SQL server from the rest of the
  host system on a network level.
- Permanent data :: Data that is to remain beyond destruction of the containers
  is stored outside, linked inside the containers by volumes.
