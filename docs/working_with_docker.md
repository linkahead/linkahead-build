# How to set up and use Docker images #

## Setting up the prerequisites ##
On Ubuntu 18.04 LTS (the only system I tested so far), do:

- Install: `apt-get install docker.io`
- Start & enable service: `systemctl start docker; systemctl enable docker`
- Add the user to the `docker` group: `usermod -aG docker USER`
  - Log in anew as the user, alternatively do `su USER`

### Known issues ###

#### Ubuntu 18.04 ####

On Ubuntu 18.04 there is an issue with the debian package ([bug on github](https://github.com/docker/compose/issues/6023)).
The fix is to remove the package `golang-docker-credential-helpers`
- `apt-get remove golang-docker-credential-helpers`


## Connect to docker

* list available docker containers `docker ps`
    ```bash
    docker ps
    CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                                NAMES
    e4443c3b6f1f        caosdb:default      "./run_docker/run"       3 hours ago         Up 3 hours          0.0.0.0:8000->10080/tcp, 0.0.0.0:10443->10443/tcp   compose_caosdb-server_1
    1303b23a8b24        mariadb:10.4        "docker-entrypoint.s…"   3 hours ago         Up 3 hours          3306/tcp                                             compose_sqldb_1
    ```
* connect as `admin` user to a docker container `compose_sqldb_1`:  
  `docker exec -ti --user admin compose_sqldb_1 /bin/bash`

# Internal features #

## Caching ##

Local caching of remotely stored content is done via the cache mount mechanism
described
[here](https://github.com/moby/buildkit/blob/master/frontend/dockerfile/docs/experimental.md).

For example, the apt cache dir is shared by running the installation with `RUN
--mount=type=cache,target=/var/cache/apt ... [apt-get install]`, similarly for
the Maven cache.  The latter is also later copied into the image, at
`/opt/caosdb/m2/`.
