Quick Guide
===========

This guide briefly explains how to install LinkAhead on a Linux system.
Thus, only necessary steps will be covered here. Please refer to the
`Installation <installation.html>`__ chapter for further details and options.

You should have received access to the Debian package (for example ``linkahead-0.4.2.deb``).

On a Debian or Ubuntu machine install the package with (assuming the file is in the current
directory): ``sudo apt install ./linkahead-0.4.2.deb`` (replace the file name with
your exact version).

.. warning::

   ``apt`` may inform you about packages that can be removed now.  Note that there is `a bug in
   Ubuntu <https://bugs.launchpad.net/ubuntu/+bug/1955047>`_ which may lead to inadvertently
   removing your graphics drivers when you follow that suggestion.

This will download and install missing dependencies and start LinkAhead.  The required docker image
(the execution environment with all necessary dependencies) will be downloaded on first start
up. This may take a while. LinkAhead is installed along with a systemd service that can be used
to start and stop your server instance. The service is automatically enabled and started.
You can monitor the progress of the download and the subsequent start with:

.. code:: console

   $ journalctl -u linkahead -f

Depending on your setup, you may have to execute the above command with ``sudo``. Once the log
output says ``Starting org.caosdb.server.CaosDBServer application`` the installation is done and
LinkAhead is running with the default configuration.

When the server is ready, you can simply visit the URL https://localhost:10443, which will access the
server via LinkAhead's web client.  Since you just installed the server on your machine, it does not
have an SSL certificate that can be checked by web browsers. Your browser may thus show a security
warning about the self-signed certificate, that warning can be ignored for now. Default credentials are
``admin`` and ``caosdb``.

For troubleshooting, please checkout the `corresponding section in the Installation
<installation.html#troubleshooting>`__ chapter.

Otherwise, you might want to continue with the `configuration <installation.html#configuration>`__ of your setup.  chapter.
