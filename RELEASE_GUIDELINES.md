# Release Guidelines for LinkAhead

This document specifies release guidelines in addition to the general release
guidelines of the CaosDB Project
([RELEASE_GUIDELINES.md](https://gitlab.indiscale.com/caosdb/src/caosdb-server/blob/dev/RELEASE_GUIDELINES.md))

## General Prerequisites

* All tests are passing.
* All Debian packages can be built and are tested.
* CHANGELOG.md is up-to-date:
  * There is a section for the current release.

## Steps

1. Create a release branch from the dev branch. This prevents further changes
   to the code base and a never ending release process.
   Important: name it `release-<VERSION>` (e.g. release-0.2.0)

2. Check all general prerequisites.

3. Update the referenced software versions in the `refs` section of `defaults.yml` to static version
   tags (`v0.7.2` instead of dynamic branch names such as `main`).
   Note that if you want to run the pipelines with different versions of other repositories than
   `main`, you have to set the environment variables accordingly in Gitlab WebUI. `.gitlab-ci.yml` no
   longer has git-ref variables to be set.

4. Update the version:
   - `version` variable in `docs/conf.py`
   - `tag` variable in `defaults.yml`
   - `VERSION` and `VERSION_DEB` variables in `debian/setup.sh`
     - If the original version has a hyphen, such as in `0.5.0-rc1`, there must be a Debian package
       revision (like `-1`) as well, otherwise it is only recommended, not necessary.
     - After this, update `debian/debian_files/changelog` with `./update_changelog.sh`.
       - Briefly list changes to the debian package in `debian/debian_files/changelog` (see
         [here](https://www.debian.org/doc/debian-policy/ch-source.html#s-dpkgchangelog)).
       - Remove "dev version" entry in changelog.
       - Replace `UNRELEASED` by `unstable`.
   - The topmost section in `CHANGELOG.md`

5. When the CI pipeline completes *up to and including to the build stage*, merge the release branch
   into the main branch.

6. Wait for the pipelines in main to pass completely. Then, tag the latest
   commit of the main branch with `v<VERSION>` (e.g. `v0.11.1`) and push the tag.

7. Release the docker image (replace version and hash in these examples):
   1. *Note:* You may need to log in to Gitlab and to the docker hub first with
      `docker login gitlab.indiscale.com:5050` and `docker login -u indiscale`.
   2. Pull the image from gitlab.indiscale.com:
      `docker pull gitlab.indiscale.com:5050/caosdb/src/caosdb-deploy:0.5.0-rc1`
   3. Verify that the image is there: `docker image ls`
   4. Add a new "official" tag to that image (fixed version and set latest to
      new image): `docker tag bf8d7e4ebe5a indiscale/linkahead:0.5.0-rc1` and
      `docker tag bf8d7e4ebe5a indiscale/linkahead:latest`
   5. Push the new image to the docker registry: `docker push
      indiscale/linkahead:0.5.0-rc1` (use IndiScale's docker-hub
      credentials in `pass`).
   6. Verify that the Docker images exist on https://hub.docker.com/r/indiscale/linkahead/tags

8. Debian packages: The pipeline builds Debian packages, which can be downloaded as artifacts from
   the CI job:

   1. https://gitlab.indiscale.com/caosdb/src/caosdb-deploy/-/commits/v0.5.0
      - Click on the pipeline checkmark
      - Click on the three dots on the right hand side
      - Choose `debian_package_build_and_test:archive`
   2. Upload the debian package (i.e., the directory `linkahead` its content) to
      `nextCloud/IT/LinkAhead-Debian-Packages/vX.Y.Z/`

9. Delete the release branch.

10. Merge the main branch back into the dev branch and:

    1. Reset the version tags of other repos in `defaults.yml`, the version in `docs/conf.py`. tag in `defaults.yml` to next version with `-dev`
    2. Also add a fresh `[Unreleased]` section at the top of CHANGELOG.md.
    3. Bump patch release number in `debian/setup.sh`, `defaults.yml`, `docs/conf.py`. Update the
       debian changelog with a preliminary entry.

11. Write a short release notice for the website, with a link to the package to be downloaded.
    Note, that you should incorporate important changes that are documented in the change logs
    of subcomponents like caosdb-server:
    1. News post in English, with Changelog section copied there (Wordpress understands Markdown).
    2. News post in German, without Changelog, but with link to English post for details.
    3. Upload .deb file.
    4. Reference .deb file on "Downloads" page in English and German, link to respective post in
       "Release Notes" column.

12. Update the checksums file. You can use the shell script `generate_checksums_file.sh` in `IT/LinkAhead-Debian-Packages/`.
