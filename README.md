# Welcome

This is **LinkAhead Build & Control**, a part of the LinkAhead project.

If you want to try out LinkAhead, you are at the right place.  For further
instruction and a 1-step-demo, please continue reading in
[README_SETUP.md](README_SETUP.md).


# Further Reading

Please refer to the [official gitlab repositories of the LinkAhead project](https://gitlab.com/linkahead/) for more information.

# License

Copyright (C) 2022-2024 IndiScale GmbH
Copyright (C) 2022-2024 Daniel Hornung, Timm Fitschen, Henrik tom Wörden.

All files in this repository are licensed under the [GNU Affero General Public
License](LICENCE.md) (version 3 or later).

